<?php     // inicializa el obj User
		$currentUser = new User();
        $currentUser->load();
?>

<header class="navbar navbar-primary navbar-top">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-4 sm-hidden xs-hidden">
				<a class="navbar-brand bac-header sm-hidden xs-hidden" href="index.php">Buenos Aires Ciudad</a>
			</div>
			<div class="timer col-md-4 col-sm-4">
				<!-- timer de sesion -->
				<div class="clock"></div>
			</div>
			<div class="col-md-4 col-sm-4 sm-hidden xs-hidden">
				<div class="dropdown">
					<button class="btn dropdown-toggle" type="button" id="user-config" data-toggle="dropdown" style="text-transform: uppercase;">
					<span class="user-name"><?php echo $currentUser->getName(); ?></span>
					<span class="caret"></span>
					</button>

					<ul class="dropdown-menu pull-right" role="menu" aria-labelledby="user-config">
						<!-- <li role="presentation" class="active"><a role="menuitem" tabindex="-1" href="#">Perfil</a></li> -->
						<li role="presentation"><a role="menuitem" tabindex="-1" href="index.php">Historial</a></li>
						<!-- <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Configuración</a></li> -->
						<?php 
						if($currentUser->isSupervisor() === 1) { ?>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="index.php?content=supervisor">Administrar declaraciones</a></li>
						<?php
						}
						if($currentUser->isGranter() === 1) { ?>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="index.php?content=users">Asignar permisos</a></li>
						<?php 
						}
						if($currentUser->isJobArchivist() === 1) { ?>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="index.php?content=overdue">Obligados</a></li>
						<?php 
						}?>
						<li role="presentation" class="divider"></li>
						<li role="presentation"><a role="menuitem" tabindex="-1" href="?logOut=TRUE">Salir</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</header>

<div class="ddjj-progress ddjj-progress">
    <div class="ddjj-progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:100%">
      <div id="countDown">
      </div>
    </div>
</div>
