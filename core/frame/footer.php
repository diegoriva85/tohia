<footer>
	<div class="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 vert-offset-top-1">
					<a class="navbar-brand bac-footer" href="index.php" target="_blank">Buenos Aires Ciudad</a>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="sub-brand">
						<p>Secretaria Legal y Técnica</p>
						<p>Gobierno de la Ciudad de Buenos Aires</p>
						<p>Version: <?php echo PROJECT_VERSION;?></p>
<!--						<a href="http://sadesoporte.gcba.gob.ar/">sadesoporte.gcba.gob.ar</a>
 -->					<p>Contacto: 5091-7668 / <a href="http://sadesoporte.gcba.gob.ar/">sadesoporte.gcba.gob.ar</a> </p>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
