<!-- requerimientos para el modal de la galeria -->

<!-- libreria -->
<!-- dropzone -->

<!-- clases -->
<?php 
    Configure::classCore('Media');
?>

<!-- funciones -->
<script type="text/javascript" src="<?php echo PROJECT_URL ?>/core/js/media.js"></script>
<script type="text/javascript" src="<?php echo PROJECT_URL ?>/core/js/media_modal.js"></script><!-- debe existe el id template al pie del documento donde volcara las estructura necesarias  -->

<!-- estilos -->
<link rel="stylesheet" type="text/css" href="<?php echo PROJECT_URL ?>/core/css/media.css">
<link rel="stylesheet" type="text/css" href="<?php echo PROJECT_URL ?>/core/css/media_modal.css">

<!-- agregar el boton a la pagina o bien  los atributos data-toggle="modal" data-target=".media-modal"-->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".media-modal"><i class="fa fa-image"></i> Media</button> -->

<div class="modal fade media-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Galería</h4>
            </div>
            <div class="modal-body col-md-12">
                <!-- incluye el contenido de los tab -->
                <?php Configure::frame('media'); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" style="display: none" class="btn btn-primary btn-submit"> Guardar</button>
            </div>

        </div>
    </div>
</div>