<nav class="navbar navbar-default" role="navigation">
  <div class="container">
    <div class="row">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
          <span class="sr-only">Cambiar navegación</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">DDJJ</a>
      </div>
      <div class="collapse navbar-collapse" id="main-nav">
        <ul class="nav navbar-nav navbar-right">

        <?php
          //captura la url
          $url = pathinfo("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");

          //si esta accediendo al modulo de template, muestra los enlaces
          if( $url['dirname'] === PROJECT_URL.'/module/template')
          {
        ?>
            <li><a href="<?php echo PROJECT_URL ?>/module/template/index.php">Comenzar</a></li>
            <li><a href="<?php echo PROJECT_URL ?>/module/template/styles.php">Estilos</a></li>
            <li><a href="<?php echo PROJECT_URL ?>/module/template/components.php">Componentes</a></li>
            <li><a href="<?php echo PROJECT_URL ?>/module/template/classes.php">Clases</a></li>
            <li><a href="<?php echo PROJECT_URL ?>/module/template/libraries.php">Bibliotecas</a></li>
            <li><a href="<?php echo PROJECT_URL ?>/module/template/examples.php">Ejemplos</a></li>
            <li><a href="<?php echo PROJECT_URL ?>/module/template/protocol.php">Lineamientos</a></li>
            <li><a href="<?php echo PROJECT_URL ?>/module/example/example.php" target="blank">Base</a></li>
            <li><a href="<?php echo PROJECT_URL ?>/module/admin/index.php">Administrar</a></li>
            
            <!-- para volver al proyecto  -->
            <li><a href="<?php echo PROJECT_URL ?>/module/index.php">Proyecto</a></li>
        <?php 
          }
          else
          {
            //caso contrario solo muestra un enlace para hacer referencia al template
        ?>
            <li><a href="<?php echo PROJECT_URL ?>/module/template/index.php">Template</a></li>
        <?php
          }
        ?>

        </ul>
      </div>
    </div>
  </div>
</nav>