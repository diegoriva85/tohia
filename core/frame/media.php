<div class="" role="tabpanel" data-example-id="togglable-tabs">
    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
        <li role="presentation" class="active"><a href="#tab_avatar" id="content-avatar" role="tab" data-toggle="tab" aria-expanded="true">Avatar</a>
        </li>
        <li role="presentation" class=""><a href="#tab_library" role="tab" id="content-library" data-toggle="tab"  aria-expanded="false">Biblioteca</a>
        </li>
        <li role="presentation" class=""><a href="#tab_recent" role="tab" id="content-recent" data-toggle="tab" aria-expanded="false">Recientes</a>
        
        <li role="presentation" class=""><a href="#tab_upload" role="tab" id="content-upload" data-toggle="tab" aria-expanded="false">Subir</a>
        </li>
    </ul>
    <div id="myTabContent" class="tab-content">
        
        <!-- pestania para los avatars -->
        <div role="tabpanel" class="tab-pane fade active in" id="tab_avatar" aria-labelledby="home-tab">

            <div class="row" id="avatar-list">
            <!-- carga dinamica - ver Media.getPath('avatar') -->
            </div>

        </div>

        <!-- pestania para la biblioteca -->
        <div role="tabpanel" class="tab-pane fade" id="tab_library" aria-labelledby="profile-tab">
            <p>Biblioteca de imágenes</p>
        
            <div class="row" id="library-list">
              <!-- carga dinamica - ver Media.getPath('library') -->
            </div>
        </div>

        <!-- pestania para los archivos recientes -->
        <div role="tabpanel" class="tab-pane fade" id="tab_recent" aria-labelledby="profile-tab">
            <p>Listado de archivos recientes</p>
            
            <div class="row" id="recent-list">
              <!-- carga dinamica - ver Media.getPath('recent') -->
            </div>
        </div>

        <!-- pestania para la carga de archivos -->
        <div role="tabpanel" class="tab-pane fade" id="tab_upload" aria-labelledby="profile-tab">
            <p>Imágenes por subir</p>
            <form id="form-media-upload" action="<?php echo PATH_MODULE_URL ?>media/save.php" class="dropzone" style="border: 1px solid #e5e5e5; height: 300px; " enctype="multipart/form-data"></form>
        </div>
    </div>
</div>