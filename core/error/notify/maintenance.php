<!DOCTYPE html>
<?php
	//configuracion inicial requerida
	require '../../conf/ini.conf';

?>
<head>

    <meta charset='utf-8'>
    <title>Mantenimiento</title>
    <link rel='shortcut icon' href='../../sys/media/img/favicon.ico'>

    <?php

        //servidor principal
        define('SERVER_DNS' , PROJECT_URL . "/sys");

	    // metas: generales
	    Configure::inc('../../../module/frame/meta.php');

	    // estilos: bootstrap, font-awesome, animate, jquery-jvectormap, ickeck, floatexamples, custom
	    Configure::inc('../../../module/frame/style.php');

    	// funciones: generales
    	Configure::inc('../../../module/frame/func.php');
		
	?>

</head>

<body class="nav-md">

    <div class="container body">

        <div class="col-md-12">
            <div class="col-middle">
                <div class="text-center text-center">
                    <h3 class="error-number" style="font-size: 5em">TRABAJANDO</h3>
                    <h2>Lo sentimos, pero no estamos trabajando para mejorar el sitio</h2>
                    <p><a href="mailto:<?php echo EMAIL_USER ?>?Subject=Sugerencia&body=Sugerencia:%20"  target="_top">Quieres enviar una sugerencia?</a></p>
                    <i class="fa fa-code" style="font-size: 20em"></i>
                </div>
            </div>
        </div>

    </div>

  	<?php

	    // pie de pagina
	    Configure::inc('../../../module/frame/notifications.php');

    	// funciones: generales
    	Configure::inc('../../../module/frame/func_footer.php');

    ?>

</body>
</html>