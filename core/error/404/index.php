<!DOCTYPE html>
<?php
	//configuracion inicial requerida
	require  $_SERVER['DOCUMENT_ROOT'].'/clone/sys'.DIRECTORY_SEPARATOR.'conf'.DIRECTORY_SEPARATOR.'ini.conf';


var_dump('hola'.$_SERVER);

    //si la sesion del usuario no esta activa, redirije al login
    // if ( !User::active() )
    {
        // header("location: " . PROJECT_URL . "/module/home.php");
    }
?>
<head>

    <?php

        //datos iniciales
        Configure::init('{"title":"Ejemplo","description":"Descripcion ejemplo","lang":"es","charset":"utf-8"}');

        //agrega las clases de front del core
        // Configure::classCore('MyClass');
        //agrega las clases del modulo actual
        Configure::classModule('Module');

        //librerias principales
        Configure::lib('smartwizard','icheck', 'select2', 'animate', 'validator');

    ?>

    <style type="text/css">
    .alien
    {
        position: fixed;
        bottom: 0;
        text-align: right;
    }
    </style>
</head>

<body class="nav-md">

    <div class="container body">

        <div class="col-md-12">
            <div class="col-middle">
                <div class="text-center text-center">
                    <h1 class="error-number">404</h1>
                    <h2>Lo sentimos, pero no pudimos encontrar esta página</h2>
                    <p>La página que busca no existe <a href="mailto:<?php echo EMAIL_USER ?>?Subject=Error%20Página&body=Error%20al%20buscar%20la%20web:%20'<?php echo $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>'"  target="_top">Quieres informe esto?</a>
                    </p>
                    <div class="mid_center">
                        <h3>Buscar</h3>
                        <form>
                            <div class="col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Buscar...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">Go!</button>
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                    <button type="button" class="btn btn-dark" onclick='window.location.href="<?php echo PROJECT_URL ?>"'>Regresar al Inicio</button>
                </div>
            </div>
        </div>
        <div class="col-md-12 alien">
        <img src="<?php echo PATH_MEDIA_URL . '/img/'.DEV_LOGO ?>" style="width: 150px">
        </div>

    </div>

  	<?php

	    /*// pie de pagina
	    Configure::inc('../../../module/frame/notifications.php');

    	// funciones: generales
    	Configure::inc('../../../module/frame/func_footer.php');*/

    ?>

</body>
</html>