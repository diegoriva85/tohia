<!-- titulo -->
<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
	<tbody>
		<tr>
			<td valign="top" style="width: 100%;">
				<h1 style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 30px; font-weight: normal; line-height: 1.35em; margin: 0px; max-height: none; max-width: none; padding: 15px; text-decoration: none;">
					<div>Clave provisoria</div>
				</h1>
			</td>
		</tr>
	</tbody>
</table>
<!-- contenido -->
<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
	<tbody>
		<tr>
			<td valign="top" style="width: 100%;">
				<div style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35em; margin: 0px; max-height: none; max-width: none; padding: 15px; text-decoration: none;">
					Hemos recibido una solicitud para restrablecer tu clave de Buenos Aires SECLyT, realizada el día $date.<br>
					Si no realizaste esta solicitud te pedimos que desestimes esta notificación.
					<br>
				</div>
			</td>
		</tr>
	</tbody>
</table>
<!-- notificacion -->
<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
	<tbody>
		<tr>
			<td valign="top" style="width: 80%;">
				<h1 style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 21px; font-weight: normal; line-height: 1.35em; margin: 15px 5px 0px 20px; max-height: none; max-width: none; padding: 0px; text-decoration: none;">
					<div>Dedica unos minutos a leer estos Consejos de Seguridad</div>
				</h1>
				<div style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35em; margin: 10px 5px 0px 20px; max-height: none; max-width: none; padding: 0px; text-decoration: none;">
				<ul>
					<li>Cambia tu contraseña a menudo</li>
					<li>Mantén tus contraseñas a salvo</li>
					<li>No utilices información personal</li>
					<li>Las contraseñas largas son más seguras</li>
				</ul>
					Nuestro sistema nunca pedirá <span style="color:#ff9b00 !important">información personal</span>, por este medio. Procura siempre realizar las operaciones desde nuestra web.
				</div>

				<a
					href=$url
					text="Recuperar"
					style="border: 0px solid transparent; color: rgb(255, 255, 255); display: inline-block; font-family: sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35em; margin: 15px 0px 15px 20px; max-height: none; max-width: none; padding: 10px 15px; text-decoration: none;background-color: #ffd300;color:#000"
				>
				Recuperar
				</a>
			</td>

			<td valign="top" style="width: 20%;">
					<img src="http://hyddra.net/octopus/recovery_password.png" width="250" style="width: 250px; border: 0px solid rgb(170, 170, 170); color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 10px; font-weight: normal; line-height: 1.35em; margin: 15px 20px 15px 15px; max-height: none; max-width: 250px; padding: 0px; text-decoration: none; min-height: 10px; background-color: rgb(204, 204, 204);">
					<div style="font-family: sans-serif; font-size: 11px; margin: 0px 5px 5px 20px;">
						<span style="color:#ff9b00">
							<b>Enviado desde el equipo:</b><br><br>
							IP:  $ip<br>
							Sistema Operativo: $so<br>
						</span>
					</div>
			</td>
		</tr>
	</tbody>
</table>