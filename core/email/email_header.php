<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1s/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Verificación de Cuenta Bs As</title>
</head>
<body>

<!-- encabezado de pagina -->
<table
	width="100%"
	cellspacing="0"
	cellpadding="0"
	border="0"
	style=" background-color: #ddd; min-width: 680px;  table-layout: fixed"
>
	<tbody>
		<tr>
			<td align="center">
				<p style="font-size: 12px; font-family: sans-serif; display: block; padding-top: 10px">
					Si no ves correctamente este correo, entrá en la
					<a href=$online_version style="text-decoration: none; color: #0088cc">versión online</a>.
				</p>
			</td>
		</tr>
	</tbody>
</table>
	
<table class="regions-container" width="100%" cellspacing="0" cellpadding="0" border="0" style="background-color: #ddd; table-layout: fixed; min-width: 680px">
<tbody>
	
	<tr>
		<td align="center">

			<div style="padding: 20px">

				<table class="page" width="660px" cellspacing="0" cellpadding="0" border="0" style="background: #fff; font-family: Helvetica, Arial, sans-serif">
					<tbody>
						<tr>
							<td class="regions-list">
								<!-- cabecera bs as -->
								<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;background-color: #ffd300; height: 100px">
									<tbody>
										<tr>
											<td valign="top" style="width: 100%;vertical-align: middle;">
												<a href="http://seclyt.gcba.gob.ar/" style="text-decoration: none;">
													<img src="http://hyddra.net/octopus/logo_ba.png"
													width="275" style="width: 275px; border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 10px; font-weight: normal; line-height: 1.35em; margin: 10px 0px 10px 10px; max-height: 49px; max-width: 275px; padding: 0px; text-decoration: none; min-height: 10px; height: 49px;">
												</a>
											</td>
										</tr>
									</tbody>
								</table>
								<!-- separador -->
								<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
									<tbody>
										<tr>
											<td valign="top" style="width: 100%;">
												<div style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 10px; font-weight: normal; line-height: 1.35em; margin: 0px; max-height: none; max-width: none; padding: 0px; text-decoration: none; height: 5px; background-color: rgb(170, 170, 170);"></div>
											</td>
										</tr>
									</tbody>
								</table>