<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1s/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Verificación de Cuenta Bs As</title>
</head>
<body>

<!-- encabezado de pagina -->
<table
	width="100%"
	cellspacing="0"
	cellpadding="0"
	border="0"
	style=" background-color: #ddd; min-width: 680px;  table-layout: fixed"
>
	<tbody>
		<tr>
			<td align="center">
				<p style="font-size: 12px; font-family: sans-serif; display: block; padding-top: 10px">
					Si no ves correctamente este correo, entrá en la
					<a href=$online_version style="text-decoration: none; color: #0088cc">versión online</a>.
				</p>
			</td>
		</tr>
	</tbody>
</table>
	
<table class="regions-container" width="100%" cellspacing="0" cellpadding="0" border="0" style="background-color: #ddd; table-layout: fixed; min-width: 680px">
<tbody>
	
	<tr>
		<td align="center">

			<div style="padding: 20px">

				<table class="page" width="660px" cellspacing="0" cellpadding="0" border="0" style="background: #fff; font-family: Helvetica, Arial, sans-serif">
					<tbody>
						<tr>
							<td class="regions-list">
								<!-- cabecera bs as -->
								<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;background-color: #ffd300; height: 100px">
									<tbody>
										<tr>
											<td valign="top" style="width: 100%;vertical-align: middle;">
												<a href="http://seclyt.gcba.gob.ar/" style="text-decoration: none;">
													<img src="http://hyddra.net/octopus/logo_ba.png"
													width="275" style="width: 275px; border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 10px; font-weight: normal; line-height: 1.35em; margin: 10px 0px 10px 10px; max-height: 49px; max-width: 275px; padding: 0px; text-decoration: none; min-height: 10px; height: 49px;">
												</a>
											</td>
										</tr>
									</tbody>
								</table>
								<!-- separador -->
								<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
									<tbody>
										<tr>
											<td valign="top" style="width: 100%;">
												<div style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 10px; font-weight: normal; line-height: 1.35em; margin: 0px; max-height: none; max-width: none; padding: 0px; text-decoration: none; height: 5px; background-color: rgb(170, 170, 170);"></div>
											</td>
										</tr>
									</tbody>
								</table>
								<!-- titulo -->
								<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
									<tbody>
										<tr>
											<td valign="top" style="width: 100%;">
												<h1 style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 30px; font-weight: normal; line-height: 1.35em; margin: 0px; max-height: none; max-width: none; padding: 15px; text-decoration: none;">
													<div>Confirma tu cuenta de correo</div>
												</h1>
											</td>
										</tr>
									</tbody>
								</table>
								<!-- contenido -->
								<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
									<tbody>
										<tr>
											<td valign="top" style="width: 100%;">
												<div style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35em; margin: 0px; max-height: none; max-width: none; padding: 15px; text-decoration: none;">
													Recuerda que te quedan algunos pasos por completar para poder empezar a usar los servicios de Buenos Aires.
														<div><br></div>
														<div>
															Lo primero es lo primero: es el momento de verificar tu correo.
														</div>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
								<!-- notificacion -->
								<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
									<tbody>
										<tr>
											<td valign="top" style="width: 80%;">
												<h1 style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 21px; font-weight: normal; line-height: 1.35em; margin: 15px 5px 0px 20px; max-height: none; max-width: none; padding: 0px; text-decoration: none;">
													<div>Dedica unos minutos a verificar tu cuenta</div>
												</h1>
												<div style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35em; margin: 10px 5px 0px 20px; max-height: none; max-width: none; padding: 0px; text-decoration: none;">
													Para poder empezar a usar los servicios de Buenos Aires con <span style="color:#ff9b00">eyune@buenosaires.gob.ar</span>, debes verificar que eres el propietario de esta cuenta. Si no verificas la propiedad de la cuenta, en el plazo de 3 días después de registrarte, tu información se eliminará de nuestra Base.
												</div>

												<a
													href="http://localhost/octopus/users/"
													text="Verificar"
													style="border: 0px solid transparent; color: rgb(255, 255, 255); display: inline-block; font-family: sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35em; margin: 15px 0px 15px 20px; max-height: none; max-width: none; padding: 10px 15px; text-decoration: none;background-color: #ffd300;color:#000"
												>
												Verificar
												</a>
											</td>
						
											<td valign="top" style="width: 20%;">
													<img src="http://hyddra.net/octopus/identity.jpg" width="250" style="width: 250px; border: 0px solid rgb(170, 170, 170); color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 10px; font-weight: normal; line-height: 1.35em; margin: 15px 20px 15px 15px; max-height: none; max-width: 250px; padding: 0px; text-decoration: none; min-height: 10px; background-color: rgb(204, 204, 204);">
											</td>
										</tr>
									</tbody>
								</table>
								<!-- separador -->
								<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
									<tbody>
										<tr>
											<td valign="top" style="width: 100%;">
												<div style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 10px; font-weight: normal; line-height: 1.35em; margin: 0px; max-height: none; max-width: none; padding: 0px; text-decoration: none; height: 5px; background-color: rgb(170, 170, 170);">
												</div>
											</td>
										</tr>
									</tbody>
								</table>
								<!-- noticias - publicidad -->
								<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
									<tbody>
										<tr>
											<td valign="top" style="width: 33%;">
												<img src="http://hyddra.net/octopus/contacts.jpg" width="190" style="width: 190px; border: 0px solid rgb(170, 170, 170); color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 10px; font-weight: normal; line-height: 1.35em; margin: 15px 20px 10px; max-height: none; max-width: 190px; padding: 0px; text-decoration: none; min-height: 10px; background-color: rgb(204, 204, 204);">

												<h1 style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 20px; font-weight: normal; line-height: 1.35em; margin: 10px 20px 0px; max-height: none; max-width: none; padding: 0px; text-decoration: none;">Conactos</h1>

												<div style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35em; margin: 10px 20px 0px; max-height: none; max-width: none; padding: 0px; text-decoration: none;">
													<div>Podras tener tu propia agenda del gobierno ...</div>
													<div><br></div>
												</div>

												<a text="Acceder" style="border: 0px solid transparent; color: rgb(255, 255, 255); display: inline-block; font-family: sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35em; margin: 10px 20px 20px; max-height: none; max-width: none; padding: 10px 15px; text-decoration: none;background-color: #ffd300;color:#000" href="http://localhost/ocotpus/contacts/">Acceder</a>

											</td>
											<td valign="top" style="width: 33%;">
												<img src="http://hyddra.net/octopus/calendar.jpg" width="190" style="width: 190px; border: 0px solid rgb(170, 170, 170); color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 10px; font-weight: normal; line-height: 1.35em; margin: 15px 20px 10px 0px; max-height: none; max-width: 190px; padding: 0px; text-decoration: none; min-height: 10px; background-color: rgb(204, 204, 204);">

												<h1 style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 20px; font-weight: normal; line-height: 1.35em; margin: 10px 20px 0px 0px; max-height: none; max-width: none; padding: 0px; text-decoration: none;">

													<div>Calendario</div>
												</h1>

												<div style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35em; margin: 10px 20px 0px 0px; max-height: none; max-width: none; padding: 0px; text-decoration: none;">
													<div>
														Agendar eventos, reuniones y todo aquello a lo que debas asistir ...
													</div>
												</div>
												<a text="Acceder" style="border: 0px solid transparent; color: rgb(255, 255, 255); display: inline-block; font-family: sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35em; margin: 10px 20px 20px 0px; max-height: none; max-width: none; padding: 10px 15px; text-decoration: none; background-color: #ffd300;color:#000" href="http://localhost/ocotpus/calendar/">
													Acceder
												</a>
											</td>

											<td valign="top" style="width: 33%;">

												<img src="http://hyddra.net/octopus/notes.jpg" width="190" style="width: 190px; border: 0px solid rgb(170, 170, 170); color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 10px; font-weight: normal; line-height: 1.35em; margin: 15px 0px 10px; max-height: none; max-width: 190px; padding: 0px; text-decoration: none; min-height: 10px; background-color: rgb(204, 204, 204);">

												<h1 style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 20px; font-weight: normal; line-height: 1.35em; margin: 10px 0px 0px; max-height: none; max-width: none; padding: 0px; text-decoration: none;">

													<div>Notas</div>
												</h1>

												<div style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35em; margin: 10px 0px 0px; max-height: none; max-width: none; padding: 0px; text-decoration: none;">Disponer de tus notas en<div>cualquier momento ...</div>

													<div><br><br></div>
												</div>

												<a text="Acceder" style="border: 0px solid transparent; color: rgb(255, 255, 255); display: inline-block; font-family: sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35em; margin: 10px 0px 20px; max-height: none; max-width: none; padding: 10px 15px; text-decoration: none; background-color: #ffd300;color:#000" href="http://localhost/ocotpus/notes/">Acceder</a>
											</td>
										</tr>
									</tbody>
								</table>
								<!-- separador -->
								<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
									<tbody>
										<tr>
											<td valign="top" style="width: 100%;">
												<div style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 10px; font-weight: normal; line-height: 1.35em; margin: 0px; max-height: none; max-width: none; padding: 0px; text-decoration: none; height: 5px; background-color: rgb(170, 170, 170);"></div>
											</td>
										</tr>
									</tbody>
								</table>
								<!-- mensaje ayuda -->
								<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
									<tbody>
										<tr>
											<td valign="top" style="width: 100%;">
												<div style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35em; margin: 0px; max-height: none; max-width: none; padding: 15px; text-decoration: none;">
													¿Necesitas más ayuda? Nuestro equipo de asistencia puede resolver tus dudas para ponerte en marcha rápidamente. Puedes&nbsp;<a href="mailto:seclyt_sistemas@buenosaires.gob.ar?Subject=Ayuda%20Registro%20Cuenta" target="_top" style="color:#ff9b00; text-decoration: none;">ponerte en contacto</a>&nbsp;con nosotros por teléfono o por correo electrónico, o bien obtener más información acerca de&nbsp;<a href="http://seclyt.gcba.gob.ar/wiki" target="_blank" style="color: #ff9b00; text-decoration: none;">cómo verificar la propiedad de &nbsp;la cuenta&nbsp; </a>en el Centro de ayuda
												</div>
											</td>
										</tr>
									</tbody>
								</table>
								<!-- despedida -->
								<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
									<tbody>
										<tr>
											<td valign="top" style="width: 100%;">
												<div style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35em; margin: 0px; max-height: none; max-width: none; padding: 15px; text-decoration: none;">Atentamente,&nbsp;El equipo de Desarrollo SECLyT</div>
											</td>
										</tr>
									</tbody>
								</table>

								<!-- redes sociales -->
<!-- 								<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
									<tbody>
										<tr>
											<td valign="top" align="center" style="width: 100%;">
												<a href="http://facebook.com/" style="text-decoration: none;">
													<img src="./email_check_files/social.facebook.png" alt="Facebook" width="64" style="width: 64px; border: 0px solid transparent; color: rgb(0, 0, 0); display: inline-block; font-family: sans-serif; font-size: 10px; font-weight: normal; line-height: 1.35em; margin: 10px 10px 10px 0px; max-height: none; max-width: 64px; padding: 0px; text-decoration: none;">
												</a>
												<a href="http://twitter.com/" style="text-decoration: none;">
													<img src="./email_check_files/social.twitter.png" alt="Twitter" width="64" style="width: 64px; border: 0px solid transparent; color: rgb(0, 0, 0); display: inline-block; font-family: sans-serif; font-size: 10px; font-weight: normal; line-height: 1.35em; margin: 10px 10px 10px 0px; max-height: none; max-width: 64px; padding: 0px; text-decoration: none;">
												</a>
												<a href="http://google.com/" style="text-decoration: none;">
													<img src="./email_check_files/social.plus.png" alt="Twitter" width="64" style="width: 64px; border: 0px solid transparent; color: rgb(0, 0, 0); display: inline-block; font-family: sans-serif; font-size: 10px; font-weight: normal; line-height: 1.35em; margin: 10px 10px 10px 0px; max-height: none; max-width: 64px; padding: 0px; text-decoration: none;">
												</a>
												<a href="http://linkedin.com/" style="text-decoration: none;">
													<img src="./email_check_files/social.linkedin.png" alt="LinkedIn" width="64" style="width: 64px; border: 0px solid transparent; color: rgb(0, 0, 0); display: inline-block; font-family: sans-serif; font-size: 10px; font-weight: normal; line-height: 1.35em; margin: 10px 10px 10px 0px; max-height: none; max-width: 64px; padding: 0px; text-decoration: none;">
												</a>
												<img src="./email_check_files/social.rss.png" alt="Blog" width="64" style="width: 64px; border: 0px solid transparent; color: rgb(0, 0, 0); display: inline-block; font-family: sans-serif; font-size: 10px; font-weight: normal; line-height: 1.35em; margin: 10px 10px 10px 0px; max-height: none; max-width: 64px; padding: 0px; text-decoration: none;">
											</td>
										</tr>
									</tbody>
								</table> -->
							</td>
						</tr>
						<tr>
							<td>
								<!-- separador -->
								<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
									<tbody>
										<tr>
											<td valign="top" style="width: 100%;">
												<div style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 10px; font-weight: normal; line-height: 1.35em; margin: 0px; max-height: none; max-width: none; padding: 0px; text-decoration: none; height: 5px; background-color: rgb(170, 170, 170);">
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<!-- pie de correo -->
								<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;background-color: #ffd300; height: 70px">
									<tbody>
										<tr>
											<td valign="top" align="center" style="width: 100%;vertical-align: middle;">
												<p style="font-size: 13px; font-style: bold font-family: sans-serif; display: block;"><b>
												© 2014&nbsp;Buenos Aires - SECLyT - <strong>Gerencia Operativa de Tecnología y Procesos</strong></b>
												</p>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</td>
	</tr>
</tbody>
</table>


<!-- pie de pagina -->
<table width="100%" cellspacing="0" cellpadding="0" border="0" style=" background-color: #ddd; min-width: 680px;  table-layout: fixed">
	<tbody>
		<tr>
			<td align="center">
			<!-- 	<p style="font-size: 12px; font-family: sans-serif; display: block; padding-top: 10px">
					© 2014&nbsp;Buenos Aires - SECLyT - Gerencia Operativa de Tecnología y Procesos<br>
				</p>-->
				
				<!-- terminos y condiciones -->
				<table cellspacing="0" cellpadding="0" border="0" style="width:660px">
					<tbody>
						<tr style="width:660px">
							<td valign="top" style="width: 100%;">
								<div style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 10px; font-weight: normal; line-height: 1.35em; margin: 0px; max-height: none; max-width: none; text-decoration: none;">No conteste directamente a este correo electrónico. Este mensaje fue enviado desde una dirección que no acepta mensajes entrantes. Si tiene preguntas o necesita asistencia,&nbsp;<a href="http://#" target="_blank" style="color: #ff9b00; text-decoration: none;">póngase en contacto con nosotros aquí</a><div></div>
									<div>
										En consecuencia, de haberlo recibido&nbsp;por&nbsp;error&nbsp;te pedimos que te contactes con nosotros y lo elimines del sistema.
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr> 
		<tr>
			<td align="center">
				<p style="font-size: 12px; font-family: sans-serif; display: block;">
					<a href=$unsubscribe style="text-decoration: none; color: #0088cc">Desuscribirme</a> de estas notificaciones
				</p>
			</td>
		</tr>
	</tbody>
</table>

</body>
</html>