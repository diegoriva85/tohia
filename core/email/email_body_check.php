<!-- titulo -->
<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
	<tbody>
		<tr>
			<td valign="top" style="width: 100%;">
				<h1 style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 30px; font-weight: normal; line-height: 1.35em; margin: 0px; max-height: none; max-width: none; padding: 15px; text-decoration: none;">
					<div>Confirmá tu cuenta de correo</div>
				</h1>
			</td>
		</tr>
	</tbody>
</table>
<!-- contenido -->
<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
	<tbody>
		<tr>
			<td valign="top" style="width: 100%;">
				<div style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35em; margin: 0px; max-height: none; max-width: none; padding: 15px; text-decoration: none;">
					Recuerda que te quedan algunos pasos por completar para poder empezar a usar los servicios de Buenos Aires.
						<div><br></div>
						<div>
							Lo primero es lo primero: es el momento de verificar tu correo.
						</div>
				</div>
			</td>
		</tr>
	</tbody>
</table>
<!-- notificacion -->
<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
	<tbody>
		<tr>
			<td valign="top" style="width: 80%;">
				<h1 style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 21px; font-weight: normal; line-height: 1.35em; margin: 15px 5px 0px 20px; max-height: none; max-width: none; padding: 0px; text-decoration: none;">
					<div>Dedica unos minutos a verificar tu cuenta</div>
				</h1>
				<div style="border: 0px solid transparent; color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35em; margin: 10px 5px 0px 20px; max-height: none; max-width: none; padding: 0px; text-decoration: none;">
					Para poder empezar a usar los servicios de Buenos Aires con <span style="color:#ff9b00 !important">$email</span>, debes verificar que eres el propietario de esta cuenta. Si no verificas la propiedad de la cuenta, en el plazo de 3 días después de registrarte, tu información se eliminará de nuestro Sistema.
				<p>Fecha de registro: $date</p>
				</div>

				<a
					href= $url 
					text="Verificar"
					style="border: 0px solid transparent; color: rgb(255, 255, 255); display: inline-block; font-family: sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35em; margin: 15px 0px 15px 20px; max-height: none; max-width: none; padding: 10px 15px; text-decoration: none;background-color: #ffd300;color:#000"
				>
				Verificar
				</a>
			</td>

			<td valign="top" style="width: 20%;">
					<img src="http://hyddra.net/octopus/identity.jpg" width="250" style="width: 250px; border: 0px solid rgb(170, 170, 170); color: rgb(0, 0, 0); display: block; font-family: sans-serif; font-size: 10px; font-weight: normal; line-height: 1.35em; margin: 15px 20px 15px 15px; max-height: none; max-width: 250px; padding: 0px; text-decoration: none; min-height: 10px; background-color: rgb(204, 204, 204);">
			</td>
		</tr>
	</tbody>
</table>