	/**
	 * Al hacer una modificacion sobre algun input o select en los formularios se guarda el cambio en sesion
	 * @param  {[string]} class  [clase a la se le quiere atribuir la accion]
	 * @author Diego Riva
	 * @return {[obj]} DDJJ  [guarda en el objeto sesion los datos de la seccion]
	 */
	function saveDDJJ(data)
    {
        $(data).on('change', function()
        {
			if ($(this).data('name') !== 'ddjj-not-data')
			{
  				var section = new Array();
	            var element = $(this); //se guarda el input en que se realizo un cambio

	            while (!element.hasClass('tab-main-container'))
	            {
	                if (element.hasClass('section'))
	                {
	                    var name = element.data('name');
	                    var id = element.data('id');

	                    if (id === "" || id === undefined )
	                    {
	                    	id=name;
	                    } 

		                section.push({id: id, name: name});
	                } 
	                element = element.parent();
	            } 
	            section = section.reverse();

	            // console.debug("section",section);
	            // console.debug("id",$(this).data('name'));
	            // console.debug("name",$(this).data('label'));
	            // console.debug("valor",$(this).val());
	            
	            DDJJ.save(
	                section,
	                [
	                    {
	                        id: $(this).data('name'),
	                        name: $(this).data('label'),
	                        value: $(this).val()
	                    }
	                ]
	            );
	        }        
	    });

    }