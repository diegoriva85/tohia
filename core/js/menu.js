// Estilo Activo para el menu

// Ubicacion actual
var path = window.location.pathname;
// Indice de la ultima barra
var lastForwardSlash = path.lastIndexOf('/');
// Si la barra no cierra la direccion
if(lastForwardSlash != -1)
{	
	// Nombre de la pagina
    var activePage = path.substring(lastForwardSlash + 1);
}

// Recorremos todos los li
$('#main-nav > ul > li').each(function(){

	// Guardamos el link del a
	var aHref = $(this).children().attr("href");

	// Si el link es igual a la pagina activa
	if( aHref == activePage ){
		// seteamos la clase activa
		$(this).addClass('active');
	}

});