//datos al leer el documento
$(document).on('ready',function()
{

    $("#content-avatar").click(function()
    { 
        listElement('avatar');
    });

    $("#content-library").click(function()
    {
        listElement('library');
    });

    $("#content-recent").click(function()
    {
        listElement('recent');
    });

    $("#content-upload").click(function()
    {
        $(".btn-submit").css('display','inline-block');
        $(".btn-submit").html('<i class="fa fa-cloud-upload"></i> Subir');
    });


    $(".btn-submit").click(function()
    {
        $("#form-media-upload").submit();
        event.preventDefault();
    });
});

/**
 * lista los elementos en la galeria
 */
function listElement(type)
{

    //obtenemos el listado de avatars
    var listAvatar = Media.getPath(type);

    // console.log(listAvatar);
    listAvatar.forEach(function( image )
    {
        var avatar = $("#template .avatar-container").clone();   
        $(avatar).find('.avatar-image').attr("src", image);
        $("#"+type+"-list").append(avatar);
    });

    $(".btn-submit").css('display','none');
    $(".btn-submit").text('Guardar');
}