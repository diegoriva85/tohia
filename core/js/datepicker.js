    /**
     * instancia datapicker.js (https://github.com/eternicode/bootstrap-datepicker)
     * sitio web del proyecto: https://bootstrap-datepicker.readthedocs.io/
     * este proyecto esta basado en : http://www.eyecon.ro/bootstrap
     * @param  {[string]}  []
     * @author Orlando Nuske
     * @return {[obj]}   []
     */
    function callDatePicker()
    {
        // deshabilita el ingreso de fechas con el teclado
        //$('.datepicker').prop('readOnly', true);
        // $('.datepicker-year').keypress(function(event){
        //     event.preventDefault();
        // });
        // $( '.datepicker').keypress(function(event){
        //     event.preventDefault();
        // });
       
        // datepicker dia-mes-año sin topes
        $('.datepicker').datepicker({
            format: "dd-mm-yyyy",
            language: "es",
            autoclose: true,
            startDate: '01-01-1900',
           // endDate: '+0d'
        });
        // datepicker dia-mes-año, con fecha menor a la actual 
        $('.datepicker-from').datepicker({
            format: "dd-mm-yyyy",
            language: "es",
            autoclose: true,
            startDate: '01-01-1900',
            endDate: '+0d'
        });
        // datepicker dia-mes-año, con fecha mayor a la actual 
        $('.datepicker-to').datepicker({
            format: "dd-mm-yyyy",
            language: "es",
            autoclose: true,
            startDate: '-0d',
        });
        
        // datepicker solo año
        $('.datepicker-year').datepicker({
            format: "yyyy",
            language: "es",
            autoclose: true,
            viewMode: "years",
            minViewMode: "years"
        });

        // define datepicker rango dia-mes-año hasta el dia de la fecha
        $('.input-daterange').datepicker({
            endDate: '+0d',
            format: "dd-mm-yyyy",
            language: "es"
        });
        
        // cuando dan click en el icono del calendario muestra el datepacker 
        $('.ddjj-calendar').click(function() {
            $(this).parent().prev().focus();
        });

    }