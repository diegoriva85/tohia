//datos al leer el documento
$(document).on('ready',function()
{
	var avatarStructure = '\
    <div class="avatar-container col-md-55">\
        <div class="thumbnail">\
            <div class="image view view-first">\
                <img class="avatar-image" style="width: 100%; display: block;" src="' + Project.get('pathMediaUrl') + '/img/user_default.png' + '" alt="avatar" />\
                <!-- datos al pasar el mouse sobre la imagen -->\
                <div class="mask">\
                    <p class="avatar-text">Avatar</p>\
                    <!-- botones -->\
                    <div class="tools tools-bottom">\
                        <a href="#"><i class="fa fa-link"></i></a>\
                    </div>\
                </div>\
            </div>\
        </div>\
    </div>';

    //agrega la estrucutra de los avatar al pie donde se encuentran los templates
    $( avatarStructure ).appendTo( "#template" );

});

//una vez que haya cargado todos los elementos
$(window).on('load',function()
{
    //setea el home del modal para que carga los avatars
    $("#content-avatar").click();
});

