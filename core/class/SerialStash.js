SerialStash = {
    url: Configure.proyectFolder +'/core/class/SerialStash.php',
    unStashCtrl: null,
    stashCtrl: null,
    unStashListeners: [],
    /**
     * Devuelve el elemento que recibe e inicia la carga de un archivo
     * @param {string} offClass La clase css que denota que el elemento no está diespuesto a recibir un archivo
     * @param {string} onClass La clase css que denota que el elemento está dispuesto a recibir un archivo
     * @returns {element}
     **/
    getUnStashCtrl: function (offClass, onClass) {
        if (SerialStash.unStashCtrl === null) {
           // SerialStash.unStashCtrl = document.createElement('div');
            SerialStash.unStashCtrl = document.createElement('input');
            SerialStash.unStashCtrl.setAttribute("type", "file"); 
            $(SerialStash.unStashCtrl).addClass(offClass);
            
            $(SerialStash.unStashCtrl).on(
                'dragenter',
                function (event) {
                    $(this).addClass(onClass);
                    $(this).removeClass(offClass);
                    event.stopPropagation();
                    event.preventDefault();
                }
            );

            $(SerialStash.unStashCtrl).on(
                'dragover',
                function (event) {
                    event.stopPropagation();
                    event.preventDefault();
                }
            );

            $(SerialStash.unStashCtrl).on(
                'dragleave',
                function (event) {
                    $(this).addClass(offClass);
                    $(this).removeClass(onClass);
                    event.stopPropagation();
                    event.preventDefault();
                }
            );

            $(SerialStash.unStashCtrl).on(
                'drop',
                function (event) {
                    $(this).addClass(offClass);
                    $(this).removeClass(onClass);
                    event.stopPropagation();
                    event.preventDefault();

                    if (event.originalEvent.dataTransfer.files.length > 0) {
                        SerialStash.unStash(event.originalEvent.dataTransfer.files[0]);
                    }
                }            
            );
            $(SerialStash.unStashCtrl).on(
                'change',
                function (event) {
                    $(this).addClass(offClass);
                    $(this).removeClass(onClass);
                    event.stopPropagation();
                    event.preventDefault();
                    
                    if (event.originalEvent.target.files.length > 0) {
                        SerialStash.unStash(event.originalEvent.target.files[0]);
                    }
                }            
            );

            $(document).on(
                'dragenter',
                'dragover',
                'drageave',
                'drop',
                'change',
                function (event) {
                    event.stopPropagation();
                    event.preventDefault();
                }
            );
        }
        return SerialStash.unStashCtrl;
    },

    /**
     * Devuelve el elemento que inicia la descarga del archivo cifrado
     * @param {string} myClass La clase css del elemento
     * @returns {element}
     **/
    getStashCtrl: function (myText, myClasses) {
        if (SerialStash.stashCtrl === null) {

            var button = document.createElement('button');
            
            $(button).html(myText);
            $(button).append(' <span class="fa fa-cloud-download"></span>');

            SerialStash.stashCtrl = button;

            for (myClass in myClasses) {
                $(SerialStash.stashCtrl).addClass(myClasses[myClass]);
            }
            
            $(SerialStash.stashCtrl).on(
                'click',
                function () {

                    SerialStash.validate();

                }
            );
        }
        
        return SerialStash.stashCtrl;
    },
    
    /**
     * Agrega una función que será ejecutada en la carga de un archivo
     * @param {function} listener La función que será ejecutada
     * @returns {function}
     **/
    addUnStashListener: function (listener) {
        SerialStash.unStashListeners.push(listener);
        
        return listener;
    },
    /**
     * Da aviso de un evento de carga de un archivo
     * @param {Object} event
     * @returns {undefined}
     **/
    fireUnStashEvent: function (event) {
        for (var i = 0; i < SerialStash.unStashListeners.length; i++) {
            SerialStash.unStashListeners[i](event);
        }
    },
    /**
     * Inicia la descarga del archivo que contiene los datos cifrados
     * @returns {undefined}
     **/
    stash: function () {
        jQuery(
            '<form>',
            {
                action: SerialStash.url,
                method: 'post'
            }
        ).append(
            jQuery(
                '<input>',
                {
                    type: 'hidden',
                    name: 'action',
                    value: 'put'
                }
            )
        ).appendTo('body').submit();
    },
    /**
     * Inicia la carga del archivo cifrado
     * @param {File} file Archivo a descifrar
     * @returns {void}
     **/
    unStash: function (file) {
        var frm = new FormData();
        frm.append('action', 'retrieve');
        frm.append('file', file);
        
        $.ajax(
            {
                url: SerialStash.url,
                type: 'POST',
                dataType: 'json',
                contentType: false,
                processData: false,
                cache: false,
                data: frm,
                success: function (rtn) {
                    SerialStash.fireUnStashEvent(
                        {
                            data: rtn.data,
                            error: rtn.error
                        }
                    );
                },
                error: function () {
                    SerialStash.fireUnStashEvent(
                        {
                            data: null,
                            error: {
                                number: 2,
                                message: 'Error en la consulta'
                            }
                        }
                    );
                }
            }
        );
    },
    validate: function () {
        $.ajax(
            {
                url: Configure.proyectFolder +'/core/class/DDJJ.php',
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'validate'
                },
                success: function (data) {

                if (data.hasOwnProperty("error"))
                {
                    $(".tab-menu ul li i").removeClass('fa-warning');
                    $("ul li.tab-menu ul li label i").removeClass('fa-warning');
                    $(".input-group").removeClass("has-error").removeClass("has-warning");

                 //   window.location.href = Configure.proyectFolder +'/core/class/DDJJ.php?action=exportPDF';
                    SerialStash.stash();
                }
                else {
                    toastr["error"]("Sección incompleta");

                    $(".tab-menu ul li i").removeClass('fa-warning');
                    $("ul li.tab-menu ul li label i").removeClass('fa-warning');
                    $(".input-group").removeClass("has-error").removeClass("has-warning");
                    $(".panel .panel-heading").removeClass("bg-danger").find(".fa.fa-warning").remove();
                    $(".input-group .input-group-btn").children(".btn").not(".ddjj-calendar").not("ddjj-hours").not(".hidden-xs").not("select.btn").html('?');
                    
                    highlightError(data);
                }

                       return data;
                },
                error: function (obj, err) {
                        return err;                }
            }
        );
    }


};