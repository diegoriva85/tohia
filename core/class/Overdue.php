<?php

class Overdue {

    public $id;
    public $CUIT;
    public $NyA;
    public $position;
    public $jurisdiction;
    public $sourceFile;
    public $admission;
    public $discharge;
    public $sort;
    public $deleted;
    private $valueSearch;

    public function Overdue() {
        $this->deleted = 0;
    }

    public function load() {
        $dbLink = Database::connect();

        // if  ((empty($this->id) xor empty($this->CUIT)) && (empty($this->id) xor empty($this->admission)) && (empty($this->id) xor empty($this->position)) && (empty($this->id) xor empty($this->jurisdiction))) {
        //     throw new Exception('No es posible encontrar un registro asociado');
        // }

        $id = $this->id;
        $cuit = $this->CUIT;
        $nya = $this->NyA;
        $position = $this->position;
        $jurisdiction = $this->jurisdiction;
        $admission = $this->admission;
        $discharge = $this->discharge;

        $where = '';
        $types = '';
        $params = [];

        $val_id = empty($id);

        if (!$val_id) {
            $where .= ' and id = ? ';
            $types .= 'i';
            $params[] = &$id;
        }

        $val_cuit = empty($cuit);

        $val_nya = empty($nya);

        if (!$val_cuit) {
            $where .= ' and cuit = ? ';
            $types .= 's';
            $params[] = &$cuit;
        }

        if (!$val_nya) {
            $where .= ' and name = ? ';
            $types .= 's';
            $params[] = &$nya;
        }

        $val_position = empty($position);

        if (!$val_position) {
            $where .= ' and position_id = ? ';
            $types .= 'i';
            $params[] = &$position;
        }

        $val_jurisdiction = empty($jurisdiction);

        if (!$val_jurisdiction) {
            $where .= ' and jurisdiction_id = ? ';
            $types .= 'i';
            $params[] = &$jurisdiction;
        }

        $val_admission = empty($admission);

        if (!$val_admission) {
            $where .= ' and admission_date = ? ';
            $types .= 's';
            $params[] = &$admission;
        }

        $stmt = $dbLink->prepare(
                'select 
                id,
                cuit,
                name,
                overdue_files_id,
                position_id,
                jurisdiction_id,
                admission_date,
                discharge_date,
                deleted
            from
                overdue  
            where 
                deleted = 0 
            ' . $where
        );

        $bind_params = array_merge(array($types), $params);

        call_user_func_array(array($stmt, 'bind_param'), $bind_params);

        $stmt->bind_result(
                $this->id, $this->CUIT, $this->NyA, $this->sourceFile, $this->position, $this->jurisdiction, $this->admission, $discharge, $this->deleted
        );

        if ($discharge != '0000-00-00') {
            $this->setDischarge($discharge);
        }

        $stmt->execute();
        
        $stmt->fetch();
        
        $stmt->close();
        $dbLink->close();
    }

    public function save($strict = 1) {
        $dbLink = Database::connect();

        /*
          TODO:

          Volver a reemplazar con php 5.6:

          if (empty($this->CUIT) || empty($this->jurisdiction) || empty($this->position) || empty($this->admission)) {
          throw new Exception('Faltan campos para la carga');
          }


         */

        $val_jurisdiction = $this->jurisdiction;
        $val_cuit = $this->CUIT;
        $val_nya = $this->NyA;
        $val_position = $this->position;
        $val_admission = $this->admission;
        $val_discharge = $this->discharge;

        if (($val_cuit == "") || ($val_jurisdiction == "") || ($val_position == "") || ($val_admission == "" || ($val_nya == ""))) {
            throw new Exception('Faltan campos para la carga');
        }

        $currentUser = new User();
        $currentUser->load();
        $currentUser_id = $currentUser->getId();

        $sourceFile = ((empty($this->sourceFile)) ? null : $this->sourceFile);
        /*POR QUE VA A USER?????*/
        $val_id = $this->id;

        if (($val_id == "")) { //($val_id == "0")
            
            $stmt = $dbLink->prepare('insert into overdue (cuit,name, position_id, jurisdiction_id, overdue_files_id, admission_date, discharge_date, created_by) values (?,?,?,?,?,?,?,?)');
            $stmt->bind_param('ssiiissi', $val_cuit, $val_nya, $val_position, $val_jurisdiction, $sourceFile, $val_admission, $val_discharge, $currentUser_id);
            /*
            // Despues de probar se debe eliminar
            $stmt->bind_param('ssiiissi', $this->CUIT, $this->NyA, $this->position, $this->jurisdiction, $sourceFile, $this->admission, $this->discharge, $currentUser_id);
            */
            $stmt->execute();

            if ($dbLink->error) {
                throw new Exception($dbLink->error);
            }

            $this->id = $dbLink->insert_id;
            
        } else {
            $val_discharge = !empty($this->discharge);

            /*
              TODO:

              Volver a reemplazar con php 5.6:

              if  (($strict === 1  && empty($this->discharge)) || $strict === 0 )

             */

            if (($strict === 1 && $val_discharge) || $strict === 0) {
                $stmt = $dbLink->prepare('
                    update
                        overdue 
                    set 
                        cuit = ?,
                        name = ?,
                        position_id = ?, 
                        admission_date = ?, 
                        discharge_date = ?,
                        edited_by = ?,
                        deleted = ?,
                        edited_at = now()
                    where
                        id = ?
                        and (0 = ? or discharge_date is null)
                    ');
                $stmt->bind_param('ssissiiii', $this->CUIT, $this->NyA, $this->position, $this->admission, $this->discharge, $currentUser_id, $this->deleted, $this->id, $strict);
                $stmt->execute();
                $affec_rows = $dbLink->affected_rows;

                if (!$affec_rows) {
                    throw new Exception('Ya existe un registro con estos datos');
                }
            } else {
                throw new Exception('No es posible eliminar la fecha de egreso');
            }
        }
    }

    public function drop() {
        $this->deleted = 1;
    }

    public function setValueSearch($valueSearch) {
        $this->valueSearch = $valueSearch;
    }
    
    public function setId($id) {
        $this->id = $id;
    }

    public function setCUIT($CUIT) {
        $this->CUIT = $CUIT;
    }

    public function setNyA($nya) {
        $this->NyA = $nya;
    }

    public function setPosition($position) {
        $this->position = $position;
    }

    public function setJurisdiction($jurisdiction) {
        $this->jurisdiction = $jurisdiction;
    }

    public function setSourceFile($sourceFile) {
        $this->sourceFile = $sourceFile;
    }

    public function setAdmission($admission) {
        $this->admission = $admission ? date_format(date_create(trim($admission)), 'Y-m-d') : '';
    }

    public function setDischarge($discharge) {
        $this->discharge = $discharge ? date_format(date_create(trim($discharge)), 'Y-m-d') : '';
    }

    public function setSort($sort){
        $this->sort = $sort;
    }
    
    public function getSort(){
        return $this->sort;
    }

    public function getValueSearch() {
        return $this->valueSearch;
    }

    public function getId() {
        return $this->id;
    }

    public function getCUIT() {
        return $this->CUIT;
    }

    public function getNyA() {
        return $this->NyA;
    }

    public function getPosition() {
        return $this->position;
    }

    public function getJurisdiction() {
        return $this->jurisdiction;
    }

    public function getSourceFile() {
        return $this->sourceFile;
    }

    public function getAdmission() {
        return $this->admission;
    }

    public function getDischarge() {
        return $this->discharge;
    }

}

?> 