<?php

class User 
{

    private $id;
    protected $loginName;
    private $objectGUID;
    private $SADEName;
    private $name;
    private $key;
    private $commentPrivileges;
    private $supervisor;
    private $publisher;
    protected $jurisdiction;
    private $granter;
    private $job_archivist;
    private $over_viewer;
    private $unlocker;
    protected $loginStatus;
    private $lastMessage;

    private $distribution; 
    private $position;

    public function User ($loginName = null) {
        Session::retrieve();
        
        $this->key = openssl_random_pseudo_bytes(32);
        $this->commentPrivileges = [];
        $this->supervisor = 0;
        $this->publisher = 0;
        $this->granter = 0;
        $this->job_archivist = 0;
        $this->over_viewer = 0;
        $this->unlocker = 0;
        $this->jurisdiction['id'] = 0;
        $this->jurisdiction['name'] = '';
        
        if (!empty($loginName)) {
            $this->loginName = $loginName;
            $this->loginStatus = FALSE;
        }
        else if (!empty($_SESSION['loginStatus'])) {
            $this->loginName =   $_SESSION['loginName'];
            $this->loginStatus = $_SESSION['loginStatus'];
            // $this->SADEName =  $_SESSION['SADEName'];
            // $this->name = $_SESSION['name'];
            // $this->jurisdiction['name'] = $_SESSION['jurisdiction']['name'];
            // $this->distribution = $_SESSION['distribution'];
            // $this->position = $_SESSION['position'];

        }
        // else {
        //     //throw new Exception('There is no active session and no login name was provided.');
        // }
    }
    


    /**
     * Chequea si un usuario existe en el AD de la ASI
     * @param  [string] clave del usuario para validar el acceso
     * @return [string] mensaje con el estado 
     */
    public function logIn ($pass)
    {
        $data ['loginName'] = $this->loginName; 
        $data ['pass'] = $pass;

        $result = Communicate::loginAD($data);

        if (isset($result['error']))
        {
            $this->loginStatus = FALSE;
            $this->lastMessage = $result['error']['message'];
        }
        else
        {
            $info = $result['success']['user'];

            //@TODO: analizar las consecuencias que implicaria quitar el objectguid, dado que fue recomendado por la ASI no usar el elemento y basarnos  el userprincipalname. El dato que no cambia es el samAccountName que se corresponde con el CUIT
            //obtiene el objectguid
            $gottenProp = $info['objectguid'][0];

            $objectGUID = User::objectGUIDToString($gottenProp);
            $this->objectGUID = $objectGUID;

            //obtiene el id de la session para el temporizador
            $_SESSION['sessionId'] = session_id();
            $loginUser = $this->getLoginName();

            $rst = Communicate::userCuitSADE($loginUser);

            if(isset($rst['error']))
            {
                $this->loginStatus = FALSE;
                $this->lastMessage = $rst['error']['message'];
            }
            else
            {
                $this->SADEName = $rst['usuario'];
                $this->name = $rst['name'];
                $this->jurisdiction['name'] = $rst['jurisdiction'];
                $this->distribution = $rst['distribution'];
                $this->position = $rst['cargo'];
                $this->loginStatus = TRUE;

                $_SESSION['loginName'] =  $this->loginName;        
                $_SESSION['SADEName'] =  $this->SADEName;
                $_SESSION['name'] =  $this->name;
                $_SESSION['jurisdiction']['name'] =  $this->jurisdiction['name'];
                $_SESSION['distribution'] =  $this->distribution;
                $_SESSION['position'] =  $this->position;
                $_SESSION['loginStatus'] =  $this->loginStatus;
                $_SESSION['key'] =  $this->key;
                $_SESSION['objectGUID'] =  $this->objectGUID;

                // // Se busca si el usario existe en base para guardar correctemtne los datos 
             
                $dbLink = Database::connect();
                $stmt = $dbLink->prepare('select `id`, `supervisor`, `publisher`, `granter`, `job_archivist`, `over_viewer`, `unlocker` from user where login = ? and deleted = 0');
                $stmt->bind_param('s', $this->loginName);
                $stmt->bind_result($this->id,$this->supervisor,$this->publisher,$this->granter,$this->job_archivist,$this->over_viewer,$this->unlocker);
                $stmt->execute();
                $stmt->fetch();
                $stmt->close();

                $_SESSION['id'] =  $this->id;        
                $_SESSION['supervisor'] =  $this->supervisor;
                $_SESSION['publisher'] =  $this->publisher;
                $_SESSION['granter'] =  $this->granter;
                $_SESSION['job_archivist'] =  $this->job_archivist;
                $_SESSION['over_viewer'] =  $this->over_viewer;
                $_SESSION['unlocker'] =  $this->unlocker;

                 //vefico si hay datos que actualizar o si hay qeu crear un usuario nuevo.
                $this->save();

                // //obtiene los datos guardados en la base correspondientes al perfil del usuario
                $this->load(); 
            // var_dump($this);
            // die;
            
            }
        }

        $_SESSION['loginStatus'] = $this->loginStatus;
        return $this->loginStatus;
    }
    
    public function logOut () {
        Session::retrieve();
        
        $this->loginStatus = FALSE;
        
        session_destroy();
    }
    
    public function load () {
        Session::retrieve();

        $dbLink = Database::connect();
        
        $stmt = $dbLink->prepare('select `id`, `key`, `supervisor`, `publisher`, `granter`, `job_archivist`, `over_viewer`, `unlocker`, `objectGUID`, `SADE_name`, `name` from user where login = ? and deleted = 0');
        $stmt->bind_param('s', $this->loginName);
        $stmt->bind_result($this->id, $this->key, $this->supervisor, $this->publisher, $this->granter, $this->job_archivist, $this->over_viewer, $this->unlocker, $this->objectGUID, $this->SADEName, $this->name);
        $stmt->execute();
        $stmt->fetch();
        $stmt->close();

        $stmt = $dbLink->prepare('select `id` from obs_grants where recipient = ? and claimed_by is null and deleted = 0 order by created_at asc');
        $stmt->bind_param('i', $this->id);
        $stmt->execute();
        
        $obs_grant = null;
        $stmt->bind_result($obs_grant);
        
        while ($stmt->fetch()) {
            $this->commentPrivileges[] = [
                'id' => $obs_grant,
                'claimed_by' => null,
                'deleted' => 0
            ];
        }
    
        $stmt->close();
        $dbLink->close();
        
        //se captura todo lo esta en session para el objeto user

        // $this->id = $_SESSION['id'];
        // $this->key = $_SESSION['key'];
        // $this->supervisor = $_SESSION['supervisor'];
        // $this->publisher = $_SESSION['publisher'];
        // $this->granter = $_SESSION['granter'];
        // $this->job_archivist = $_SESSION['job_archivist'];
        // $this->over_viewer = $_SESSION['over_viewer'];
        // $this->unlocker = $_SESSION['unlocker'];
        // $this->objectGUID = $_SESSION['objectGUID'];
        // $this->SADEName = $_SESSION['SADEName'];
        // $this->name = $_SESSION['name'];
        // $this->loginName = $_SESSION['loginName'];  
        $this->jurisdiction['name'] = $_SESSION['jurisdiction']['name'];
        $this->distribution = $_SESSION['distribution'];
        $this->position = $_SESSION['position'];
        $this->loginStatus = $_SESSION['loginStatus'];
       
 


        $id = '';
        $dbLink = Database::connect();
        $stmt = $dbLink->prepare('select id from jurisdiction where name = ?');
        $stmt->bind_param('s',$this->jurisdiction['name']);
        $stmt->execute();
        $stmt->bind_result($id);
        $stmt->fetch();
        $stmt->close();
        

        $this->jurisdiction['id'] = $id;
        $_SESSION['jurisdiction']['id'] = $this->jurisdiction['id'];
    }
    
    public function save () {
        $dbLink = Database::connect();
        
        if (empty($this->id)) {
            $stmt = $dbLink->prepare('insert into user (`login`, `objectGUID`, `SADE_name`, `name`, `key`, `supervisor`, `publisher`, `granter`, `unlocker`,`job_archivist`, `over_viewer`, `deleted`, `created_by`, `created_at`, `updated_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0, 1, now(), now())');
            // echo('insert into user (`login`, `objectGUID`, `SADE_name`, `name`, `key`, `supervisor`, `publisher`, `granter`, `unlocker`,`job_archivist`, `over_viewer`, `deleted`, `created_by`, `created_at`, `updated_at`) values ('.$this->loginName.','. $this->objectGUID.','. $this->SADEName.','. $this->name.','. $this->key.','. $this->supervisor.','. $this->publisher.','. $this->granter.','. $this->unlocker.','. $this->job_archivist.','. $this->over_viewer.', 0, 1, now(), now())');
            // die;

            $stmt->bind_param('sssssiiiiii', $this->loginName, $this->objectGUID, $this->SADEName, $this->name, $this->key, $this->supervisor, $this->publisher, $this->granter, $this->unlocker, $this->job_archivist, $this->over_viewer);

        }
        else {
            $stmt = $dbLink->prepare('update user set `login` = ?, `objectGUID` = ?, `SADE_name` = ?, `name` = ?, `key` = ?, `supervisor` = ?, `publisher` = ?, `granter` = ?, `unlocker` = ?, `job_archivist` = ?, `over_viewer` = ?  where id = ?');
            $stmt->bind_param('sssssiiiiiii', $this->loginName, $this->objectGUID, $this->SADEName, $this->name, $this->key, $this->supervisor, $this->publisher, $this->granter, $this->unlocker,$this->job_archivist, $this->over_viewer, $this->id);
        }

        $stmt->execute();
        
        if (empty($this->id)) {
            $this->id = $dbLink->insert_id;
        }
        
        $stmt->close();
        
        $obsGrantAdd = $dbLink->prepare('insert into obs_grants (id, recipient, claimed_by, created_by, claimed_at, created_at, deleted) values (null, ?, null, ?, null, now(), ?)');
        $obsGrantClaim = $dbLink->prepare('update obs_grants set claimed_by = ? where id = ?');
        $obsGrantDelete = $dbLink->prepare('update obs_grants set deleted = 1 where id = ?');
        
        $obsGrantId = null;
        $grantClaimed = null;
        $grantDeleted = null;
        
        $obsGrantAdd->bind_param('iii', $this->id, $this->id, $grantDeleted);
        $obsGrantClaim->bind_param('ii', $grantClaimed, $obsGrantId);
        $obsGrantDelete->bind_param('i', $obsGrantId);
        
        for ($i = 0; $i < count($this->commentPrivileges); $i++) {
            if (empty($this->commentPrivileges[$i]['id'])) {
                $grantDeleted = $this->commentPrivileges[$i]['deleted'];
                
                $obsGrantAdd->execute();
                
                $this->commentPrivileges[$i]['id'] = $dbLink->insert_id;
            }
            else {
                $obsGrantId = $this->commentPrivileges[$i]['id'];
                
                if ($this->commentPrivileges[$i]['deleted'] === 1) {
                    $obsGrantDelete->execute();
                }                
                elseif ($this->commentPrivileges[$i]['claimed_by'] !== null) {
                    $grantClaimed = $this->commentPrivileges[$i]['claimed_by'];
                    
                    $obsGrantClaim->execute();
                }
            }
        }
        
        $obsGrantAdd->close();
        $obsGrantClaim->close();
        $obsGrantDelete->close();
        
        $dbLink->close();
    }
    
    public function getId () {
        return $this->id;
    }
    
    public function getLoginName () {
        return $this->loginName;
    }
    
    public function setSADEName ($SADEName) {
        $this->SADEName = $SADEName;
    }
    
    public function getSADEName () {
        return $this->SADEName;
    }
    
    public function getName () {
        return $this->name;
    }
    
    public function getKey () {
        return $this->key;
    }

    public function getDistribution () {
        return  $this->distribution;
    }

    public function getPosition () {
        return  $this->position;
    }

    public function hasCommentPrivilege () {
        $count = 0;
        
        for ($i = 0; $i < count($this->commentPrivileges); $i++) {
            if ($this->commentPrivileges[$i]['claimed_by'] === null && $this->commentPrivileges[$i]['deleted'] === 0) {
                $count++;
            }
        }
        
        return ($count > 0);
    }
    
    public function grantCommentPrivilege () {
        $this->commentPrivileges[] = [
            'id' => '',
            'claimed_by' => null,
            'deleted' => 0
        ];
    }
    
    public function claimCommentPrivilege ($affidavit) {
        $count = -1;
        
        for ($i = 0; $i < count($this->commentPrivileges); $i++) {
            if ($this->commentPrivileges[$i]['claimed_by'] === null && $this->commentPrivileges[$i]['deleted'] === 0) {
                $count++;
            }
        }
        
        if ($count > -1) {
            $this->commentPrivileges[$count]['claimed_by'] = $affidavit;
        }
        else {
            throw new Exception('No tiene permisos disponibles para agregar una observación');
        }
    }
    
    public function revokeCommentPrivilege () {
        $count = -1;
        
        for ($i = 0; $i < count($this->commentPrivileges); $i++) {
            if ($this->commentPrivileges[$i]['claimed_by'] === null && $this->commentPrivileges[$i]['deleted'] === 0) {
                $count++;
            }
        }
        
        if ($count > -1) {
            $this->commentPrivileges[$count]['deleted'] = 1;
        }
        else {
            throw new Exception('No tiene permisos disponibles para revocar');
        }
    }
    
    public function promoteSupervisor () {
        $this->supervisor = 1;
    }
    
    public function demoteSupervisor () {
        $this->supervisor = 0;
    }
    
    public function isSupervisor () {
        return $this->supervisor;
    }
    
    public function promotePublisher () {
        $this->publisher = 1;
    }
    
    public function demotePublisher () {
        $this->publisher = 0;
    }
    
    public function isPublisher () {
        return $this->publisher;
    }
    
    public function promoteGranter () {
        $this->granter = 1;
    }
    
    public function demoteGranter () {
        $this->granter = 0;
    }
    
    public function isGranter () {
        return $this->granter;
    }
    
    public function promoteJobArchivist () {
        $this->job_archivist = 1;
    }
    
    public function demoteJobArchivist () {
        $this->job_archivist = 0;
    }

    public function isJobArchivist () {
        return $this->job_archivist;
    }
    
    public function promoteOverviewer () {
        $this->over_viewer = 1;
    }
    
    public function demoteOverviewer () {
        $this->over_viewer = 0;
    }

    public function isOverviewer () {
        return $this->over_viewer;
    }
    
    public function promoteUnlocker () {
        $this->unlocker = 1;
    }
    
    public function demoteUnlocker () {
        $this->unlocker = 0;
    }
    
    public function isUnlocker () {
        return $this->unlocker;
    }

    public function isLoggedIn () {
        return $this->loginStatus;
    }
    
    public function getLastMessage () {
        return $this->lastMessage;
    }

    public function getIdJurisdiction () {
        return  $this->jurisdiction['id'];
    }
    
    public function getNameJurisdiction () {
        return  $this->jurisdiction['name'];
    }


    public static function getLDAPUsers () {
        
    }
    
    private static function objectGUIDToString ($objectGUID) {
        $rtn = unpack('Va/v2b/n2c/Nd', $objectGUID);
        
        return sprintf('%08x-%04x-%04x-%04x-%04x%08x', $rtn['a'], $rtn['b1'], $rtn['b2'], $rtn['c1'], $rtn['c2'], $rtn['d']);
    }
}

?>