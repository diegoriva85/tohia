<?php
/**
 *
 */
class Database
{

    private function __construct()
    {
        # code...
    }

    /**
     * establece la conexion con la base de datos
     * @param  string $host ip privada de la base de datos
     * @param  string $port puerto de la base de datos
     * @param  string $name nombre de la base de datos
     * @param  string $user usuario de acceso a la base de datos
     * @param  string $pass clave de acceso a la base de datos
     * @return object retorna una conexion con la base de datos
     */
    public static function connect($host = DB_IP, $port = DB_PORT, $name = DB_NAME, $user = DB_USER, $pass = DB_PASS)
    {
        $db = array($host, $port, $user, $pass, $name);

        //imprime en consola los valores de la conexion
        //Console::debug($db);

        //establece la conexion
        $link = mysqli_connect($host, $user, $pass, $name, $port) or die (ERROR_DB_CONEXION . " - Error " . mysqli_error($link));

        return $link;
    }

    /**
     * ejecuta una consulta
     * @param  string $query consulta sql
     * @return  array retorna la matriz con los datos de la consulta
     */
    public static function query($query, $associative = false)
    {

        $link = static::connect();

        //ejecuta la consulta
        $result = mysqli_query($link, $query);

        self::close($link);

        if($associative)
        {
            return mysqli_fetch_assoc($result);
        }
        else
        {
            return $result;
        }
    }

    /**
     * ejecuta una consulta
     * @param  string $query consulta sql
     * @param boolean $numeric  si desea que la matriz se acceda por indice y asociacicion
     * @return  array retorna la matriz con los filas de la tabla
     */
    public static function result($query, $numeric = false)
    {

        $link = static::connect();

        //ejecuta la consulta
        $result = self::query($query);

        //variable que alojara la matriz
        $matrix = array();

        //si solicito acceder a los resultados por asociacion columnas y por número
        if ($numeric)
        {
            // guarda cada fila con sus respectivos numero y nombres de columnas
            while ( $rows = mysqli_fetch_array($result) )
            {
                $matrix[] = $rows;
            }
        }
        else
        {
            // guarda cada fila con sus respectivo nombre de columna
            while ( $rows = mysqli_fetch_assoc($result) )
            {
                $matrix[] = $rows;
            }
        }

        self::close($link);

        return $matrix;
    }

    /**
     * ejecuta una modificacion sobre la base de datos
     * @param  string $query consulta sql
     * @return  boolean retorna false en caso de no poder ejecutar la sentencia caso de exito la cantidad de registros modificados
     */
    public static function modify($query)
    {
        $modify = false; //chequea si la base fue modificada con exito
        $sentence = strstr($query, ' ', true);//detecta el tipo de sentencia
        
        $link = static::connect();
        $result = mysqli_query($link, $query);
        
        //si es una insercion
        if($sentence == "INSERT")
        {
            $modify = mysqli_insert_id($link);
        }
        else
        {
            $modify = mysqli_affected_rows($link);
        }

        self::close($link);

        return $modify;
    }

    /**
     * imprime las columnas pasada por parametros
     * @param  string $table_name nombre de la tabla
     * @param  array $fields  listado de campos a imprimir
     * @return  array con una tabla preformateada segun los campos $rows que desea imprimir
     */
    public static function prints($table_name, $fields)
    {
        $table = array(); //guarda los valores de la nueva tabla de los campos $fields

        $link = static::connect();

        //prepara la consulta
        $query = "SELECT " . implode(',', $fields) . " FROM $table_name";

        $result = database::result($query);

        //mientras existan registros
        foreach ($result as $value) {
            //mientras existan campos (indicados anteriormente en el array $fields)
            foreach ($fields as $indice) {
                echo $value[$indice] . '<br>';
            }
        }

        self::close($link);

        return $table;
    }

    /**
     * cuenta la cantidad de filas que devuelve la consulta
     * @param  string $query consulta sql
     * @param  array $rows  listado de campos a imprimir
     * @return  string cantidad de filas que devuelve la consulta
     */
    public static function count($query)
    {
        $count = 0; //cantidad filas que devuelva la consulta

        $link = static::connect();
        
        //ejecuta la consulta
        $result = self::query($query);

        $count = @mysqli_num_rows($result);

        self::close($link);

        return $count;
    }

    /**
     * selecciona una base de datos
     * @param  string $database nombre de la base de datos
     */
    public static function select($database)
    {
        mysql_select_db($database) or die(ERROR_DB_EXIST);
    }

    /**
     * metodo de testeo para saber que clase la llama y que devuelve el metodo
     */
    public static function dba()
    {
        echo __CLASS__;
        static::connect();
    }

    /**
     * metodo que llama a dba segun la instancia del objeto correspondiente
     */
    public static function link()
    {
        //clase extendida
        static::dba();
        //clase padre
        // self::dba();
        //no estoy seguro
        // parent::dba();
    }
    
    /**
     * cierra la conexion con la base de datos
     * @param  object $link conexion a la base de datos
     */
    public static function close($link)
    {
        //libera la conexion
        mysqli_close($link);
    }
}
