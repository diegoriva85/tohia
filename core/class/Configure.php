<?php
//llamada a la configuracion global del sistema si se realiza una peticion por AJAX
//si no esta incluido la configuracion general, lo incluye /var/www/html/<project>/sys/conf/ini.php

// (!defined('PROJECT_INIT')) ? require $_SERVER['DOCUMENT_ROOT'].'/'.strstr(substr($_SERVER['PHP_SELF'],1), '/', true)."/sys/conf/ini.conf" : '';
require_once  __DIR__.'/../../sys'.DIRECTORY_SEPARATOR.'conf'.DIRECTORY_SEPARATOR.'ini.conf';
/**
 * 
 */
class Configure {

    private static $lang = "es"; //idioma por defecto
    private static $class = array('Configure', 'Project', 'User'); //clases del front necesarias

    function __construct() {
        # code...
    }

    /**
     * [chequea si el parametro no esta vacio y fue definido]
     * @param  [type] $var [description]
     * @return [string]      [description]
     */
    public static function request($var)
    {
        if (isset($_REQUEST[$var]) && !empty($_REQUEST[$var]))
        {
            return $_REQUEST[$var];
        }
        else
        {
            return "";
        }
    }

    /**
     * [setea la zona horaria]
     * @param  [string] $zoneNew [nueva zona]
     * @return [void]          []
     */
    public static function timeZone($zoneNew = 'America/Argentina/Buenos_Aires') {

        $script_tz = date_default_timezone_get();

        if (strcmp($script_tz, ini_get('date.timezone'))) {
            // echo 'La zona horaria del script difiere de la zona horaria de la configuracion ini.';
        } else {
            // echo 'La zona horaria del script y la zona horaria de la configuración ini coinciden.';
        }
        // date_default_timezone_set('America/Argentina/Buenos_Aires');

        date_default_timezone_set($zoneNew);
    }
    
    public static function charset( $value )
    {
        echo "<meta charset=".$value.">";
    }    
    
    public static function init( $options = NULL )
    {

        //obtiene las opciones si se definieron
        $options = (isset($options)) ? json_decode($options, TRUE) : NULL;

        //obtiene el link actual
        $linkCurrent = $_SERVER["PHP_SELF"];

        //si se esta debugeando la web y la pagina actual no corresponde a la configuracion debug
        if (DEBUG_WEB && $linkCurrent != "developer_debug.php")
        {
            //redirige a la pagina de mantenimiento
            header('location:' . PROJECT_URL . '/core/error/notify/maintenance.php');
        }

        if ($options)
        {

            if ($options["charset"]) {
                echo "<meta charset='" . $options["charset"] . "'>\n\t";
            }

            if ($options["title"]) {
                echo "<title>" . $options["title"] . "</title>\n\t";
            }

            if ($options["lang"]) {
                //ver como agregar al <html lang= $options["lang"] >
            }
            if (isset($options["description"])) {
                echo '<meta name="description" content="' . $options["description"] . '">' . "\n\t";
            }

            //traqueo de la plataforma web con google analytics
            if (isset($options["analytics"]) && $options["analytics"] == "true")
            {
                include PROJECT_DIR . DS . 'core' . DS . 'frame' . DS . 'analytics_tracking.php';
            }
        }
        ?>

        <!-- ie -->
        <!--[if lt IE 7]><html lang="es" class="lt-ie10 lt-ie9 lt-ie8 lt-ie7"><![endif]-->
        <!--[if IE 7]><html lang="es" class="lt-ie10 lt-ie9 lt-ie8"><![endif]-->
        <!--[if IE 8]><html lang="es" class="lt-ie10 lt-ie9"> <![endif]-->
        <!--[if IE 9]><html lang="es" class="lt-ie10"> <![endif]-->

        <!--metas-->
        <meta http-equiv="Cache-control" content="no-cache, no-store, must-revalidate, private">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="proba">
        <meta name="robots" content="noindex, nofollow">

        <?php
        //favicon del sitio
        echo "\n\t"; //solo estetica para identar el codigo
        echo "<link rel='shortcut icon' type='image/x-icon' href='" . CORE_URL . "/media/img/favicon.ico'>\n\t";

        //setea la zona horaria
        self::timeZone();

        //inicia la sesion
        // deshbilito el inicio de sesion en Configure::init() porque se inicia cuando el usuario se loguea (webservice)
        //Session::init();
        
        //obtiene el servidor activo
        // $serverActive = Dns::server();
        //@TODO: chequea si es necesaria la constante
        //define la constante que se usara para incluir librerias, clases, etc en comun
        define('SERVER_DNS', PROJECT_URL . "/sys");
        
        //define los autoload de php para las instancias de los objetos
        // self::classPhp();
        //agrega las clases del front necesarias
        self::classJs('Configure', 'Project', 'User', 'Security');

        //librerias generales para todas las paginas
        //self::lib('jquery', 'bastrap', 'toastr', 'nprogress');

        //si esta logueado, incluye las lib necesarias para el muestreo del contenido
        //if(!empty($_SESSION['sessionId']))
        //if($user->isLoggedIn())
        //{
            //agrega las librerias necesarias para la applicacion
        //    self::lib('nicescroll', 'font-awesome', 'jquery-ui', 'datepicker', 'progressbar', 'flipclock', 'jqpagination', 'animate', 'hover-master', 'slick');
        //}

    }

    public static function read() {
        echo 'funca';
    }

    public static function load($value = '') {
        echo 'funca';
    }

    public static function lib() {
        $filePath = CORE_DIR . DS . 'lib' . DS;
        $length = func_num_args();

        for ($i = 0; $i < $length; $i++) {
            $fileName = func_get_arg($i);
            $fileLib = $filePath . $fileName . DS . $fileName . ".php";

            include $fileLib;
        }
    }

    public static function libLocal() {
        $filePath = PROJECT_DIR . DS . 'core' . DS . 'lib' . DS;
        $length = func_num_args();

        for ($i = 0; $i < $length; $i++) {
            $fileName = func_get_arg($i);
            $fileLib = $filePath . $fileName . DS . $fileName . ".php";

            include $fileLib;
        }
    }

    public static function inc($link) {
        $length = func_num_args();


        for ($i = 0; $i < $length; $i++) {
            include func_get_arg($i);
        }
    }

    public static function req($link) {
        $length = func_num_args();

        for ($i = 0; $i < $length; $i++) {
            require func_get_arg($i);
        }
    }

    public static function frame($link) {
        $length = func_num_args();


        for ($i = 0; $i < $length; $i++) {
            // include self::dir( func_get_arg($i) );
            include PROJECT_DIR . DS . 'core' . DS . 'frame' . DS . func_get_arg($i) . '.php';
        }
    }

    public static function incAllJs($dir, $path) {
        //solo estetica para identar el codigo
        echo "\n\t";

        foreach (glob($dir) as $file) {

            $dir = explode(DS, $file);

            $last = sizeof($dir) - 1;

            $file = $dir[$last];

            echo '<script type="text/javascript" src="' . (($path == "local") ? ((SERVER_URL == 'localhost' || SERVER_URL == SERVER_IP) ? PROJECT_URL : PROTOCOL ."://" . SERVER_URL) : PROTOCOL . "://" . SERVER_IP) . '/core/class/' . $file . '?v=' . PROJECT_VERSION . '"></script>' . "\n\t";


            // echo $file;
            // readfile($file);
        }
    }

    /**
     * incluye las clases del front
     * @param  [type] $allClass [description]
     * @return [type]           [description]
     */
    public static function classJs($allClass = null) {
        
        //si no especifico que clases quiere incluir, incluye todas las de core
        if ($allClass == null)
        {
            //directorio local donde buscara las clases de JS
            $pathClass = CORE_URL . DS . 'core' . DS . 'class' . DS . '*.js';

            //incluye todas las clases locales al proyecto
            self::incAllJs($pathClass, "local");
        }
        else
        {
            //obtiene la cantidad de parametros
            $length = func_num_args();

            for ($i = 0; $i < $length; $i++) {
                //obtiene el parametro
                $file = func_get_arg($i);

                echo '<script type="text/javascript" src="' . CORE_URL . '/class/' . $file . '.js?v=' . PROJECT_VERSION . '"></script>' . "\n\t";
            }
        }
    }

    public static function classModule() {
        //obtiene la ruta url de donde se llama
        $self = explode("/", $_SERVER['PHP_SELF']);
        $module = $self[3];

        //url de la clase para el modulo donde se llama
        $urlRoot = PROJECT_URL . '/module/' . $module . '/class/';

        // obtiene la cantidad de parametros
        $length = func_num_args();

        for ($i = 0; $i < $length; $i++) {
            // obtiene el parametro
            $file = func_get_arg($i);
            $url = $urlRoot . $file . '.js?v=' . PROJECT_VERSION;

            // echo 'la url ' .$url;
            echo "<script type='text/javascript' src='$url'></script>" . "\n\t";
        }
    }

    public static function classCore() {
        //obtiene la cantidad de parametros
        $length = func_num_args();

        for ($i = 0; $i < $length; $i++) {
            //obtiene el parametro
            $file = func_get_arg($i);

            echo '<script type="text/javascript" src="' . PROJECT_URL . 'core/class/' . $file . '.js?v=' . PROJECT_VERSION . '"></script>' . "\n\t";
        }
    }

    public static function classSys() {
        //obtiene la cantidad de parametros
        $length = func_num_args();

        for ($i = 0; $i < $length; $i++) {
            //obtiene el parametro
            $file = func_get_arg($i);

            echo '<script type="text/javascript" src="' . CORE_URL . '/class/' . $file . '.js?v=' . PROJECT_VERSION . '"></script>' . "\n\t";
        }
    }

    public static function classPhp($allClass = null) {
        if ($allClass == null) {
            //de esto se ocupa el spl_autoload_register
            //
            // spl_autoload_register('MyAutoloader::HelperLoader');
            // spl_autoload_register(array('Manage', 'autoload'));
        } else {
            //directorio local donde buscara las clases de PHP
            $pathClass = PROJECT_DIR . DS . 'sys' . DS . 'class' . DS;

            //obtiene la cantidad de parametros
            $length = func_num_args();

            for ($i = 0; $i < $length; $i++) {
                //obtiene el parametro
                $file = func_get_arg($i);

                //incluye el objeto
                require $pathClass . $file . '.php';
            }
        }
    }

    public static function getAllJs() {
        $class = array();

        //directorio local donde buscara las clases de JS
        $dir = PROJECT_DIR . DS . 'core' . DS . 'class' . DS . '*.js';

        foreach (glob($dir) as $file) {
            //obtiene el nombre del archivo
            $file = pathinfo($file);

            //si no esta en las clases ya incluidas
            if (!in_array($file["filename"], self::$class)) {
                $class[] = $file["basename"];
            }
        }

        echo json_encode($class);
    }

    /**
     * calcula el directorio navegando ../../
     * @param  [string] $link [ruta relativa]
     * @return [string]       [ruta abosluta]
     * */
    public static function dir($link) {
        $dir = explode("/", $link);
        $path = explode(DS, PATH_DIR);

        array_shift($path);

        //si en en ruta del directorio indica retroceder
        if (in_array("..", $dir)) {
            foreach ($dir as $value) {
                //si hay un retroceso
                if ($value == "..") {
                    array_pop($path);
                    array_shift($dir);
                }
            }
        }

        $newDir = array_merge($path, $dir);

        return DS . implode(DS, $newDir);
    }

    /**
     * reemplaza las constantes principales del sistema
     * debe tener permiso de escritura en la carpeta sys/conf
     * @param  [string] $findConst    [constante a buscar]
     * @param  [string] $valueConst   [nuevo valor]
     * @param  [string] $filePath     [ruta del archivo, por default sys/conf/system.conf]
     */
    public static function constant($findConst, $valueConst, $filePath = NULL) {
        // si no definio la ruta del archivo
        if (!$filePath) {
            //toma la ruta por defecto de las constantes del sistema
            $filePath = PATH_SYS . DS . 'conf' . DS . 'system.conf.bak';
        }

        //obtiene todas las lineas del archivo
        $lines = file($filePath);
        //para ajustar la precision y obviando los comentarios, al valor a buscar le agrega el inicio de la definicion de una constante
        $const = "define('$findConst',";
        foreach ($lines as $number => $value) {
            if (strpos($value, $const) !== FALSE) {
                //la primer coincidencia la guarda y corte la busca
                $lineConst = $number + 1;
                break;
            }
        }
        //si encontro el valor buscado
        if ($number > 1) {
            //reemplaza la linea en el archivo
            $lines[$number] = "define('" . $findConst . "','" . $valueConst . "');\n";
            file_put_contents($filePath, implode('', $lines), LOCK_EX);
        }
    }

    /**
     * recupera el archivo de configuracion por defecto en caso de un fallo general del sistema
     */
    public static function restore() {
        $fileName = "system.conf";
        $filePath = PATH_SYS . DS . "bak" . DS . "$fileName.bak";
        $fileNew = PATH_SYS . DS . "conf" . DS . "$fileName";

        if (!copy($filePath, $fileNew)) {
            echo "Error al recuperar las configuraciones por defecto $fileName...\n";
        }
    }

}

/* * *************************************************** */
/* * ********************** AJAX *********************** */
/* * *************************************************** */

//permite visualizar los resultados AJAX web solo para este Objeto
//si realizo una consulta AJAX
// La tercera condicion debe estar en false a menos que se quiera debuggear solo este objeto 
if (Security::isAjax() || DEBUG_AJAX || FALSE) {
    // verificamos la condicion de la solicitud (definida y con contenido)
    if (isset($_REQUEST["action"]) && !empty($_REQUEST["action"])) {
        //asignacion de los valores
        $action = $_REQUEST["action"];

        //determinamos la accion que se solicito ejecutar en el servidor
        switch ($action) {
            // [esta seccion deberia cambiarse acorde a las llamadas AJAX del lado del cliente]
            // case "metodoDelObjeto" : Object::method($_REQUEST["options"]); break;
            // 
            case "getAllJs": Configure::getAllJs();
                break;
        }
    }
}
?>