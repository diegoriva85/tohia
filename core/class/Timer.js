

//declara variable para usar en flipclock.js
var clock;
// tiempo para calcular % de barra de progreso
var timeGlobal = '';
// tiempo restante para contador regresivo
var timeRemain = '';
//flag para mostrar el modal solo 1 vez
var showModalflag = 0 ;

// definicion del objeto
var Timer = {
    
    // instancia el reloj y la barra de progreso
    init: function () {
        
        // bootstrap-progressbar.js
        $('.ddjj-progress .ddjj-progress-bar').progressbar(
        {
            display_text: 'center',
            use_percentage: false,
            refresh_speed: 500
        });

        //instancia del contador
        clock = $('.clock').FlipClock(
        {
            clockFace: 'HourlyCounter',
            autoStart: false,
            callbacks:
            {
                stop: function()
                {
                    //$('.message').html('The clock has stopped!')
                    console.log('The clock has stopped!');
                },
                interval: function()
                {

                    var time = this.factory.getTime().time;

                    if(time)
                    {
                        //console.log('time          '+time);
                        //console.log('timeGlobal    '+timeGlobal);
                        var percentRemain = Math.round(time * 100 / timeGlobal);
                        //console.log('percentRemain '+percentRemain);
                        var percentRemainWith = percentRemain + '%';

                        // objeto progress-bar
                        var progressBar = $('.ddjj-progress-bar');

                        // cambia el % de la barra de progreso
                        progressBar.css("width", percentRemainWith );

                        // setea el color de la barra de progreso
                        switch(true)
                        {
                            //verde rgb(110, 170, 46)
                            case ((percentRemain >= 50 && percentRemain <= 100) && (progressBar.css('background-color') != 'rgb(29, 148, 144)')): progressBar.css('background-color', '#1d9490'); break;
                            //amarillo rgb(110, 170, 46)
                            case ((percentRemain >= 20 && percentRemain < 50) && (progressBar.css('background-color') != 'rgb(255, 176, 0)')): progressBar.css('background-color', '#FFB000'); break;
                            //rojo rgb(255, 176, 0)
                            case ((percentRemain >= 0 && percentRemain < 20) && (progressBar.css('background-color') != 'rgb(186, 39, 39)')): progressBar.css('background-color', '#BA2727'); break;
                        }
                        // si el tiempo restante es menor a 10 minutos (600seg) y el modal no se ha mostrado
                        if(time <= 300 && showModalflag == 0){
                            Timer.showModal('extend');
                            showModalflag = 1;
                        }

                    }else{

                        //destruye la session
                        DDJJ.destroySession();

                        // cambia el % de la barra de prograso a 0
                        var percentRemainWith = 0 + '%';
                        $('.ddjj-progress-bar').css("width", percentRemainWith);

                        window.location.href = 'index.php?logOut=TRUE';
                    }
                }
            }
        });

    },
    // inicia el contador
    start: function() {
        
        // obtiene el tiempo total de session
        $.when(DDJJ.requestSessionTimeOut('async')).done(function(data)
        {
            // setea el tiempo devuelto por php
            timeGlobal = data;
            // obtiene el tiempo restante de session
            $.when(DDJJ.requestSessionTimeRemain('async')).done(function(data)
            {
                timeRemain = data;
                Timer.initClock(timeRemain);
            });
        });

    },
    // reinicia el contador
    reStart: function() {
        
        // detiene el contador y lo relanza con el tiempo extendido
        clock.stop();
        
        // setea el tiempo total de session(se ejecuta despues de la creacion
        //  del objeto Affidavit en sesion de manera que setee el tiempo correspondiente)
        $.when(DDJJ.requestSetLifeTime('async')).done(function(data)
        {
            Timer.start();
        });

    },
    // extiende el tiempo del contador (definido en la constante SESSION_TIME_EXTEND)
    extend: function(){
        
        // detiene el contador y lo relanza con el tiempo extendido
        clock.stop();
        showModalflag = 0;
        // ejecuta la funcion con el parametro async(false) para hacerla sincronica
        $.when(DDJJ.requestExtendSessionTimeOut('async')).done(function(data)
        {
            Timer.start();
        });
        
    },
    // funcion para inicializar el plugin FlipClock (clock)
    initClock: function (time) {

        if(time > 0){ // comprueba que el tiempo devuelto por php no es 0

            //indica que el conteo sera en retroceso
            clock.setCountdown(true);

            //setea el tiempo de conteo
            clock.setTime(time); //repuesta de php 1440 segundos

            //inicializa el contador
            clock.start();
        } 
    },
    // muestra el modal
    showModal: function (type) {
        
        if(Timer.editMode()){

            //$("#modal-progress-bar").modal("hide");
            $('.ddjj-container-export-button button').attr('data-dismiss', 'modal');

            if(type === 'extend'){
                $('#modal-progress-bar .modal-header .modal-title').html('Atención! la sesión esta por terminar');
                $('#modal-progress-bar .modal-body p').html('El tiempo de la sesión esta por terminar, exporte el formulario o extienda el tiempo por 10 minutos');
                $("#modal-progress-bar").modal("show");

            }else if(type === 'finish'){
                $('#modal-progress-bar .modal-header .modal-title').html('Fin de la session');
                $('#modal-progress-bar .modal-body p').html('Se agotó el tiempo de la sesión');
                $('.ddjj-extend-session, ddjj-container-export-button button').addClass('collapse');
                $("#modal-progress-bar").modal("show");
            }

            // setea el focus para el boton cerrar del modal
            $('#modal-progress-bar').on('shown.bs.modal', function ()
            {
                $('.close').focus()
            })
        }
    },
    // detiene el timer
    stop: function () {

        clock.stop();

    },
    // consulta si el timer esta corriendo
    isRunning: function () {
        
        var result = false; //define el resultado a retornar por defecto
        var timeRemain;
        // ejecuta la funcion con el parametro async(false) para hacerla sincronica
        $.when(DDJJ.requestSessionTimeRemain('async')).done(function(data)
        { 
            timeRemain = data;
            // si timeRemain es mayor a 1 el timer esta corriendo
            if(timeRemain > 1){
                result = true;
            }
           
        });
        
        return result;
        
    },
    editMode: function () {
        
        var result = false; //define variable a retornar
        
        $.when(DDJJ.requestSessionMode('async')).done(function(data)
        {
            if(data === 0){
                result = false;
            }else if((data === 1)){
                result = true;
            }
            
        });
        
        return result;
    },
    // oculta el reloj y la barra de progreso
    hide: function() {
        $('.clock, .ddjj-progress-bar').addClass('collapse');
        //$('.clock').addClass('collapse');
    }

};
