<?php
/**
 *
 */
class Security
{
	
	function __construct()
	{
		# code...
	}

    /**
     * seguriza la carga de datos impidiendo xss
     * @param  string $value valor cargado en el input 
     * @return string retorna el valor segurizado         
     */
    public static function xss($value)
    {
        //elimina los espacios en blanco
        $value = trim($value);

        //reemplazo de caracteres desahabilitados
        $value = str_replace(";", "", $value); 
        $value = str_replace("'", "", $value); 
        $value = str_replace('"', "", $value);

        //seguriza el contenido quitando caracteres especiales
        $value = htmlentities($value);
        
        return $value;        
    }
 
    /**
     * seguriza la carga de los datos impidiendo xss
     * @param  array $value valor cargado en el input 
     * @return array retorna el valor segurizado         
     */
    public static function input($value)
    {
        if (is_array($value))
        {
            $inputs = array(); //guarda los valores segurizados

            foreach ($value as $key => $input)
            {
                //seguriza el campo
                $inputs[$key] = self::xss($input);
            }
        }
        else
        {
            $inputs = self::xss($value);            
        }

        return $inputs;
    }

    /**
     * verifica si la solicitud es una consulta ajax
     * @return boolean [true] si la es una peticion es ajax
     */
    public static function isAjax()
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }


    /**
     * [compara si la clave pasada por parametro corresponde a la predefinida por el sistema, sino se pasa parametro solo el hash]
     * @param  [string] $pass [clave a comprar]
     * @return [boolean]       [si se paso un parametro retorna, y la compracion fue exitosa retorna true, sin parametros el hash]
     */
    public static function salt($pass)
    {
        $salt = crypt('seclyt', '$3cLyT');

        $lenght = func_num_args();
  
        if($lenght == 1)
        {
            return ( hash_equals( $pass, $salt ) );
        }
        else
        {
            return $salt;
        }
    }

    /**
     * dependiendo la peticion AJAX o PHP
     * @param  [array] $data [datos a procesar]
     * @return [mixed]       [si es AJAX: retorna JSON,
     *                       si es PHP: retorna el valor, si son varios valores, se debe indicar el segundo parametro en TRUE, para que devuelva un array asociativo]
     */
    public static function request($data, $array = false)
    {
        if (self::isAjax())
        {
            echo json_encode($data);
        }
        else
        {
            
            if(is_array($data))
            {
                return (!$array) ? $data[key($data)] : $data;
            }
            else
            {
                return $data;
            }
        }
    }
}
?>