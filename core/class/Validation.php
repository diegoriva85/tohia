<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Validation
 *
 * clase generica de validacón del Back 
 * retorna Falso si no se pasan las validaciones
 * 
 * @version 1.3
 * @author nzambrano
 */
class Validation {
        
    protected $_atributos;
    protected $_error;
    protected $labels;
    public $mensaje;

    public function setLabels(array $labels)
    {
        $this->labels = $labels;
    }

    public function getLabels()
    {
        return $this->labels;
    }

    public function getLabel($campo)
    {
        $labels = $this->getLabels();

        if (!is_null($labels) && isset($labels[$campo]))
        {
            return $labels[$campo];
        } else
        {
            return $campo;
        }
    }

    public function rules($rule = array(), $data)
    {
        $success = false;

        if (!is_array($rule))
        {
            $this->mensaje = "las reglas deben de estar en formato de arreglo";
            return $this;
        }
        foreach ($rule as $key => $rules)
        {
            $reglas = explode(',', $rules['regla']);
            if (array_key_exists($rules['name'], $data))
            {
                foreach ($data as $indice => $valor)
                {
                    if ($indice === $rules['name'])
                    {
                        foreach ($reglas as $clave => $valores)
                        {
                            $validator = $this->getInflectedName($valores);
                            if (!is_callable(array($this, $validator)))
                            {
                                throw new BadMethodCallException("No se encontro el metodo actual");
                            }
                            $respuesta = $this->$validator($rules['name'], $valor, isset($rules['model']) ? $rules['model'] : null );

                            if ($respuesta == true)
                                $success = true;
                        }
                        break;
                    }
                }
            }
            else
            if (isset($data))
            {
                $name_input = $rules['name'];

                if ($rules["regla"] == "no-empty")
                {
                    $success = $this->_noEmpty($rules['name'], "");
                }

                //$this->mensaje[$rules['name']] = " $name_input error  datos para validar vacios";
            } else
            {
                $this->mensaje[$rules['name']][] = "el campo $value no esta dentro de la regla de validación o en el formulario";
            }
        }
        return $success;
    }

    /**
     * Metodo inflector de la clase 
     * por medio de este metodo llamamos a las reglas de validacion que se generen
     */
    private function getInflectedName($text)
    {
        $validator = "";
        $_validator = preg_replace('/[^A-Za-z0-9]+/', ' ', $text);
        $arrayValidator = explode(' ', $_validator);
        if (count($arrayValidator) > 1)
        {
            foreach ($arrayValidator as $key => $value)
            {
                if ($key == 0)
                {
                    $validator .=  $value;
                } else
                {
                    $validator .= ucwords($value);
                }
            }
        } else
        {
            $validator = $_validator;
        }
        return $validator;
    }

    /**
     * Comprueba que el valor enviado no sea vacio
     * @return [true o false]
     */
    public function noEmpty($value)
    {
        if (isset($value) && !empty($value))
        {
            return TRUE;
        }
        return FALSE;
        
    }

    /**
     * Metodo de verificacion de tipo email
     * @return [true o false]
     */
    public function alfabetic($value)
    {
        if (preg_match("/^[á-úÁ-Úa-zA-Z\ñ\Ñ\\\_\.\,\-\s\/]*$/",$value)) {
            return TRUE;        
        }
        
        return FALSE;
    }
    
    /**
     * Metodo de verificacion de tipo alfanumerico
     * @return [true o false]
     */
    public function alfanumeric($value)
    {
        if (preg_match("/^[á-úÁ-Úa-zA-Z0-9\ñ\Ñ\\\_\.\,\-\s\/]*$/",$value)) {
            return TRUE;        
        }
        
        return FALSE;
    }
    
    /**
     * Metodo de verificacion de tipo alfanumerico
     * @return [true o false]
     */
    public function percentage($value)
    {

        if (preg_match("/^(?:100|\d{1,2})$/",$value)) {
            return TRUE;        
        }
        //"/[0-9]+%/"

        return FALSE;
    }
    
     /**
     * Metodo de verificacion de tipo numerico
     * @return [true o false]
     */
    public function numeric($value)
    {
        // ejemplo de opciones para campo de tipo number
        /*
        $options = array(
            'options' => array(
                'min_range' => 0,
                'max_range' => 3,
                )
        );*/
        //is_numeric($value)
        
        //if (filter_var($value, FILTER_VALIDATE_INT)) {
        if(preg_match("/^[0-9]*$/", $value)) {
            return TRUE;
        }
        return FALSE;
    }
    
    /**
     * Metodo de verificacion de tipo email
     * @return [true o false]
     */
    public function mail($value)
    {
        if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
            return TRUE;
        }
        return FALSE;
    }
    
    /**
     * Metodo de verificacion de tipo date en formato DD-MM-YYYY
     * @return [true o false]
     */
    public function date($value)
    {
        $date = self::validateDate($value);
        /*
        $value = date("Y-m-d", strtotime($value));
        $fecha = time() - strtotime($value);
        $age = floor((($fecha / 3600) / 24) / 360);
        */
        if ($date) {
            return TRUE;
        }
        return FALSE;
    }
    
    /**
     * Metodo de verificacion de tipo year en formato YYYY
     * @return [true o false]
     */
    public function year($value)
    {
        $date = self::validateDate($value,'Y');

        if ($date) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Metodo de verificacion de tipo email
     * El metodo retorna un valor verdadero si la validacion es correcta de lo contrario retorna un valor falso
     * y llena el atributo validacion::$mensaje con un arreglo indicando el campo que mostrara el mensaje y el 
     * mensaje que visualizara el usuario
     */
    protected function email($campo, $valor)
    {
        $label = $this->getLabel($campo);

        if (preg_match("/^[a-z]+([\.]?[a-z0-9_-]+)*@[a-z]+([\.-]+[a-z0-9]+)*\.[a-z]{2,}$/", $valor))
        {
            // $this->mensaje[$campo][] = "";
            return false;
        } else
        {
            $this->mensaje[$campo][] = "el campo $label de estar en el formato de email usuario@servidor.com";
            return true;
        }
    }

    private function unique($campo, $valor, $model)
    {
        $label = $this->getLabel($campo);

        if (isset($valor) && !empty($valor) && isset($model) && !empty($model))
        {
            $sql = "SELECT $campo FROM $model where $campo = $valor";
            $result = Database::result($sql);

            if (count($result) == 0)
            {                
                return false;
            } else
            {
                $this->mensaje[$campo][] = "$label con valor $valor ya se encuentra registrado";
                return true;
            }
        }
    }
    
    /**
     * Metodo de verificacion por tipo campos y que no esten vacios, acepta los siguientes tipos: string, number, email, date, year
     * @return [true o false]
     */
    public static function field($type, $value, $strict = FALSE){
        // define array con resultado a enviar con los valores por defecto
        $rtn = [
            "validated" => true,
            "error" => [
                "code" => 0,
                "message" => ""
            ]
        ];
        
        $validated = $strict == TRUE ? FALSE : TRUE; 
        
        if(!empty($value)){
            switch ($type) {
                case 'string':
                    if (!preg_match("/^[á-úÁ-Úa-zA-Z\ñ\Ñ\\\_\.\-\s\/]*$/",$value)) {
                        $rtn['validated'] = $validated;
                        $rtn['error']['code'] = 1;
                        $rtn['error']['message'] = "Solo esta permitido letras y espacios";
                    }
                    break;
                case 'number':
                    // ejemplo de opciones para campo de tipo number
                    /*
                    $options = array(
                        'options' => array(
                            'min_range' => 0,
                            'max_range' => 3,
                            )
                    );*/
                    if (!filter_var($value, FILTER_VALIDATE_INT)) {
                        $rtn['validated'] = $validated;
                        $rtn['error']['code'] = 2;
                        $rtn['error']['message'] = "Solo esta permitido numeros";
                    }
                    break;
                case 'email':
                    if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                        $rtn['validated'] = $validated;
                        $rtn['error']['code'] = 3;
                        $rtn['error']['message'] = "El correo no es valido";
                    }
                    break;
                case 'date':
                    $date = self::validateDate($value);
                    /*
                    $value = date("Y-m-d", strtotime($value));
                    $fecha = time() - strtotime($value);
                    $age = floor((($fecha / 3600) / 24) / 360);
                    */
                    if (!$date) {
                        $rtn['validated'] = $validated;
                        $rtn['error']['code'] = 4;
                        $rtn['error']['message'] = "la fecha o el formato(dd/mm/aaaa) es incorrecto";

                    }
                    break;
                case 'year':
                    $value = '2011';
                    $date = self::validateDate($value,'Y');

                    if (!$date) {
                        $rtn['validated'] = $validated;
                        $rtn['error']['code'] = 5;
                        $rtn['error']['message'] = "el año o el formato(aaaa) es incorrecto";
                    }
                    break;
            }
        }else{
            $rtn['validated'] = $validated;
            $rtn['error']['code'] = 6;
            $rtn['error']['message'] = "el campo esta vacio";            
        }
        
        return $rtn;
    }
    
    // valida el formato de la fecha (incluido año biciesto)
    public static function validateDate($date, $format = 'd-m-Y')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
    
    // elimina espacios en blanco al principio de la cadena
    public static function trimValue(&$value) 
    { 
        $value = trim($value); 
    }    


}