Security =
{
	url: window.urlPrimaryPath +'/core/class/Security.php',//[cambiame] url para obtener datos de la base (tener en cuenta que debe setearse de donde se llama el archivo)

	/**
	 * [chequea si se debe mostrar los resultados AJAX por consola]
	 * @return {[boolean]} [true si debe mostrar resultados]
	 */
	isAjax: function()
	{
		//si setearon la constante de DEBUG_AJAX del lado del servidor
		DEBUG_AJAX = typeof DEBUG_AJAX !== 'undefined' ? DEBUG_AJAX : false;

		return DEBUG_AJAX;
	},

	/**
	 * [lee una funcion del lado del servidor de acuerdo a los parametros]
	 * @param  {[array]} options [lista de opciones]
	 * @return {[void]}         [no retorna nada]
	 * para que pueda retornar un valor el metodo invocado en la seccion de ajax debe tener seteado la opcion async:false
	 */
	load: function(options)
	{
		/**
		 * [datos que seran procesados del lado del servidor]
		 * @type {[json]}
		 * "action:" accion a ejecutarse del lado de servidor
		 * las opciones que se esperan son del tipo...
		 * "options": [{'data1':'algo'},{'data2': 'algo2'}]
		 */
		
		var result = '';//resultado de la consulta

		var data =
		{
			"action": "load",
			"options": options
		};

		var that = this;

		// datos a enviar
		if (this.isAjax()){console.log("AJAX:SEND  =>",data);}


		data = $(this).serialize() + "&" + $.param(data);

		// datos serializados para url
		if (this.isAjax()){console.log("AJAX:URL  =>",data);}


		$.ajax(
		{
			type: "post",
			dataType: "json",
			url: this.url,
			data: data,
			success: function(data)
			{
				// datos recibidos
				if (that.isAjax()){console.log("AJAX:GET  =>",data);}

				// indicar lo que se quiere procesar con los datos
				result = data;//se asigna a la variable result el valor procesado en el back
			},
			error: function()
			{
				// mensaje
				var msg = 'no se pudo procesar';
				//datos no procesados por error
				if (that.isAjax()){console.log("AJAX:GET  =>", msg);}
			}
		});

		return result;//valor a retornar
	},

}