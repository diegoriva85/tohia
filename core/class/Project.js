Project =
{
	url: Configure.proyectFolder +'/core/class/Project.php',//[cambiame] url para obtener datos de la base (tener en cuenta que debe setearse de donde se llama el archivo)
	/**
	 * [mostrar AJAX en la consola solo para este Objeto]
	 * @return {[type]} [description]
	 */
	isAjax: function()
	{
		//poner en true si se desea debuggear este objeto en particular 
		var view = false;
		//chequea la condicion general para ver Ajax hasta habilitado para todos los ojbetos o para el objeto en cuestion (this)
		return (Security.isAjax() || view);
	},
	/**
	 * [constant description]
	 * @return {[type]} [description]
	 */
	constant: function()
	{
		// Guardamos el nombre del projecto para reconstruir en el back la dire del conf
		var project = document.location.pathname.substring(1).split(/\//)[0];

		var data =
		{
			"action": "constant",
			"project": project
		};

		var that = this;

		// datos a enviar
		if (this.isAjax()){console.log("AJAX:SEND  =>",data);}


		data = $(this).serialize() + "&" + $.param(data);

		// datos serializados para url
		if (this.isAjax()){console.log("AJAX:URL  =>",data);}

		$.ajax(
		{
			type: "post",
			dataType: "json",
			url: this.url,
			data: data,
			success: function(data)
			{
				// datos recibidos
				if (that.isAjax()){console.log("AJAX:GET  =>",data);}

				// indicar lo que se quiere procesar con los datos
			},
			error: function()
			{
				// mensaje
				var msg = 'no se pudo procesar';
				//datos no procesados por error
				if (that.isAjax()){console.log("AJAX:GET  =>", msg);}
			}
		});
	},

	/**
	 * [get description]
	 * @param  {[type]} atribute [description]
	 * @return {[type]}          [description]
	 */
	get: function(atribute)
	{		
		// Guardamos el nombre del projecto para reconstruir en el back la dire del conf
		var project = document.location.pathname.substring(1).split(/\//)[0];

		var data =
		{
			"action": "get",
			"atribute": atribute,
			"project": project
		};
		
		var result = '';

		var that = this;

		// datos a enviar
		if (this.isAjax()){console.log("AJAX:SEND  =>",data);}


		data = $(this).serialize() + "&" + $.param(data);

		// datos serializados para url
		if (this.isAjax()){console.log("AJAX:URL  =>",data);}


		$.ajax(
		{
			type: "post",
			dataType: "json",
			url: this.url,
		    async: false,
		    cache: false,
			data: data,
			success: function(data)
			{
				// datos recibidos
				if (that.isAjax()){console.log("AJAX:GET  =>",data);}

				// indicar lo que se quiere procesar con los datos
				result = data.value;

			},
			error: function()
			{
				// mensaje
				var msg = 'no se pudo procesar';
				//datos no procesados por error
				if (that.isAjax()){console.log("AJAX:GET  =>", msg);}
			}
		});

		return result;

	},
}