var DDJJ = {
    URL: Configure.proyectFolder +'/core/class/DDJJ.php',
    destroySession: function () {
        $.ajax(
            {
                url: DDJJ.URL,
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'destroySession'
                },
                success: function (data) {
                    $(DDJJ).trigger('sessionDestroy');
                },
                error: function () {
                    $(DDJJ).trigger('sessionDestroy', -1);
                }
            }
        );
    },
    requestSessionTimeOut: function (async) {
        var result = '0'; //declara variable a retornar
        $.ajax(
            {
                url: DDJJ.URL,
                type: 'POST',
                dataType: 'json',
                async: async ? false : true,
                data: {
                    action: 'getSessionTimeOut'
                },
                success: function (data) {
                    $(DDJJ).trigger('sessionTimeOutRequested', data);
                    result = data;
                },
                error: function (a, b) {
                    $(DDJJ).trigger('sessionTimeOutRequested', null);
                }
            }
        );
        return result;
    },
    requestSetLifeTime: function (async) {
        var result = '0'; //declara variable a retornar
        $.ajax(
            {
                url: DDJJ.URL,
                type: 'POST',
                dataType: 'json',
                async: async ? false : true,
                data: {
                    action: 'setLifeTime'
                },
                success: function (data) {
                    $(DDJJ).trigger('setLifeTimeRequested', data);
                    result = data;
                },
                error: function (a, b) {
                    $(DDJJ).trigger('setLifeTimeRequested', null);
                }
            }
        );
        return result;
    },
    requestSessionTimeRemain: function (async) {
        var result = '0'; //declara variable a retornar
        $.ajax(
            {
                url: DDJJ.URL,
                type: 'POST',
                dataType: 'json',
                async: async ? false : true,
                data: {
                    action: 'getSessionTimeRemain'
                },
                success: function (data) {
                    $(DDJJ).trigger('sessionTimeRemainRequested', data);
                    if(data > 0){
                        result = data;
                    }else{
                        window.location.href = 'index.php?logOut=TRUE';
                    }
                },
                error: function (a, b) {
                    $(DDJJ).trigger('sessionTimeRemainRequested', null);
                }
            }
        );
        return result;
    },
    requestExtendSessionTimeOut: function (async) {
        var result = '0'; //declara variable a retornar
        $.ajax(
            {
                url: DDJJ.URL,
                type: 'POST',
                dataType: 'json',
                async: async ? false : true,
                data: {
                    action: 'extendSessionTimeOut'
                },
                success: function (data) {
                    $(DDJJ).trigger('extendSessionTimeOutRequested', data);
                    result = data;
                },
                error: function (a, b) {
                    $(DDJJ).trigger('extendSessionTimeOutRequested', null);
                }
            }
        );
        return result;
    },
    requestSessionMode: function (async) {
        var result = '0'; //declara variable a retornar
        $.ajax(
            {
                url: DDJJ.URL,
                type: 'POST',
                dataType: 'json',
                async: async ? false : true,
                data: {
                    action: 'getSessionMode'
                },
                success: function (data) {
                    result = data;
                },
                error: function (a, b) {
                    result = 0;
                }
            }
        );
        return result;
    },
    create: function (type) {
        $.ajax(
            {
                url: DDJJ.URL,
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'create',
                    type: type
                },
                success: function (data) {
                    $(DDJJ).trigger('onCreate', data);
                },
                error: function (obj, err) {
                    $(DDJJ).trigger('onCreate', {error: {code: -1, message: err}});
                }
            }
        );
    },
    cloneTemplate: function (section,type,id,name) {
        $.ajax(
            {
                url: DDJJ.URL,
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'cloneTemplate',
                    section: section,
                    type: type,
                    id: id,
                    name: name
                },
                success: function (data) {
                    $(DDJJ).trigger('oncloneTemplate', data);
                },
                error: function (obj, err) {
                    $(DDJJ).trigger('oncloneTemplate', {error: {code: -1, message: err}});
                }
            }
        );
    },
    removeFields: function (sections,id) {
        $.ajax(
            {
                url: DDJJ.URL,
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'removeFields',
                    sections: sections,
                    id: id
                },
                success: function (data) {
                    $(DDJJ).trigger('onremoveFields', data);
                },
                error: function (obj, err) {
                    $(DDJJ).trigger('onremoveFields', {error: {code: -1, message: err}});
                }
            }
        );
    },
    addFields: function (sections,id) {
        $.ajax(
            {
                url: DDJJ.URL,
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'addFields',
                    sections: sections,
                    id: id
                },
                success: function (data) {
                    $(DDJJ).trigger('onaddFields', data);
                },
                error: function (obj, err) {
                    $(DDJJ).trigger('onaddFields', {error: {code: -1, message: err}});
                }
            }
        );
    },
    removeTemplate: function (section) {
        var sections = new Array();
        var tabClassSection = $(section).parents('.section');
        for (i = 0; i < tabClassSection.length; i++) {
             var elemet = $(tabClassSection[i]).attr('data-id');
             sections.push(elemet);
        }
        sections = sections.reverse();
        $.ajax(
            {
                url: DDJJ.URL,
                type: 'POST',
                dataType: 'json',
                async: false,
                data: {
                    action: 'removeTemplate',
                    sections: sections,
                    id: $(section).data('id')
                },
                success: function (data) {
                    $(DDJJ).trigger('onremoveTemplate', data);
                },
                error: function (obj, err) {
                    $(DDJJ).trigger('onremoveTemplate', {error: {code: -1, message: err}});
                }
            }
        );
    },
    save: function (section, fields) {
        $.ajax(
            {
                url: DDJJ.URL,
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'save',
                    section: section,
                    fields: fields
                },
                success: function (data) {
                    $(DDJJ).trigger('onSave', data);
                    /*if (data.error.code != 0) {
                        $('.'+section[1].id+' .'+fields[0].id).val('').change();
                    }*/
                },
                error: function (obj, err) {
                    $(DDJJ).trigger('onSave', {error: {code: -1, message: err}});
                }
            }
        );
    },
    commit: function () {
        $.ajax(
            {
                url: DDJJ.URL,
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'commit'
                },
                success: function (data) {
                    $(DDJJ).trigger('onCommit', data);
                },
                error: function (obj, err) {
                    $(DDJJ).trigger('onCommit', {error: {code: -1, message: err}});
                }
            }
        );
    },
    PDFExportRequest: function () {
        $.ajax(
            {
                url: DDJJ.URL,
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'PDFExportRequest'
                },
                success: function (data) {
                    $(DDJJ).trigger('onPDFExportRequest', data);
                },
                error: function (obj, err) {
                    $(DDJJ).trigger('onPDFExportRequest', {error: {code: -1, message: err}});
                }
            }
        );
    }
};