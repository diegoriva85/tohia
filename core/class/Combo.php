<?php
class Combo {

    private static $position;
    private static $jurisdiction;
    private static $ministry;
    private static $generalDirection;

    public static function load() {

        $query = "select name,inactive from position where deleted = 0 order by id;";


        self::setComboPosition(Database::result($query));

        $query = "select name,inactive from jurisdiction where deleted = 0 order by id;";


        self::setComboJurisdiction(Database::result($query));

        $query = "select name,inactive from  ministry where deleted = 0 order by id;";


        self::setComboMinistry(Database::result($query));
        
        $query = "select name,inactive from  general_direction where deleted = 0 order by id;";


        self::setGeneralDirection(Database::result($query));
        
        
    }

    private static function setComboPosition($ComboPosition) {
        self::$position = $ComboPosition;
    }

    public function getComboPosition() {

        return self::$position;
    }

    private static function setComboJurisdiction($ComboJurisdiction) {
        self::$jurisdiction = $ComboJurisdiction;
    }

    public function getComboJurisdiction() {

        return self::$jurisdiction;
    }
    
    private static function setComboMinistry($ComboMinistry) {
        self::$ministry = $ComboMinistry;
    }

    public function getComboMinistry() {

        return self::$ministry;
    }
    
      private static function setGeneralDirection($ComboMinistry) {
        self::$generalDirection = $ComboMinistry;
    }

    public function getComboGeneralDirection() {

        return self::$generalDirection;
    }

}

?>