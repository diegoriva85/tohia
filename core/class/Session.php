<?php

/**
 * 
 */
class Session
{
    
    function __construct()
    {
        # code...
    }

    /**
     * [configuraciones init_set() llamar antes de ejecutar session_start]
     * @return [none]
     *  opciones configurables en tiempo de ejecucion : http://php.net/manual/es/session.configuration.php#ini.session.use-only-cookies
     */
    public static function init()
    {  
        /**
         *   1. configuraciones en php.ini de tiempo de vida de archivos de sesion a partir de la hora de modificacion (atime)
         *   de cada archivo (timeout por inactividad) o (purga de archivos expirados)
         *      
         *      setea el directorio donde se guardan los archivos en el servidor:
         *          session.save_path = "/var/lib/php5"
         *      setea el tiempo de vida de cada archivo de sesion:
         *          session.gc_maxlifetime = 1440
         *   
         *   segun el Sistema Operativo, el handler encargado de eliminar los archivos de sesion expirados puede ser
         *   el garbage collector (GC) de php o una tarea CRON del SO (debian y derivados)
         * 
         *   sea cual fuera el handler y la frecuencia con la que se ejecuta, la accion es siempre la misma:
         *   [
         *       find "session.save_path" -type f -cmin "session.gc_maxlifetime" -delete
         *   ]
         * 
         *      a.  SO no debian:
         *          handler: GC de php ejecutado por el usuario (apache) www-data,
         *          la frecuencia de ejecucion es determinada por la siguiente configuracion
         * 
         *              session.gc_probability = 100
         *              session.gc_divisor = 100
         * 
         *              como se explica en el archivo php.ini:
         *                  [
         *                      Defines the probability that the 'garbage collection' process is started
         *                      on every session initialization. The probability is calculated by using
         *                      gc_probability/gc_divisor.
         *                  ]
         * 
         *          con esta configuracion el GC se ejecuta sobre el directorio "/var/lib/php5" un 100% de las veces 
         *          que se llama a session_start() por cualquier script php ejecutado por cualquier usuario
         *          (por lo que obviamente generaria una sobrecarga del servidor)
         * 
         *          la configuracion de php.ini puede sobreescribirse con ini_set("session.gc_maxlifetime", 1800)si se setea
         *          antes de la llamada a session_start() el nuevo valor (1800 secs) va a ser tenido en cuenta como el tiempo
         *          de vida del archivo de sesion --solo para el archivo correspondiente al id de session con el que fue ejecutado
         *          y siempre que no se lo haya llamado posteriormente sin haber seteado el mismo valor con ini_set()--
         *          la misma logica se aplica para ini_set('session.save_path', '/path')
         * 
         * 
         *       b.  SO debian:
         *           handler: tarea CRON "/etc/cron.d/php5" es ejecutada por el usuario root a los 9 y 39 minutos de cada hora
         * 
         *              session.gc_probability = 0    //<-- en 0 por defecto para que nunca se ejecute el GC de php
         *              session.gc_divisor = 100
         *              
         *              si se modifica el valor por defecto de session.save_path es necesario activar el GC de php
         *                  [
         *                      session.save_path = "/my/own/path"
         *                      session.gc_probability = 1 // 1% chance the gc will run on any give request(session initialization)
         *                  ]
         * 
         *              de la misma manera si se quiere usar el GC de php (y desactivar el CRON "php5") se debe modificar el 
         *              directorio de sesiones por uno con permisos de lectura/escritura para el usuario www-data, de otra manera
         *              cuando se ejecute el GC dara el siguiente error/Notice:
         *                  [
         *                      ps_files_cleanup_dir: opendir(/var/lib/php5) failed: Permission denied
         *                  ]
         *                  [        
         *                      en debian los permisos del directorio de sesiones "/var/lib/php5" son 1733 o drwx-wx-wt root/root, 
         *                      el usuario apache puede escribir en los archivos del directorio pero no puede leerlos 
         *                      ( ref http://serverfault.com/a/511705 )
         *                  ]
         * 
         *          en el caso de la tarea CRON el valor de "session.gc_maxlifetime" solo se toma en cuenta si se modifica en 
         *          php.ini y el valor es superior a 1440 por otra parte la ubicacion del directorio de archivos de sesion esta
         *          definida en el propio CRON "php5", por lo que no toma el valor de session.save_path de php.ini 
         *          -- es decir que si se modifica el valor por defecto (/var/lib/php5) los archivos se guardaran en el nuevo
         *          directorio pero el cron seguira corriendo sobre el directorio original, de manera que no se hara la purga 
         *          de archivos de sesion en la nueva ubicacion a menos que se habilite el GC --
         *
         *   2.tiempo de session en cliente(session.cookie_lifetime)
         * 
         */
        
        /* seteos tomados en cuenta solo por garbage collector de php */
        ini_set("session.gc_maxlifetime", SESSION_TIME_DDJJ);// 3hs
        ini_set('session.save_path', PROJECT_DIR . '/sys/tmp/session' );
        
        
        
        ini_set("session.cookie_lifetime", SESSION_TIME_DDJJ);
        //session_set_cookie_params(SESSION_TIME_DDJJ);// 3hs
        
        //da los permismos necesarios para escribir
        //$pathFile = $path.'/sess_'.session_id();
        //chmod($pathFile, 0777);

        ini_set("display_errors", 1);

    }

    /**
     * [si el tiempo de sesion no finalizo inicia la session]
     * @return [boolean] [TRUE]
     */
    public static function start()
    {
        //si la sesion no esta iniciada
        if(!self::status())
        {
            // setea los parametros necesarios antes de la llamada a session_start
            self::init();
            
            // abre la comunicacion con la session actual
            // (una vez por cada REQUEST para tener acceso a $_SESSION)
            session_start();

        }
        
        return TRUE;
    }
    
    
    /**
     * [si el tiempo de sesion no finalizo inicia la session]
     * @return [boolean] [TRUE]
     */
    public static function retrieve()
    {
        //si la sesion no esta iniciada
        if(!self::status())
        {
            // setea los parametros necesarios antes de la llamada a session_start
            self::init();
            
            // abre la comunicacion con la session actual
            // (una vez por cada REQUEST para tener acceso a $_SESSION)
            session_start();
            
            // maneja la session cuando el usuario esta logueado pero no tiene una ddjj en edicion
            if(self::isUserLogged() && !self::isDDJJCreated()){

                $now = time();
                // comprueba si esta seteado el tiempo maximo de session (session normal)
                if(isset($_SESSION['discardAfter'])) {
                    // comprueba si el tiempo de la session normal expiro
                    if($now > $_SESSION['discardAfter']){
                        // elimina la session
                        session_unset();
                        session_destroy();
                        session_start();
                    }
                // si no, setea el tiempo maximo de session (session normal)
                }else{
                    $_SESSION['discardAfter'] = $now + SESSION_TIME; //30 min
                }
                
            }else if(!self::isUserLogged()){ 
                throw new Exception('La session expiro');
            }
        }
        
        return TRUE;
    }
    
    /**
     * comprueba si la sesion esta activa 
     * @return [boolean] [estado de la sesion]
     */
    public static function status()
    {
        $status = (session_status() === PHP_SESSION_ACTIVE ? TRUE : FALSE);

        return $status;

    }
    
    /**
     * [destruye la sesion ]
     * @return [none]
     */
    public static function destroy()
    {
        self::retrieve();        
        
        session_unset(); 
        session_destroy();        
    }
    
    /**
     * [comprueba si se agoto el tiempo de session]
     * @return [boolean] [si resta tiempo de session]
     */
    public static function checkTime()
    {
        self::retrieve();
        
        if(isset($_SESSION['created']) && isset($_SESSION['timeOut'])){
            
            $timeLapsed = time() - $_SESSION['created'];
        
            // si tiempo de la session es mayor a tiempo transcurrido 
            if($_SESSION['timeOut'] > $timeLapsed){
                return true;
            }
        }
        return false;
    }
    
    /**
     * [obtiene el tiempo restante de la session]
     * @return [el valor del tiempo en segundos]
     */
    public static function getTimeOut()
    {
        self::retrieve();

        if(!empty($_SESSION['sessionId'])){
            
            // si la session tiene seteado timeout
            if(!empty($_SESSION['timeOut'])){
                
                $timeOut = $_SESSION['timeOut'];
            // si no, lo setea..          
            }else{
                
                self::setLifeTime();

                $timeOut = $_SESSION['timeOut'];

            }
            return $timeOut;
        }
        return 0;
    }
    
    /**
     * [obtiene el tiempo restante de la session]
     * @return [el valor del tiempo en segundos]
     */
    public static function getTimeRemain()
    {
        self::retrieve();

        if(!empty($_SESSION['sessionId'])){
            
            if(!empty($_SESSION['timeOut'])){
                
                // obtiene el tiempo de duracion de la sesion
                $timeOut = $_SESSION['timeOut'];
                // calcula el tiempo transcurrido
                $timeLapsed = time() - $_SESSION['created'];
                
                // resta el tiempo transcurrido a timeOut 
                $timeOut -= $timeLapsed;
                
                // si el tiempo restante de session es menor a 0 destruye la misma
                if($timeOut <= 0){
                    self::destroy();
                }else{
                    return $timeOut;
                }
            }
        }
        return 0;
    }
    
    /**
     * [devuelve si el usuario esta logueado]
     * @return [TRUE or FALSE]
     */
    private static function isUserLogged(){
        
        return (isset($_SESSION['loginStatus']) && $_SESSION['loginStatus']) ? TRUE : FALSE;
        
    }
    
    /**
     * [devuelve si la session esta en modo edicion de DDJJ]
     * @return [TRUE or FALSE]
     */
    private static function isDDJJCreated(){
        
        return isset ($_SESSION['Affidavit']) ? TRUE : FALSE;
        
    }
    
    /**
     * [devuelve si la session esta en modo edicion de DDJJ]
     * @return [TRUE or FALSE]
     */
    private static function modeEdit(){
        
        return (self::isUserLogged() && self::isDDJJCreated()) ? TRUE : FALSE;
        
    }
    
  
    /**
     * [devuelve el modo de la session]
     * @return [0 o 1]
     */        
    public static function getMode()
    {
        self::retrieve();
        
        //return (self::modeEdit()) ? 1 : 0;
        return (isset($_SESSION['editMode']) && $_SESSION['editMode'] == TRUE) ? 1 : 0;

    }
    
    
    /**
     * [suma el tiempo SESSION_TIME_EXTEND al tiempo restante de la session]
     * @return [total de tiempo restante en segundos]
     */
    public static function extendTimeOut()
    {
        self::retrieve();
        
        // comprueba que los valores sessionId y ceated esten presentes en $_SESSION
        if(!empty($_SESSION['sessionId']) && !empty($_SESSION['created'])){
            
            // comprueba que el tiempo de sesion no haya finalizado
            if(self::checkTime()){

                // obtiene el tiempo restante
                $timeRemain = self::getTimeRemain();

                // suma el valor configurado al tiempo restante de la session
                $timeRemain += SESSION_TIME_EXTEND;

                // setea el nuevo tiempo de session para las COOKIES
                setcookie(session_name(), $_COOKIE[session_name()], time() + $timeRemain);

                // setea el nuevo valor en variable de sesion
                $_SESSION['timeOut'] += SESSION_TIME_EXTEND;

                // cierra el archivo de session para evitar problemas de concurrencia (en simultaneo por Ajax)
                session_commit(); //alias of session_write_close(); 
                
                //CAMBIA EL ID DE LA SESSION Y ELIMINA EL ANTERIOR CON EL PARAM TRUE
                //session_regenerate_id(TRUE);

                return $timeRemain;
            }
        }
        return 0;
    }
    
    /**
     * [setea el tiempo total de session en $_SESSION['timeOut']]]
     * @return [none]
     */
    public static function setLifeTime()
    {
        self::retrieve();
        
        if(!self::isDDJJCreated()){
            $timeOut = $_SESSION['discardAfter'] - time();
            
            $_SESSION['editMode'] = FALSE;
            
        }else{
            $timeOut = ini_get("session.gc_maxlifetime");
            
            unset($_SESSION['discardAfter']);
            
            $_SESSION['editMode'] = TRUE;
            
        }
        
        $_SESSION['created'] = time();
        
        $_SESSION['timeOut'] = $timeOut;
 
    }
     
    /**
     * [modify - modifica una session]
     * @param  [type] $name     [nombre de la sesion]
     * @param  [type] $value    [valor de la sesion]
     */
    public static function modify($name, $value)
    {
        $_SESSION[$name] = $value;
    }
    

}

?>