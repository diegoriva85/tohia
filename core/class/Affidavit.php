<?php
/**
 * Colección de clases para componer un documento DJ
 **/
class AffidavitFieldType {
    public $name;
    private $desc;
    private $regex;

    public function AffidavitFieldType ($name, $desc, $regex) {
        $this->name = $name;
        $this->desc = $desc;
        $this->regex = $regex;
    }

    public function getName () {
        return $this->name;
    }

    public function getDesc () {
        return $this->desc;
    }

    public function getRegex () {
        return $this->regex;
    }
}

class AffidavitField {
    private $id;
    private $name;
    private $value;
    private $type;
    private $isRequired;
    private $isPublic;

    /**
     * Campo de una sección del documento DJ
     * @param String $id Identificador del elemento
     * @param String $name Nombre del campo
     * @param String $value Valor del campo
     * @param Boolean $isPublic Determina si el campo es público (valor por defecto: falso)
     **/
    public function AffidavitField ($id, $name, $value, $type, $isRequired = true, $isPublic = false) {
        $this->id = $id;
        $this->name = $name;
        $this->value = $value;
        $this->type = $type;
        $this->isRequired = $isRequired;
		$this->isPublic = $isPublic;
    }

    /**
     * Obtiene el identificador del elemento
     * @return String
     **/
    public function getId () {
        return $this->id;
    }

    /**
     * Obtiene el nombre del campo
     * @return String
     **/
    public function getName () {
        return $this->name;
    }

    /**
     * Establece el nombre del campo
     * @param String $name Nombre del campo
     **/
    public function setName ($name) {
        $this->name = $name;
    }

    /**
     * Obtiene el valor del campo
     * @return String
     **/
    public function getValue () {
        return $this->value;
    }

    /**
     * Establece el valor del campo
     * @param String $value Valor del campo
     **/
    public function setValue ($value) {
        $this->value = $value;
    }

    public function setType ($type) {
        $this->type = $type;
    }

    public function getType () {
        return $this->type;
    }

    /**
     * Determina si el campo es público
     * @return Boolean
     **/
    public function isRequired () {
        return $this->isRequired;
    }

    /**
     * Determina si el campo es público
     * @return Boolean
     **/
    public function isPublic () {
        return $this->isPublic;
    }

    /**
     * Determina si el valor del campo cumple los requerimientos de su tipo
     * @return Array 'code' => int, 'message' => str
     **/
    public function validateType () {
        $rtn = [
            'code' => 0,
            'message' => ''
        ];
        $myType = $this->getType();
        $regex = $myType->getRegex();
        $value = $this->getValue();

        $matchRegex = preg_match($regex, $value);

        if (!$matchRegex) {
            $rtn['code'] = 2;
            $rtn['message'] = 'El campo ' . $this->getName() . ' solo puede contener ' . $myType->getDesc();
        }

        return $rtn;
    }

    /**
     * Determina si el valor del campo cumple los requerimientos necesarios para ser almacenado
     * @return Array 'code' => int, 'message' => str
     **/
    public function validate () {
        $rtn = [
            'code' => 0,
            'message' => ''
        ];
        
        if ($this->isRequired === true && $this->value === '') {
            $rtn['code'] = 1;
            $rtn['message'] = 'El campo ' . $this->getName() . ' es requerido';
        }
        else if($this->value !== ''){
            $rtn = $this->validateType();
        }

        return $rtn;
    }
}

class AffidavitSection {
    private $id;
    private $name;
    private $sections;
    private $fields;
    private $common;
    private $index;

    /**
     * Sección de un documento DJ
     * @param String $id Identificador del elemento
     **/
    public function AffidavitSection ($id, $name = '') {
        $this->id = $id;
        $this->name = $name;
        $this->sections = array();
        $this->fields = array();
        $this->common = array();
        $this->index = array(
            'sections' => array(),
            'fields' => array(),
            'common' => array(
            	'sections' => array(),
                'fields' => array()
            )
        );
    }

    /**
     * Obtiene el identificador del elemento
     * @return String
     **/
    public function getId () {
        return $this->id;
    }

    /**
     * Obtiene el nombre de la sección
     * @return String
     **/
    public function getName () {
        return $this->name;
    }

    /**
     * Establece el nombre de la sección
     * @param String $name Nombre de la sección
     **/
    public function setName ($name) {
        $this->name = $name;
    }

    /**
     * Obtiene las secciones de la sección
     * @return AffidavitSection[]
     **/
    public function getSections () {
        return $this->sections;
    }

    /**
     * Obtiene todo el contenido de la sección
     * @return Mixed[]
     **/
    public function getEverything () {
        return $this->common;
    }

    /**
     * Obtiene los campos de la sección
     * @return AffidavitField[]
     **/
    public function getFields () {
        return $this->fields;
    }

    /**
     * Incorpora una sección a la sección
     * @param AffidavitSection $section Sección a incorporar
     * @return AffidavitSection Sección incorporada
     **/
    public function addSection ($section) {
        $this->sections[] = $section;
        $this->common[] = &$this->sections[count($this->sections) - 1];
        $this->index['sections'][$section->getId()] = count($this->sections) - 1;
        $this->index['common']['sections'][$section->getId()] = count($this->common) - 1;

        return $section;
    }

    /**
     * Remueve una sección
     * @param type $id Identificador de la sección
     **/
    public function removeSection ($id) {
        $indexSection = $this->index['sections'][$id];
        $indexCommon = $this->index['common']['sections'][$id];

        unset($this->index['sections'][$id]);
        unset($this->index['common']['sections'][$id]);
        unset($this->sections[$indexSection]);
        $this->sections = array_values($this->sections);
        unset($this->common[$indexCommon]);
        $this->common = array_values($this->common);

        foreach ($this->index['sections'] as &$i) {
            if ($i > $indexSection) {
                $i--;
            }
        }

        foreach ($this->index['common']['sections'] as &$i) {
            if ($i > $indexCommon) {
                $i--;
            }
        }
        
        foreach ($this->index['common']['fields'] as &$i) {
            if ($i > $indexCommon) {
                $i--;
            }
        }
    }

    /**
     * Obtiene una sección de la sección
     * @param String $id Identificador de la sección
     * @return AffidavitSection
     **/
    public function getSection ($id) {
        if (array_key_exists($id, $this->index['sections'])) {
            $return = $this->sections[$this->index['sections'][$id]];
        }
        else
        {
            $return['error']['message']  = 'No se encontro la seccion con Id : '.$id;
            //return $this->addSection(new AffidavitSection($id, '', ''));
        }
        return $return;
    }

    /**
     * Incorpora un campo a la sección
     * @param AffidavitField $field Campo a incorporar
     * @return AffidavitField Campo incorporado
     **/
    public function addField ($field) {
        $this->fields[] = $field;
        $this->common[] = &$this->fields[count($this->fields) - 1];
        $this->index['fields'][$field->getId()] = count($this->fields) - 1;
        $this->index['common']['fields'][$field->getId()] = count($this->common) - 1;

        return $field;
    }

    /**
     * Remueve un campo
     * @param type $id Identificador del campo
     **/
    public function removeField ($id) {
        $indexField = $this->index['fields'][$id];
        $indexCommon = $this->index['common']['fields'][$id];

        unset($this->index['fields'][$id]);
        unset($this->index['common']['fields'][$id]);
        unset($this->fields[$indexField]);
        $this->fields = array_values($this->fields);
        unset($this->common[$indexCommon]);
        $this->common = array_values($this->common);

        foreach ($this->index['fields'] as &$i) {
           if ($i > $indexField) {
               $i--;
           }
        }

        foreach ($this->index['common']['fields'] as &$i) {
           if ($i > $indexCommon) {
               $i--;
           }
        }
    }

    /**
     * Obtiene un campo de la sección
     * @param String $id Identificador del campo
     * @return AffidavitField
     **/
    public function getField ($id)
    {
        if (array_key_exists($id, $this->index['fields']))
        {
            $return =  $this->fields[$this->index['fields'][$id]];
        }
        else
        {
            $return['error']['message'] = 'No se encontro el campo con Id : '.$id;
            //return $this->addField(new AffidavitField($id, '', '', ''));
        }
        return $return;
    }
}

class AffidavitDocument {
    private $sections;
    private $index;

    /**
     * Documento DJ
     **/
    public function AffidavitDocument () {
        $this->sections = array();
        $this->index = array();
    }

    /**
     * Popula la estructura del documento
     * @return undefined
     **/
    public function load () {
        $dbLink = Database::connect();

        $stmt = $dbLink->prepare(
            'select
                    count(parent.name) - 1 as depth,
                    node.id as sectionId,
                    node.element_id as sectionElementId,
                    node.name as sectionName,
                    node.label as sectionLabel,
                    node.description as sectionDescription,
                    field.id as fieldId,
                    field.element_id as fieldElementId,
                    field.SADE_idFormComp as fieldSADEIdFormComp,
                    field.SADE_inputName as fieldSADEInputName,
                    field.name as fieldName,
                    field.label as fieldLabel,
                    field.description as fieldDescription,
                    field_type.name as fieldType,
                    field_value.value as value,
                    field_value.label as valueLabel
            from
                    section as node
                    inner join section as parent
                            on node.lft between parent.lft and parent.rgt
                            and parent.deleted = 0
                    left join field
                            on field.section_id = node.id
                            and field.deleted = 0
                    left join field_type
                            on field_type.id = field.field_type_id
                            and field_type.deleted = 0
                    left join field_value
                            on field_value.field_id = field.id
                            and field_value.deleted = 0
            where
                    node.deleted = 0
            group by
                    node.id,
                    field.id,
                    field_type.id,
                    field_value.id
            order by
                    node.lft,
                    field.order,
                    field_value.order'
        );
        $stmt->execute();

        $stmt->bind_result(
            $depth,
            $sectionId,
            $sectionElementId,
            $sectionName,
            $sectionLabel,
            $sectionDescription,
            $fieldId,
            $fieldElementId,
            $fieldSADEIdFormComp,
            $fieldSADEInputName,
            $fieldName,
            $fieldLabel,
            $fieldDescription,
            $fieldType,
            $value,
            $valueLabel
        );
        $stmt->fetch();

        $stmt->close();
        $dbLink->close();
    }

    /**
     * Obtiene las secciones del documento DJ
     * @return AffidavitSecction[]
     **/
    public function getSections () {
        return $this->sections;
    }
    /**
     * Incorpora una sección al documento
     * @param AffidavitSection $section Sección a incorporar
     * @return AffidavitSection Sección incorporada
     **/
    public function addSection ($section) {
        $this->sections[] = $section;
        $this->index[$section->getId()] = count($this->sections) - 1;

        return $section;
    }

    /**
     * Remueve una sección
     * @param type $id Identificador de la sección
     **/
    public function removeSection ($id) {
        $index = $this->index[$id];

        unset($this->index[$id]);
        unset($this->sections[$index]);
        $this->sections = array_values($this->sections);

        foreach ($this->index as &$i) {
            if ($i > $index) {
                $i--;
            }
        }
    }

    /**
     * Obtiene una sección del documento
     * @param String $id Identificador de la sección
     * @return AffidavitSection
     **/
    public function getSection ($id) {
        if (array_key_exists($id, $this->index)) {
            return $this->sections[$this->index[$id]];
        }
        else
        {
            $return['error']['message'] = 'No se encontro el seccion con Id : '.$id;
            //return $this->addSection(new AffidavitSection($id));
        }
    }
}

?>
