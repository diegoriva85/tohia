<?php

//llamada a la configuracion global del sistema si se realiza una peticion por AJAX
//si no esta incluido la configuracion general, lo incluye /var/www/html/<project>/sys/conf/ini.php
// (!defined('PROJECT_INIT')) ? require $_SERVER['DOCUMENT_ROOT'].'/'.strstr(substr($_SERVER['PHP_SELF'],1), '/', true)."/conf/ini.conf" : '';
//(!defined('PROJECT_INIT')) ? require $_SERVER['DOCUMENT_ROOT'].'/'.explode('/', $_SERVER["HTTP_REFERER"])[3]."/sys/conf/ini.conf" : '';

require_once  __DIR__.'/../../sys'.DIRECTORY_SEPARATOR.'conf'.DIRECTORY_SEPARATOR.'ini.conf';

// REQUERIMIENTO
// Se debe enviar como parametro el nombre del proyecto para construir el directorio donde se ubican las constantes
// $project = ($_REQUEST['project']) ? $_REQUEST['project'] : 'clone';

// require_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR . $project . DIRECTORY_SEPARATOR .'sys'.DIRECTORY_SEPARATOR.'conf'.DIRECTORY_SEPARATOR.'ini.conf';

/**
* 
*/
class Project
{

	/******************************************************/
	/********************** ATRIBUTOS *********************/
	/******************************************************/


	private static $serverIp = SERVER_IP;
	private static $serverUrl = SERVER_URL;
	private static $serverDns = '';//se calcula en tiempo de ejecucion
	
	private static $dnsPrimary = DNS_PRIMARY;
	private static $dnsSecondary = DNS_SECONDARY;
	private static $dnsLocal = DNS_LOCAL;

	private static $projectName = PROJECT_NAME;
	private static $projectIp = PROJECT_IP;
	private static $projectUrl = PROJECT_URL;
	private static $projectDir = PROJECT_DIR;
	private static $projectVersion = PROJECT_VERSION;
	private static $protocol = PROTOCOL;

	private static $dbIp = DB_IP;
	private static $dbPort = DB_PORT;
	private static $dbName = DB_NAME;
	private static $dbUser = DB_USER;
	private static $dbPass = DB_PASS;
	private static $dbMotor = DB_MOTOR;
	private static $dbCharset = DB_CHARSET;

	private static $emailHost = EMAIL_HOST;
	private static $emailUSer = EMAIL_USER;
	private static $emailPass = EMAIL_PASS;

	private static $pathProject = PATH_PROJECT;
	private static $pathSys = PATH_SYS;
	private static $pathModuleDir = PATH_MODULE_DIR;
	private static $pathModuleUrl = PATH_MODULE_URL;
	private static $pathMediaDir = PATH_MEDIA_DIR;
	private static $pathMediaUrl = PATH_MEDIA_URL;
	private static $pathLib = PATH_LIB;
	//@TODO: Ver quien lo puso.
        //private static $pathLibLocal = PATH_LIB_LOCAL;
	private static $pathClass = PATH_CLASS;
	private static $pathIp = PATH_IP;
	private static $pathUrl = PATH_URL;
	private static $pathDir = PATH_DIR;
	private static $pathFile = PATH_FILE;

	private static $debugAjax = DEBUG_AJAX;
	private static $debugWeb = DEBUG_WEB;
	private static $debugPhp = DEBUG_PHP;
	private static $debugDb = DEBUG_DB;
	private static $debugSession = DEBUG_SESSION;
	private static $debugEmail = DEBUG_EMAIL;

	private static $session = SESSION;
	private static $sessionUser = SESSION_USER;

	private static $errorDbConexion = ERROR_DB_CONEXION;
	private static $errorDbExist = ERROR_DB_EXIST;


	/******************************************************/
	/*********************** METODOS **********************/
	/******************************************************/

	function __construct()
	{

	}

	/**
	 * @return {json}       objecto json con los datos cargados
	 */ 
	public static function constant($print = FALSE)
	{

		$result['serverIp'] = self::getServerIp();
		$result['serverUrl'] = self::getServerUrl();
		$result['serverDns'] = self::getServerDns();

		$result['dnsPrimary'] = self::getDnsPrimary();
		$result['dnsSecondary'] = self::getDnsSecondary();
		$result['dnsLocal'] = self::getDnsLocal();

		$result['projectName'] = self::getProjectName();
		$result['projectIp'] = self::getProjectIp();
		$result['projectUrl'] = self::getProjectUrl();
		$result['projectDir'] = self::getProjectDir();
		$result['projectVersion'] = self::getprojectVersion();

		$result['dbName'] = self::getDbName();
		$result['dbIp'] = self::getDbIp();
		$result['dbPort'] = self::getDbPort();
		$result['dbUser'] = self::getDbUser();
		$result['dbPass'] = self::getDbPass();
		$result['dbMotor'] = self::getDbMotor();
		$result['dbCharset'] = self::getDbCharset();

		$result['emailHost'] = self::getEmailHost();
		$result['emailUser'] = self::getEmailUser();
		$result['emailPass'] = self::getEmailPass();

		$result['pathProject'] = self::getPathProject();
		$result['pathSys'] = self::getPathSys();
		$result['pathModuleDir'] = self::getPathModuleDir();
		$result['pathModuleUrl'] = self::getPathModuleUrl();
		$result['pathMediaDir'] = self::getPathMediaDir();
		$result['pathMediaUrl'] = self::getPathMediaUrl();
		$result['pathImg'] = self::getPathImg();
		$result['pathLib'] = self::getPathLib();
		$result['pathClass'] = self::getPathClass();
		$result['pathIp'] = self::getPathIp();
		$result['pathUrl'] = self::getPathUrl();
		$result['pathDir'] = self::getPathDir();
		$result['pathFile'] = self::getPathFile();

		$result['debugAjax'] = self::getDebugAjax();
		$result['debugWeb'] = self::getDebugWeb();

		$result['session'] = self::getSession();
		$result['sessionUser'] = self::getSessionUser();

		$result['errorDbConexion'] = self::getErrorDbConexion();
		$result['errorDbExist'] = self::getErrorDbExist();

		//si solicito la impresion de las mismas por consola
		if ($print)
		{
			echo '<script>';

			echo 'console.debug("************ CONSTANTES DEL SISTEMA :INICIO ***********");';

			//separador de windows
			$slash = "\\";

		    foreach ($result as $key => $value)
		    {
		    		//reemplazamos el separador \ por \\
					echo 'console.debug("'.$key. ': '.str_replace($slash, $slash.$slash, $value ).'");';
			}
			
			echo 'console.debug("");';
			echo 'console.debug("************ CONSTANTES DEL SISTEMA : FIN ************");';
			echo 'console.debug("");';

			echo '</script>';
		}

	    echo json_encode($result);
	}

	public static function get($atribute)
	{

		switch ($atribute)
		{
		
			case 'serverIp': $result['value'] = self::getServerIp(); break;
			case 'serverUrl': $result['value'] = self::getServerUrl(); break;
			case 'serverDns': $result['value'] = self::getServerDns(); break;

			case 'dnsPrimary': $result['value'] = self::getDnsPrimary(); break;
			case 'dnsSecondary': $result['value'] = self::getDnsSecondary(); break;
			case 'dnsLocal': $result['value'] = self::getDnsLocal(); break;

			case 'projectName': $result['value'] = self::getProjectName(); break;
			case 'projectIp': $result['value'] = self::getProjectIp(); break;
			case 'projectUrl': $result['value'] = self::getProjectUrl(); break;
			case 'projectDir': $result['value'] = self::getProjectDir(); break;
			case 'projectVersion': $result['value'] = self::getprojectVersion(); break;
			case 'protocol': $result['value'] = self::getProtocol(); break;

			case 'dbIp': $result['value'] = self::getDbIp(); break;
			case 'dbPort': $result['value'] = self::getDbPort(); break;
			case 'dbName': $result['value'] = self::getDbName(); break;
			case 'dbUser': $result['value'] = self::getDbUser(); break;
			case 'dbPass': $result['value'] = self::getDbPass(); break;
			case 'dbMotor': $result['value'] = self::getDbMotor(); break;
			case 'dbCharset': $result['value'] = self::getDbCharset(); break;

			case 'emailHost': $result['value'] = self::getEmailHost(); break;
			case 'emailUser':$result['value'] = self::getEmailUser(); break;
			case 'emailPass':$result['value'] = self::getEmailPass(); break;
			
			case 'pathProject': $result['value'] = self::getPathProject(); break;
			case 'pathSys': $result['value'] = self::getPathSys(); break;
			case 'pathModuleDir': $result['value'] = self::getPathModuleDir(); break;
			case 'pathModuleUrl': $result['value'] = self::getPathModuleUrl(); break;
			case 'pathMediaDir': $result['value'] = self::getPathMediaDir(); break;
			case 'pathMediaUrl': $result['value'] = self::getPathMediaUrl(); break;
			case 'pathImg': $result['value'] = self::getPathImg(); break;			
			case 'pathImgProduct': $result['value'] = self::getPathImgProduct(); break;			
			case 'pathLib': $result['value'] = self::getPathLib(); break;
			case 'pathClass': $result['value'] = self::getPathClass(); break;
			case 'pathIp': $result['value'] = self::getPathIp(); break;
			case 'pathUrl': $result['value'] = self::getPathUrl(); break;
			case 'pathDir': $result['value'] = self::getPathDir(); break;
			case 'pathFile': $result['value'] = self::getPathFile(); break;

			case 'debugAjax': $result['value'] = self::getDebugAjax(); break;
			case 'debugWeb': $result['value'] = self::getDebugWeb(); break;
			case 'debugPhp': $result['value'] = self::getDebugPhp(); break;
			case 'debugDb': $result['value'] = self::getDebugDb(); break;
			case 'debugSession': $result['value'] = self::getDebugSession(); break;
			case 'debugEmail': $result['value'] = self::getDebugEmail(); break;

			case 'session': $result['value'] = self::getSession(); break;
			case 'sessionUser': $result['value'] = self::getSessionUser(); break;

			case 'errorDbConexion': $result['value'] = self::getErrorDbConexion(); break;
			case 'errorDbExist': $result['value'] = self::getErrorDbExist(); break;

		}
	
	    echo json_encode($result);
	}

	/**
	 * devuelve el estado de debug para un tipo determinado, pudiendo especificar el tipo
	 * @param  [string] $debug [tipo de debug (ajax, php, db,...)]
	 * @param  [string] $type  [tipo de mensaje (fatal, error, warn,...)]
	 * @return [string]        [estado del debug si no se especifico un tipo, retorna todos]
	 */
	public static function getStatusDebug($debug, $type = null)
	{
		//convierte a minuscula el debug solicitado
		$debugValue = strtolower($debug);
		//convierte en mayuscula el tipo
		$typeValue = ($type == null) ? null : strtoupper($type);;
		//lista de los tipos aceptados de debug
		$typeList = array('FATAL', 'ERROR', 'WARN', 'INFO', 'DEBUG', 'TRACE', 'ALL');
		//lista de los tipos de debug posibles
		$debugList = array('ajax', 'web', 'php', 'db', 'session', 'email');
		//resultado
		$result = '';

		//si el debug solicitado existe en el listado posible
		if (in_array($debugValue, $debugList))
		{
		    switch ($debugValue)
			{
				case 'ajax': $result = self::getDebugAjax(); break;
				case 'web': $result = self::getDebugWeb(); break;
				case 'php': $result = self::getDebugPhp(); break;
				case 'db': $result = self::getDebugDb(); break;
				case 'session': $result = self::getDebugSession(); break;
				case 'email': $result = self::getDebugEmail(); break;
			}
		}

		if (in_array($typeValue, $typeList))
		{
			//guardara la posicion dependiendo el tipo
			$position = null;

			switch ($typeValue)
			{
				case 'FATAL': $position = 0; break;
				case 'ERROR': $position = 1; break;
				case 'WARN': $position = 2; break;
				case 'INFO': $position = 3; break;
				case 'DEBUG': $position = 4; break;
				case 'TRACE': $position = 5; break;
				case 'ALL': $position = 6; break;
			}
			//retorna la posicion indicado segun el tipo de mensaje
			return $result[$position];
		}
		else
		{
			return $result;
		}
	}

	/******************************************************/
	/****************** GETTER AND SETTER *****************/
	/******************************************************/

	public static function getServerIp()
	{
		return self::$serverIp;
	}

	public static function setServerIp($serverIp)
	{
		self::$serverIp = $serverIp;
	}

	public static function getServerUrl()
	{
		return self::$serverUrl;
	}

	public static function setServerUrl($serverUrl)
	{
		self::$serverUrl = $serverUrl;
	}

	public static function getServerDns()
	{

		if(defined('SERVER_DNS'))
		{
			return SERVER_DNS;
		}
		else
		{
			return PROJECT_IP."/sys";
		}
	}

	public static function setServerDns($serverDns)
	{
		self::$serverDns = $serverDns;
	}

	public static function getDnsPrimary()
	{
		return self::$dnsPrimary;
	}

	public static function setDnsPrimary($dnsPrimary)
	{
		self::$dnsPrimary = $dnsPrimary;
	}

	public static function getDnsSecondary()
	{
		return self::$dnsSecondary;
	}

	public static function setDnsSecondary($dnsSecondary)
	{
		self::$dnsSecondary = $dnsSecondary;
	}

	public static function getDnsLocal()
	{
		return self::$dnsLocal;
	}

	public static function setDnsLocal($dnsLocal)
	{
		self::$dnsLocal = $dnsLocal;
	}

	public static function getProjectName()
	{
		return self::$projectName;
	}

	public static function setProjectName($projectName)
	{
		self::$projectName = $projectName;
	}

	public static function getProjectIp()
	{
		return self::$projectIp;
	}

	public static function setProjectIp($projectIp)
	{
		self::$projectIp = $projectIp;
	}

	public static function getProjectUrl()
	{
		return self::$projectUrl;
	}

	public static function setProjectUrl($projectUrl)
	{
		self::$projectUrl = $projectUrl;
	}

	public static function getProtocol()
	{
		return self::$protocol;
	}

	public static function getProjectDir()
	{
		return self::$projectDir;
	}

	public static function setProjectDir($projectDir)
	{
		self::$projectDir = $projectDir;
	}

	public static function getprojectVersion()
	{
		return self::$projectVersion;
	}

	public static function setprojectVersion($projectVersion)
	{
		self::$projectVersion = $projectVersion;
	}

	public static function getCompanyName()
	{
		return self::$CompanyName;
	}

	public static function setCompanyName($CompanyName)
	{
		self::$CompanyName = $CompanyName;
	}

	public static function getCompanyLogo()
	{
		return self::$CompanyLogo;
	}

	public static function setCompanyLogo($CompanyLogo)
	{
		self::$CompanyLogo = $CompanyLogo;
	}

	public static function getDbIp()
	{
		return self::$dbIp;
	}

	public static function setDbIp($dbIp)
	{
		self::$dbIp = $dbIp;
	}

	public static function getDbPort()
	{
		return self::$dbPort;
	}

	public static function setDbPort($dbPort)
	{
		self::$dbPort = $dbPort;
	}

	public static function getDbName()
	{
		return self::$dbName;
	}

	public static function setDbName($dbName)
	{
		self::$dbName = $dbName;
	}

	public static function getDbUser()
	{
		return self::$dbUser;
	}

	public static function setDbUser($dbUser)
	{
		self::$dbUser = $dbUser;
	}

	public static function getDbPass()
	{
		return self::$dbPass;
	}

	public static function setDbPass($dbPass)
	{
		self::$dbPass = $dbPass;
	}

	public static function getDbMotor()
	{
		return self::$dbMotor;
	}

	public static function setDbMotor($dbMotor)
	{
		self::$dbMotor = $dbMotor;
	}

	public static function getDbCharset()
	{
		return self::$dbCharset;
	}

	public static function setDbCharset($dbCharset)
	{
		self::$dbCharset = $dbCharset;
	}

	public static function getEmailHost()
	{
		return self::$emailHost;
	}

	public static function setEmailHost($emailHost)
	{
		self::$emailHost = $emailHost;
	}

	public static function getEmailUser()
	{
		return self::$emailUSer;
	}

	public static function setEmailUser($emailUSer)
	{
		self::$emailUser = $emailUser;
	}
	
	public static function getEmailPass()
	{
		return self::$emailPass;
	}

	public static function setEmailPass($emailPass)
	{
		self::$emailPass = $emailPass;
	}

	public static function getPathProject()
	{
		return self::$pathProject;
	}

	public static function setPathProject($pathProject)
	{
		self::$pathProject = $pathProject;
	}

	public static function getPathSys()
	{
		return self::$pathSys;
	}

	public static function setPathSys($pathSys)
	{
		self::$pathSys = $pathSys;
	}

	public static function getPathModuleDir()
	{
		return self::$pathModuleDir;
	}

	public static function setPathModuleDir($pathModuleDir)
	{
		self::$pathModuleDir = $pathModuleDir;
	}

	public static function getPathModuleUrl()
	{
		return self::$pathModuleUrl;
	}

	public static function setPathModuleUrl($pathModuleUrl)
	{
		self::$pathModuleUrl = $pathModuleUrl;
	}

	public static function getPathMediaDir()
	{
		return self::$pathMediaDir;
	}

	public static function setPathMediaDir($pathMediaDir)
	{
		self::$pathMediaDir = $pathMediaDir;
	}

	public static function getPathMediaUrl()
	{
		return self::$pathMediaUrl;
	}
	
	public static function setPathMediaUrl($pathMediaUrl)
	{
		self::$pathMediaUrl = $pathMediaUrl;
	}

	public static function getPathImg()
	{
		return self::$pathMediaUrl.'/img/';
	}

	public static function getPathImgProduct()
	{
		return self::$projectUrl .'/core/upload/products';
	}

	public static function getPathLib()
	{
		return self::$pathLib;
	}
	public static function setPathLib($pathLib)
	{
		self::$pathLib = $pathLib;
	}
	
	public static function getPathClass()
	{
		return self::$pathClass;
	}
	public static function setPathClass($pathClass)
	{
		self::$pathClass = $pathClass;
	}

	public static function getPathIp()
	{
		return self::$pathIp;
	}

	public static function setPathIp($pathIp)
	{
		self::$pathIp = $pathIp;
	}

	public static function getPathUrl()
	{
		return self::$pathUrl;
	}

	public static function setPathUrl($pathUrl)
	{
		self::$pathUrl = $pathUrl;
	}

	public static function getPathDir()
	{
		return self::$pathDir;
	}

	public static function setPathDir($pathDir)
	{
		self::$pathDir = $pathDir;
	}

	public static function getPathFile()
	{
		return self::$pathFile;
	}

	public static function setPathFile($pathFile)
	{
		self::$pathFile = $pathFile;
	}

	public static function getDebugAjax()
	{
		return self::$debugAjax;
	}

	public static function setDebugAjax($debugAjax)
	{
		self::$debugAjax = $debugAjax;
	}

	public static function getDebugWeb()
	{
		return self::$debugWeb;
	}

	public static function setDebugWeb($debugWeb)
	{
		self::$debugWeb = $debugWeb;
	}

	public static function getDebugPhp()
	{
		return self::$debugPhp;
	}

	public static function setDebugPhp($debugPhp)
	{
		self::$debugPhp = $debugPhp;
	}

	public static function getDebugDb()
	{
		return self::$debugDb;
	}

	public static function setDebugDb($debugDb)
	{
		self::$debugDb = $debugDb;
	}

	public static function getDebugSession()
	{
		return self::$debugSession;
	}

	public static function setDebugSession($debugSession)
	{
		self::$debugSession = $debugSession;
	}
	
	public static function getDebugEmail()
	{
		return self::$debugEmail;
	}

	public static function setDebugEmail($debugEmail)
	{
		self::$debugEmail = $debugEmail;
	}

	public static function getSession()
	{
		return self::$session;
	}

	public static function setSession($session)
	{
		self::$session = $session;
	}

	public static function getSessionUser()
	{
		return self::$sessionUser;
	}

	public static function setSessionUser($sessionUser)
	{
		self::$sessionUser = $sessionUser;
	}

	public static function getErrorDbConexion()
	{
		return self::$errorDbConexion;
	}

	public static function setErrorDbConexion($errorDbConexion)
	{
		self::$errorDbConexion = $errorDbConexion;
	}

	public static function getErrorDbExist()
	{
		return self::$errorDbExist;
	}

	public static function setErrorDbExist($errorDbExist)
	{
		self::$errorDbExist = $errorDbExist;
	}



}


/******************************************************/
/************************ AJAX ************************/
/******************************************************/

//permite visualizar los resultados AJAX web solo para este Objeto

//si realizo una consulta AJAX
// La tercera condicion debe estar en false a menos que se quiera debuggear solo este objeto 
if ( Security::isAjax() || DEBUG_AJAX || TRUE )
{
	// verificamos la condicion de la solicitud (definida y con contenido)
	if (isset($_REQUEST["action"]) && !empty($_REQUEST["action"]))
	{ 
	    //asignacion de los valores
	    $action = $_REQUEST["action"];

	    //determinamos la accion que se solicito ejecutar en el servidor
	    switch($action)
	    {
	    	// [esta seccion deberia cambiarse acorde a las llamadas AJAX del lado del cliente]
	    	// case "metodoDelObjeto" : Object::method($_REQUEST["options"]); break;
	    	// 
	    	case "constant": Project::constant(); break;
	    	case "get": Project::get($_REQUEST["atribute"]); break;
	    }
	}
}

?>