<?php

require_once __DIR__.'/../../sys' . DIRECTORY_SEPARATOR . 'conf' . DIRECTORY_SEPARATOR . 'ini.conf';

class FormCriteria {
    public function FormCriteria () {
        
    }
    
    /**
     * Devuelve un array asociativo que contiene el panel determinado por $formPanelId o todos los paneles si es omitido
     * @param String $formPanelId El id del elemento HTML
     * @return Array
     **/
    public static function getPanels ($formPanelId = null) {
        $rtn = array();
        $rst = Database::result('select form_panel.id as form_panel_id, form_panel.name as form_panel_name, form_panel.description as form_panel_description, form_panel.element_id as form_panel_element_id, form_field.id as form_field_id, form_field.SADE_name as form_field_SADE_name, form_field.name as form_field_name, form_field.description as form_field_description, form_field.element_id as form_field_element_id from form_panel inner join form_field on form_field.form_panel_id = form_panel.id where form_panel.deleted = 0 and form_field.deleted = 0' . (empty($formPanelId) ? '' : ' and form_panel.element_id = '.$formPanelId));
        
        $rstIt = (new ArrayObject($rst))->getIterator();
        
        while ($rstIt->valid()) {
            $rtn[$rstIt->current()['form_panel_element_id']] = [
                'id' => $rstIt->current()['form_panel_id'],
                'name' => $rstIt->current()['form_panel_name'],
                'description' => $rstIt->current()['form_panel_description'],
                'fields' => array()
            ];
            while (isset($rtn[$rstIt->current()['form_panel_element_id']])) {
                $rtn[$rstIt->current()['form_panel_element_id']]['fields'][$rstIt->current()['form_field_element_id']] = [
                    'id' => $rstIt->current()['form_field_id'],
                    'SADEName' => $rstIt->current()['form_field_SADE_name'],
                    'name' => $rstIt->current()['form_field_name'],
                    'description' => $rstIt->current()['form_field_description']
                ];
                $rstIt->next();
            }
        }
        
        return $rtn;
    }
    
    /**
     * Actualiza los valores de un panel en la base de datos
     * @param Object $panel El panel que contiene los cambios
     * @return Boolean Verdadero si tuvo exito al guardar los cambios
     **/
    public static function savePanel ($panel) {
        $rtn = true;
        
        foreach ($panel->fields as $field) {
            $rtn &= (Database::modify('update form_field set name = \'' . $field->name . '\', description = \'' . $field->description . '\', updated_at = now() where id = ' . $field->id . ' and form_panel_id = ' . $panel->id)) >= 0;
        }

        return $rtn;
    }
}

if (Security::isAjax() || DEBUG_AJAX) {
    if (isset($_REQUEST["action"]) && !empty($_REQUEST["action"])) {
        switch ($_REQUEST["action"]) {
            case 'getPanels':
                echo json_encode(FormCriteria::getPanels());
                break;
            case 'savePanel':
                echo json_encode(FormCriteria::savePanel(json_decode($_REQUEST["panel"])));
                break;
        }
    }
}

?>