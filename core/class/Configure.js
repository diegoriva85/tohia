Configure =
{
	proyectFolder:window.location.origin + '',
	url: this.proyectFolder + '/core/class/Configure.php',

	/**
	 * [mostrar AJAX en la consola solo para este Objeto]
	 * @return {[type]} [description]
	 */
	isAjax: function()
	{
		//poner en true si se desea debuggear este objeto en particular 
		var view = false;
		//chequea la condicion general para ver Ajax hasta habilitado para todos los ojbetos o para el objeto en cuestion (this)
		return (Security.isAjax() || view);
	},
	// setProyectFolder: function()
	// {
	// 	this.proyectFolder = window.location.origin + '/ddjj';
	// },

	init: function()
	{
//		this.setProyectFolder();

	    this.ie();

		this.loader();

		this.cssSys('vert-offset');

		this.cssCore('footer');

		this.jsCore("menu");

	},

	class: function()
	{
		var fileList = [];
		var fileExtension = '.js';

		if(arguments.length == 0)
		{
			var data =
			{
				"action": "getAllJs"
			};

			var that = this;

			// datos a enviar
			if (this.isAjax()){console.log("AJAX:SEND  =>",data);}


			data = $(this).serialize() + "&" + $.param(data);

			// datos serializados para url
			if (this.isAjax()){console.log("AJAX:URL  =>",data);}


			$.ajax(
			{
				type: "post",
				dataType: "json",
				url: this.url,
				async:false,
				data: data,
				success: function(data)
				{
					// datos recibidos
					if (that.isAjax()){console.log("AJAX:GET  =>",data);}
					
					// indicar lo que se quiere procesar con los datos
					fileList = data;
					//dado que al obtener el nombre desde el directorio ya incluye la extension, al setea en vacio
					fileExtension = '';

				},
				error: function()
				{
					// mensaje
					var msg = 'no se pudo procesar';
					//datos no procesados por error
					if (that.isAjax()){console.log("AJAX:GET  =>", msg);}
				}
			});
		}
		// console.log('list ',fileList);

		fileUrl = Project.get("projectUrl") + "/sys/class/";

		for (var i = 0; i < fileList.length; i++)
		{
			var fileName = fileList[i];
			//crea el elemento y setea los valores
			var script = document.createElement('script');
			script.type = "text/javascript";
			//genera la ruta del archivo, chequea si hay que navegar ../../
			script.src = fileUrl + fileName + fileExtension;
			//agrega el elemento
			// document.head.appendChild(script);
			// document.getElementsByTagName('head')[0].appendChild(script);
			// document.getElementsByTagName("head")[0].appendChild(script);
			  // document.body.appendChild(script);
			var head = document.getElementsByTagName("head")[0];

			head.insertBefore(script, head.firstChild);

			// console.log('inlcuir ' + fileUrl + fileName + fileExtension);
		}
	},

	/**
	 * [cambia el valor de una constante especificada]
	 * @param  {[type]} findConst  [description]
	 * @param  {[type]} valueConst [description]
	 * @param  {[type]} filePath   [description]
	 * @return {[type]}            [description]
	 */
	constant: function(findConst, valueConst, filePath)
	{
	},

	css: function()
	{
		for (var i = 0; i < arguments.length; i++)
		{
			var file = arguments[i];
			//crea el elemento css y setea los valores
			var link = document.createElement('link');
			link.rel = 'stylesheet';

			console.debug(dir(file));
			
			//genera la ruta del archivo, chequea si hay que navegar ../../
			link.href = this.dir(file) + '?v=' + Project.get("projectVersion");
			//@TODO VARIABLE DEL SISTEMA
			// link.href = "http://localhost/clone/module/"+file + '?v=' + Project.get("projectVersion");
			//agrega el elemento
			document.head.appendChild(link);
		}
	},

	cssInc: function(filePath, fileName)
	{
		//crea el elemento css y setea los valores
		var link = document.createElement('link');
		link.rel = 'stylesheet';
		link.href = filePath + fileName  + '.css?v=' + Project.get("projectVersion");
		//agrega el elemento
		document.head.appendChild(link);
	},

	cssSys: function(name)
	{
		var pathModule = this.proyectFolder + '/core/css/';
		this.cssInc(pathModule, name);
	},
	
	cssCore: function()
	{
		var pathModule = Project.get('projectUrl') + 'core/css/';

		for (var i = 0; i < arguments.length; i++)
		{
			this.cssInc(pathModule, arguments[i]);
		}
	},

	cssModule: function()
	{
		var pathModule = 'x';

		for (var i = 0; i < arguments.length; i++)
		{
			this.cssInc(pathModule, arguments[i]);
		}
	},

	js: function()
	{
		for (var i = 0; i < arguments.length; i++)
		{
			var file = arguments[i];
			//crea el elemento y setea los valores
			var script = document.createElement('script');
			script.type = "text/javascript";
			//genera la ruta del archivo, chequea si hay que navegar ../../
			script.src = this.dir(file) + '?v=' + Project.version;
			//agrega el elemento
			document.head.appendChild(script);
		}
	},

	jsInc: function(filePath, fileName)
	{
		//crea el elemento y setea los valores
		var script = document.createElement('script');
		script.type = "text/javascript";
		//genera la ruta del archivo, chequea si hay que navegar ../../
		script.src = filePath + fileName  + '.js?v=' + Project.get("projectVersion");
		//agrega el elemento
		document.head.appendChild(script);
	},

	jsCore: function()
	{	
		//console.debug(Project.get('projectUrl'));

		var pathCore = Project.get('projectUrl') + 'core/js/';

		for (var i = 0; i < arguments.length; i++)
		{
			this.jsInc(pathCore, arguments[i]);
		}
	},

	jsModule: function()
	{
		//nombre del modulo
		var module = document.location.pathname.substring(1).split(/\//)[2];
		var pathModule = Project.get('projectUrl') + '/module/' + module + '/js/';

		for (var i = 0; i < arguments.length; i++)
		{
			this.jsInc(pathModule, arguments[i]);
		}
	},

	/**
	 * calcula el directorio navegando ../../
	 * @param  {[string]} pathRelative [ruta relativa]
	 * @return {[string]}              [ruta absoluta]
	 */
	dir: function(pathRelative)
	{
		//obtiene el dominio del archivo actual, http://dominio/path/file.php
		var url = document.URL;
		//quita el nombre que corresponde al archivo actual, http://dominio/path/
		var dir = url.substring(0, url.lastIndexOf('/'));
		//contedra la ruta final del archivo
		var paht = '';

		//chequea si solicito navegacion en la ruta del archivo mediando ../../
		if(pathRelative.split('/').indexOf('..') != -1)
		{
			//obtiene los directorios
			var pathAbsolute = location.pathname.split('/');
			//elimina el primer inidice vacio
			pathAbsolute.splice(0, 1);
			//elimina el ultimo elemento que corresponde al archivo
			pathAbsolute.pop();

			//crea la ruta relativa solicitada
			var pathRelative = pathRelative.split('/');
			//elimina el ultimo elemento que corresponde al archivo
			var fileName = pathRelative.pop();
			var i = 0;

			while(pathRelative[i] == '..')
			{
				//retroce un directorio
				pathAbsolute.pop();
				i++;
			}

			//elimina la referencia ../ de retroceso
			pathRelative.splice(0, i);

			//crea el path a partir del dominio principal
			path = location.origin + '/';

			//agrega los directorios raices
			for (var i = 0; i < pathAbsolute.length; i++)
			{
				path += pathAbsolute[i] + '/';
			}

			//agrega los directorios de navegacion
			for (var i = 0; i < pathRelative.length; i++)
			{
				path += pathRelative[i] + '/';
			}

			//agrega el nombre del archivo
			path += fileName;
		}
		else
		{
			//genera el dominio + el nombre del archivo nuevo
			path = dir + '/' + pathRelative;
		}

		return path; 
	},
	/**
	 * detecta si el navegador es internet explorer
	 * alert "su navagador no es compatible
	 */
	ie: function()
	{
		// Comprobacion de navegador
		var ie = navigator.userAgent.indexOf('MSIE') != -1;
		
		if(ie == true){
	        //una vez que haya cargado todos los elementos
	        $(window).on('load',function()
	        {
				var stack_modal = {"firstpos1": 200, "firstpos2": 0, "dir1": "down", "dir2": "left", "push": "middle", "modal": true, "overlay_close": false};
			    new PNotify({
			        title: "Su navegador no es compatible.<br>",
			        text: "Utilice <span class='fa fa-firefox'></span> <a href='https://www.mozilla.org/es-AR/firefox/new/'>Mozilla Firefox</a> o <a href='https://www.google.com/chrome/browser/desktop/index.html'>Google Chrome.</a>",
			        addclass: "stack-modal",
			        stack: stack_modal,
			        styling: "fontawesome",
			        hide: false,
			        type: "error"
			    });
	        });
		}
	},

	/**
	 * [lee una funcion del lado del servidor de acuerdo a los parametros]
	 * @param  {[array]} options [lista de opciones]
	 * @return {[void]}         [no retorna nada]
	 * para que pueda retornar un valor el metodo invocado en la seccion de ajax debe tener seteado la opcion async:false
	 */
	inc: function(file)
	{
		// alert(location.pathname);  // /tmp/test.html
		// alert(location.hostname);  // localhost
		// alert(location.search);    // ?blah=2
		// alert(document.URL);       // http://localhost/tmp/test.html?blah=2#foobar
		// alert(location.href);      // http://localhost/tmp/test.html?blah=2#foobar
		// alert(location.protocol);  // http:
		// alert(location.host);      // localhost
		// alert(location.origin);    // http://localhost
		// alert(location.hash);      // #foobar
	
		for (var i = 0; i < arguments.length; i++)
		{
			var file = arguments[i];
			//crea el elemento y setea los valores
			var script = document.createElement('script');
			script.type = "text/javascript";
			//genera la ruta del archivo, chequea si hay que navegar ../../
			script.src = this.dir(file);
			//agrega el elemento
			document.head.appendChild(script);
		}

	},

	loader: function()
	{
        //datos al leer el documento
        $(document).on('ready',function()
        {
            //inicia la barra de progreso
            NProgress.start();
        });

        //una vez que haya cargado todos los elementos
        $(window).on('load',function()
        {
            //finaliza la barra de progreso
            NProgress.done();
        });
	},

	lib: function()
	{
		var fileUrl = Project.get("projectUrl") + "/core/lib/";
		var fileExtension = '.js';

		for (var i = 0; i < arguments.length; i++)
		{
			var fileName = arguments[i];
			//crea el elemento y setea los valores
			var script = document.createElement('script');
			script.type = "text/javascript";
			//genera la ruta del archivo, chequea si hay que navegar ../../
			script.src = fileUrl + fileName + "/" + fileName + fileExtension;
			//agrega el comentario
			document.head.appendChild(document.createComment(' incluir ' + fileName + ' '));
			//agrega el elemento
			document.head.appendChild(script);
		}
	}

}