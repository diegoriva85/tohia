Overdue = 
{
    URL: Configure.proyectFolder +'/content_overdue.php', //[cambiame] url para obtener datos de la base (tener en cuenta que debe setearse de donde se llama el archivo)
    
    afterSaveListeners: [],
    
    afterSearchListeners: [],

    afterDropListeners: [],

    afteruploadFileListeners: [],
    
    addAfterSaveListener: function (listener) {
        this.afterSaveListeners.push(listener);
    },
    
    addAfterSearchListener: function (listener) {
        this.afterSearchListeners.push(listener);
    },

    addAfterDropListener: function (listener) {
        this.afterDropListeners.push(listener);
    },
    addAfteruploadFileListener: function (listener) {
        this.afteruploadFileListeners.push(listener);
    },
    
    fireAfterSaveEvent: function (e) {
        for (var i = 0; i < this.afterSaveListeners.length; i++) {
            this.afterSaveListeners[i](e);
        }
    },
    
    fireAfterSearchEvent: function (e) {
        for (var i = 0; i < this.afterSearchListeners.length; i++) {
            this.afterSearchListeners[i](e);
        }
    },

    fireAfterDropEvent: function (e) {
        for (var i = 0; i < this.afterDropListeners.length; i++) {
            this.afterDropListeners[i](e);
        }
    },
    fireAfteruploadFileEvent: function (e) {
        for (var i = 0; i < this.afteruploadFileListeners.length; i++) {
            this.afteruploadFileListeners[i](e);
        }
    },
    
    save: function(id, jurisdiction, cuit, position, admission_date, discharge_date,nya){
        $.ajax(
            {
                url: Overdue.URL,
                type: 'POST',
                dataType: 'json',
                data: {
                    'action':'save',
                    'id' : id,
                    'jurisdiction' : jurisdiction,
                    'cuit' : cuit,
                    'nya':nya,
                    'position' : position,
                    'admission_date' : admission_date,
                    'discharge_date' : discharge_date
                    
                },
                success: function (data) {
                    Overdue.fireAfterSaveEvent(data);
                    $(".ddjj-create-resultados").hide();
                  
                },
                error: function (obj, err) {
                    Overdue.fireAfterSaveEvent(
                        {
                            error: {
                                code: 1,
                                message: err
                            }
                        }
                    );
                }
            }
        );
    },
    load: function(options = null){
        var jurisdiction = $('select.ddjj-jurisdiction').val();
        $.ajax(
            {
                url: Overdue.URL,
                type: 'POST',
                dataType: 'json',
                data: {
                    'action':'load',
                    'jurisdiction': jurisdiction,
                    'options': options
                },
                success: function (data) {
                    Overdue.fireAfterSearchEvent(data);
                },
                error: function (obj, err) {
                    Overdue.fireAfterSearchEvent(
                        {
                            error: {
                                code: 1,
                                message: err
                            }
                        }
                    );
                }
            }
        );
        
    },
    search: function(term){
        $.ajax(
            {
                url: Overdue.URL,
                type: 'POST',
                dataType: 'json',
                data: {
                    'action':'search',
                    'term' : term
                },
                success: function (data) {
                    Overdue.fireAfterSearchEvent(data);
                },
                error: function (obj, err) {
                    Overdue.fireAfterSearchEvent(
                        {
                            error: {
                                code: 1,
                                message: err
                            }
                        }
                    );
                }
            }
        );
        
    },

    drop: function(id){
        $.ajax(
            {
                url: Overdue.URL,
                type: 'POST',
                dataType: 'json',
                data: {
                    'action':'drop',
                    'id' : id
                },
                success: function (data) {
                    Overdue.fireAfterDropEvent(data);
                },
                error: function (obj, err) {
                    Overdue.fireAfterDropEvent(
                        {
                            error: {
                                code: 1,
                                message: err
                            }
                        }
                    );
                }
            }
        );
    },
    uploadFile: function(jurisdiction,file){
        
        var frm = new FormData();
        frm.append('action', 'uploadFile');
        frm.append('jurisdiction',jurisdiction);
        frm.append('file',file);
  
        $.ajax(
        {
            url: Overdue.URL,
            type: 'POST',
            dataType: 'json',
            contentType: false,
            processData: false,
            cache: false,
            data: frm,

            success: function (data) {
                Overdue.fireAfteruploadFileEvent(data);
            },
            error: function (obj, err) {
                Overdue.fireAfteruploadFileEvent({
                        error: {
                            code: 1,
                            message: err
                        }
                    });
                }
        });   
    },

};