<?php
   
require_once __DIR__.'/../../sys'.DIRECTORY_SEPARATOR.'conf'.DIRECTORY_SEPARATOR.'ini.conf';

class SerialStash {
    /**
     * Coloca las variables de sesión en un string serializado, lo encripta y lo devuelve en formato binario
     * @todo Almacenar en la base de datos la key asociada al usuario
     **/
    public static function put () {
        Session::retrieve();
        
        $currentUser = new User();
        $currentUser->load();
        $key = $currentUser->getKey();
        
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        
        $cipher = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, serialize($_SESSION['Affidavit']), MCRYPT_MODE_CBC, $iv);
        $cipher = $iv . $cipher;
        
        return $cipher;
    }
    
    /**
     * Desencripta el contenido proveniente de un archivo cargado
     * @todo Recobrar de la base de datos la key asociada al usuario
     */
    public static function retrieve () {
        $rtn = [
            'error' => [
                'number' => 0,
                'message' => ''
            ],
            'data' => ''
        ];
        
        try {
            if (!isset($_FILES['file']['error']) || is_array($_FILES['file']['error'])) {
                throw new RuntimeException('Invalid parameters');
            }

            switch ($_FILES['file']['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit');
                default:
                    throw new RuntimeException('Unknown error');
            }

            $finfo = new finfo(FILEINFO_MIME_TYPE);

            if (!array_search(
                $finfo->file($_FILES['file']['tmp_name']),
                array(
                    'bin' => 'application/octet-stream'
                ),
                true
            )) {
                throw new RuntimeException('Invalid file format.');
            }

            $file = file_get_contents($_FILES['file']['tmp_name']);

            $currentUser = new User();
            $currentUser->load();
            $key = $currentUser->getKey();
            $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
            $iv_dec = substr($file, 0, $iv_size);
            $cipher = substr($file, $iv_size);
            
            Session::retrieve();
            if (($affidavit = @unserialize(rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $cipher, MCRYPT_MODE_CBC, $iv_dec), "\0")))) {
                $_SESSION['Affidavit'] = $affidavit;
            }
            else {
                throw new RuntimeException('El archivo no contiene información legible');
            }
        }
        catch (RuntimeException $e) {
            $rtn['error']['number'] = 1;
            $rtn['error']['message'] = $e->getMessage();
        }
        
        return $rtn;
    }
}

if ( Security::isAjax() || DEBUG_AJAX || FALSE ) {
    if (isset($_REQUEST['action']) && !empty($_REQUEST['action'])) {
        switch ($_REQUEST['action']) {
            case 'put':
                $cipher = SerialStash::put();
                
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename=declaracion');
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Content-Length: ' . strlen($cipher));

                ob_clean();
                echo $cipher;
                break;
            case 'retrieve':
                echo json_encode(SerialStash::retrieve());
                break;
        }
    }
}

?>