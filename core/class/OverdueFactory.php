<?php

class OverdueFactory {
    private $prototype;

    public function OverdueFactory (Overdue $prototype) {
        $this->prototype = $prototype;
    }

    public function search () {
        $rtn = [];
                        
        $valueSearch = $this->prototype->getValueSearch();
        $jurisdiction = $this->prototype->getJurisdiction();
        $sort = $this->prototype->getSort();

        if (isset($sort) && $sort!= '' )
        {
            $order = ' order by ';
            $count = count($sort);
            for ($i=0; $i < $count ; $i++)
            { 
                foreach ($sort[$i] as $key => $value)
                {
                    $order .= $value.' '; 
                }
            
            $order .= ($count>1) ? $order.', ' : '';

            }
        }
        else
        {
            $order = ' order by o.admission_date desc';
        }

        $search = '%' . $valueSearch . '%';
        $where = ' (o.deleted = 0 AND o.jurisdiction_id = ?) 
                   AND (o.cuit like ?
                   OR o.name like ?
                   OR o.position_id in (SELECT id FROM position where deleted = 0 and name like ?))';

        $dbLink = Database::connect();
        
        //$stmt = $dbLink->prepare('select id, cuit, name, position_id, admission_date, discharge_date, overdue_files_id from overdue where' . $where .' '. $order);
        $stmt = $dbLink->prepare('select o.id, o.cuit, o.name, o.position_id, p.name as position_name, o.admission_date, o.discharge_date, o.overdue_files_id from overdue o left join position p on p.id = o.position_id
                where' . $where .' '. $order);

        $stmt->bind_param('isss', $jurisdiction, $search, $search, $search);
        
       // echo 'select id, cuit, name, position_id, admission_date, discharge_date, overdue_files_id from overdue where' . $where . 'order by admission_date desc';
        
        $stmt->execute();

                
        $stmt->bind_result($id, $cuit, $nya, $position, $position_name, $admission, $discharge, $sourceFile);

        while ($stmt->fetch()) {
            $overduertn = new Overdue(); 
            $overduertn->setId($id); 	
            $overduertn->setCUIT($cuit); 
            $overduertn->setNyA($nya); 
            $overduertn->setSourceFile($sourceFile); 	
            $overduertn->setPosition($position); 	
            $overduertn->setJurisdiction($jurisdiction); 	
            $overduertn->setAdmission($admission); 	
            $overduertn->setDischarge($discharge); 	

            $rtn[] = $overduertn;
        }

        $stmt->close();
        $dbLink->close();

        return $rtn;
        
        
        /*
        $id = $this->prototype->getId();
        $cuit = $this->prototype->getCUIT();
        $nya = $this->prototype->getNyA();
        $sourceFile = $this->prototype->getSourceFile();
        $position = $this->prototype->getPosition();
        $admission = $this->prototype->getAdmission();
        $discharge = $this->prototype->getDischarge();
        */
         /*
        TODO: 

        Volver a reemplazar con php 5.6:

        if (!empty($dato)) {
           //query
        }

        $val_id = empty($id);

        if (!$val_id) {
                $where .= ' and id = ? ';
                $types .= 'i';
                $params[] = &$id;
        }

        
        $val_cuit = empty($cuit);

        if (!$val_cuit) {
                $where .= ' and cuit = ? ';
                $types .= 's';
                $params[] = &$cuit;
        }

        
        $val_sourceFile = empty($sourceFile);

        if (!$val_sourceFile) {
                $where .= ' and overdue_files_id = ? ';
                $types .= 'i';
                $params[] = &$sourceFile;
        }

        
        $val_position = empty($position);

        if (!$val_position) {
                $where .= ' and position_id = ? ';
                $types .= 'i';
                $params[] = &$position;
        }

        
        $val_jurisdiction = empty($jurisdiction);

        if (!$val_jurisdiction) {
                $where .= ' and jurisdiction_id = ? ';
                $types .= 'i';
                $params[] = &$jurisdiction;
        }

        
        $val_admission = empty($admission);

        if (!$val_admission) {
                $where .= ' and admission_date = ? ';
                $types .= 's';
                $params[] = &$admission;
        }

        
        $val_discharge = empty($discharge);

        if (!$val_discharge) {
                $where .= ' and discharge_date = ? ';
                $types .= 's';
                $params[] = &$discharge;
        }

        $stmt = $dbLink->prepare(
           'select
               id,
               cuit,
               overdue_files_id,
               position_id,
               jurisdiction_id,
               admission_date,
               case discharge_date when \'0000-00-00\' then \'\' else discharge_date end as discharge_date
           from
               overdue
           where 
               deleted = 0
               ' . $where . ' order by admission_date desc ' 
       );
*/
    }
    
    public function load () {
        $rtn = [];
                        
        $valueSearch = $this->prototype->getValueSearch();
        $jurisdiction = $this->prototype->getJurisdiction();
        $sort =  $sort = $this->prototype->getSort();

        if (isset($sort) && $sort!= '' )
        {
            $order = ' order by ';
            $count = count($sort);
            for ($i=0; $i < $count ; $i++)
            { 
                foreach ($sort[$i] as $key => $value)
                {
                    $order .= $value.' '; 
                }
            
            $order .= ($count>1) ? $order.', ' : '';

            }
        }
        else
        {
            $order = ' order by o.admission_date desc';
        }

        $search = '%' . $valueSearch . '%';
        $where = '  o.deleted = 0 AND o.jurisdiction_id = ?';

        $dbLink = Database::connect();
        
        // $stmt = $dbLink->prepare('select id, cuit, name, position_id, admission_date, discharge_date, overdue_files_id from overdue where' . $where .' '. $order);

        $stmt = $dbLink->prepare('select o.id, o.cuit, o.name, o.position_id, p.name as position_name, o.admission_date, o.discharge_date, o.overdue_files_id from overdue o left join position p on p.id = o.position_id
                where' . $where .' '. $order);
        $stmt->bind_param('i', $jurisdiction);
        
        $stmt->execute();

        
        
        $stmt->bind_result($id, $cuit, $nya, $position,$position_name, $admission, $discharge, $sourceFile);
        
        while ($stmt->fetch()) {
            $overduertn = new Overdue(); 
            $overduertn->setId($id); 	
            $overduertn->setCUIT($cuit); 
            $overduertn->setNyA($nya); 
            $overduertn->setSourceFile($sourceFile); 	
            $overduertn->setPosition($position); 	
            $overduertn->setJurisdiction($jurisdiction); 	
            $overduertn->setAdmission($admission); 
            if ($discharge != '0000-00-00') {
                $overduertn->setDischarge($discharge);
            }
            else{
                $overduertn->setDischarge("");
            }
             	

            $rtn[] = $overduertn;
        }

        $stmt->close();
        $dbLink->close();

        return $rtn;
        
    }
}

?>