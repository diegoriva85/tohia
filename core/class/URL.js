/**
 * Obtiene y procesa datos de la Url
 * @class URL
 * @author Enzo Yune
 */
var URL =
{
    /**
     * retorna la url del la web
     * @return {string} url 
     */
    http: function()
    {
        //  capturamos la url
        return document.location.href;
    },

    /**
     * lista de parametros
     * @return {array} array asociativo (undefined)
     */
    parameters: function ()
    {
        var http = this.http();

        // si existe el interrogante
        if( http.indexOf('?') > 0 )
        {
            // obtiene los parametros de la url
            var parameters = http.split('?')[1];
            
            // obtenemos un array con cada clave=valor
            var GET = parameters.split('&');
            var length = GET.length; 
            var get = {};
            
            // recorre todo el array de valores
            for(var i = 0; i < length; i++)
            {
                var tmp = GET[i].split('=');
                get[tmp[0]] = unescape(decodeURI(tmp[1]));
            }

            return get;
        }
    },

    /**
     * lista de parametros en formato JSON
     * @return {json} lista de parametros
     */
    printJson: function ()
    {
        return JSON.stringify(this.parameters());
    }
};
