<?php
require_once __DIR__ . '/../../sys' . DIRECTORY_SEPARATOR . 'conf' . DIRECTORY_SEPARATOR . 'ini.conf';
//require_once PATH_PROJECT . '/core/class/PHPMailer.php';

/**
 * [obejto para consumo de los servicios de SADE]
 * @type {[type]}
 * @version: 1.0
 */
class Communicate
{

	/******************************************************/
	/********************** ATRIBUTOS *********************/
	/******************************************************/
		
	//Parametros requeridos para los servicios Soap de SADE
	private static $parameter = array('trace' => 1, 'exception' => 0);
	private static $system 		= 'DDJJ';//nombre del sistema identificador en SADE

	/**************************************************************************/
	/*********************** METODO GENERAL DE EJECUCION **********************/
	/**************************************************************************/

	/**
	* executeServiceSoap Es el metodo encargaod de ejecuta un servicio a traves de Soap 
	* @param  [array] $data []
 	*					$data['url'] 		= '/EUServices/consultaCuitCuil?wsdl';//url del servicio
	*					$data['method'] 	= 'consultaEEDetallado'; //metodo del servicio
	*					$data['request'] 	=(array de parametros para cada caso);parametros de entrada
	*											 
	* @return En caso de exito
	*			$result['success']['response'] //respuesta del servicio invocado
	*		  	$result['success']['message']  // mensje de exito previamente enviado
	*		  En caso de error
	*			$result['error']['response'] //respuesta del servicio invocado con detalle del error
	*		  	$result['error']['message']  // mensje de exito previamente enviado
	*/
	public static function executeServiceSoap($data)
	{
		//@TODO 
		//mandar mensaje discriminado de error
		//mandar el ok de reservido y el if generado 	
		try
		{
			$result = [];
			
			$service = self::soap($data['url']);
			$response = $service->$data['method']($data['request']);
		
			$result['success']['response'] = $response;
			return $result;
		} 
		catch (Exception $e) 
		{
		    // file_put_contents('/var/www/html/TMP/CFRequest.txt', $service->__getLastRequest());
			// file_put_contents('/var/www/html/TMP/CFResponse.txt', $service->__getLastResponse());
			
			if isset($data['request']['data']) || $data['request']['data']
$data['request']['Request']['file']['content']
$data['request']['Request']['file']['content']) 
			//Guardo en base la transaccion fallida y enbio un mial con error
			$errorData['webservice'] = 'URL: '.$data['url'].' METODO: '.$data['method'];
			$errorData['request'] = $data['request'];
			$errorData['response'] = $e->getMessage();
        	self::saveError($errorData);
			$result['error']['response'] = $e->getMessage();
 		
			return $result;
		}
 	}
	/**
	 * [soap retorna un soap para el servicio]
	 * @param  [string] $url [url sel soap]
	 * @return [soap] [respuesta del servicio]
	 */
	private static function soap($url)
	{
		return new SoapClient($url, self::$parameter);
	}
	
	/**
	 * [saveError guaren base el error generado por el consumo de servicio y envia un mail con el request y response]
	 * @param  [string] $url [url sel soap]
	 * @return [soap] [respuesta del servicio]
	 */
	private static function saveError($data)
    {
        Session::retrieve();
           
		$curUserCuit = "No disponible";
		$curUsername = "No disponible";
	    $curUserSADE = "No disponible";
	    $curUserJur  = "No disponible";

	    $webservice = $data['webservice'];
	    $request = json_encode($data['request']);//hayq ue ver que formato es el mas adecuado par ael envio del mail.
	    
	    $response = $data['response'];

        if (isset($_SESSION['loginStatus']) && !empty($_SESSION['loginStatus']))
        {
	        $currentUser = new User();
	        $currentUser->load();
			$curUserCuit = $currentUser->getLoginName();
	        $curUsername = $currentUser->getName();
	        $curUserSADE = $currentUser->getSADEName();
	        $curUserJur = $currentUser->getNameJurisdiction();
        }

        $dbLink = Database::connect();
        $stmt = $dbLink->prepare('INSERT INTO catch_error( `cuit`,`name`,`user_SADE`,`jurisdiction`,`webservice`,`request`,`response`) values (?,?,?,?,?,?,?)');
        $stmt->bind_param('sssssss', $curUserCuit, $curUsername, $curUserSADE, $curUserJur, $webservice, $request, $response);
	    $stmt->execute();
  
        $stmt->close();

        $sql = 'SELECT user_name FROM user_sendmail WHERE inactive = 0 AND deleted  = 0';
        $to = Database::result($sql);//devuelve los mails que se envian los errores

       	//Se prepara el html par aenvio de mail 
		$body = '<table width="100%" border="1" cellspacing="0" cellpadding="0" align="center" valign="top">
				<tr><td>Nombre</td><td>$curUsername</td></tr>
				<tr><td>Cuit</td><td>$curUserCuit</td></tr>
				<tr><td>User_SADE</td><td>$curUserSADE</td></tr>
				<tr><td>jurisdiccion</td><td>$curUserJur</td></tr>
				<tr><td>Webservice</td><td>$webservice</td></tr>
				<tr><td>Request</td><td>$request</td></tr>
				<tr><td>Response</td><td>$response</td></tr>                 
				</table>';

		$subject = 'Alert-DDJJ';
		$parameters = array($curUserCuit, $curUsername, $curUserSADE, $curUserJur, $webservice, $request, $response);
		$fields = array('$curUserCuit','$curUsername','$curUserSADE','$curUserJur','$webservice','$request','$response');
		if (isset($to))
		{
			self::sendMail($to,$subject, $body, $fields, $parameters);		
		}
    }
	/**
	 * [sendMail consume la libreria PHPMailer para envia un mail]
	 * @param  [string] $url [url sel soap]
	 * @return [soap] [respuesta del servicio]
	 */
	
    private static function sendMail($to,$subject, $body, $fields = '', $parameters = '') {

        $result = "";
        $mailer = new PHPMailer();


        $body = str_replace($fields, $parameters, $body);

        #Configuracion SMTP
        $mailer->IsSMTP();
        $mailer->SMTPAuth = true;
        //$mailer->CharSet = 'utf-8';
        //$mailer->Encoding = 'quoted­printable';

        $mailer->Username = 'seclyt.mat@buenosaires.gob.ar';
        $mailer->Password = 'H4_c1rvv';
        $mailer->Host = 'smtp.buenosaires.gob.ar';

        #Remitente y destinatarios
        $mailer->From = 'ddjj@buenosaires.gob.ar';
        $mailer->FromName = "DDJJ";
        //  $mailer->AddReplyTo = 'seclyt@buenosaires.gob.ar';
        if (is_array($to)) {
            foreach ($to as $email) {
                $mailer->AddAddress($email['user_name']);
            }
        } else {
            $mailer->AddAddress($to);
        }

        #Contenido
        $mailer->Subject = $subject;
        $mailer->Body = $body;
        $mailer->AltBody = 'Cuerpo TXT';
        $mailer->CharSet = 'UTF-8';
        #Envio
        if (!$mailer->send()) {
            $result = 'error enviando: ' . $mailer->ErrorInfo;
        } else {
            $result = 'ok';
        }

      //  echo ($result);
    }

	/******************************************************************/
	/*********************** METODOS DE COMINICACION ******************/
	/*****************************************************************/

	/**
	 * [userCuit retorna los datos de un usuario de SADE bucnado por cuit]
	 * @param  [string] $cuit [cuit del suario a buscar]
	 *                       ej.:
	 *                       	$cuit = '20316571531'
	 * @return [array]       [description]
	 *         				ej:
	 *						$result['name']		 	= 'Diego Riva';
	 *						$result['usuario'] 		= 'DRIVA';
	 *						$result['cuit'] 		= '20316571531';
	 *						$result['email'] 		= 'driva@buenosaires.gob.ar';
	 *						$result['cargo'] 		= 'Analista';
	 *						$result['reparticion'] 	= '';
	 *						$result['jurisdiction'] = '';
	 *						$result['distribution'] = 'SECLYT';
	 *						$result['sector'] 		= '03';
	 * 	
	 */
	public static function userCuitSADE($cuit)
	{
		try 
		{
			$result = [];

			$data['url'] 		= SADE_CONSULTA_CUIT_CUIL;
			$data['method'] 	= 'consultaCuitCuil'; //metodo del servicio
			$data['request']['consultaCuitCuilRequest'] = $cuit;

			$result = self::executeServiceSoap($data);
			if(isset($result['error']))
			{
				throw new Exception($result['error']['response']);  
			}
			$result['succes']['message']  = 'mensaje en caso de exito';

			$user = $result['success']['response']->consultaCuitCuilResponse;

			$result['name']		 	= $user->apellidoNombre;
			$result['usuario'] 		= $user->usuario;
			$result['cuit'] 		= $user->cuitCuil;
			$result['email'] 		= $user->email;
			$result['cargo'] 		= $user->cargo;
			$result['reparticion'] 	= $user->reparticion;
			$result['jurisdiction'] = $user->jurisdiccion;
			$result['distribution'] = $user->reparticion;
			$result['sector'] 		= $user->sector;

			return $result;
		}
		catch (Exception $e)
		{
			$result['error']['response'] = $e->getMessage();
			$error_soap = strchr($result['error']['response'],'SOAP-ERROR');
			$error_message = 'Error al ejecutar el servicio de consulta CUIT/CUIL';
			$result['error']['message'] = ($error_soap) ? $error_message : $result['error']['response'];
			return $result;
		}
	 	
	}
		/**
	 * [userCuit retorna los datos de un usuario de SADE buscando por nombre]
	 * @param  [string] $cuit [cuit del usuario a buscar]
	 *                       ej.:
	 *                       	$user = 'DRIVA'
	 * @return [array]       [description]
	 *         				ej:
	 *						$result['name']		 	= 'Diego Riva';
	 *						$result['usuario'] 		= 'DRIVA';
	 *						$result['cuit'] 		= '20316571531';
	 *						$result['email'] 		= 'driva@buenosaires.gob.ar';
	 *						$result['cargo'] 		= 'Analista';
	 *						$result['reparticion'] 	= '';
	 *						$result['jurisdiction'] = '';
	 *						$result['distribution'] = 'SECLYT';
	 *						$result['sector'] 		= '03';
	 * 	
	 */
	public static function userSADE($userSADE)
	{
		try 
		{
			$result = [];
	
			$data['url'] 		= SADE_CONSULTA_USUARIO_SADE; //http://sade-mule.hml.gcba.gob.ar/EUServices/consultaUsuarioSade?wsdl
			$data['method'] 	= 'consultaUsuarioSade'; //metodo del servicio
			$data['request']['consultaUsuarioSadeRequest'] = $userSADE;
			
			$result = self::executeServiceSoap($data);
			if(isset($result['error']))
			{
				throw new Exception($result['error']['response']);  
			}
			$result['succes']['message']  = 'mensaje en caso de exito';


			$user = $result['success']['response']->consultaUsuarioSadeResponse;
			$result['name']		 	= $user->apellidoNombre;
			$result['usuario'] 		= $user->usuario;
			$result['cuit'] 		= $user->cuitCuil;
			$result['email'] 		= $user->email;
			$result['cargo'] 		= $user->cargo;
			$result['reparticion'] 	= $user->reparticion;
			$result['jurisdiction'] = $user->jurisdiccion;
			$result['distribution'] = $user->reparticion;
			$result['sector'] 		= $user->sector;

			return $result;
		}
		catch (Exception $e)
		{
			$result['error']['response'] = $e->getMessage();
			$error_soap = strchr($result['error']['response'],'SOAP-ERROR');
			$error_message = 'Error al ejecutar el servicio de consulta de datos del usuario';
			$result['error']['message'] = ($error_soap) ? $error_message : $result['error']['response'];
			return $result;
		}
	 	
	}
	

	/**
	 * [consumeForm envia informacion a un formualrio controlado de SADE]
	 * @param  [string] $option ['form_name'] [el nombre del formulario que se decea consumir ]
	 * @param  [array] $option ['form_data'] [detalle de los campos que se envian al formulario]
	 *                       ej:
	 *                       	$form_name = 'FFCC_Flow Cerrado_IFCES';
	 *                       	$form_data[0]['etiqueta'] ='¿Documentación Correcta?';
	 *                       	$form_data[0]['idFormComp'] =755669;
	 *                       	$form_data[0]['inputName'] ='documentacion_correcta';
	 *                       	$form_data[0]['valorStr'] ='Si';
	 *                       	$form_data[1]['etiqueta'] ='¿Se solicitó Prorroga?';
	 *                       	$form_data[1]['idFormComp'] =755670;
	 *                       	$form_data[1]['inputName'] ='solicito_prorroga';
	 *                       	$form_data[1]['valorStr'] ='No';
 	 *      					$successMsg = 'Envio exitoso';
	 *            				$errorMsg = 'Error en el envio';
	 * @return [array]       [mensaje detallando que accion se realizo]
	 *                       
	 *                      ej:
	 *        				$result['return'] = $rtn;
	 *        			 	$result['success']['message'] = $data ['successMsg'];
	 *
	 */
	public static function consumeFormSADE($option)
	{
		try 
		{
			$result = [];
	
			$data['url'] 		= SADE_TRANSACTION_SERVICE; //http://euf.hml.gcba.gob.ar/dynform-web/transaccionService?wsdl
			$data['method'] 	= 'grabarTransaccion'; //metodo del servicio
			$data['request']['arg0']['fechaCreacion'] 		= date('Y-m-d\TH:i:s.u-P');
			$data['request']['arg0']['nombreFormulario'] 	= $option['form_name'];
			$data['request']['arg0']['sistOrigen'] 			= self::$system;
			$data['request']['arg0']['uuid'] 				= '';
			$data['request']['arg0']['valorFormComps'] 		= $option['form_data'];
			$result = self::executeServiceSoap($data);
			if(isset($result['error']))
			{
				throw new Exception($result['error']['response']); 
			}
			$result['succes']['message']  = 'mensaje en caso de exito';
			return $result;

		} 
		catch (Exception $e)
		{
			$result['error']['response'] = $e->getMessage();
			$result['error']['message'] = 'Ha ocurrido un error, por favor exporte su declaración e intente nuevamente en unos minutos. Gracias';
			return $result;
		}

	}
	/**
	 * [consumeForm envia informacion a un formulario controlado de SADE]
	 * @param  [string] $trans [el numero de transccaccion realizada]
	 * @return [array]       [mensaje detallando que accion se realizo]
	 *                       
	 *                      ej:
	 *        				$result['return'] = $rtn;
	 *        			 	$result['success']['message'] = $data ['successMsg'];
	 *
	 */
	public static function searchDataTrans($trans)
	{
		try 
		{
			$result = [];
	
			$data['url'] 		= SADE_TRANSACTION_SERVICE; //http://euf.hml.gcba.gob.ar/dynform-web/transaccionService?wsdl
			$data['method'] 	= 'buscarTransaccionPorUUID'; //metodo del servicio
			$data['request']['arg0']= $trans;

			$result = self::executeServiceSoap($data);
			if(isset($result['error']))
			{
				throw new Exception($result['error']['response']); 
			}
			$result['succes']['message']  = 'mensaje en caso de exito';

			return $result;
		} 
		catch (Exception $e)
		{
			$result['error']['response'] = $e->getMessage();
			$result['error']['message'] = 'Ha ocurrido un error, por favor exporte su declaración e intente nuevamente en unos minutos. Gracias';
			return $result;
		}

	}
	
	/**
	 * [generateDoc firma un documento y/o genera un documento oficial GEDO]
	 * @param  [array] $option [los datos requeridos para la firma, o se envia un id de transaccion o un binario]
	 *                  ej:
	 *					$option['id_transaction'] = '928532'; //numero de transaccion
	 *					$option['file'] = BINARIO; // ARCHIVO QUE SE QUIERE TRASNFORMAR EN GEDO
	 *					$option['acronym'] = 'DJPUB';
	 *					$option['references']='Declaración Jurada';		
	 *					$option['user'] = 'PCASTELLO';//Usuario SADE
	 *      			$option ['successMsg'] = 'Documento creado con exito';
	 *           		$option ['errorMsg'] = 'Error al crear el documento oficial';
	 * 
	 *                       
	 * @return [array] [En caso de exito responde con los datos del numero de gedo y los mensjes de exito]
	 *                 ej:
	 *                 $result['return'][numero] = IF-2018-00138419-   -DGGDOC;
	 *                 $result['return'][urlArchivoGenerado] ='SADE/2018/DGGDOC/00/138/IF-2018-00138419-   -DGGDOC/IF-2018-00138419-   -DGGDOC.pdf';
	 *                 $result['success']['message'] = $option ['successMsg'];
	 *
	 */
 	public static function generateDocSADE($option)
	{
		try 
		{
			$result = [];

			$data['url'] 		= SADE_GEDO; //http://sade-mule.hml.gcba.gob.ar/GEDOServices/generarDocumento?wsdl
			$data['method']     = 'generarDocumentoGEDO'; //metodo del servicio

			//el servicio puede firmar un documento existente
			if (isset($option['id_transaction']))
			{
				$data['request']['request']['idTransaccion'] = $option['id_transaction'];
				$data['request']['request']['data'] = ''; //este campo lo espera siempre
			}
			//o un binario enviado a firmar
			else
			{
				$data['request']['data'] = $option['file'];
			}

			$data['request']['request']['acronimoTipoDocumento'] = $option['acronym'];
			$data['request']['request']['referencia'] = $option['references'];
			$data['request']['request']['sistemaOrigen'] = self::$system;
			$data['request']['request']['tipoArchivo'] = 'pdf';
			$data['request']['request']['listaUsuariosDestinatariosExternos'] = array();
			$data['request']['request']['metaDatos'] = array();
			$data['request']['request']['usuario'] = $option['user'];

			$result = self::executeServiceSoap($data);
			if(isset($result['error']))
			{
				throw new Exception($result['error']['response']); 
			}
			$result['succes']['message']  = 'mensaje en caso de exito';

 			return $result;
		} 
		catch (Exception $e) 
		{
			$result['error']['response'] = $e->getMessage();
			$result['error']['message'] = 'Ha ocurrido un error, por favor exporte su declaración e intente nuevamente en unos minutos. Gracias';
 			return $result;
		}
	}

	/**
	 * [getContentDocSADE devuelve el contenido del decumento oficial consultado]
	 * @param  [array] $data [los datos requeridos para la consulta]
	 * 					ej:
	 * 					$data['doc'] = 'IF-2017-03206381-   -MGEYA'; 
	 *					$data['user'] = 'PCASTELLO';
	 *					$data ['errorMsg'] = 'Mensaje de error'; //OPCIONAL
	 *							
	 * @return [array] [listado de los expedientes en el buzon ]
	 *        				$result = Binario con el contenido de la documento consultado;
	 *        			 	$result['error']['message'] = mensaje de error
	 *
	 */
	
	public static function getContentDocSADE($data)
	{    
		try 
		{
			$result= [];

 			$data['url'] = SADE_GEDO_CONSULTA;
			$data['method'] 	= 'consultarDocumentoPdf'; //metodo del servicio
			$data['request']['request']['numeroDocumento'] = $data['doc'];
			$data['request']['request']['usuarioConsulta'] = $data['user'];
			$data['request']['request']['assignee'] = '';
            $data['request']['request']['numeroEspecial'] = '';

			$result = self::executeServiceSoap($data);
			if(isset($result['error']))
			{
				throw new Exception($result['error']['response']); 
			}
			$result['succes']['message']  = 'mensaje en caso de exito';

			return $result;
		} 
		catch (Exception $e) 
		{
         //   $result['error']['message'] = isset($data['errorMsg']) ? $data['errorMsg'] : $e->getMessage();
			$result['error']['response'] = $e->getMessage();
			$result['error']['message'] = 'Ha ocurrido un error intentando obtener los datos del documento consultado. Por favor intente nuevamente en unos minutos.';
 			return $result;

		}

	}
	/**
	 * [sendFileFilenet envie l archivo encriptado e filenet]
	 * @param  [file]  $file_encrypted= 'BINARIA ENCRYPTADO'; 
	 *							
	 * @return [array] [listado de los expedientes en el buzon ]

	 *        				$result['success']['return'] = Binario con el contenido de la documento consultado;
	 *        			 	$result['success']['message'] = $data ['successMsg'];
	 *        			 	
	 *
	 */
	public static function sendFileFilenet($file_encrypted)
	{    
		try 
		{
			$result= [];

			$data['url'] 		= FILENET; //http://euf.hml.gcba.gob.ar/dynform-web/transaccionService?wsdl
			$data['method'] 	= 'Create'; //metodo del servicio
			$data['request']['Request']['file']['filename']= 'privado.pdf';
			$data['request']['Request']['file']['content']= base64_encode($file_encrypted);
			$data['request']['Request']['document']['classname']= 'Document';
			$data['request']['Request']['document']['properties']['property']['name']= 'DocumentTitle';
			$data['request']['Request']['document']['properties']['property']['value']= 'Declaracion Jurada';

			$result = self::executeServiceSoap($data);

			if(isset($result['error']))
			{
				throw new Exception($result['error']['response']); 
			}
			$result['succes']['message']  = 'mensaje en caso de exito';
 
 			return $result;
		} 
		catch (Exception $e) 
		{
			$result['error']['response'] = $e->getMessage();
			$result['error']['message'] = 'Ha ocurrido un error, por favor exporte su declaración e intente nuevamente en unos minutos. Gracias';
 			return $result;
		}
	}
	/**
	 * [downloadFileFilenet devuelve el archivo generado]
	 * @param  [file]  $file_encrypted= 'BINARIA ENCRYPTADO'; 
	 *							
	 * @return [array] [listado de los expedientes en el buzon ]

	 *        				$result['success']['return'] = Binario con el contenido de la documento consultado;
	 *        			 	$result['success']['message'] = $data ['successMsg'];
	 *        			 	
	 *
	 */
	public static function downloadFileFilenet($id_file)
	{    
		try 
		{
			$result= [];

			$data['url'] 		= FILENET; //http://euf.hml.gcba.gob.ar/dynform-web/transaccionService?wsdl
			$data['method'] 	= 'Download'; //metodo del servicio
			$data['request']['Request']['Id'] = $id_file;

			$result = self::executeServiceSoap($data);
			if(isset($result['error']))
			{
				throw new Exception($result['error']['response']); 
			}
			$result['succes']['message']  = 'mensaje en caso de exito';
  			 //          $filenet_id = substr($result['success']['response']->Response->document->properties->property->value;, 1, 36);
			
			// if (empty($filenet_id)) 
			// {
			//     throw new Exception('No fue posible almacenar los datos de forma encriptada. Por su seguridad, la declaración no fue enviada');
			// }
			
			return $result;
		} 
		catch (Exception $e) 
		{
			$result['error']['response'] = $e->getMessage();
			$result['error']['message'] = 'Ha ocurrido un error, por favor exporte su declaración e intente nuevamente en unos minutos. Gracias';
 			return $result;
		}
	}
	/**
	 * [signFileLivecycle devuelve el contenido del decumento oficial consultado]
	 * @param  [file]	$pdfbinary = $pdf->Output('S');la repsuesta del metodo fpdf 
	 *					$data ['successMsg'] = 'Mensaje de éxito';
	 *					$data ['errorMsg'] = 'Mensaje de error';
	 *							
	 * @return [array] [listado de los expedientes en el buzon ]

	 *        				$result['success']['return'] = Binario con el contenido de la documento consultado;
	 *        			 	$result['success']['message'] = $data ['successMsg'];
	 *        			 	
	 *
	 */
	public static function signFileLivecycle($pdf)
	{    
		try 
		{
			$soap = new SoapClient(LIVECYCLE_SIGNATURE_SERVICE . '?wsdl', array('trace' => 1,
																            'exception' => 0,
																            'login' => 'ddjj-seclyt',
																            'password' => 'rohirrim3'
																            )
	        	);

        $soap->__setLocation(LIVECYCLE_SIGNATURE_SERVICE . '?blob=base64');

        $rst = $soap->addSignatureField(
                array(
                    'inPDFDoc' => array(
                        'contentType' => 'application/pdf',
                        'binaryData' => $pdf->Output('S')
                    ),
                    'signatureFieldName' => 'Firma',
                    'pageNumber' => $pdf->PageNo(),
                    'positionRectangle' => array(
                        'height' => 50,
                        'lowerLeftX' => 20,
                        'lowerLeftY' => 20,
                        'width' => 200
                    )
                )
        );

        $rst = $soap->sign(
                array(
                    'inPDFDoc' => array(
                        'contentType' => 'application/pdf',
                        'binaryData' => $rst->outPDFDoc->binaryData
                    ),
                    'signatureFieldName' => 'Firma',
                    'credential' => array(
                        'alias' => 'DDJJSECLYT'
                    ),
                    'hashAlgorithm' => 'SHA1',
                    'reason' => 'Documento generado por DDJJ',
                    'location' => 'CABA, Argentina',
                    'contactInfo' => 'ASI',
                    'embedRevocationInfo' => true,
                    'appearanceOptionSpec' => array(
                        'appearanceType' => 'NOGRAPHIC',
                        'logoOpacity' => 0,
                        'showDN' => true,
                        'showDate' => true,
                        'showDefaultLogo' => false,
                        'showLabels' => true,
                        'showLocation' => false,
                        'showName' => true,
                        'showReason' => false,
                        'textDirection' => 'AUTO'
                    )
                )
        );

		return $rst->outPDFDoc->binaryData;
		} 
		catch (Exception $e) 
		{
			$result['error']['response'] = $e->getMessage();
			$result['error']['message'] = 'Ha ocurrido un error firmando el documento, por favor exporte su declaración e intente nuevamente en unos minutos. Gracias';
 			return $result;
		}

	}



	/**
	 * [loginAD devuelve el contenido del decumento oficial consultado]
	 * @param  [array] $data [los datos requeridos para la consulta]
	 * 					ej:
	 * 					$data ['loginName'] = '20303030'; 
	 *					$data ['pass'] = '1234';
	 *							
	 * @return [array] [el objecto info con los datos de AD y el mensaje de success, o un mensaje de error ]
	 *
	 *        			$result['success']['user'] = array de arrays con "cn", "distinguishedName", "dn", "samAccountName", "mail", "userPrincipalName", "pwdLastSet", "physicalDeliveryOfficeName","objectguid"
	 *
	 */
	public static function loginAD($data)
	{    
		$result = [];
		try 
		{
            //conexion al LDAP
            $ldapConnection = ldap_connect(LDAP_SERVICE);

            ldap_set_option($ldapConnection, LDAP_OPT_REFERRALS, 0);
            ldap_set_option($ldapConnection, LDAP_OPT_PROTOCOL_VERSION, 3);

            //usuario de servicio otorgado para la app por la ASI
            // $conexionUser = 'svc_sadeddjj@ad.buenosaires.gob.ar';
            // $conexionPass = '+xxMVN-NQ3S8M5q9';

            //chequea que se pueda conectar con el AD con el usuario administrador
            $logInAdmin = @ldap_bind($ldapConnection, CONEXION_AD_USER, CONEXION_AD_PASS);

            if ($logInAdmin)
            {
                //atributos del AD
                $dn = "DC=ad,DC=buenosaires,DC=gob,DC=ar";
                //condicion de busqueda para que corresponda al CUIT del usuario
                $filter = "(|(samAccountName=". $data['loginName'] ."))";
                //filtro para obtener ciertos campos del usuario 
                $justthese = array("cn", "distinguishedName", "dn", "samAccountName", "mail", "userPrincipalName", "pwdLastSet", "physicalDeliveryOfficeName","objectguid");

                //busqueda parametrizada
                $ldapSearch = ldap_search($ldapConnection, $dn, $filter, $justthese);
                $info = ldap_get_entries($ldapConnection, $ldapSearch);

                //si existe al menos una cuenta asociada al CUIT del usuario
                if($info["count"] > 0)
                {
                    //obtiene el nombre principal de la cuenta del usuario
                    $getUserName = $info[0]['userprincipalname'][0];

                    //chequea que la cuenta se corresponda con la clave
                    $logIn = @ldap_bind($ldapConnection,$getUserName,$data['pass']);

                    //si se corresponde la clave con el CUIT
                    if ($logIn)
                    {
                    	$result['success']['user'] = $info[0];
                    }
                    else
                    {
                        throw new Exception('Usuario y/o contraseña incorrectos');
                    }
                }
                else
                {
                    throw new Exception('Usuario inexistente');
                }
                
            }
            else 
            {
                throw new Exception('El sistema no se pudo conectar inténtelo mas tarde');
            }
        return $result;
        }
        catch (Exception $e)
        {
			$result['error']['message'] = $e->getMessage();
		//	$result['error']['message'] = 'mensaje en caso de error';
 			return $result;
        }
    }



 }

?>
