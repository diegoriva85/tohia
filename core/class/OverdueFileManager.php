<?php

class OverdueFileManager {
    private $id;
    private $name;
    private $jurisdiction;


    public function OverdueFileManager () {}

    public function upload ($file) {
        
        $rtn = ['error' => 
                    ['code' => 0,
                     'message' => ''
                     ],
                'lines' => []
                    
                ];
        try {
            $dbLink = Database::connect();

            $fileContent = file_get_contents($file);
            $fileName = $this->name;

            $currentUser = new User();
            $currentUser->save();
            $currentUser->load();
            $currentUser_id = $currentUser->getId();

            $stmt = $dbLink->prepare('insert into overdue_files (name, content, created_by) values (?,?,?)');
            $stmt->bind_param('ssi',$fileName,$fileContent ,$currentUser_id);
            $stmt->execute();
            $overduefileId = $dbLink->insert_id;
            $stmt->close();
  
            if ($overduefileId != 0) {
                
                $filecsv = fopen($file,"r");
                $regNum = 0;

                while (($linecsv = fgetcsv($filecsv, 1000, ",")) != false) {
                    if ($regNum == 0) {
                        $regNum++;
                        continue;
                    }

                    if (is_numeric($linecsv[0]) && (count($linecsv) >= 3) ) {

                        $overdue = new Overdue();
                        $overdue->setJurisdiction($this->jurisdiction);
                        $overdue->setCUIT(trim($linecsv[0]));
                        $overdue->setNyA(trim($linecsv[1]));
                        $overdue->setPosition(intval(trim($linecsv[2])));
                        $overdue->setAdmission(trim($linecsv[3]));
                        $overdue->setDischarge(trim((count($linecsv) > 4 ? $linecsv[4] : '')));
                        $overdue->setSourceFile($overduefileId);

                        $overdue->load();

                        $getCuit = $overdue->getCUIT();
                        $cuitlen = strlen($getCuit);
                        $getPosition = $overdue->getPosition();
                        $getAdmission = $overdue->getAdmission();
                        $getDischarge = $overdue->getDischarge();
 
                        if (empty($getCuit) || empty($getPosition) || empty($getAdmission) || ($cuitlen < 11)) {
                            $rtn['lines'][] = ['number' => $regNum, 
                             'error' => 
                                ['code' => 1,
                                 'message' => 'Faltan datos para realizar la carga del registro numero '. $regNum
                                 ]
                            ];
                        }else if (!empty($getDischarge) && (strtotime($getAdmission) > strtotime($getDischarge))) {
                            $rtn['lines'][] = ['number' => $regNum, 
                             'error' => 
                                ['code' => 1,
                                 'message' => 'La fecha de egreso tiene que ser mayor a la fecha de ingreso en el registro numero '. $regNum
                                 ]
                            ];
                        }else if (!validateCuit($getCuit)) {
                            $rtn['lines'][] = ['number' => $regNum, 
                             'error' => 
                                ['code' => 1,
                                 'message' => 'CUIT erroneo o incompleto '. $regNum
                                 ]
                            ];
                        }else {
                            try {
                                $overdue->save(1);
                            }
                            catch(Exception $e) {
                               $rtn['lines'][] = ['number' => $regNum, 
                                 'error' => 
                                    ['code' => 1,
                                     'message' => $e->getMessage().' en la linea '. $regNum
                                     ]
                                ];
                            }
                        }
                        unset($overdue); 
                    }
                    else {
                        $rtn['lines'][] = ['number' => $regNum, 
                             'error' => 
                                ['code' => 1,
                                 'message' => 'No fue posible procesar la linea '. $regNum
                                 ]
                        ];
                    }

                    $regNum++;
                }
             fclose($filecsv);

            }
        }
         catch (Exception $e) {
               $rtn['error']['code'] = 1;
               $rtn['error']['message'] = $e->getMessage();
           }

    return $rtn;

    }    
    public function validateCuit($cuit){
        $rtn = false;

        if (!preg_match("/^\d[0-9]{10}/", $cuit)) {
           return $rtn;
        }

        $acum = 0;
        $digits = str_split($cuit);
        $digit = array_pop($digits);
            
        for( $i = 0; $i < count( $digits ); $i++ ){
            $acum += $digits[ 9 - $i ] * ( 2 + ( $i % 6 ) );
        }
        $verif = 11 - ( $acum % 11 );
        $verif = $verif == 11? 0 : $verif;
        

        if (intval($digit) === $verif)
        {
            $rtn = true;
        }
        
        return $rtn;
    }
    public function setId ($id) {
        $this->id = $id;
    }
    public function setName ($name) {
        $this->name = $name;
    }
    public function setJurisdiction ($jurisdiction) {
        $this->jurisdiction = $jurisdiction;
    }
    public function getId () {
        return $this->id;
    }
    public function getName () {
        return $this->name;
    }
    public function getJurisdiction () {
        return $this->curisdiction;
    }

}

?>
