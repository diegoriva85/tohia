var FormCriteria = {
    url: window.urlPrimaryPath +'/core/class/FormCriteria.php',
    /**
     * Devuelve un objeto encargado de administar la información de los paneles.
     * @returns {Object}
     **/
    getPanelHandler: function () {
        var obj = {
            isLoaded: 0,
            data: null,
            loadListeners: [],
            saveListeners: [],
            addLoadListener: function (listener) {
                this.loadListeners.push(listener);
                
                return listener;
            },
            removeLoadListener: function (listener) {
                for (var i = 0; i < this.loadListeners.length; i++) {
                    if (this.loadListeners[i] === listener) {
                        this.loadListeners.splice(i, 1);
                    }
                }
            },
            addSaveListener: function (listener) {
                this.saveListeners.push(listener);
                
                return listener;
            },
            removeSaveListener: function (listener) {
                for (var i = 0; i < this.saveListeners.length; i++) {
                    if (this.saveListeners[i] === listener) {
                        this.saveListeners.splice(i, 1);
                    }
                }
            },
            fireLoadEvent: function (event) {
                this.data = event;
                
                for (var i = 0; i < this.loadListeners.length; i++) {
                    this.loadListeners[i](event);
                }
                
                this.isLoaded = 1;
            },
            fireSaveEvent: function (event) {
                for (var i = 0; i < this.saveListeners.length; i++) {
                    this.saveListeners[i](event);
                }
            },
            load: function () {
                $.ajax(
                    {
                        url: FormCriteria.url,
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            action: 'getPanels'
                        },
                        success: function (data) {
                            obj.fireLoadEvent(data);
                        },
                        error: function () {

                        }
                    }
                );
            },
            save: function (panel) {
                panelObj = {
                    id: panel.id,
                    fields: []
                };

                for (var field in panel.fields) {
                    panelObj.fields.push(
                        {
                            id: panel.fields[field].id,
                            name: $(panel.fields[field].nameField).val(),
                            description: $(panel.fields[field].descField).val()
                        }
                    );
                }

                $.ajax(
                    {
                        url: FormCriteria.url,
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            action: 'savePanel',
                            panel: JSON.stringify(panelObj)
                        },
                        success: function (data) {
                            obj.fireSaveEvent(data);
                        },
                        error: function () {
                            obj.fireSaveEvent(-1);
                        }
                    }
                );
            }
        };
        
        return obj;
    }
};