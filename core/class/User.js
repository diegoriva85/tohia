User =
{
	//[cambiame] url para obtener datos de la base (tener en cuenta que debe setearse de donde se llama el archivo)
	//recomendacion ruta absoluta
	url: window.location.origin + '/area51/core/class/User.php',
	// url: Project.get('pathClass') +  '/User.php',
	id: 2,
	/**
	 * [mostrar AJAX en la consola solo para este Objeto]
	 * @return {[type]} [description]
	 */
	isAjax: function()
	{
		//poner en true si se desea debuggear este objeto en particular 
		var view = false;
		//chequea la condicion general para ver Ajax hasta habilitado para todos los ojbetos o para el objeto en cuestion (this)
		return (Security.isAjax() || view);
	},
	/**
	 * cheque si el usuario esta logueado
	 * @return {[boolean]}     [true si esta logueado]
	 */
	active: function()
	{
		var data =
		{
			"action": "active"
		};

		var that = this;

		// datos a enviar
		if (this.isAjax()){console.log("AJAX:SENT  =>",data);}

		data = $(this).serialize() + "&" + $.param(data);

		// datos serializados para url
		if (this.isAjax()){console.log("AJAX:URL  =>",data);}

		var exist = false;

		$.ajax(
		{
			type: "post",
			dataType: "json",
			url: this.url,
			async: false,//debe estar debido a que se retorna un valor
			data: data,
			success: function(data)
			{
				// datos recibidos
				if (that.isAjax()){console.log("AJAX:GET  =>",data);}

				// indicar lo que se quiere procesar con los datos
				exist = data;
			},
			error: function()
			{
				// mensaje
				var msg = 'no se pudo procesar';
				//datos no procesados por error
				if (that.isAjax()){console.log("AJAX:GET  =>", msg);}
			}
		});

		return exist;
	},

	/**
	 * cheque si el dni existe en base
	 * @param  {[integer]} dni [numero a cotejar en base]
	 * @return {[boolean]}     [true si existe]
	 */
	exist: function(dni)
	{
		var data =
		{
			"action": "exist",
			"dni": dni
		};

		var that = this;
		// datos a enviar
		if (this.isAjax()){console.log("AJAX:SENT  =>",data);}

		data = $(this).serialize() + "&" + $.param(data);

		// datos serializados para url
		if (this.isAjax()){console.log("AJAX:URL  =>",data);}

		var exist = false;

		$.ajax(
		{
			type: "post",
			dataType: "json",
			url: this.url,
			async: false,//debe estar debido a que se retorna un valor
			data: data,
			success: function(data)
			{
				// datos recibidos
				if (that.isAjax()){console.log("AJAX:GET  =>",data);}

				// indicar lo que se quiere procesar con los datos
				exist = data.login;
			},
			error: function()
			{
				// mensaje
				var msg = 'no se pudo procesar';
				//datos no procesados por error
				if (that.isAjax()){console.log("AJAX:GET  =>", msg);}
			}
		});

		return exist;
	},

	/**
	 * retorna el rol de un usuario
	 * @return {integer} [numero del rol]
	 */
	rol: function()
	{
		var data =
		{
			"action": "rol",
			"id": this.id
		};

		var that = this;
		// datos a enviar
		if (this.isAjax()){console.log("AJAX:SENT  =>",data);}

		data = $(this).serialize() + "&" + $.param(data);

		// datos serializados para url
		if (this.isAjax()){console.log("AJAX:URL  =>",data);}

		var rol = -1;//incializa el rol en -1 indicando que no existe

		$.ajax(
		{
			type: "post",
			dataType: "json",
			url: this.url,
			async: false,//debe estar debido a que se retorna un valor
			data: data,
			success: function(data)
			{
				// datos recibidos
				if (that.isAjax()){console.log("AJAX:GET  =>",data);}

				// indicar lo que se quiere procesar con los datos
				rol = data.id_rol;
			},
			error: function()
			{
				// mensaje
				var msg = 'no se pudo procesar';
				//datos no procesados por error
				if (that.isAjax()){console.log("AJAX:GET  =>", msg);}
			}
		});
		return rol;
	},


	/**
	 * retorna los datos de un usuario
	 */
	getPhoto: function(id)
	{
		var data =
		{
			"action": "getPhoto",
			"id": id
		};

		var that = this;
		// datos a enviar
		if (this.isAjax()){console.log("AJAX:SENT  =>",data);}

		data = $(this).serialize() + "&" + $.param(data);

		// datos serializados para url
		if (this.isAjax()){console.log("AJAX:URL  =>",data);}

		//path por defecto si hay un error y no definio imagen
		var photo = Project.get("pathMediaUrl")+"/img/user_default.png";

		$.ajax(
		{
			type: "post",
			dataType: "json",
			async: false,//debe estar debido a que se retorna un valor
			url: this.url,
			data: data,
			success: function(data)
			{
				// datos recibidos
				if (that.isAjax()){console.log("AJAX:GET  =>",data);}

				//si existe foto la setea
				if(data.photo)
				{
					photo = "data:image/jpeg;base64," + data.photo;
				}
			},
			error: function()
			{
				// mensaje
				var msg = 'no se pudo procesar';
				//datos no procesados por error
				if (that.isAjax()){console.log("AJAX:GET  =>", msg);}
			}
		});

		return photo;
	},

	/**
	 * retorna los datos de un usuario
	 */
	data: function()
	{
		var data =
		{
			"action": "data",
			"id": this.id
		};

		var that = this;
		// datos a enviar
		if (this.isAjax()){console.log("AJAX:SENT  =>",data);}

		data = $(this).serialize() + "&" + $.param(data);

		// datos serializados para url
		if (this.isAjax()){console.log("AJAX:URL  =>",data);}

		$.ajax(
		{
			type: "post",
			dataType: "json",
			url: this.url,
			data: data,
			success: function(data)
			{
				// datos recibidos
				if (that.isAjax()){console.log("AJAX:GET  =>",data);}

				$.each(data, function( key, value )
				{
					console.log('key ' + key, 'value ' + value);
				});
			},
			error: function()
			{
				// mensaje
				var msg = 'no se pudo procesar';
				//datos no procesados por error
				if (that.isAjax()){console.log("AJAX:GET  =>", msg);}
			}
		});
	},

	/**
	 * carga los datos del perfil de un usuario
	 */
	profile: function(id)
	{
		var data =
		{
			"action": "profile",
			"id": id
		};

		var that = this;
		// datos a enviar
		if (this.isAjax()){console.log("AJAX:SENT  =>",data);}

		data = $(this).serialize() + "&" + $.param(data);

		// datos serializados para url
		if (this.isAjax()){console.log("AJAX:URL  =>",data);}

		$.ajax(
		{
			type: "post",
			dataType: "json",
			url: this.url,
			data: data,
			success: function(data)
			{
				// datos recibidos
				if (that.isAjax()){console.log("AJAX:GET  =>",data);}

				//si hay foto
				if(data.photo)
				{
					$(".user-photo").attr("src", "data:image/jpeg;base64," + data.photo);
				}


				$.each(data, function( key, value )
				{
					//debug
					// console.log(key, value);
	                
	                var inputNew;

	                switch(key)
	                {
	                	case 'gender': inputNew = $("#clon #input-gender").clone(); break;
	                	case 'birthday': inputNew = $("#clon #input-date").clone(); break;
	                	default: inputNew = $("#clon #input-text").clone();
	                }

                	//setea los valores del contenedor
                	$(inputNew).find(".control-label").text(key);
                	$(inputNew).find(".control-label").attr("for", key);


                	$(inputNew).find(".form-control").val(value);
                	$(inputNew).find(".form-control").attr("id",key);
                	$(inputNew).find(".form-control").attr("name",key);

	                //si corresponde al genero seta la clase activa
	                if(key == 'gender')
	                {
	                	if(value == "m")
	                	{
	                		$(inputNew).find("#gender-male").addClass("btn-primary active");
	                		$(inputNew).find("#gender-female").addClass("btn-default");
	                	}
	                	else
	                	{
	                		$(inputNew).find("#gender-female").addClass("btn-primary active");
	                		$(inputNew).find("#gender-male").addClass("btn-default");
	                	}
	                }

	                if(key == 'password')
	                {
	                	var check = $("#clon #input-checkbox").clone().attr("id","change-password");
	                	// var checkSwitch = $(checkInput).attr("id","change-password");
	                	$(check).find('input').attr('name','change-password');
	                	$(inputNew).append(check);

	                	//agrega el tipo switch al html, por eso la posicion 0
				        var switchery = new Switchery($(check).find('input')[0], {
				            color: '#26B99A'
				        });
	                }

                	$("#data-list").append(inputNew);
				});

		        //setea los campos a castellano
		        $("[for=user_name]").text("Usuario");
		        $("[for=first_name]").text("Nombre");
		        $("[for=last_name]").text("Apellido");
		        $("[for=alias]").text("Alias");
		        $("[for=photo]").text("Foto");
		        $("[for=password]").text("Clave");
		        $("[for=gender]").text("Género");
		        $("[for=birthday]").text("Cumpleaños");
		        $("[for=company]").text("Compañía");
		        $("[for=department]").text("Departamento");
		        $("[for=position]").text("Posición");
		        $("[for=profession]").text("Profesión");
		        // $("[for=notification]").text("Notificación");
		        // $("[for=security]").text("Seguridad");

		        $("[id=password]").attr("type","password");
		        $("[id=password]").attr("disabled","disabled");
		        
		        $("[id=photo]").css("display","none");
		        $("[for=photo]").css("display","none");
				

				//setea el onclick el change password
		        $("#change-password").change(function()
		        {
					//intercambia el estado del input dependiendo su estado y habilita el campo de password
					if($(this).prop('checked'))
					{
						$(this).prop('checked', false);
						$("#password").prop('disabled', true);
					}
					else
					{
						$(this).prop('checked', true);
						$("#password").prop('disabled', false);
					}
		        });

		        //setea el campo fecha como datapicker
		        $('#birthday').daterangepicker(
		        {
		            singleDatePicker: true,
		            calender_style: "picker_4",
		            locale:
		            {
		                format: 'MM/DD/YYYY h:mm A'
		            }
		        },
		        function (start, end, label)
		        {
		            console.log(start.toISOString(), end.toISOString(), label);
		        });
			},
			error: function()
			{
				// mensaje
				var msg = 'no se pudo procesar';
				//datos no procesados por error
				if (that.isAjax()){console.log("AJAX:GET  =>", msg);}
			}
		});
	},

	listAll: function()
	{
		var data =
		{
			"action": "listAll",
		};

		var that = this;
		// datos a enviar
		if (this.isAjax()){console.log("AJAX:SENT  =>",data);}


		data = $(this).serialize() + "&" + $.param(data);

		// datos serializados para url
		if (this.isAjax()){console.log("AJAX:URL  =>",data);}

		$.ajax(
		{
			type: "post",
			dataType: "json",
			url: this.url,
			data: data,
			success: function(userList)
			{
				// datos recibidos
				if (that.isAjax()){console.log("AJAX:GET  =>",userList);}

				//path por defecto si hay un error y no definio imagen
				var photo = Project.get("pathMediaUrl")+"/img/user_default.png";

	            // var user = $("div[data-clone='user']").clone().appendTo("#user-view");
		        userList.forEach( function(user)
				{
					//clona la estructura del usuario
		        	var userNew = $("#clon #user-clon").clone();    
		        	
					//si existe foto la setea
					if(user.photo)
					{
						photo = "data:image/jpeg;base64," + user.photo;
					}
	
		        	//setea los valores del contenedor
		        	$(userNew).find('.user-company').text(user.company);
		        	$(userNew).find('.user-name').text(user.first_name);
		        	$(userNew).find('.user-photo').attr("src", photo);
		        	$(userNew).find('.user-profession').html("<strong>Profesion: </strong>"+ user.profession + "</p>");
		        	$(userNew).find('.user-phone-list').html("<li><i class='fa fa-phone'></i>Tel: "+user.number + "</li>");

		        	//agrega el usuario al listado
		        	$("#user-list").append(userNew);
				});
			},
			error: function()
			{
				// mensaje
				var msg = 'no se pudo procesar';
				//datos no procesados por error
				if (that.isAjax()){console.log("AJAX:GET  =>", msg);}
			}
		});
	},

	roles: function()
	{
		var data =
		{
			"action": "roles",
		};

		var that = this;
		// datos a enviar
		if (this.isAjax()){console.log("AJAX:SENT  =>",data);}


		data = $(this).serialize() + "&" + $.param(data);

		// datos serializados para url
		if (this.isAjax()){console.log("AJAX:URL  =>",data);}

		$.ajax(
		{
			type: "post",
			dataType: "json",
			url: this.url,
			data: data,
			success: function(rolList)
			{
				// datos recibidos
				if (that.isAjax()){console.log("AJAX:GET  =>",data);}

		        rolList.forEach( function(rol)
				{
					//clona la estructura del rol
		        	var rolNew = $("#clon #rol-clon").clone();    
		        	
		        	//setea los valores del contenedor
		        	$(rolNew).find('.rol-name').text(rol.name.toUpperCase());
		        	$(rolNew).find('.rol-nivel').text("Nivel: "+rol.id_rol);
		        	$(rolNew).find('.rol-description').html("<strong>Descripción: </strong>"+ rol.description + "</p>");
		        	$(rolNew).find('.rol-photo').attr("src", "data:image/jpeg;base64,"+ rol.photo );

		        	//cantidad de estrellas a mostrar
		        	var star = 6 - rol.id_rol ;

					$(rolNew).find('.rol-ratings').html("<a>"+ (6 - rol.id_rol) +".0</a>");
		        	
		        	for (var i=1; i <= 5; i++)
		        	{
		        		if ( (6 - rol.id_rol) >= i )
		        		{
							$(rolNew).find('.rol-ratings').append("<a style='margin: 0 0 0 5px' href='#'><span class='fa fa-star'></span></a>");
		        		}
		        		else
		        		{
							$(rolNew).find('.rol-ratings').append("<a style='margin: 0 0 0 5px' href='#'><span class='fa fa-star-o'></span></a>");
		        		}
		        	};
		        	

		        	//agrega el rol al listado
		        	$("#rol-list").append(rolNew);
				});
			},
			error: function()
			{
				// mensaje
				var msg = 'no se pudo procesar';
				//datos no procesados por error
				if (that.isAjax()){console.log("AJAX:GET  =>", msg);}
			}
		});
	},
}