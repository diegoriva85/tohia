/**
 * @requiere URL.js
 * @type {Array}
 * @author Enzo Yune
 */

/**
 * IMPORTANTE: palabra reservada 'lenght' no se debe usar
 */

//array que contendra los parametros
var $_GET = new Array();

//obtiene la lista de parametros
var parameters = URL.parameters();

//si contiene parametros la url
if(parameters)
{
    // hacemos un bucle para pasar por cada indice del array de valores
    for(var index in parameters)
    {
        $_GET[index] = parameters[index];
    }
}

//asignamos el largo del objeto una posicion denominada lenght 
$_GET['length'] = Object.getOwnPropertyNames($_GET).length - 1; 


//console.log("Cantidad de parametros recibidos: " + $_GET['length']);