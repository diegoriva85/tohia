<?php

//llamada a la configuracion global del sistema si se realiza una peticion por AJAX
require_once __DIR__.'/../../sys' . DIRECTORY_SEPARATOR . 'conf' . DIRECTORY_SEPARATOR . 'ini.conf';

/**
 * Description of Captcha
 * 
 * In the views you can use this by saying:
 * <img src="Captcha.php?action=generateImg" />
 *
 * Check if the typed captcha is correct by saying:
 * if ($_POST["captcha"] == $_SESSION['captcha']) { ... } else { ... }
 */
class Captcha {

    /**
     * this method renders a fresh captcha graphic file to the browser.
     */
    public static function generateImg(){
        
        // check if php gd extension is loaded
        if (!extension_loaded('gd')) {
            die("It looks like GD is not installed");
        }

        // begin to create the image with PHP's GD tools
        $im = imagecreatetruecolor(150, 70);

        $bg = imagecolorallocate($im, 255, 255, 255);
        imagefill($im, 0, 0, $bg);

        // create background with 1000 short lines
        /*for($i=0;$i<1000;$i++) {
            $lines = imagecolorallocate($im, rand(200, 220), rand(200, 220), rand(200, 220));
            $start_x = rand(0,150);
            $start_y = rand(0,70);
            $end_x = $start_x + rand(0,5);
            $end_y = $start_y + rand(0,5);
            imageline($im, $start_x, $start_y, $end_x, $end_y, $lines);
        }*/

        $str_captcha = self::generateCode(4);
        $iCaptchaLength = strlen($str_captcha);
        
        // create letters. for more info on how this works, please
        // @see php.net/manual/en/function.imagefttext.php
        // TODO: put the font path into the config
        for ($i=0; $i < $iCaptchaLength; $i++) {
            $text_color = imagecolorallocate($im, rand(0, 100), rand(10, 100), rand(0, 100));
            // font-path relative to this file
           // var_dump(PATH_PROJECT . '/core/media/font/times_new_yorker.ttf');
            imagefttext($im, 35, rand(-10, 10), 20 + ($i * 30) + rand(-5, +5), 35 + rand(10, 30), $text_color, PATH_PROJECT . '/core/media/font/times_new_yorker.ttf', $str_captcha[$i]);
        }
        
        // send http-header to prevent image caching (so we always see a fresh captcha image)
        header('Content-type: image/png');
        header('Pragma: no-cache');
        header('Cache-Control: no-store, no-cache, proxy-revalidate');
        
        // send image to browser and destroy image from php "cache"
        imagepng($im);
        imagedestroy($im);
    }
    
    /**
     * This file generates a captcha string, writes it into the $_SESSION['captcha']
     * @param type $amount
     */
    public static function generateCode($amount){
        //inicia la session utilizando el metodo start que incluye las configuraciones de php_init
        //(lifetime y path donde guarda el archivo de session en el servidor)
        Session::start();

        // target captcha string length
        $iCaptchaLength = $amount;

        // following letters are excluded from captcha: I, O, Q, S, 0, 1, 5
        $str_choice = 'ABCDEFGHJKLMNPRTUVWXYZ2346789';
        $str_captcha = '';
        // create target captcha with letters comming from $str_choice
        for ($i=0; $i < $iCaptchaLength; $i++) {
            do {
                $ipos = rand(0, strlen($str_choice) - 1);
            // checks that each letter is used only once
            } while (stripos($str_captcha, $str_choice[$ipos]) !== false);

            $str_captcha .= $str_choice[$ipos];
        }
        
        // write the captcha into a SESSION variable
        $_SESSION['captcha'] = $str_captcha;
        
        return $str_captcha;
    }
    
    
    
}


/******************************************************/
/************************ AJAX ************************/
/******************************************************/

//permite visualizar los resultados AJAX web solo para este Objeto

//si realizo una consulta AJAX
// La tercera condicion debe estar en false a menos que se quiera debuggear solo este objeto 
if ( Security::isAjax() || DEBUG_AJAX || FALSE )
{
	// verificamos la condicion de la solicitud (definida y con contenido)
	if (isset($_REQUEST["action"]) && !empty($_REQUEST["action"]))
	{ 
	    //asignacion de los valores
	    $action = $_REQUEST["action"];

	    //determinamos la accion que se solicito ejecutar en el servidor
	    switch($action)
	    {
	    	// [esta seccion deberia cambiarse acorde a las llamadas AJAX del lado del cliente]
	    	// case "metodoDelObjeto" : Object::method($_REQUEST["options"]); break;
	    	// 
	    	case "generateImg": Captcha::generateImg(); break;
	    }
	}
}
