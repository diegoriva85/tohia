<?php

require_once __DIR__ . '/../../sys' . DIRECTORY_SEPARATOR . 'conf' . DIRECTORY_SEPARATOR . 'ini.conf';

/**
 * @todo Cambio este require por un require_once porque necesito la descripción de la clase previo a iniciar la sesión o el objeto no se va a deserializar correctamente (ver index.php)
 * */
require_once PATH_PROJECT . '/core/class/Affidavit.php';
require_once PATH_PROJECT . '/core/class/PHPMailer.php';
require PATH_PROJECT . '/core/lib/FPDF/fpdf.php';

class DDJJ {


    public static function destroySession() {

        Session::destroy();
    }

    public static function getSessionTimeOut() {
        return Session::getTimeOut();
    }

    public static function encodeDateSade($date) {
        $rtn = date("Y-m-d\TH:i:sP", strtotime($date));
        return $rtn;
    }

    private static function decodeDateSade($date) {
        $rtn = date("d-m-Y", strtotime($date));
        return $rtn;
    }

    public static function encodeDecimalSade($decimal) {
        $rtn = str_replace(",", ".", $decimal);
        return $rtn;
    }

    private static function decodeDecimalSade($decimal) {
        $rtn = str_replace(".", ",", $decimal);
        return $rtn;
    }

    public static function setLifeTime() {
        return Session::setLifeTime();
    }

    public static function getSessionTimeRemain() {
        return Session::getTimeRemain();
    }

    public static function extendSessionTimeOut() {
        return Session::extendTimeOut();
    }

    public static function getSessionMode() {
        return Session::getMode();
    }

    public static function getTypeValue($name) {
        switch ($name) {
            case 'date':
                $rtn = 'valorDate';
                break;
            case 'year':
                $rtn = 'valorLong';
                break;
            case 'numeric':
                $rtn = 'valorLong';
                break;
            case 'alfabetic':
                $rtn = 'valorStr';
                break;
            case 'alfanumeric':
                $rtn = 'valorStr';
                break;
            case 'percentage':
                $rtn = 'valorStr';
                break;
            case 'hours_week':
                $rtn = 'valorStr';
                break;
            case 'money':
                $rtn = 'valorDouble';
                break;
            case 'money_decimal':
                $rtn = 'valorDouble';
                break;
            default:
                $rtn = 'valorStr';
        }
        return $rtn;
    }

    public static function removeTemplate($sections, $id) {
        $rtn = ['error' => ['code' => 0, 'message' => '']];

        try {
            Session::retrieve();
            $affidavit = $_SESSION['Affidavit'];
            foreach ($sections as $dato) {
                $affidavit = $affidavit->getSection($dato);
            }
            $affidavit->removeSection($id);
        } catch (Exception $e) {
            $rtn['error']['code'] = 1;
            $rtn['error']['message'] = $e->getMessage();
        }

        return $rtn;
    }

    public static function cloneTemplate($section, $type, $id, $name) {
        $rtn = ['error' => ['code' => 0, 'message' => '']];

        Session::retrieve();
        $affidavit = $_SESSION['Affidavit'];
        //Se crean los tipos de datos para validar
        $alfabetic = new AffidavitFieldType("alfabetic", "letras y espacios", "/^[á-úÁ-Úa-zA-Z\ñ\Ñ\\\_\[\*\]\.\:\(\)\,\-\s\/]*$/");
        $numeric = new AffidavitFieldType("numeric", "números", "/^\d[0-9]*$/");
        $money = new AffidavitFieldType("money", "numeros", "/^\d[0-9]*$/");
        $money_decimal = new AffidavitFieldType("money_decimal", "numeros y hasta dos decimales separados por una coma", "/^\d+(\,\d{1,2})?$/");
        $alfanumeric = new AffidavitFieldType("alfanumeric", "letras, números, espacios y signos", "/^[á-úÁ-Úa-zA-Z0-9\ñ\Ñ\\\_\.\:\(\)\,\-\s\/]*$/");
        $percentage = new AffidavitFieldType("percentage", "números entre el 0 y 100", "/^(?:100|\d{1,2})$/");
        $hours_week = new AffidavitFieldType("hours_week", "números entre 1 y 168", "/^[0-9]{1,3}(?: Hs\.)$/");

        //@TODO hay que mejorar las expreciones que valida el tipo date y el tipo year
        $date = new AffidavitFieldType("date", "una fecha con el formato(dd-mm-aaaa)", "/^[á-úÁ-Úa-zA-Z0-9\ñ\Ñ\\\_\.\,\-\s\/]*$/");
        //$date =  new AffidavitFieldType("date","una fecha con el formato(dd-mm-aaaa)","([0-9]{2})/([0-9]{2})/([0-9]{4})");
        $year = new AffidavitFieldType("year", "una fecha con el formato(aaaa)", "/^[0-9]*$/");
        $email = new AffidavitFieldType('email', 'un mail con formato correcto', '/^[a-z]+([\.]?[a-z0-9_-]+)*@[a-z]+([\.-]+[a-z0-9]+)*\.[a-z]{2,}$/');


        foreach ($section as $dato) {
            $affidavit = $affidavit->getSection($dato);
        }
        try {
            $seccionST = new AffidavitSection($id);
            $seccionST->setName($name);


            switch ($type) {
                case 'ddjj-work-history-temp':
                    $fieldST = new AffidavitField('ddjj-company', 'Empresa/Organismo', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-entity', 'Entidad', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-company-activity', 'Actividad de la Empresa', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-company-position', 'Cargo o Función', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-company-date-from', 'Desde', '', $date, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-company-date-to', 'Hasta', '', $date, false, true);
                    $seccionST->addField($fieldST);
                    break;
                case 'ddjj-current-activity-temp':
                    $fieldST = new AffidavitField('ddjj-current-company', 'Empresa/Organismo', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-current-entity', 'Entidad', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-current-company-activity', 'Actividad de la Empresa', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-current-company-position', 'Funcion o Cargo', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-current-company-from', 'Desde', '', $date, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-current-licence', 'Licencia o suspensión', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-current-licence-to', 'Fecha inicio de licencia', '00-00-0000', $date, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-current-company-hours-week', 'Dedicacion de horas semanales', '', $hours_week, false, true);
                    $seccionST->addField($fieldST);
                    break;

                case 'ddjj-family-temp':
                    $fieldST = new AffidavitField('ddjj-family-bond', 'Vínculo', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-du-type', 'Tipo de Documento', '', $alfabetic, true, false);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-du-number', 'Número de documento', '', $numeric, true, false);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-cuit', 'CUIT/CUIL', '', $numeric, true, false);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-user', 'Apellido y Nombres', '', $alfabetic, true, false);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-date', 'Fecha de Nacimiento', '', $date, true, false);
                    $seccionST->addField($fieldST);
                    break;
                case 'ddjj-personal-asset-temp':
                    $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-owner-name', 'Apellido y Nombres del Propietario', 'N/A', $alfabetic,true,false);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-law', 'Derecho', 'N/A', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-benefit-cause', 'Causa del Beneficio', 'N/A', $alfanumeric, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-time-limit', 'Plazo', 'N/A', $alfanumeric, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-title-cost', 'Título', 'N/A', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-bond', 'Titular', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-asset', 'Tipo de bien', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-brand', 'Marca/Descripción', '', $alfanumeric, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-manufacture-year', 'Año fabricación', '', $year, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-domain', 'Dominio/Matrícula', '', $alfanumeric, true, false);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-entry-year', 'Año de Ingreso/Adquisición', '', $year, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-funds-origin', 'Origen de fondos', '', $alfanumeric, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-titularity-percentage', 'Porcentaje de titularidad', '', $percentage, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-fiscal-value', 'Valuación', '', $money, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-aquired-value', 'Valor de adquisición', '', $money, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-improvements', 'Mejoras', '', $alfanumeric, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-amount-improvements', 'Monto total de mejoras', '', $money, false, true);
                    $seccionST->addField($fieldST);
                    break;
                case 'ddjj-unregistered-asset-temp':
                    $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-bond', 'Titular', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-asset', 'Tipo de bien', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-brand', 'Descripción', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-entry-year', 'Año de ingreso/Adquisición', '', $year, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-funds-origin', 'Origen de fondos', '', $alfanumeric, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-titularity-percentage', 'Porcentaje de titularidad', '', $percentage, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-fiscal-value', 'Valuación', '', $money, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-aquired-value', 'Valor de adquisición', '', $money, true, true);
                    $seccionST->addField($fieldST);
                    break;
                case 'ddjj-property-temp':
                    $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-owner-name', 'Apellido y Nombres del Propietario', '', $alfabetic,true,false);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-law', 'Derecho', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-benefit-cause', 'Causa del Beneficio', '', $alfanumeric, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-time-limit', 'Plazo', '', $alfanumeric, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-title-cost', 'Título', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-bond', 'Titular', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-asset', 'Tipo de Bien', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-country', 'País', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-province', 'Provincia', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-town', 'Localidad', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-neightborhood', 'Barrio/Zona', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-address', 'Domicilio', '', $alfanumeric, true, false);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-titularity-percentage', 'Porcentaje de titularidad', '', $percentage, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-entry-year', 'Año de ingreso/adquisición', '', $year, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-funds-origin', 'Origen de fondos', '', $alfanumeric, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-area', 'Superficie total', '', $alfanumeric, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-unit', 'Unidad', '', $alfanumeric, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-fiscal-value', 'Valuación', '', $money, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-aquired-value', 'Valor de adquisición', '', $money, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-improvements', 'Mejoras', '', $alfanumeric, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-amount-improvements', 'Monto total de mejoras', '', $money, false, true);
                    $seccionST->addField($fieldST);
                    break;
                case 'ddjj-stock-temp':
                    $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-bond', 'Titular', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-title-type-value', 'Tipo de bien', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-title-description', 'Descripción', '', $alfanumeric, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-issuing-entity', 'Entidad emisora', '', $alfabetic, true, false);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-trade', 'Objeto', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-stock-qty', 'Cantidad de acciones/títulos/cuotas', '', $numeric, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-aquired-date', 'Fecha de adquisición', '', $date, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-funds-origin', 'Origen de fondos', '', $alfanumeric, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-quote-value', 'Valor de cotización', '', $money_decimal, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-current-value', 'Valor actual', '', $money_decimal, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-aquired-value', 'Valor de adquisición', '', $money_decimal, true, true);
                    $seccionST->addField($fieldST);
                    break;
                case 'ddjj-partnership-temp':
                    $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-bond', 'Titular', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-cuit-society', 'CUIT de la sociedad', '', $numeric, false, false);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-kind-society', 'Tipo de sociedad', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-partnership-name', 'Nombre de sociedad', '', $alfabetic, true, false);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-partnership-trade', 'Objeto Social', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-titularity-percentage', 'Porcentaje de Participación', '', $percentage, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-aquired-date', 'Fecha de adquisición', '', $date, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-funds-origin', 'Origen de fondos', '', $alfanumeric, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-current-value', 'Valor actual', '', $money, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-aquired-value', 'Valor de adquisición', '', $money, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-contributions', 'Aportes', '', $alfanumeric, false, true);
                    $seccionST->addField($fieldST);
                    break;
                case 'ddjj-account-temp':
                    $fieldST = new AffidavitField('ddjj-asset-type', 'Tipo de bien', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-bond', 'Titular', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-account-type', 'Tipo de cuenta', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-titularity-percentage', 'Porcentaje de titularidad', '', $percentage, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-entity', 'Entidad', '', $alfabetic, true, false);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-entity-cuit', 'CUIT de la Entidad', '', $numeric, false, false);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-account-number', 'Número de cuenta', '', $numeric, true, false);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-currency', 'Moneda', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-funds-origin', 'Origen fondos', '', $alfanumeric, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-amount', 'Monto total', '', $money, true, true);
                    $seccionST->addField($fieldST);
                    break;
                case 'ddjj-other-job-temp':
                    $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-bond', 'Titular', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-position', 'Cargo o función', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-employment-relationship', 'Relación Laboral', '', $alfanumeric, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-employer', 'Empleador/Entidad', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-employer-activity', 'Actividad de la empresa, etc/ámbito', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-net-anual-amount', 'Monto anual neto', '', $money, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-other-job-from', 'Desde', '', $date, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-activity-status', '¿Sigue en actividad?', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    break;
                case 'ddjj-other-activity-temp':
                    $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-bond', 'Titular', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-income-type', 'Tipo de ingreso / activo', '', $alfanumeric, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-origin-income', 'Origen / concepto', '', $alfanumeric, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-total-received-income', 'Monto total recibido', '', $numeric, true, true);
                    $seccionST->addField($fieldST);
                    break;
                case 'ddjj-real-state-selling-temp':
                    $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-bond', 'Titular', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-asset', 'Tipo de Bien', '', $alfanumeric, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-country', 'País', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-province', 'Provincia', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-town', 'Localidad', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-neightborhood', 'Barrio/Zona', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-address', 'Domicilio', '', $alfanumeric, true, false);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-titularity-percentage', 'Porcentaje de titularidad', '', $percentage, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-entry-year', 'Año de ingreso', '', $year, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-currency', 'Moneda', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-amount', 'Monto', '', $numeric, true, true);
                    $seccionST->addField($fieldST);
                    break;
                case 'ddjj-debt-temp':
                    $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-bond', 'Titular', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-debt-type', 'Tipo de deuda', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-creditor', 'Acreedor', '', $alfabetic, true, false);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-cuit', 'CUIT/CUIL', '', $numeric, true, false);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-currency', 'Moneda', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-amount', 'Monto', '', $numeric, true, false);
                    $seccionST->addField($fieldST);
                    break;
                case 'ddjj-claim-temp':
                    $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-bond', 'Titular', '', $alfabetic, false, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-debt-type', 'Tipo de acreencia', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-creditor', 'Deudor', '', $alfabetic, true, false);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-cuit', 'CUIT/CUIL', '', $numeric, true, false);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-currency', 'Moneda', '', $alfabetic, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-amount', 'Monto', '', $money, true, true);
                    $seccionST->addField($fieldST);
                    $fieldST = new AffidavitField('ddjj-funds-origin', 'Origen de fondos', '', $alfanumeric, true, false);
                    $seccionST->addField($fieldST);
                    break;
            }

            $affidavit->addSection($seccionST);
        } catch (Exception $e) {
          //  self::saveError("Clone Template", $e);
            $rtn['error']['code'] = 1;
            $rtn['error']['message'] = $e->getMessage();
        }

        return $rtn;
    }

    public static function create($type,$SADE_trans_UUID = null) {
        $rtn = [
            'error' => [
                'code' => 0,
                'message' => ''
            ]
        ];
        try {
            Session::retrieve();

            $currentUser = new User();
            $currentUser->load();

      

            $currentUserId = $currentUser->getId();
            $currentUserCuit = $currentUser->getLoginName();
            $currentUserName = $currentUser->getName();
            $currentUserJuris = $currentUser->getNameJurisdiction();
            $currentUserDistri = $currentUser->getDistribution();
            $currentUserPosi = $currentUser->getPosition();

            $SADE_trans_Fields = array();

            $SADE_trans_Fields['cuil_cuit'] = isset($currentUserCuit) ? $currentUserCuit : '';
            $SADE_trans_Fields['nombre_apellido'] = isset($currentUserName) ? $currentUserName : '';
            $SADE_trans_Fields['jurisdiccion'] = isset($currentUserJuris) ? $currentUserJuris : '';
            $SADE_trans_Fields['reparticion'] = isset($currentUserDistri) ? $currentUserDistri : '';
            $SADE_trans_Fields['cargo'] = isset($currentUserPosi) ? $currentUserPosi : '';


            
           // $SADE_trans_UUID = '';
   
            if (!isset($SADE_trans_UUID) ) {
                
                $dbLink = Database::connect();

                $stmt = $dbLink->prepare('select SADE_trans_UUID from affidavit where created_by = ? order by created_at desc limit 1');
                $stmt->bind_param('i', $currentUserId);
                $stmt->execute();

                $stmt->bind_result($SADE_trans_UUID);
                $stmt->fetch();

                $stmt->close();
                $dbLink->close();

            }

            if (!empty($SADE_trans_UUID))
            {
                $rst  = Communicate::searchDataTrans($SADE_trans_UUID);
                if(isset($rst['error']))
                {
                    throw new Exception($rst['error']['message']);
                }

                $rst = $rst['success']['response'];
                
                foreach ($rst->return->valorFormComps as $field) {
                    switch (true) {
                        case property_exists($field, 'valorStr'):
                            $SADE_trans_Fields[$field->inputName] = $field->valorStr;
                            break;
                        case property_exists($field, 'valorLong'):
                            $SADE_trans_Fields[$field->inputName] = $field->valorLong;
                            break;
                        case property_exists($field, 'valorDouble'):
                            $SADE_trans_Fields[$field->inputName] = $field->valorDouble;
                            break;
                        case property_exists($field, 'valorDate'):
                            $SADE_trans_Fields[$field->inputName] = DDJJ::decodeDateSade($field->valorDate);
                            break;
                    }
                }
            }

            $affidavit = new AffidavitDocument();
            //Se crean los tipos de datos para validar
            $alfabetic = new AffidavitFieldType("alfabetic", "letras y espacios", "/^[á-úÁ-Úa-zA-Z\ñ\Ñ\\\_\[\*\]\.\:\(\)\,\-\s\/]*$/");
            $numeric = new AffidavitFieldType("numeric", "numeros", "/^\d[0-9]*$/");
            $money = new AffidavitFieldType("money", "numeros", "/^\d[0-9]*$/");
            $money_decimal = new AffidavitFieldType("money_decimal", "numeros y hasta dos decimales separados por una coma", "/^\d+(\,\d{1,2})?$/");
            $alfanumeric = new AffidavitFieldType("alfanumeric", "letras, números, espacios y signos", "/^[á-úÁ-Úa-zA-Z0-9\ñ\Ñ\\\_\.\:\(\)\,\-\s\/]*$/");
            $percentage = new AffidavitFieldType("percentage", "números entre el 0 y 100", "/^(?:100|\d{1,2})$/");
            $hours_week = new AffidavitFieldType("hours_week", "numeros", "/^[0-9]{1,3}(?: Hs\.)$/");
            //@TODO hay que mejorar las expreciones que valida el tipo date y el tipo year
            $date = new AffidavitFieldType("date", "una fecha con el formato(dd-mm-aaaa)", "/^[á-úÁ-Úa-zA-Z0-9\ñ\Ñ\\\_\.\,\-\s\/]*$/");
            //$date =  new AffidavitFieldType("date","una fecha con el formato(dd-mm-aaaa)","([0-9]{2})/([0-9]{2})/([0-9]{4})");
            $year = new AffidavitFieldType("year", "una fecha con el formato(aaaa)", "/^[0-9]*$/");
            $email = new AffidavitFieldType('email', 'un mail con formato correcto', '/^[a-z]+([\.]?[a-z0-9_-]+)*@[a-z]+([\.-]+[a-z0-9]+)*\.[a-z]{2,}$/');

            $seccionF = new AffidavitSection('ddjj-personal-data');
            $seccionF->setName('Datos Personales');

            $seccionS = new AffidavitSection('ddjj-personal-info');
            $seccionS->setName('Información personal');
            $fieldS = new AffidavitField('ddjj-type', 'Tipo de Presentación', $type, $alfabetic, true, true);
            $seccionS->addField($fieldS);
            $fieldS = new AffidavitField('ddjj-du-type', 'Tipo de documento', (array_key_exists('tipo_docum', $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_docum'] : ''), $alfabetic, true, true);
            $seccionS->addField($fieldS);
            $fieldS = new AffidavitField('ddjj-du-number', 'N° de documento', (array_key_exists('num_docum', $SADE_trans_Fields) ? $SADE_trans_Fields['num_docum'] : ''), $numeric, true, true);
            $seccionS->addField($fieldS);
            $fieldS = new AffidavitField('ddjj-cuit', 'CUIT/CUIL', (array_key_exists('cuil_cuit', $SADE_trans_Fields) ? $SADE_trans_Fields['cuil_cuit'] : ''), $numeric, true, true);
            $seccionS->addField($fieldS);
            $fieldS = new AffidavitField('ddjj-user', 'Apellido y Nombres', (array_key_exists('nombre_apellido', $SADE_trans_Fields) ? $SADE_trans_Fields['nombre_apellido'] : ''), $alfabetic, true, true);
            $seccionS->addField($fieldS);
            $fieldS = new AffidavitField('ddjj-date', 'Fecha de Nacimiento', (array_key_exists('fecha_nacimiento', $SADE_trans_Fields) ? $SADE_trans_Fields['fecha_nacimiento'] : ''), $date, true, true);
            $seccionS->addField($fieldS);
            $fieldS = new AffidavitField('ddjj-civil-status', 'Estado Civíl', (array_key_exists('estado_civil', $SADE_trans_Fields) ? $SADE_trans_Fields['estado_civil'] : ''), $alfabetic, true, true);
            $seccionS->addField($fieldS);

            $seccionF->addSection($seccionS);

            $seccionS = new AffidavitSection('ddjj-gcba-work-info');
            $seccionS->setName('Información laboral');
            $fieldS = new AffidavitField('ddjj-date-gcba', 'Ingreso al cargo actual', (array_key_exists('fecha_ingreso', $SADE_trans_Fields) ? $SADE_trans_Fields['fecha_ingreso'] : ''), $date, true, true);
            $seccionS->addField($fieldS);
            $fieldS = new AffidavitField('ddjj-jurisdiction', 'Jurisdiccion', (array_key_exists('jurisdiccion', $SADE_trans_Fields) ? $SADE_trans_Fields['jurisdiccion'] : ''), $alfabetic, true, true);
            $seccionS->addField($fieldS);
            $fieldS = new AffidavitField('ddjj-ministry', 'Secretaría', (array_key_exists('secretaria', $SADE_trans_Fields) ? $SADE_trans_Fields['secretaria'] : ''), $alfabetic, true, true);
            $seccionS->addField($fieldS);
            $fieldS = new AffidavitField('ddjj-general-direction', 'Dirección General', (array_key_exists('reparticion', $SADE_trans_Fields) ? $SADE_trans_Fields['reparticion'] : ''), $alfabetic, true, true);
            $seccionS->addField($fieldS);

            $fieldS = new AffidavitField('ddjj-job', 'Cargo', (array_key_exists('cargo', $SADE_trans_Fields) ? $SADE_trans_Fields['cargo'] : ''), $alfabetic, true, true);
            $seccionS->addField($fieldS);
            $fieldS = new AffidavitField('ddjj-employment-relationship', 'Relación Laboral', (array_key_exists('relacion_laboral', $SADE_trans_Fields) ? $SADE_trans_Fields['relacion_laboral'] : ''), $alfanumeric, true, true);
            $seccionS->addField($fieldS);
            // setea a el campo monto anual neto como requerido solo si no es una DDJJ Inicial
            $fieldS = new AffidavitField('ddjj-net-annual-amount', 'Monto Anual Neto', (array_key_exists('monto_anual_pesos', $SADE_trans_Fields) ? $SADE_trans_Fields['monto_anual_pesos'] : ''), $money, true, true);
            $seccionS->addField($fieldS);
            $fieldS = new AffidavitField('ddjj_member_commission', 'Miembro de comisión', (array_key_exists('miembro_comision', $SADE_trans_Fields) ? $SADE_trans_Fields['miembro_comision'] : ''), $alfabetic, true, true);
            $seccionS->addField($fieldS);
            $fieldS = new AffidavitField('ddjj-voluntary-retirement', 'Retiro voluntario', (array_key_exists('retiro_voluntario', $SADE_trans_Fields) ? $SADE_trans_Fields['retiro_voluntario'] : ''), $alfabetic, true, true);
            $seccionS->addField($fieldS);
            $fieldS = new AffidavitField('ddjj-retain-budget', 'Retengo Partida', (array_key_exists('retengo_partida', $SADE_trans_Fields) ? $SADE_trans_Fields['retengo_partida'] : ''), $alfabetic, true, true);
            $seccionS->addField($fieldS);
            $fieldS = new AffidavitField('ddjj-retain-budget-owner', 'Organismo de Origen', (array_key_exists('organismo', $SADE_trans_Fields) ? $SADE_trans_Fields['organismo'] : ''), $alfabetic, true, true);
            $seccionS->addField($fieldS);

            $seccionF->addSection($seccionS);

            $seccionS = new AffidavitSection('ddjj-academic-info');
            $seccionS->setName('Estudios');

            $fieldS = new AffidavitField('ddjj-studies', 'Estudios Cursados', (array_key_exists('estudio', $SADE_trans_Fields) ? $SADE_trans_Fields['estudio'] : ''), $alfanumeric, true, true);
            $seccionS->addField($fieldS);
            $fieldS = new AffidavitField('ddjj-earned-degree', 'Título Obtenido', '', $alfabetic, false, false);
            $seccionS->addField($fieldS);

            $seccionF->addSection($seccionS);

            $affidavit->addSection($seccionF);     //fin del tab 1
            //padre
            $seccionF = new AffidavitSection('ddjj-work-data');
            $seccionF->setName('Datos Laborales');
            //hijos
            $seccionS = new AffidavitSection('ddjj-works-history');
            $seccionS->setName('Antecedentes laborales/profesionales');

            if (array_key_exists('empresa', $SADE_trans_Fields) || array_key_exists('entidad', $SADE_trans_Fields) || array_key_exists('actividad_empresa', $SADE_trans_Fields) || array_key_exists('cargo_funcion', $SADE_trans_Fields) || array_key_exists('fecha_desde', $SADE_trans_Fields) || array_key_exists('fecha_hasta', $SADE_trans_Fields)) {
                $seccionST = new AffidavitSection('ddjj-work-history');
                $seccionST->setName('Antecedente laboral');
                $fieldST = new AffidavitField('ddjj-company', 'Empresa/Organismo', (array_key_exists('empresa', $SADE_trans_Fields) ? $SADE_trans_Fields['empresa'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-entity', 'Entidad', (array_key_exists('entidad', $SADE_trans_Fields) ? $SADE_trans_Fields['entidad'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-company-activity', 'Actividad de la Empresa', (array_key_exists('actividad_empresa', $SADE_trans_Fields) ? $SADE_trans_Fields['actividad_empresa'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-company-position', 'Cargo o Función', (array_key_exists('cargo_funcion', $SADE_trans_Fields) ? $SADE_trans_Fields['cargo_funcion'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-company-date-from', 'Desde', (array_key_exists('fecha_desde', $SADE_trans_Fields) ? $SADE_trans_Fields['fecha_desde'] : ''), $date, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-company-date-to', 'Hasta', (array_key_exists('fecha_hasta', $SADE_trans_Fields) ? $SADE_trans_Fields['fecha_hasta'] : ''), $date, false, true);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);
            }
            $i = 1;
            //while (array_key_exists('empresa_R'.$i, $SADE_trans_Fields)
            while (array_key_exists('empresa_R' . $i, $SADE_trans_Fields) || array_key_exists('entidad_R' . $i, $SADE_trans_Fields) || array_key_exists('actividad_empresa_R' . $i, $SADE_trans_Fields) || array_key_exists('cargo_funcion_R' . $i, $SADE_trans_Fields) || array_key_exists('fecha_desde_R' . $i, $SADE_trans_Fields) || array_key_exists('fecha_hasta_R' . $i, $SADE_trans_Fields)) {

                $seccionST = new AffidavitSection('ddjj-work-history_R' . $i);
                $seccionST->setName('Antecedente laboral');
                $fieldST = new AffidavitField('ddjj-company', 'Empresa/Organismo', (array_key_exists('empresa_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['empresa_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-entity', 'Entidad', (array_key_exists('entidad_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['entidad_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-company-activity', 'Actividad de la Empresa', (array_key_exists('actividad_empresa_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['actividad_empresa_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-company-position', 'Cargo o Función', (array_key_exists('cargo_funcion_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['cargo_funcion_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-company-date-from', 'Desde', (array_key_exists('fecha_desde_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['fecha_desde_R' . $i] : ''), $date, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-company-date-to', 'Hasta', (array_key_exists('fecha_hasta_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['fecha_hasta_R' . $i] : ''), $date, false, true);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST); //fin del tab 2-1
                $i++;
            }
            $seccionF->addSection($seccionS);

            $seccionS = new AffidavitSection('ddjj-current-activities');
            $seccionS->setName('Actividades laborales o Profesonales simultáneas');
            if (array_key_exists('empresa_organismo', $SADE_trans_Fields) || array_key_exists('entidad_actual', $SADE_trans_Fields) || array_key_exists('actividad', $SADE_trans_Fields) || array_key_exists('cargo_actual', $SADE_trans_Fields) || array_key_exists('fecha_actividad', $SADE_trans_Fields) || array_key_exists('licencia', $SADE_trans_Fields) || array_key_exists('fecha_hasta_licencia', $SADE_trans_Fields) || array_key_exists('horas', $SADE_trans_Fields)) {
                $seccionST = new AffidavitSection('ddjj-current-activity');
                $seccionST->setName('Ingreso/activo');

                $fieldST = new AffidavitField('ddjj-current-company', 'Empresa/Organismo', (array_key_exists('empresa_organismo', $SADE_trans_Fields) ? $SADE_trans_Fields['empresa_organismo'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-current-entity', 'Entidad', (array_key_exists('entidad_actual', $SADE_trans_Fields) ? $SADE_trans_Fields['entidad_actual'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-current-company-activity', 'Actividad de la Empresa', (array_key_exists('actividad', $SADE_trans_Fields) ? $SADE_trans_Fields['actividad'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-current-company-position', 'Funcion o Cargo', (array_key_exists('cargo_actual', $SADE_trans_Fields) ? $SADE_trans_Fields['cargo_actual'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-current-company-from', 'Desde', (array_key_exists('fecha_actividad', $SADE_trans_Fields) ? $SADE_trans_Fields['fecha_actividad'] : ''), $date, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-current-licence', 'Licencia o suspensión', (array_key_exists('licencia', $SADE_trans_Fields) ? $SADE_trans_Fields['licencia'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-current-licence-to', 'Fecha inicio de licencia', (array_key_exists('fecha_hasta_licencia', $SADE_trans_Fields) ? $SADE_trans_Fields['fecha_hasta_licencia'] : '00-00-0000'), $date, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-current-company-hours-week', 'Dedicacion de horas semanales', (array_key_exists('horas', $SADE_trans_Fields) ? $SADE_trans_Fields['horas'] : ''), $hours_week, false, true);
                $seccionST->addField($fieldST);

                $seccionS->addSection($seccionST);
            }
            $i = 1;
            // while (array_key_exists('empresa_organismo_R'.$i, $SADE_trans_Fields)) {

            while (array_key_exists('empresa_organismo_R' . $i, $SADE_trans_Fields) || array_key_exists('entidad_actual_R' . $i, $SADE_trans_Fields) || array_key_exists('actividad_R' . $i, $SADE_trans_Fields) || array_key_exists('cargo_actual_R' . $i, $SADE_trans_Fields) || array_key_exists('fecha_actividad_R' . $i, $SADE_trans_Fields) || array_key_exists('licencia_R' . $i, $SADE_trans_Fields) || array_key_exists('fecha_hasta_licencia_R' . $i, $SADE_trans_Fields) || array_key_exists('horas_R' . $i, $SADE_trans_Fields)) {

                $seccionST = new AffidavitSection('ddjj-current-activity_R' . $i);
                $seccionST->setName('Ingreso/activo');

                $fieldST = new AffidavitField('ddjj-current-company', 'Empresa/Organismo', (array_key_exists('empresa_organismo_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['empresa_organismo_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-current-entity', 'Entidad', (array_key_exists('entidad_actual_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['entidad_actual_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-current-company-activity', 'Actividad de la Empresa', (array_key_exists('actividad_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['actividad_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-current-company-position', 'Funcion o Cargo', (array_key_exists('cargo_actual_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['cargo_actual_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-current-company-from', 'Desde', (array_key_exists('fecha_actividad_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['fecha_actividad_R' . $i] : ''), $date, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-current-licence', 'Licencia o suspensión', (array_key_exists('licencia_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['licencia_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-current-licence-to', 'Fecha inicio de licencia', (array_key_exists('fecha_hasta_licencia_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['fecha_hasta_licencia_R' . $i] : '00-00-0000'), $date, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-current-company-hours-week', 'Dedicacion de horas semanales', (array_key_exists('horas_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['horas_R' . $i] : ''), $hours_week, false, true);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);

                $i++;
            }
            $seccionF->addSection($seccionS);
            $affidavit->addSection($seccionF); //fin del tab 2

            $seccionF = new AffidavitSection('ddjj-family-data');
            $seccionF->setName('Datos Familiares');
            // 248071   'vinculo_familiar_R'.$i

            if (array_key_exists('vinculo_familiar', $SADE_trans_Fields)) {
                $seccionST = new AffidavitSection('ddjj-familiar');
                $seccionST->setName('Familiar');
                $fieldST = new AffidavitField('ddjj-family-bond', 'Vínculo', (array_key_exists('vinculo_familiar', $SADE_trans_Fields) ? $SADE_trans_Fields['vinculo_familiar'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-du-type', 'Tipo de Documento', '', $alfabetic, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-du-number', 'Número de documento', '', $numeric, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-cuit', 'CUIT/CUIL', '', $numeric, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-user', 'Apellido y Nombres', '', $alfabetic, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-date', 'Fecha de Nacimiento', '', $date, true, false);
                $seccionST->addField($fieldST);
                $seccionF->addSection($seccionST);
            }
            $i = 1;
            while (array_key_exists('vinculo_familiar_R' . $i, $SADE_trans_Fields)) {

                $seccionST = new AffidavitSection('ddjj-familiar_R' . $i);
                $seccionST->setName('Familiar');
                $fieldST = new AffidavitField('ddjj-family-bond', 'Vínculo', (array_key_exists('vinculo_familiar_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['vinculo_familiar_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-du-type', 'Tipo de Documento', '', $alfabetic, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-du-number', 'Número de documento', '', $numeric, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-cuit', 'CUIT/CUIL', '', $numeric, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-user', 'Apellido y Nombres', '', $alfabetic, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-date', 'Fecha de Nacimiento', '', $date, true, false);
                $seccionST->addField($fieldST);
                $seccionF->addSection($seccionST);

                $i++;
            }
            $affidavit->addSection($seccionF); //fin del tab 3


            $seccionF = new AffidavitSection('ddjj-assets');
            $seccionF->setName('Bienes');

            $seccionS = new AffidavitSection('ddjj-goods');
            $seccionS->setName('Bienes muebles registrables');

            if (array_key_exists('titularidad_mueble', $SADE_trans_Fields)) {
                $seccionST = new AffidavitSection('ddjj-personal-asset');
                $seccionST->setName('Bien mueble');


                if ($SADE_trans_Fields['titularidad_mueble'] != 'Otros') {

                    $defaultValue = 'N/Assasa';

                } else {
                    $defaultValue = '';
                }



                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', (array_key_exists('titularidad_mueble', $SADE_trans_Fields) ? $SADE_trans_Fields['titularidad_mueble'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-owner-name', 'Apellido y Nombres del Propietario',$defaultValue, $alfabetic, true ,false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-law', 'Derecho', (array_key_exists('derecho_mueble', $SADE_trans_Fields) ? $SADE_trans_Fields['derecho_mueble'] : $defaultValue), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-benefit-cause', 'Causa del Beneficio', (array_key_exists('causa_beneficio_mueble', $SADE_trans_Fields) ? $SADE_trans_Fields['causa_beneficio_mueble'] : $defaultValue), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-time-limit', 'Plazo', (array_key_exists('plazo_mueble', $SADE_trans_Fields) ? $SADE_trans_Fields['plazo_mueble'] : $defaultValue ), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-title-cost', 'Título', (array_key_exists('titulo_mueble', $SADE_trans_Fields) ? $SADE_trans_Fields['titulo_mueble'] : $defaultValue ), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_mueble', $SADE_trans_Fields) ? $SADE_trans_Fields['titular_mueble'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-asset', 'Tipo de Bien', (array_key_exists('tipo_bien_mueble', $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_bien_mueble'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-brand', 'Marca/Descripción', (array_key_exists('marca_bien_mueble', $SADE_trans_Fields) ? $SADE_trans_Fields['marca_bien_mueble'] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-manufacture-year', 'Año fabricación', (array_key_exists('anio_fabricacion_mueble', $SADE_trans_Fields) ? $SADE_trans_Fields['anio_fabricacion_mueble'] : ''), $year, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-domain', 'Dominio/Matrcula', '', $alfanumeric, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-entry-year', 'Año ingreso', (array_key_exists('anio_ingreso_mueble', $SADE_trans_Fields) ? $SADE_trans_Fields['anio_ingreso_mueble'] : ''), $year, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-funds-origin', 'Origen de fondos', (array_key_exists('origen_fondos_mueble', $SADE_trans_Fields) ? $SADE_trans_Fields['origen_fondos_mueble'] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-titularity-percentage', 'Porcentaje de titularidad', (array_key_exists('porcentaje_titularidad_mueble', $SADE_trans_Fields) ? $SADE_trans_Fields['porcentaje_titularidad_mueble'] : ''), $percentage, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-fiscal-value', 'Valuación', (array_key_exists('valor_fiscal_mueble', $SADE_trans_Fields) ? $SADE_trans_Fields['valor_fiscal_mueble'] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-aquired-value', 'Valor de adquisición', (array_key_exists('valor_adquirido_mueble', $SADE_trans_Fields) ? $SADE_trans_Fields['valor_adquirido_mueble'] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-improvements', 'Mejoras', (array_key_exists('mejoras', $SADE_trans_Fields) ? $SADE_trans_Fields['mejoras'] : ''), $alfanumeric, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-amount-improvements', 'Monto total de mejoras', (array_key_exists('monto_total_mejoras', $SADE_trans_Fields) ? $SADE_trans_Fields['monto_total_mejoras'] : ''), $money, false, true);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);
            }
            $i = 1;
            while (array_key_exists('titularidad_mueble_R' . $i, $SADE_trans_Fields)) {

                if ($SADE_trans_Fields['titularidad_mueble_R'. $i] != 'Otros') {

                    $defaultValue = 'N/A';

                } else {
                    $defaultValue = '';
                }

                $seccionST = new AffidavitSection('ddjj-personal-asset_R' . $i);
                $seccionST->setName('Bien mueble');

                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', (array_key_exists('titularidad_mueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titularidad_mueble_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-owner-name', 'Apellido y Nombres del Propietario', $defaultValue, $alfabetic, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-law', 'Derecho', (array_key_exists('derecho_mueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['derecho_mueble_R' . $i] : $defaultValue), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-benefit-cause', 'Causa del Beneficio', (array_key_exists('causa_beneficio_mueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['causa_beneficio_mueble_R' . $i] : $defaultValue), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-time-limit', 'Plazo', (array_key_exists('plazo_mueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['plazo_mueble_R' . $i] : $defaultValue), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-title-cost', 'Título', (array_key_exists('titulo_mueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titulo_mueble_R' . $i] : $defaultValue), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_mueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titular_mueble_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-asset', 'Tipo de Bien', (array_key_exists('tipo_bien_mueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_bien_mueble_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-brand', 'Marca/Descripción', (array_key_exists('marca_bien_mueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['marca_bien_mueble_R' . $i] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-manufacture-year', 'Año fabricación', (array_key_exists('anio_fabricacion_mueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['anio_fabricacion_mueble_R' . $i] : ''), $year, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-domain', 'Dominio/Matrícula', '', $alfanumeric, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-entry-year', 'Año ingreso', (array_key_exists('anio_ingreso_mueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['anio_ingreso_mueble_R' . $i] : ''), $year, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-funds-origin', 'Origen de fondos', (array_key_exists('origen_fondos_mueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['origen_fondos_mueble_R' . $i] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-titularity-percentage', 'Porcentaje de titularidad', (array_key_exists('porcentaje_titularidad_mueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['porcentaje_titularidad_mueble_R' . $i] : ''), $percentage, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-fiscal-value', 'Valuación', (array_key_exists('valor_fiscal_mueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['valor_fiscal_mueble_R' . $i] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-aquired-value', 'Valor de adquisición', (array_key_exists('valor_adquirido_mueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['valor_adquirido_mueble_R' . $i] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-improvements', 'Mejoras', (array_key_exists('mejoras_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['mejoras_R' . $i] : ''), $alfanumeric, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-amount-improvements', 'Monto total de mejoras', (array_key_exists('monto_total_mejoras_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['monto_total_mejoras_R' . $i] : ''), $money, false, true);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);

                $i++;
            }
            $seccionF->addSection($seccionS);
            //fin del tab 4-1

            $seccionS = new AffidavitSection('ddjj-unregistered-assets');
            $seccionS->setName('Bienes muebles no registrables');
            //                                            Bienes muebles no registrables  248084  separator_bienes_val_ind
            if (array_key_exists('titularidad_indi', $SADE_trans_Fields)) {
                $seccionST = new AffidavitSection('ddjj-unregistered-asset');
                $seccionST->setName('Bien mueble no registrable');

                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', (array_key_exists('titularidad_indi', $SADE_trans_Fields) ? $SADE_trans_Fields['titularidad_indi'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_indi', $SADE_trans_Fields) ? $SADE_trans_Fields['titular_indi'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-asset', 'Tipo de bien', (array_key_exists('tipo_bien_val_indi', $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_bien_val_indi'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-brand', 'Descripción', (array_key_exists('marca_bien_val_indi', $SADE_trans_Fields) ? $SADE_trans_Fields['marca_bien_val_indi'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-entry-year', 'Año de Ingreso/Adquisición', (array_key_exists('anio_ingreso_val_indi', $SADE_trans_Fields) ? $SADE_trans_Fields['anio_ingreso_val_indi'] : ''), $year, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-funds-origin', 'Origen de fondos', (array_key_exists('origen_fondos_val_indi', $SADE_trans_Fields) ? $SADE_trans_Fields['origen_fondos_val_indi'] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-titularity-percentage', 'Porcentaje de titularidad', (array_key_exists('porcentaje_titularidad_valor', $SADE_trans_Fields) ? $SADE_trans_Fields['porcentaje_titularidad_valor'] : ''), $percentage, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-fiscal-value', 'Valuación', (array_key_exists('valor_fiscal_val_indi', $SADE_trans_Fields) ? $SADE_trans_Fields['valor_fiscal_val_indi'] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-aquired-value', 'Valor de Adquisición', (array_key_exists('valor_adquirido_val_indi', $SADE_trans_Fields) ? $SADE_trans_Fields['valor_adquirido_val_indi'] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);
            }
            $i = 1;
            while (array_key_exists('titularidad_indi_R' . $i, $SADE_trans_Fields)) {

                $seccionST = new AffidavitSection('ddjj-unregistered-asset_R' . $i);
                $seccionST->setName('Bien mueble no registrable');

                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', (array_key_exists('titularidad_indi_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titularidad_indi_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_indi_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titular_indi_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-asset', 'Tipo de Bien', (array_key_exists('tipo_bien_val_indi_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_bien_val_indi_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-brand', 'Descripción', (array_key_exists('marca_bien_val_indi_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['marca_bien_val_indi_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-entry-year', 'Año de Ingreso/Adquisición', (array_key_exists('anio_ingreso_val_indi_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['anio_ingreso_val_indi_R' . $i] : ''), $year, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-funds-origin', 'Origen de fondos', (array_key_exists('origen_fondos_val_indi_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['origen_fondos_val_indi_R' . $i] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-titularity-percentage', 'Porcentaje de titularidad', (array_key_exists('porcentaje_titularidad_valor_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['porcentaje_titularidad_valor_R' . $i] : ''), $percentage, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-fiscal-value', 'Valuación', (array_key_exists('valor_fiscal_val_indi_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['valor_fiscal_val_indi_R' . $i] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-aquired-value', 'Valor de Adquisición', (array_key_exists('valor_adquirido_val_indi_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['valor_adquirido_val_indi_R' . $i] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);

                $i++;
            }

            $seccionF->addSection($seccionS); //fin del tab 4-2

            $seccionS = new AffidavitSection('ddjj-properties');
            $seccionS->setName('Bienes inmuebles');

            if (array_key_exists('titularidad_inmueble', $SADE_trans_Fields)) {
                $seccionST = new AffidavitSection('ddjj-property');
                $seccionST->setName('Bien inmueble');

                if ($SADE_trans_Fields['titularidad_inmueble'] != 'Otros') {

                    $defaultValue = 'N/A';

                } else {
                    $defaultValue = '';
                }

                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', (array_key_exists('titularidad_inmueble', $SADE_trans_Fields) ? $SADE_trans_Fields['titularidad_inmueble'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_bien_inmueble', $SADE_trans_Fields) ? $SADE_trans_Fields['titular_bien_inmueble'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-owner-name', 'Apellido y Nombres del Propietario',$defaultValue, $alfabetic, true ,false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-law', 'Derecho', (array_key_exists('derecho_inmueble', $SADE_trans_Fields) ? $SADE_trans_Fields['derecho_inmueble'] : $defaultValue), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-benefit-cause', 'Causa del Beneficio', (array_key_exists('causa_beneficio_inmueble', $SADE_trans_Fields) ? $SADE_trans_Fields['causa_beneficio_inmueble'] : $defaultValue), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-time-limit', 'Plazo', (array_key_exists('plazo_inmueble', $SADE_trans_Fields) ? $SADE_trans_Fields['plazo_inmueble'] : $defaultValue), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-title-cost', 'Título', (array_key_exists('titulo_inmueble', $SADE_trans_Fields) ? $SADE_trans_Fields['titulo_inmueble'] : $defaultValue), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-asset', 'Tipo de Bien', (array_key_exists('tipo_bien_inmueble', $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_bien_inmueble'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-country', 'País', (array_key_exists('pais', $SADE_trans_Fields) ? $SADE_trans_Fields['pais'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-province', 'Provincia', (array_key_exists('provincia_inmueble', $SADE_trans_Fields) ? $SADE_trans_Fields['provincia_inmueble'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-town', 'Localidad', (array_key_exists('localidad_inmueble', $SADE_trans_Fields) ? $SADE_trans_Fields['localidad_inmueble'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-neightborhood', 'Barrio/Zona', (array_key_exists('zona_inmueble', $SADE_trans_Fields) ? $SADE_trans_Fields['zona_inmueble'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-address', 'Domicilio', '', $alfanumeric, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-titularity-percentage', 'Porcentaje de titularidad', (array_key_exists('porcentaje_titularidad_inm', $SADE_trans_Fields) ? $SADE_trans_Fields['porcentaje_titularidad_inm'] : ''), $percentage, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-entry-year', 'Año de ingreso/adquisición', (array_key_exists('anio_ingreso_inmueble', $SADE_trans_Fields) ? $SADE_trans_Fields['anio_ingreso_inmueble'] : ''), $year, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-funds-origin', 'Origen de fondos', (array_key_exists('origen_fondos_inmueble', $SADE_trans_Fields) ? $SADE_trans_Fields['origen_fondos_inmueble'] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-area', 'Superficie total', (array_key_exists('superficie', $SADE_trans_Fields) ? $SADE_trans_Fields['superficie'] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-unit', 'Unidad', (array_key_exists('unidad_inmueble', $SADE_trans_Fields) ? $SADE_trans_Fields['unidad_inmueble'] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-fiscal-value', 'Valuación', (array_key_exists('valor_fiscal_inmueble', $SADE_trans_Fields) ? $SADE_trans_Fields['valor_fiscal_inmueble'] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-aquired-value', 'Valor de adquisición', (array_key_exists('valor_adquirido_inmueble', $SADE_trans_Fields) ? $SADE_trans_Fields['valor_adquirido_inmueble'] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-improvements', 'Mejoras', (array_key_exists('mejoras_1', $SADE_trans_Fields) ? $SADE_trans_Fields['mejoras_1'] : ''), $alfanumeric, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-amount-improvements', 'Monto total de mejoras', (array_key_exists('monto_mejoras', $SADE_trans_Fields) ? $SADE_trans_Fields['monto_mejoras'] : ''), $money, false, true);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);
            }
            $i = 1;
            while (array_key_exists('titularidad_inmueble_R' . $i, $SADE_trans_Fields)) {

                if ($SADE_trans_Fields['titularidad_inmueble_R'. $i] != 'Otros') {

                    $defaultValue = 'N/A';

                } else {
                    $defaultValue = '';
                }

                $seccionST = new AffidavitSection('ddjj-property_R' . $i);
                $seccionST->setName('Bien inmueble');

                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', (array_key_exists('titularidad_inmueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titularidad_inmueble_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-owner-name', 'Apellido y Nombres del Propietario',$defaultValue, $alfabetic, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-law', 'Derecho', (array_key_exists('derecho_inmueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['derecho_inmueble_R' . $i] : $defaultValue), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-benefit-cause', 'Causa del Beneficio', (array_key_exists('causa_beneficio_inmueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['causa_beneficio_inmueble_R' . $i] : $defaultValue), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-time-limit', 'Plazo', (array_key_exists('plazo_inmueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['plazo_inmueble_R' . $i] : $defaultValue), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-title-cost', 'Título', (array_key_exists('titulo_inmueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titulo_inmueble_R' . $i] : $defaultValue), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_bien_inmueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titular_bien_inmueble_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-asset', 'Tipo de Bien', (array_key_exists('tipo_bien_inmueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_bien_inmueble_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-country', 'País', (array_key_exists('pais_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['pais_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-province', 'Provincia', (array_key_exists('provincia_inmueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['provincia_inmueble_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-town', 'Localidad', (array_key_exists('localidad_inmueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['localidad_inmueble_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-neightborhood', 'Barrio/Zona', (array_key_exists('zona_inmueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['zona_inmueble_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-address', 'Domicilio', '', $alfanumeric, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-titularity-percentage', 'Porcentaje de titularidad', (array_key_exists('porcentaje_titularidad_inm_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['porcentaje_titularidad_inm_R' . $i] : ''), $percentage, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-entry-year', 'Año de ingreso/adquisición', (array_key_exists('anio_ingreso_inmueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['anio_ingreso_inmueble_R' . $i] : ''), $year, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-funds-origin', 'Origen de fondos', (array_key_exists('origen_fondos_inmueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['origen_fondos_inmueble_R' . $i] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-area', 'Superficie total', (array_key_exists('superficie_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['superficie_R' . $i] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-unit', 'Unidad', (array_key_exists('unidad_inmueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['unidad_inmueble_R' . $i] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-fiscal-value', 'Valuación', (array_key_exists('valor_fiscal_inmueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['valor_fiscal_inmueble_R' . $i] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-aquired-value', 'Valor de adquisición', (array_key_exists('valor_adquirido_inmueble_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['valor_adquirido_inmueble_R' . $i] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-improvements', 'Mejoras', (array_key_exists('mejoras_1_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['mejoras_1_R' . $i] : ''), $alfanumeric, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-amount-improvements', 'Monto total de mejoras', (array_key_exists('monto_mejoras_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['monto_mejoras_R' . $i] : ''), $money, false, true);
                $seccionST->addField($fieldST);

                $seccionS->addSection($seccionST);

                $i++;
            }

            $seccionF->addSection($seccionS); //fin del tab 4-3

            $seccionS = new AffidavitSection('ddjj-stocks');
            $seccionS->setName('Títulos, acciones, fondos comunes de inversión');
            if (array_key_exists('titularidad_titulos', $SADE_trans_Fields)) {
                $cotizacion_acciones = DDJJ::decodeDecimalSade($SADE_trans_Fields['valor_cotizacion_acciones']);
                $actual_acciones = DDJJ::decodeDecimalSade($SADE_trans_Fields['valor_actual_acciones']);
                $adquirido_acciones = DDJJ::decodeDecimalSade($SADE_trans_Fields['valor_adquirido_acciones']);


                $seccionST = new AffidavitSection('ddjj-stock');
                $seccionST->setName('Título');

                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', (array_key_exists('titularidad_titulos', $SADE_trans_Fields) ? $SADE_trans_Fields['titularidad_titulos'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_titulos', $SADE_trans_Fields) ? $SADE_trans_Fields['titular_titulos'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-title-type-value', 'Tipo de bien', (array_key_exists('tipo_titulo', $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_titulo'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-title-description', 'Descripción', (array_key_exists('descripcion', $SADE_trans_Fields) ? $SADE_trans_Fields['descripcion'] : ''), $alfanumeric, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-issuing-entity', 'Entidad emisora', '', $alfabetic, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-trade', 'Objeto', (array_key_exists('objeto', $SADE_trans_Fields) ? $SADE_trans_Fields['objeto'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-stock-qty', 'Cantidad de acciones/títulos/cuotas', (array_key_exists('cantidad_acciones', $SADE_trans_Fields) ? $SADE_trans_Fields['cantidad_acciones'] : ''), $numeric, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-aquired-date', 'Fecha de adquisición', (array_key_exists('fecha_adquiere_acciones', $SADE_trans_Fields) ? $SADE_trans_Fields['fecha_adquiere_acciones'] : ''), $date, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-funds-origin', 'Origen de fondos', (array_key_exists('origen_fondos_acciones', $SADE_trans_Fields) ? $SADE_trans_Fields['origen_fondos_acciones'] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-quote-value', 'Valor de cotización', (array_key_exists('valor_cotizacion_acciones', $SADE_trans_Fields) ? $cotizacion_acciones : ''), $money_decimal, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-current-value', 'Valor actual', (array_key_exists('valor_actual_acciones', $SADE_trans_Fields) ? $actual_acciones : ''), $money_decimal, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-aquired-value', 'Valor de adquisición', (array_key_exists('valor_adquirido_acciones', $SADE_trans_Fields) ? $adquirido_acciones : ''), $money_decimal, true, false);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);
            }
            $i = 1;
            while (array_key_exists('titularidad_titulos_R' . $i, $SADE_trans_Fields)) {

                $cotizacion_acciones = DDJJ::decodeDecimalSade($SADE_trans_Fields['valor_cotizacion_acciones_R' . $i]);
                $actual_acciones = DDJJ::decodeDecimalSade($SADE_trans_Fields['valor_actual_acciones_R' . $i]);
                $adquirido_acciones = DDJJ::decodeDecimalSade($SADE_trans_Fields['valor_adquirido_acciones_R' . $i]);

                $seccionST = new AffidavitSection('ddjj-stock_R' . $i);
                $seccionST->setName('Título');

                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', (array_key_exists('titularidad_titulos_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titularidad_titulos_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_titulos_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titular_titulos_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-title-type-value', 'Tipo de bien', (array_key_exists('tipo_titulo_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_titulo_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-title-description', 'Descripción', (array_key_exists('descripcion_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['descripcion_R' . $i] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-issuing-entity', 'Entidad emisora', '', $alfabetic, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-trade', 'Objeto', (array_key_exists('objeto_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['objeto_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-stock-qty', 'Cantidad de acciones/títulos/cuotas', (array_key_exists('cantidad_acciones_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['cantidad_acciones_R' . $i] : ''), $numeric, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-aquired-date', 'Fecha de adquisición', (array_key_exists('fecha_adquiere_acciones_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['fecha_adquiere_acciones_R' . $i] : ''), $date, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-funds-origin', 'Origen de fondos', (array_key_exists('origen_fondos_acciones_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['origen_fondos_acciones_R' . $i] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-quote-value', 'Valor de cotización', (array_key_exists('valor_cotizacion_acciones_R' . $i, $SADE_trans_Fields) ? $cotizacion_acciones : ''), $money_decimal, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-current-value', 'Valor actual', (array_key_exists('valor_actual_acciones_R' . $i, $SADE_trans_Fields) ? $actual_acciones : ''), $money_decimal, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-aquired-value', 'Valor de adquisición', (array_key_exists('valor_adquirido_acciones_R' . $i, $SADE_trans_Fields) ? $adquirido_acciones : ''), $money_decimal, true, true);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);

                $i++;
            }
            $seccionF->addSection($seccionS); //fin del tab 4-4

            $seccionS = new AffidavitSection('ddjj-partnerships');
            $seccionS->setName('Sociedades');
            if (array_key_exists('titularidad_sociedad', $SADE_trans_Fields)) {

                $seccionST = new AffidavitSection('ddjj-partnership');
                $seccionST->setName('Sociedad');

                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', (array_key_exists('titularidad_sociedad', $SADE_trans_Fields) ? $SADE_trans_Fields['titularidad_sociedad'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_sociedad', $SADE_trans_Fields) ? $SADE_trans_Fields['titular_sociedad'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-cuit-society', 'CUIT de la sociedad', '', $numeric, false, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-kind-society', 'Tipo de sociedad', (array_key_exists('tipo_sociedad', $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_sociedad'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-partnership-name', 'Nombre de sociedad', '', $alfabetic, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-partnership-trade', 'Objeto Social', (array_key_exists('objeto_social_sociedad', $SADE_trans_Fields) ? $SADE_trans_Fields['objeto_social_sociedad'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-titularity-percentage', 'Porcentaje de Participación', (array_key_exists('porcentaje_participacion_soc', $SADE_trans_Fields) ? $SADE_trans_Fields['porcentaje_participacion_soc'] : ''), $percentage, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-aquired-date', 'Fecha de adquisición', (array_key_exists('fecha_aduiere_sociedad', $SADE_trans_Fields) ? $SADE_trans_Fields['fecha_aduiere_sociedad'] : ''), $date, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-funds-origin', 'Origen de fondos', (array_key_exists('origen_fondos_sociedad', $SADE_trans_Fields) ? $SADE_trans_Fields['origen_fondos_sociedad'] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-current-value', 'Valor actual', (array_key_exists('valor_actual_sociedad', $SADE_trans_Fields) ? $SADE_trans_Fields['valor_actual_sociedad'] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-aquired-value', 'Valor de adquisición', (array_key_exists('valor_adquisicion_sociedad', $SADE_trans_Fields) ? $SADE_trans_Fields['valor_adquisicion_sociedad'] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-contributions', 'Aportes', (array_key_exists('aportes', $SADE_trans_Fields) ? $SADE_trans_Fields['aportes'] : ''), $alfanumeric, false, true);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);
            }
            $i = 1;
            while (array_key_exists('titularidad_sociedad_R' . $i, $SADE_trans_Fields)) {

                $seccionST = new AffidavitSection('ddjj-partnership_R' . $i);
                $seccionST->setName('Sociedad');

                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', (array_key_exists('titularidad_sociedad_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titularidad_sociedad_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_sociedad_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titular_sociedad_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-cuit-society', 'CUIT de la sociedad', '', $numeric, false, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-kind-society', 'Tipo de sociedad', (array_key_exists('tipo_sociedad_R', $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_sociedad_R'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-partnership-name', 'Nombre de sociedad', '', $alfabetic, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-partnership-trade', 'Objeto Social', (array_key_exists('objeto_social_sociedad_R', $SADE_trans_Fields) ? $SADE_trans_Fields['objeto_social_sociedad_R'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-titularity-percentage', 'Porcentaje de Participación', (array_key_exists('porcentaje_participacion_soc_R', $SADE_trans_Fields) ? $SADE_trans_Fields['porcentaje_participacion_soc_R'] : ''), $percentage, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-aquired-date', 'Fecha de adquisición', (array_key_exists('fecha_aduiere_sociedad_R', $SADE_trans_Fields) ? $SADE_trans_Fields['fecha_aduiere_sociedad_R'] : ''), $date, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-funds-origin', 'Origen de fondos', (array_key_exists('origen_fondos_sociedad_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['origen_fondos_sociedad_R' . $i] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-current-value', 'Valor actual', (array_key_exists('valor_actual_sociedad_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['valor_actual_sociedad_R' . $i] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-aquired-value', 'Valor de adquisición', (array_key_exists('valor_adquisicion_sociedad_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['valor_adquisicion_sociedad_R' . $i] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-contributions', 'Aportes', (array_key_exists('aportes_R', $SADE_trans_Fields) ? $SADE_trans_Fields['aportes_R'] : ''), $alfanumeric, false, true);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);

                $i++;
            }
            $seccionF->addSection($seccionS);  // fin del tab 4-5

            $seccionS = new AffidavitSection('ddjj-accounts');
            $seccionS->setName('Depósitos bancarios / Dinero en efectivo');
            if (array_key_exists('titularidad_efectivo', $SADE_trans_Fields)) {
                if ($SADE_trans_Fields['tipo_bien_efectivo'] === 'Tenencia de dinero en efectivo') {
                    $entity_cuit = 0;
                    $entity = 'N/A';
                    $account_number = 0;
                    $account_type = 'N/A';
                    $titularity_percentage = 0;
                } else {
                    $entity_cuit = '';
                    $entity = '';
                    $account_number = '';
                    $account_type = '';
                    $titularity_percentage = '';
                }

                $seccionST = new AffidavitSection('ddjj-account');
                //$seccionST->setName($SADE_trans_Fields['titularidad_efectivo']);
                $seccionST->setName('Depósito bancario / Dinero en efectivo');

                $fieldST = new AffidavitField('ddjj-asset-type', 'Tipo de bien', (array_key_exists('tipo_bien_efectivo', $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_bien_efectivo'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', (array_key_exists('titularidad_efectivo', $SADE_trans_Fields) ? $SADE_trans_Fields['titularidad_efectivo'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_efectivo', $SADE_trans_Fields) ? $SADE_trans_Fields['titular_efectivo'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-account-type', 'Tipo de cuenta', (array_key_exists('tipo_cuenta_efectivo', $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_cuenta_efectivo'] : $account_type), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-titularity-percentage', 'Porcentaje de titularidad', (array_key_exists('porcentaje_titular_efectivo', $SADE_trans_Fields) ? $SADE_trans_Fields['porcentaje_titular_efectivo'] : $titularity_percentage), $percentage, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-entity', 'Entidad', $entity, $alfabetic, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-entity-cuit', 'CUIT de la Entidad', $entity_cuit, $numeric, false, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-account-number', 'Número de cuenta', $account_number, $numeric, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-currency', 'Moneda', (array_key_exists('moneda_efectivo', $SADE_trans_Fields) ? $SADE_trans_Fields['moneda_efectivo'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-funds-origin', 'Origen de fondos', (array_key_exists('origen_fondos_efectivo', $SADE_trans_Fields) ? $SADE_trans_Fields['origen_fondos_efectivo'] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-amount', 'Monto total', (array_key_exists('monto_efectivo', $SADE_trans_Fields) ? $SADE_trans_Fields['monto_efectivo'] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);
            }
            $i = 1;
            while (array_key_exists('titularidad_efectivo_R' . $i, $SADE_trans_Fields)) {

                if ($SADE_trans_Fields['tipo_bien_efectivo_R' . $i] === 'Tenencia de dinero en efectivo') {
                    $entity_cuit = 0;
                    $entity = 'N/A';
                    $account_number = 0;
                    $account_type = 'N/A';
                    $titularity_percentage = 0;
                } else {
                    $entity_cuit = '';
                    $entity = '';
                    $account_number = '';
                    $account_type = '';
                    $titularity_percentage = '';
                }

                $seccionST = new AffidavitSection('ddjj-account_R' . $i);
                //$seccionST->setName($SADE_trans_Fields['titularidad_efectivo_R'.$i]);
                $seccionST->setName('Depósito bancario / Dinero en efectivo');
                $fieldST = new AffidavitField('ddjj-asset-type', 'Tipo de bien', (array_key_exists('tipo_bien_efectivo_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_bien_efectivo_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', (array_key_exists('titularidad_efectivo_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titularidad_efectivo_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_efectivo_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titular_efectivo_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-account-type', 'Tipo de cuenta', (array_key_exists('tipo_cuenta_efectivo_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_cuenta_efectivo_R' . $i] : $account_type), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-titularity-percentage', 'Porcentaje de titularidad', (array_key_exists('porcentaje_titular_efectivo_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['porcentaje_titular_efectivo_R' . $i] : $titularity_percentage), $percentage, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-entity', 'Entidad', $entity, $alfabetic, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-entity-cuit', 'CUIT de la Entidad', $entity_cuit, $numeric, false, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-account-number', 'Número de cuenta', $account_number, $numeric, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-currency', 'Moneda', (array_key_exists('moneda_efectivo_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['moneda_efectivo_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-funds-origin', 'Origen de fondos', (array_key_exists('origen_fondos_efectivo_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['origen_fondos_efectivo_R' . $i] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-amount', 'Monto total', (array_key_exists('monto_efectivo_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['monto_efectivo_R' . $i] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);

                $i++;
            }

            $seccionF->addSection($seccionS); // fin del tab 4-6

            $seccionS = new AffidavitSection('ddjj-other-jobs');
            $seccionS->setName('Ingreso por otros trabajos / actividades');
            if (array_key_exists('titularidad_otros_trabajos', $SADE_trans_Fields)) {
                $seccionST = new AffidavitSection('ddjj-other-job');
                $seccionST->setName('Trabajo');

                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', (array_key_exists('titularidad_otros_trabajos', $SADE_trans_Fields) ? $SADE_trans_Fields['titularidad_otros_trabajos'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_otros_trabajos', $SADE_trans_Fields) ? $SADE_trans_Fields['titular_otros_trabajos'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-position', 'Cargo o función', (array_key_exists('cargo_otros_trabajos', $SADE_trans_Fields) ? $SADE_trans_Fields['cargo_otros_trabajos'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-employment-relationship', 'Relación Laboral', (array_key_exists('relacion_laboral_otros_trabajo', $SADE_trans_Fields) ? $SADE_trans_Fields['relacion_laboral_otros_trabajo'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-employer', 'Empleador/Entidad', (array_key_exists('empleador_otros_trabajos', $SADE_trans_Fields) ? $SADE_trans_Fields['empleador_otros_trabajos'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-employer-activity', 'Actividad de la empresa, etc/ámbito', (array_key_exists('actividad_otros_trabajos', $SADE_trans_Fields) ? $SADE_trans_Fields['actividad_otros_trabajos'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-net-anual-amount', 'Monto anual neto', (array_key_exists('monto_otros_trabajos', $SADE_trans_Fields) ? $SADE_trans_Fields['monto_otros_trabajos'] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-other-job-from', 'Desde', (array_key_exists('fecha_desde_otros_trabajos', $SADE_trans_Fields) ? $SADE_trans_Fields['fecha_desde_otros_trabajos'] : ''), $date, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-activity-status', '¿Sigue en actividad?', (array_key_exists('en_actividad_otros_trabajos', $SADE_trans_Fields) ? $SADE_trans_Fields['en_actividad_otros_trabajos'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);
            }
            $i = 1;
            while (array_key_exists('titularidad_otros_trabajos_R' . $i, $SADE_trans_Fields)) {

                $seccionST = new AffidavitSection('ddjj-other-job_R' . $i);
                $seccionST->setName('Trabajo');

                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', (array_key_exists('titularidad_otros_trabajos_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titularidad_otros_trabajos_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_otros_trabajos_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titular_otros_trabajos_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-position', 'Cargo o función', (array_key_exists('cargo_otros_trabajos_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['cargo_otros_trabajos_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-employment-relationship', 'Relación Laboral', (array_key_exists('relacion_laboral_otros_trabajo_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['relacion_laboral_otros_trabajo_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-employer', 'Empleador/Entidad', (array_key_exists('empleador_otros_trabajos_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['empleador_otros_trabajos_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-employer-activity', 'Actividad de la empresa, etc/ámbito', (array_key_exists('actividad_otros_trabajos_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['actividad_otros_trabajos_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-net-anual-amount', 'Monto anual neto', (array_key_exists('monto_otros_trabajos_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['monto_otros_trabajos_R' . $i] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-other-job-from', 'Desde', (array_key_exists('fecha_desde_otros_trabajos_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['fecha_desde_otros_trabajos_R' . $i] : ''), $date, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-activity-status', '¿Sigue en actividad?', (array_key_exists('en_actividad_otros_trabajos_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['en_actividad_otros_trabajos_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);

                $i++;
            }
            $seccionF->addSection($seccionS); //fin del tab 4-7

            $seccionS = new AffidavitSection('ddjj-other-activities');
            $seccionS->setName('Otros ingresos / activos');

            if (array_key_exists('titularidad_otro_ingreso', $SADE_trans_Fields)) {

                $seccionST = new AffidavitSection('ddjj-other-activity');
                $seccionST->setName('Actividades');

                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', $SADE_trans_Fields['titularidad_otro_ingreso'], $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_otro_ingreso', $SADE_trans_Fields) ? $SADE_trans_Fields['titular_otro_ingreso'] : ''), $alfanumeric, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-income-type', 'Tipo de ingreso / activo', (array_key_exists('tipo_ingreso', $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_ingreso'] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-origin-income', 'Origen / concepto', (array_key_exists('derecho_originante_ingreso', $SADE_trans_Fields) ? $SADE_trans_Fields['derecho_originante_ingreso'] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-total-received-income', 'Monto total recibido', (array_key_exists('monto_total_ingreso', $SADE_trans_Fields) ? $SADE_trans_Fields['monto_total_ingreso'] : ''), $numeric, true, true);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);
            }
            $i = 1;
            while (array_key_exists('titularidad_otro_ingreso_R' . $i, $SADE_trans_Fields)) {

                $seccionST = new AffidavitSection('ddjj-other-activity_R' . $i);
                $seccionST->setName('Actividades');

                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', $SADE_trans_Fields['titularidad_otro_ingreso_R' . $i], $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_otro_ingreso_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titular_otro_ingreso_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-income-type', 'Tipo de ingreso / activo', (array_key_exists('tipo_ingreso_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_ingreso_R' . $i] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-origin-income', 'Origen / concepto', (array_key_exists('derecho_originante_ingreso_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['derecho_originante_ingreso_R' . $i] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-total-received-income', 'Monto total recibido', (array_key_exists('monto_total_ingreso_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['monto_total_ingreso_R' . $i] : ''), $numeric, true, true);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST); //fin del tab 4-8

                $i++;
            }

            $seccionF->addSection($seccionS); //fin del tab 4-8

            $seccionS = new AffidavitSection('ddjj-real-state-sellings');
            $seccionS->setName('Ingreso por venta de bienes inmuebles');

            if (array_key_exists('titularidad_venta', $SADE_trans_Fields)) {

                $seccionST = new AffidavitSection('ddjj-real-state-selling');
                $seccionST->setName('Venta inmueble');
                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', (array_key_exists('titularidad_venta', $SADE_trans_Fields) ? $SADE_trans_Fields['titularidad_venta'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_venta', $SADE_trans_Fields) ? $SADE_trans_Fields['titular_venta'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-asset', 'Tipo de Bien', (array_key_exists('tipo_bien_venta', $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_bien_venta'] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-country', 'País', (array_key_exists('pais_venta', $SADE_trans_Fields) ? $SADE_trans_Fields['pais_venta'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-province', 'Provincia', (array_key_exists('provincia_venta', $SADE_trans_Fields) ? $SADE_trans_Fields['provincia_venta'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-town', 'Localidad', (array_key_exists('localidad_venta', $SADE_trans_Fields) ? $SADE_trans_Fields['localidad_venta'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-neightborhood', 'Barrio/Zona', (array_key_exists('barrio_venta', $SADE_trans_Fields) ? $SADE_trans_Fields['barrio_venta'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-address', 'Domicilio', '', $alfanumeric, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-titularity-percentage', 'Porcentaje de titularidad', (array_key_exists('porcentaje_titularidad_venta', $SADE_trans_Fields) ? $SADE_trans_Fields['porcentaje_titularidad_venta'] : ''), $percentage, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-entry-year', 'Año de ingreso', (array_key_exists('anio_ingreso_venta', $SADE_trans_Fields) ? $SADE_trans_Fields['anio_ingreso_venta'] : ''), $year, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-currency', 'Moneda', (array_key_exists('moneda_venta', $SADE_trans_Fields) ? $SADE_trans_Fields['moneda_venta'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                //$fieldST = new AffidavitField('ddjj-amount', 'Monto', '', $numeric, true, false);
                $fieldST = new AffidavitField('ddjj-amount', 'Monto', (array_key_exists('monto_venta', $SADE_trans_Fields) ? $SADE_trans_Fields['monto_venta'] : ''), $numeric, true, true);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);
            }
            $i = 1;
            while (array_key_exists('titularidad_venta_R' . $i, $SADE_trans_Fields)) {

                $seccionST = new AffidavitSection('ddjj-real-state-selling_R' . $i);
                $seccionST->setName('Venta inmueble');
                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', (array_key_exists('titularidad_venta_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titularidad_venta_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_venta_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titular_venta_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-asset', 'Tipo de Bien', (array_key_exists('tipo_bien_venta_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_bien_venta_R' . $i] : ''), $alfanumeric, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-country', 'País', (array_key_exists('pais_venta_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['pais_venta_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-province', 'Provincia', (array_key_exists('provincia_venta_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['provincia_venta_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-town', 'Localidad', (array_key_exists('localidad_venta_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['localidad_venta_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-neightborhood', 'Barrio/Zona', (array_key_exists('barrio_venta_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['barrio_venta_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                // $fieldST = new AffidavitField('ddjj-address', 'Domicilio', (array_key_exists('domicilio_venta_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['domicilio_venta_R' . $i] : ''), $alfanumeric, true, true);

                $fieldST = new AffidavitField('ddjj-address', 'Domicilio', '', $alfanumeric, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-titularity-percentage', 'Porcentaje de titularidad', (array_key_exists('porcentaje_titularidad_venta_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['porcentaje_titularidad_venta_R' . $i] : ''), $percentage, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-entry-year', 'Año de ingreso', (array_key_exists('anio_ingreso_venta_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['anio_ingreso_venta_R' . $i] : ''), $year, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-currency', 'Moneda', (array_key_exists('moneda_venta_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['moneda_venta_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-amount', 'Monto', (array_key_exists('monto_venta_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['monto_venta_R' . $i] : ''), $numeric, true, true);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);

                $i++;
            }
            $seccionF->addSection($seccionS); //fin del tab 4-9


            $seccionS = new AffidavitSection('ddjj-debts');
            $seccionS->setName('Deudas');
            if (array_key_exists('titularidad_deuda', $SADE_trans_Fields)) {

                $seccionST = new AffidavitSection('ddjj-debt');
                $seccionST->setName('Deuda');

                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', (array_key_exists('titularidad_deuda', $SADE_trans_Fields) ? $SADE_trans_Fields['titularidad_deuda'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_deuda', $SADE_trans_Fields) ? $SADE_trans_Fields['titular_deuda'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-debt-type', 'Tipo de deuda', (array_key_exists('tipo_deuda', $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_deuda'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-creditor', 'Acreedor', '', $alfabetic, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-cuit', 'CUIT/CUIL', '', $numeric, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-currency', 'Moneda', (array_key_exists('moneda_deuda', $SADE_trans_Fields) ? $SADE_trans_Fields['moneda_deuda'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-amount', 'Monto', '', $numeric, true, false);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);
            }
            $i = 1;
            while (array_key_exists('titularidad_deuda_R' . $i, $SADE_trans_Fields)) {

                $seccionST = new AffidavitSection('ddjj-debt_R' . $i);
                $seccionST->setName('Deuda');

                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', (array_key_exists('titularidad_deuda_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titularidad_deuda_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_deuda_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titular_deuda_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-debt-type', 'Tipo de deuda', (array_key_exists('tipo_deuda_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_deuda_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-creditor', 'Acreedor', '', $alfabetic, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-cuit', 'CUIT/CUIL', '', $numeric, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-currency', 'Moneda', (array_key_exists('moneda_deuda_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['moneda_deuda_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-amount', 'Monto', '', $numeric, true, false);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);
                $i++;
            }
            $seccionF->addSection($seccionS); //fin del tab 4-10

            $seccionS = new AffidavitSection('ddjj-claims');
            $seccionS->setName('Acreencias');
            if (array_key_exists('titularidad_acreencias', $SADE_trans_Fields)) {

                $seccionST = new AffidavitSection('ddjj-claim');
                $seccionST->setName('Acreencia');

                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', (array_key_exists('titularidad_acreencias', $SADE_trans_Fields) ? $SADE_trans_Fields['titularidad_acreencias'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_acreencias', $SADE_trans_Fields) ? $SADE_trans_Fields['titular_acreencias'] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-debt-type', 'Tipo de acreencia', (array_key_exists('tipo_acreencia', $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_acreencia'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-creditor', 'Deudor', '', $alfabetic, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-cuit', 'CUIT/CUIL', '', $numeric, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-currency', 'Moneda', (array_key_exists('moneda_acreencia', $SADE_trans_Fields) ? $SADE_trans_Fields['moneda_acreencia'] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-amount', 'Monto', (array_key_exists('monto_acreencia', $SADE_trans_Fields) ? $SADE_trans_Fields['monto_acreencia'] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-funds-origin', 'Origen de fondos', '', $alfanumeric, true, false);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);
            }
            $i = 1;
            while (array_key_exists('titularidad_acreencias_R' . $i, $SADE_trans_Fields)) {

                $seccionST = new AffidavitSection('ddjj-claim_R' . $i);
                $seccionST->setName('Acreencia');
                $fieldST = new AffidavitField('ddjj-ownership', 'Titularidad', (array_key_exists('titularidad_acreencias_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titularidad_acreencias_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-bond', 'Titular', (array_key_exists('titular_acreencias_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['titular_acreencias_R' . $i] : ''), $alfabetic, false, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-debt-type', 'Tipo de acreencia', (array_key_exists('tipo_acreencia_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['tipo_acreencia_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-creditor', 'Deudor', '', $alfabetic, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-cuit', 'CUIT/CUIL', '', $numeric, true, false);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-currency', 'Moneda', (array_key_exists('moneda_acreencia_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['moneda_acreencia_R' . $i] : ''), $alfabetic, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-amount', 'Monto', (array_key_exists('monto_acreencia_R' . $i, $SADE_trans_Fields) ? $SADE_trans_Fields['monto_acreencia_R' . $i] : ''), $money, true, true);
                $seccionST->addField($fieldST);
                $fieldST = new AffidavitField('ddjj-funds-origin', 'Origen de fondos', '', $alfanumeric, true, false);
                $seccionST->addField($fieldST);
                $seccionS->addSection($seccionST);
                $i++;
            }
            $seccionF->addSection($seccionS); //fin del tab 4-11

            $affidavit->addSection($seccionF); //fin del tab 4

            $seccionF = new AffidavitSection('ddjj-disclaimers');
            $seccionF->setName('Disclaimers');
            $fieldS = new AffidavitField('ddjj-observations', 'Observaciones', '', $alfanumeric, false, true);
            $seccionF->addField($fieldS);
            $fieldS = new AffidavitField('ddjj-disclaimer-1', 'Disclaimer 1', '', $alfanumeric, true, false);
            $seccionF->addField($fieldS);
            $fieldS = new AffidavitField('ddjj-disclaimer-2', 'Disclaimer 2', '', $alfanumeric, true, false);
            $seccionF->addField($fieldS);
            $fieldS = new AffidavitField('ddjj-disclaimer-3', 'Disclaimer 3', '', $alfanumeric, true, false);
            $seccionF->addField($fieldS);
            $fieldS = new AffidavitField('ddjj-disclaimer-4', 'Disclaimer 4', '', $alfanumeric, true, false);
            $seccionF->addField($fieldS);
            $fieldS = new AffidavitField('ddjj-disclaimer-5', 'Disclaimer 5', '', $alfanumeric, true, false);
            $seccionF->addField($fieldS);

            $affidavit->addSection($seccionF); //fin del tab 5

            Session::modify('Affidavit', $affidavit);


            // setea el tiempo de session para una nueva DDJJ
            self::setLifeTime();
        } catch (Exception $e) {
         //   self::saveError(SADE_TRANSACTION_SERVICE, $e);
            $rtn['error']['code'] = 1;
            $rtn['error']['message'] = $e->getMessage();
        }
        return $rtn;
    }

    /**
     * Funcion deprecada.. eliminar en prox. versión
     * */
    public static function loadDocument() {

        $rtn = [
            'error' => [
                'code' => 0,
                'message' => ''
            ]
        ];

        Session::retrieve();

        if (isset($_SESSION['Affidavit']) && is_a($_SESSION['Affidavit'], 'AffidavitDocument')) {
            
        } else {
            DDJJ::create();
        }

        $rtn['error']['code'] = 0;

        return $rtn;
    }

    public static function save($section, $fields) {
        $rtn = [
            'error' => [
                'code' => 0,
                'message' => ''
            ]
        ];
        try {

            if (is_array($section) && count($section) > 0) {

                Session::retrieve();
                $aSection = $_SESSION['Affidavit']->getSection($section[0]['id']);
                // $aSection->setName($section[0]['name']); setea el nombre de la seccion NO BORRAR POSIBLE MEJORA

                for ($i = 1; $i < count($section); $i++) {
                    $aSection = $aSection->getSection($section[$i]['id']);
                    //     $aSection->setName($section[$i]['name']); setea el nombre de la seccion NO BORRAR POSIBLE MEJORA
                }

                if (is_array($fields) && count($fields) > 0)
                {
                    foreach ($fields as $field) {

                        $aField = $aSection->getField($field['id']);
    
                        if  (is_object($aField))
                        {
                            if ($field['id'] == 'ddjj-current-company-hours-week') {
                                $value = strpos($field['value'], ' Hs.') ? $field['value'] : ($field['value'] . ' Hs.');
                                $aField->setValue($value);
                            }
                            else 
                            {
                                $aField->setValue($field['value']);
                            }
                                
                        }
                        else 
                        {
                            throw new Exception($aField['error']['message']);
                        }
                    }
                } 
                else 
                {
                    throw new Exception('Se requiere el menos un campo');
                }

            } else {
                throw new Exception('Se requiere al menos una sección');
            }

        } catch (Exception $e) {
            $rtn['error']['code'] = 1;
            $rtn['error']['message'] = $e->getMessage();
        }

        return $rtn;
    }

    public static function loadJson() {

        $rtn = [
            'error' => [
                'code' => 0,
                'message' => ''
            ]
        ];
        $JsonArray = array();

        Session::retrieve();

        if (isset($_SESSION['Affidavit']) && is_a($_SESSION['Affidavit'], 'AffidavitDocument')) {
            foreach ($_SESSION['Affidavit']->getSections() as $aSession) {
                DDJJ::recursiveJson($aSession, $JsonArray[$aSession->getName()]);
            }

            return($JsonArray);
        } else {
            $rtn['error']['cde'] = 0;
            return $rtn;
        }
    }

    public static function recursiveJson($aSession, &$JsonArray) {
        // array_push($JsonArray, "manzana", "arándano");
        // $JsonArray[$key] = $value;
        // $JsonArray = array("kusanagi" => "japon"

        foreach ($aSession->getEverything() as $element) {
            if (is_a($element, "AffidavitSection")) {
                //$JsonArray[$element->getName()] = '';
                DDJJ::recursiveJson($element, $JsonArray[$element->getName()][]);
            } elseif (is_a($element, "AffidavitField")) {
                $JsonArray[$element->getName()] ['isPublic'] = $element->isPublic();
                //$JsonArray[$element->getName()] ['Value'] = $element->getValue();
                if ($element->isPublic()) {
                    $JsonArray[$element->getName()] ['Value'] = $element->getValue();
                } else {
                    $JsonArray[$element->getName()] ['Value'] = '';
                }
            }
        }
        return $JsonArray;
    }

    public static function recursivePDF($aSession, &$pdf) {
        $fontTipe = 'Arial'; //tipo de fuente
        $linehigh = 4; //alto de un linea de texto


        foreach ($aSession->getEverything() as $element) {
            if (is_a($element, "AffidavitSection")) {
                if ($element->getName() != 'Disclaimers') {
                    // $pdf->SetLeftMargin(15);
                    $pdf->SetFont($fontTipe, 'B', 12);
                    $pdf->Cell(0, 10, utf8_decode($element->getName()), 0, 0);
                    $pdf->Ln();
                }
                DDJJ::recursivePDF($element, $pdf);
            } elseif (is_a($element, "AffidavitField")) {

                $pdf->SetFont($fontTipe, '', 10);

                if (strlen($element->getName()) >= 80) {
                    if ($element->getId() === 'ddjj_member_commission' && $element->getValue() === 'Si') {
                        $pdf->MultiCell(0, $linehigh, utf8_decode($element->getName()), 0, 'L', 0);
                    } else if ($element->getId() != 'ddjj_member_commission') {
                        $pdf->MultiCell(0, $linehigh, utf8_decode($element->getName()), 0, 'L', 0);
                        $pdf->Cell(0, $linehigh, utf8_decode($element->getValue()), 0, 1);
                    }
                } else if (substr($element->getName(), 0, 10) == 'Disclaimer') {
                    $pdf->SetFont($fontTipe, 'B', 10);
                    $pdf->MultiCell(0, $linehigh + 2, utf8_decode($element->getValue()), 0, 'L', 0);
                    $pdf->Ln();
                } else if ($element->getId() === 'ddjj-observations') {
                    $currentUser = new User();
                    $currentUser->load();

                    if ($currentUser->hasCommentPrivilege()) {
                        $pdf->SetFont($fontTipe, 'B', 10);
                        $pdf->Cell($pdf->GetStringWidth(utf8_decode($element->getName()) . ': '), $linehigh, utf8_decode($element->getName()) . ': ', 0, 0);
                        $pdf->Ln(5);
                        $pdf->SetFont($fontTipe, '', 10);

                        $pdf->MultiCell(0, $linehigh + 2, utf8_decode($element->getValue()), 0, 'L', 0);
                        $pdf->Ln(10);
                    }
                } else {
                    if (!($element->getValue() === '00-00-0000')) {
                        $pdf->Cell($pdf->GetStringWidth(utf8_decode($element->getName()) . ': '), $linehigh, utf8_decode($element->getName()) . ': ', 0, 0);
                        $pdf->Cell(0, $linehigh, utf8_decode($element->getValue()), 0, 1);
                        $pdf->Ln();
                    }
                }
            }
        }
        return $pdf;
    }

    public static function createPDF() {
        Session::retrieve();

        $pdf = new FPDF('P', 'mm', 'A4');
        $fontTipe = 'Arial'; //tipo de fuente
        $linehigh = 4; //alto de un linea de texto
        setlocale(LC_TIME, 'es_ES.UTF-8');
        $dateString = strftime("Buenos Aires, %A %d de %B de %Y");

        $pdf->AddPage();
        $pdf->Image('../media/img/logo_ciudad_sm.png', 100, 12, 15, 20, 'PNG'); // logo de BA
        $pdf->SetFont($fontTipe, 'B', 12);
        $pdf->Ln(22);
        $pdf->cell(0, $linehigh, utf8_decode("GOBIERNO DE LA CIUDAD DE BUENOS AIRES"), 0, 1, 'C');
        $pdf->Ln(5);
        $pdf->SetFont($fontTipe, 'B', 12);
        $pdf->cell(0, ($linehigh + 2), utf8_decode("Declaración Jurada"), 0, 1, 'C');
        $pdf->cell(0, ($linehigh + 2), utf8_decode("Número:"), 0, 1, 'L');
        $pdf->SetFont($fontTipe, '', 12);
        $pdf->cell(0, ($linehigh + 2), utf8_decode($dateString), 0, 1, 'R');
        $pdf->SetFont($fontTipe, 'B', 12);
        $pdf->cell(24, ($linehigh + 2), utf8_decode("Referencia :"), 0, 0, 'L');
        $pdf->SetFont($fontTipe, '', 12);
        $pdf->cell(0, ($linehigh + 2), utf8_decode("Declaración Jurada"), 0, 1, 'L');
        $pdf->SetDrawColor(173, 173, 173);
        $pdf->Line(10, 70, 200, 70);

        // $pdf->SetLeftMargin(10);


        foreach ($_SESSION['Affidavit']->getSections() as $aSession) {
            $pdf->Ln();
            if ($aSession->getName() != 'Disclaimers') {
                $pdf->SetFont($fontTipe, 'B', 14); //negrita
                $pdf->cell(0, $linehigh, utf8_decode($aSession->getName()), 0, 1, 'L');
            }
            $pdf->Ln(3);
            DDJJ::recursivePDF($aSession, $pdf);
        }
        
        return DDJJ::signPDF($pdf);
    }

    private static function signPDF(FPDF $pdf) 
    {
        try
        {
            $rtn = Communicate::signFileLivecycle($pdf);
            if(isset($rtn['error']))
            {
                throw new Exception($rtn['error']['message']);
            }
        } catch (Exception $e) {
            $rtn['error']['code'] = 1;
            $rtn['error']['message'] = $e->getMessage();
        }

        return $rtn;
    }

    public static function commit() {

        require_once __DIR__ . '/../../sys' . DIRECTORY_SEPARATOR . 'conf' . DIRECTORY_SEPARATOR . 'SADEFrm.php';

        $rtn = [
            'error' => [
                'code' => 0,
                'message' => ''
            ]
        ];

        try {

            Session::retrieve();
            $currentUser = new User();

            $currentUser->load();

            $tab1Parent = $_SESSION['Affidavit']->getSection('ddjj-personal-data');
            $tab1_1Parent = $tab1Parent->getSection('ddjj-personal-info');
            $tab1_2Parent = $tab1Parent->getSection('ddjj-gcba-work-info');

            $arrFields = array(
                /**
                 * Solapa I: Inicio
                 * */
                array(
                    'etiqueta' => $tab1_1Parent->getField('ddjj-type')->getName(),
                    'idFormComp' => $SADEFrm[ENVIROMENT]['tipo_presentacion'], //'33526',
                    'inputName' => 'tipo_presentacion',
                    'valorStr' => $tab1_1Parent->getField('ddjj-type')->getValue()
                ),
                array(
                    'etiqueta' => $tab1_1Parent->getField('ddjj-du-type')->getName(),
                    'idFormComp' => $SADEFrm[ENVIROMENT]['tipo_docum'], //'33521',
                    'inputName' => 'tipo_docum',
                    'valorStr' => $tab1_1Parent->getField('ddjj-du-type')->getValue()
                ),
                array(
                    'etiqueta' => $tab1_1Parent->getField('ddjj-du-number')->getName(),
                    'idFormComp' => $SADEFrm[ENVIROMENT]['num_docum'], //'33582',
                    'inputName' => 'num_docum',
                    'valorLong' => $tab1_1Parent->getField('ddjj-du-number')->getValue()
                ),
                array(
                    'etiqueta' => $tab1_1Parent->getField('ddjj-cuit')->getName(),
                    'idFormComp' => $SADEFrm[ENVIROMENT]['cuil_cuit'], //'33484',
                    'inputName' => 'cuil_cuit',
                    'valorLong' => $tab1_1Parent->getField('ddjj-cuit')->getValue()
                ),
                array(
                    'etiqueta' => $tab1_1Parent->getField('ddjj-user')->getName(),
                    'idFormComp' => $SADEFrm[ENVIROMENT]['nombre_apellido'], //'33494',
                    'inputName' => 'nombre_apellido',
                    'valorStr' => $tab1_1Parent->getField('ddjj-user')->getValue()
                ),
                array(
                    'etiqueta' => $tab1_1Parent->getField('ddjj-date')->getName(),
                    'idFormComp' => $SADEFrm[ENVIROMENT]['fecha_nacimiento'], //'33527',
                    'inputName' => 'fecha_nacimiento',
                    'valorStr' => $tab1_1Parent->getField('ddjj-date')->getValue()
                ),
                array(
                    'etiqueta' => $tab1_1Parent->getField('ddjj-civil-status')->getName(),
                    'idFormComp' => $SADEFrm[ENVIROMENT]['estado_civil'], //'33495',
                    'inputName' => 'estado_civil',
                    'valorStr' => $tab1_1Parent->getField('ddjj-civil-status')->getValue()
                ),
                array(
                    'etiqueta' => $tab1_2Parent->getField('ddjj-date-gcba')->getName(),
                    'idFormComp' => $SADEFrm[ENVIROMENT]['fecha_ingreso'], //'33618',
                    'inputName' => 'fecha_ingreso',
                    'valorDate' => DDJJ::encodeDateSade($tab1_2Parent->getField('ddjj-date-gcba')->getValue())
                ),
                array(
                    'etiqueta' => $tab1_2Parent->getField('ddjj-jurisdiction')->getName(),
                    'idFormComp' => $SADEFrm[ENVIROMENT]['jurisdiccion'], //'33508',
                    'inputName' => 'jurisdiccion',
                    'valorStr' => $tab1_2Parent->getField('ddjj-jurisdiction')->getValue()
                ),
                array(
                    'etiqueta' => $tab1_2Parent->getField('ddjj-ministry')->getName(),
                    'idFormComp' => $SADEFrm[ENVIROMENT]['secretaria'], //'33600',
                    'inputName' => 'secretaria',
                    'valorStr' => $tab1_2Parent->getField('ddjj-ministry')->getValue()
                ),
                array(
                    'etiqueta' => $tab1_2Parent->getField('ddjj-general-direction')->getName(),
                    'idFormComp' => $SADEFrm[ENVIROMENT]['reparticion'], //'33620',
                    'inputName' => 'reparticion',
                    'valorStr' => $tab1_2Parent->getField('ddjj-general-direction')->getValue()
                ),
                array(
                    'etiqueta' => $tab1_2Parent->getField('ddjj-job')->getName(),
                    'idFormComp' => $SADEFrm[ENVIROMENT]['cargo'], //'33599',
                    'inputName' => 'cargo',
                    'valorStr' => $tab1_2Parent->getField('ddjj-job')->getValue()
                ),
                array(
                    'etiqueta' => $tab1_2Parent->getField('ddjj-employment-relationship')->getName(),
                    'idFormComp' => $SADEFrm[ENVIROMENT]['relacion_laboral'], //'33596',
                    'inputName' => 'relacion_laboral',
                    'valorStr' => $tab1_2Parent->getField('ddjj-employment-relationship')->getValue()
                ),
                array(
                    'etiqueta' => $tab1_2Parent->getField('ddjj-net-annual-amount')->getName(),
                    'idFormComp' => $SADEFrm[ENVIROMENT]['monto_anual_pesos'], //'33617',
                    'inputName' => 'monto_anual_pesos',
                    'valorDouble' => $tab1_2Parent->getField('ddjj-net-annual-amount')->getValue()
                ),
                array(
                    'etiqueta' => $tab1_2Parent->getField('ddjj-voluntary-retirement')->getName(),
                    'idFormComp' => $SADEFrm[ENVIROMENT]['retiro_voluntario'], //'33597',
                    'inputName' => 'retiro_voluntario',
                    'valorStr' => $tab1_2Parent->getField('ddjj-voluntary-retirement')->getValue()
                ),
                array(
                    'etiqueta' => $tab1_2Parent->getField('ddjj_member_commission')->getName(),
                    'idFormComp' => $SADEFrm[ENVIROMENT]['miembro_comision'], //'33574',
                    'inputName' => 'miembro_comision',
                    'valorStr' => $tab1_2Parent->getField('ddjj_member_commission')->getValue()
                ),
                array(
                    'etiqueta' => $tab1_2Parent->getField('ddjj-retain-budget')->getName(),
                    'idFormComp' => $SADEFrm[ENVIROMENT]['retengo_partida'], //'33569',
                    'inputName' => 'retengo_partida',
                    'valorStr' => $tab1_2Parent->getField('ddjj-retain-budget')->getValue()
                ),
                array(
                    'etiqueta' => $tab1_2Parent->getField('ddjj-retain-budget-owner')->getName(),
                    'idFormComp' => $SADEFrm[ENVIROMENT]['organismo'], //'33506',
                    'inputName' => 'organismo',
                    'valorStr' => $tab1_2Parent->getField('ddjj-retain-budget-owner')->getValue()
                ),
                array(
                    'etiqueta' => $_SESSION['Affidavit']->getSection('ddjj-personal-data')->getSection('ddjj-academic-info')->getField('ddjj-studies')->getName(),
                    'idFormComp' => $SADEFrm[ENVIROMENT]['estudio'], //'33586',
                    'inputName' => 'estudio',
                    'valorStr' => $_SESSION['Affidavit']->getSection('ddjj-personal-data')->getSection('ddjj-academic-info')->getField('ddjj-studies')->getValue()
                )
            );

            if ($currentUser->hasCommentPrivilege()) {
                $field = array(
                    'etiqueta' => $_SESSION['Affidavit']->getSection('ddjj-disclaimers')->getField('ddjj-observations')->getName(),
                    'idFormComp' => $SADEFrm[ENVIROMENT]['aclaraciones'], //'33557',
                    'inputName' => 'aclaraciones',
                    'valorStr' => $_SESSION['Affidavit']->getSection('ddjj-disclaimers')->getField('ddjj-observations')->getValue()
                );
                array_push($arrFields, $field);
            }

            /**
             * Solapa I: Fin
             * */
            /**
             * Solapa : Inicio
             * */
            $sections = $_SESSION['Affidavit']->getSection('ddjj-work-data')->getSection('ddjj-works-history')->getSections();
            $field = array('etiqueta' => 'Antecedentes Laborales / Profesionales',
                'idFormComp' => $SADEFrm[ENVIROMENT]['separator_antecedentes'], //'33540',
                'inputName' => 'separator_antecedentes',
                'relevanciaBusqueda' => '0',
                'separadorRepetidor' => 'true',
                'valorLong' => (count($sections) >> 0) ? (count($sections) - 1) : count($sections)
            );
            array_push($arrFields, $field);
            if (count($sections) >= 1) {
                $i = 0;
                foreach ($sections as $aSession) {
                    foreach ($aSession->getFields() as $aField) {
                        $idFormComp = '';
                        $inputName = '';
                        switch ($aField->getId()) {
                            case 'ddjj-company':
                                $idFormComp = $SADEFrm[ENVIROMENT]['empresa']; //'33559';
                                $inputName = 'empresa_R' . $i;
                                break;
                            case 'ddjj-entity':
                                $idFormComp = $SADEFrm[ENVIROMENT]['entidad']; //'33528';
                                $inputName = 'entidad_R' . $i;
                                break;
                            case 'ddjj-company-activity':
                                $idFormComp = $SADEFrm[ENVIROMENT]['actividad_empresa']; //'33551';
                                $inputName = 'actividad_empresa_R' . $i;
                                break;
                            case 'ddjj-company-position':
                                $idFormComp = $SADEFrm[ENVIROMENT]['cargo_funcion']; // '33606';
                                $inputName = 'cargo_funcion_R' . $i;
                                break;
                            case 'ddjj-company-date-from':
                                $idFormComp = $SADEFrm[ENVIROMENT]['fecha_desde']; // '33605';
                                $inputName = 'fecha_desde_R' . $i;
                                break;
                            case 'ddjj-company-date-to':
                                $idFormComp = $SADEFrm[ENVIROMENT]['fecha_hasta']; //'33537';
                                $inputName = 'fecha_hasta_R' . $i;
                                break;
                        }
                        //valido el tipo de dato que se va enviar
                        $type = DDJJ::getTypeValue($aField->getType()->name);
                        $field = array(
                            'etiqueta' => $aField->getName(),
                            'idFormComp' => $idFormComp,
                            'inputName' => ($i == 0) ? rtrim($inputName, '_R0') : $inputName,
                            $type => ($type === 'valorDate' ? DDJJ::encodeDateSade($aField->getValue()) : $aField->getValue())
                        );
                        $getDataValue = $aField->getValue();
                        $valueField = empty($getDataValue);
                        if ($getDataValue != "00-00-0000" && $valueField === false) {
                            array_push($arrFields, $field);
                        }

                        //array_push($arrFields,$field);
                    }
                    $i++;
                }
            }

            /**
             * Solapa : Fin
             * */
            /**
             * Solapa : Inicio
             * */
            $sections = $_SESSION['Affidavit']->getSection('ddjj-work-data')->getSection('ddjj-current-activities')->getSections();

            $field = array('etiqueta' => 'Actividades laborales o profesionales simultáneas',
                'idFormComp' => $SADEFrm[ENVIROMENT]['separator_actividad'], //'33481',
                'inputName' => 'separator_actividad',
                'relevanciaBusqueda' => '0',
                'separadorRepetidor' => 'true',
                'valorLong' => (count($sections) >> 0) ? (count($sections) - 1) : count($sections)
            );
            array_push($arrFields, $field);
            if (count($sections) >= 1) {
                $i = 0;
                foreach ($sections as $aSession) {
                    foreach ($aSession->getFields() as $aField) {
                        $idFormComp = '';
                        $inputName = '';
                        switch ($aField->getId()) {
                            case 'ddjj-current-company':
                                $idFormComp = $SADEFrm[ENVIROMENT]['empresa_organismo']; //'33491';
                                $inputName = 'empresa_organismo_R' . $i;
                                break;
                            case 'ddjj-current-entity':
                                $idFormComp = $SADEFrm[ENVIROMENT]['entidad_actual']; //'33629';
                                $inputName = 'entidad_actual_R' . $i;
                                break;
                            case 'ddjj-current-company-activity':
                                $idFormComp = $SADEFrm[ENVIROMENT]['actividad']; //'33536';
                                $inputName = 'actividad_R' . $i;
                                break;
                            case 'ddjj-current-company-position':
                                $idFormComp = $SADEFrm[ENVIROMENT]['cargo_actual']; //'33532';
                                $inputName = 'cargo_actual_R' . $i;
                                break;
                            case 'ddjj-current-company-from':
                                $idFormComp = $SADEFrm[ENVIROMENT]['fecha_actividad']; //'33548';
                                $inputName = 'fecha_actividad_R' . $i;
                                break;
                            case 'ddjj-current-licence':
                                $idFormComp = $SADEFrm[ENVIROMENT]['licencia']; //'33482';
                                $inputName = 'licencia_R' . $i;
                                break;
                            case 'ddjj-current-licence-to':
                                $idFormComp = $SADEFrm[ENVIROMENT]['fecha_hasta_licencia']; //'33610';
                                $inputName = 'fecha_hasta_licencia_R' . $i;
                                break;
                            case 'ddjj-current-company-hours-week':
                                $idFormComp = $SADEFrm[ENVIROMENT]['horas']; //'33524';
                                $inputName = 'horas_R' . $i;
                                break;
                        }
                        //valido el tipo de dato que se va enviar
                        $type = DDJJ::getTypeValue($aField->getType()->name);
                        $field = array(
                            'etiqueta' => $aField->getName(),
                            'idFormComp' => $idFormComp,
                            'inputName' => ($i == 0) ? rtrim($inputName, '_R0') : $inputName,
                            $type => ($type === 'valorDate' ? DDJJ::encodeDateSade($aField->getValue()) : $aField->getValue())
                        );
                        $getDataValue = $aField->getValue();
                        $valueField = empty($getDataValue);
                        if ($getDataValue != "00-00-0000" && $valueField === false) {
                            array_push($arrFields, $field);
                        }
                    }
                    $i++;
                }
            }
            /**
             * Solapa : Fin
             * */
            /**
             * Solapa : Inicio
             * */
            $sections = $_SESSION['Affidavit']->getSection('ddjj-family-data')->getSections();

            $field = array('etiqueta' => 'Datos Familiares',
                'idFormComp' => $SADEFrm[ENVIROMENT]['separator_dat_familia'], //'33535',
                'inputName' => 'separator_dat_familia',
                'relevanciaBusqueda' => '0',
                'separadorRepetidor' => 'true',
                'valorLong' => (count($sections) >> 0) ? (count($sections) - 1) : count($sections)
            );
            array_push($arrFields, $field);
            if (count($sections) >= 1) {
                $i = 0;
                foreach ($sections as $aSession) {
                    //$data_id = $aSession->getId();
                    foreach ($aSession->getFields() as $aField) {
                        $idFormComp = '';
                        $inputName = '';
                        switch ($aField->getId()) {
                            case 'ddjj-family-bond':
                                $idFormComp = $SADEFrm[ENVIROMENT]['vinculo_familiar']; //'33589';
                                $inputName = 'vinculo_familiar_R' . $i;
                                break;
                        }
                        //valido el tipo de dato que se va enviar
                        $type = DDJJ::getTypeValue($aField->getType()->name);
                        $field = array(
                            'etiqueta' => $aField->getName(),
                            'idFormComp' => $idFormComp,
                            'inputName' => ($i == 0) ? rtrim($inputName, '_R0') : $inputName,
                            $type => ($type === 'valorDate' ? DDJJ::encodeDateSade($aField->getValue()) : $aField->getValue())
                        );
                        $getDataValue = $aField->getValue();
                        $valueField = empty($getDataValue);
                        if ($getDataValue != "00-00-0000" && $valueField === false) {
                            array_push($arrFields, $field);
                        }
                    }
                    $i++;
                }
            }
            /**
             * Solapa : Inicio
             * */
            $sections = $_SESSION['Affidavit']->getSection('ddjj-assets')->getSection('ddjj-goods')->getSections();
            $field = array('etiqueta' => 'Bienes muebles registrables',
                'idFormComp' => $SADEFrm[ENVIROMENT]['separator_bienes_registrables'], //'33515',
                'inputName' => 'separator_bienes_registrables',
                'relevanciaBusqueda' => '0',
                'separadorRepetidor' => 'true',
                'valorLong' => (count($sections) >> 0) ? (count($sections) - 1) : count($sections)
            );

            array_push($arrFields, $field);
            if (count($sections) >= 1) {
                $i = 0;
                foreach ($sections as $aSession) {
                    foreach ($aSession->getFields() as $aField) {
                        $idFormComp = '';
                        $inputName = '';
                        switch ($aField->getId()) {
                            case 'ddjj-ownership':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titularidad_mueble'];
                                $inputName = 'titularidad_mueble_R' . $i;
                                break;
                            case 'ddjj-law':
                                $idFormComp = $SADEFrm[ENVIROMENT]['derecho_mueble']; 
                                $inputName = 'derecho_mueble_R' . $i;
                                break;
                            case 'ddjj-benefit-cause':
                                $idFormComp = $SADEFrm[ENVIROMENT]['causa_beneficio_mueble']; 
                                $inputName = 'causa_beneficio_mueble_R' . $i;
                                break;
                            case 'ddjj-time-limit':
                                $idFormComp = $SADEFrm[ENVIROMENT]['plazo_mueble']; 
                                $inputName = 'plazo_mueble_R' . $i;
                                break;
                            case 'ddjj-title-cost':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titulo_mueble']; 
                                $inputName = 'titulo_mueble_R' . $i;
                                break;
                            case 'ddjj-bond':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titular_mueble']; //'33594';
                                $inputName = 'titular_mueble_R' . $i;
                                break;
                            case 'ddjj-asset':
                                $idFormComp = $SADEFrm[ENVIROMENT]['tipo_bien_mueble']; //'33496';
                                $inputName = 'tipo_bien_mueble_R' . $i;
                                break;
                            case 'ddjj-brand':
                                $idFormComp = $SADEFrm[ENVIROMENT]['marca_bien_mueble']; //'33516';
                                $inputName = 'marca_bien_mueble_R' . $i;
                                break;
                            case 'ddjj-manufacture-year':
                                $idFormComp = $SADEFrm[ENVIROMENT]['anio_fabricacion_mueble']; //'33587';
                                $inputName = 'anio_fabricacion_mueble_R' . $i;
                                break;
                            case 'ddjj-entry-year':
                                $idFormComp = $SADEFrm[ENVIROMENT]['anio_ingreso_mueble']; //'306114';
                                $inputName = 'anio_ingreso_mueble_R' . $i;
                                break;
                            case 'ddjj-funds-origin':
                                $idFormComp = $SADEFrm[ENVIROMENT]['origen_fondos_mueble']; //'33553';
                                $inputName = 'origen_fondos_mueble_R' . $i;
                                break;
                            case 'ddjj-titularity-percentage':
                                $idFormComp = $SADEFrm[ENVIROMENT]['porcentaje_titularidad_mueble']; //'33544';
                                $inputName = 'porcentaje_titularidad_mueble_R' . $i;
                                break;
                            case 'ddjj-fiscal-value':
                                $idFormComp = $SADEFrm[ENVIROMENT]['valor_fiscal_mueble']; //'33498';
                                $inputName = 'valor_fiscal_mueble_R' . $i;
                                break;
                            case 'ddjj-aquired-value':
                                $idFormComp = $SADEFrm[ENVIROMENT]['valor_adquirido_mueble']; //'33577';
                                $inputName = 'valor_adquirido_mueble_R' . $i;
                                break;
                            case 'ddjj-improvements':
                                $idFormComp = $SADEFrm[ENVIROMENT]['mejoras']; //'33511';
                                $inputName = 'mejoras_R' . $i;
                                break;
                            case 'ddjj-amount-improvements':
                                $idFormComp = $SADEFrm[ENVIROMENT]['monto_total_mejoras']; //'33554';
                                $inputName = 'monto_total_mejoras_R' . $i;
                                break;
                        }
                        //valido el tipo de dato que se va enviar
                        $type = DDJJ::getTypeValue($aField->getType()->name);
                        $field = array(
                            'etiqueta' => $aField->getName(),
                            'idFormComp' => $idFormComp,
                            'inputName' => ($i == 0) ? rtrim($inputName, '_R0') : $inputName,
                            $type => ($type === 'valorDate' ? DDJJ::encodeDateSade($aField->getValue()) : $aField->getValue())
                        );
                        $getDataValue = $aField->getValue();
                        $valueField = empty($getDataValue);
                        if ($getDataValue != "00-00-0000" && $valueField === false) {
                            array_push($arrFields, $field);
                        }
                    }
                    $i++;
                }
            }
            /**
             * Solapa : Fin
             * */
            /**
             * Solapa : Inicio
             * */
            $sections = $_SESSION['Affidavit']->getSection('ddjj-assets')->getSection('ddjj-unregistered-assets')->getSections();
            $field = array('etiqueta' => 'Bienes muebles no registrables',
                'idFormComp' => $SADEFrm[ENVIROMENT]['separator_bienes_val_ind'], //'33512',
                'inputName' => 'separator_bienes_val_ind',
                'relevanciaBusqueda' => '0',
                'separadorRepetidor' => 'true',
                'valorLong' => (count($sections) >> 0) ? (count($sections) - 1) : count($sections)
            );
            array_push($arrFields, $field);
            if (count($sections) >= 1) {
                $i = 0;
                foreach ($sections as $aSession) {
                    foreach ($aSession->getFields() as $aField) {
                        $idFormComp = '';
                        $inputName = '';
                        switch ($aField->getId()) {
                            case 'ddjj-ownership':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titularidad_indi']; //'33566';
                                $inputName = 'titularidad_indi_R' . $i;
                                break;
                            case 'ddjj-bond':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titular_indi']; //'33590';
                                $inputName = 'titular_indi_R' . $i;
                                break;
                            case 'ddjj-asset':
                                $idFormComp = $SADEFrm[ENVIROMENT]['tipo_bien_val_indi']; //'33499';
                                $inputName = 'tipo_bien_val_indi_R' . $i;
                                break;
                            case 'ddjj-brand':
                                $idFormComp = $SADEFrm[ENVIROMENT]['marca_bien_val_indi']; //'33612';
                                $inputName = 'marca_bien_val_indi_R' . $i;
                                break;
                            case 'ddjj-entry-year':
                                $idFormComp = $SADEFrm[ENVIROMENT]['anio_ingreso_val_indi']; //'33593';
                                $inputName = 'anio_ingreso_val_indi_R' . $i;
                                break;
                            case 'ddjj-funds-origin':
                                $idFormComp = $SADEFrm[ENVIROMENT]['origen_fondos_val_indi']; //'33523';
                                $inputName = 'origen_fondos_val_indi_R' . $i;
                                break;
                            case 'ddjj-titularity-percentage':
                                $idFormComp = $SADEFrm[ENVIROMENT]['porcentaje_titularidad_valor']; //'33488';
                                $inputName = 'porcentaje_titularidad_valor_R' . $i;
                                break;
                            case 'ddjj-fiscal-value':
                                $idFormComp = $SADEFrm[ENVIROMENT]['valor_fiscal_val_indi']; //'33501';
                                $inputName = 'valor_fiscal_val_indi_R' . $i;
                                break;
                            case 'ddjj-aquired-value':
                                $idFormComp = $SADEFrm[ENVIROMENT]['valor_adquirido_val_indi']; //'33611';
                                $inputName = 'valor_adquirido_val_indi_R' . $i;
                                break;
                        }
                        //valido el tipo de dato que se va enviar
                        $type = DDJJ::getTypeValue($aField->getType()->name);
                        $field = array(
                            'etiqueta' => $aField->getName(),
                            'idFormComp' => $idFormComp,
                            'inputName' => ($i == 0) ? rtrim($inputName, '_R0') : $inputName,
                            $type => ($type === 'valorDate' ? DDJJ::encodeDateSade($aField->getValue()) : $aField->getValue())
                        );
                        $getDataValue = $aField->getValue();
                        $valueField = empty($getDataValue);
                        if ($getDataValue != "00-00-0000" && $valueField === false) {
                            array_push($arrFields, $field);
                        }
                    }
                    $i++;
                }
            }
            /**
             * Solapa : Fin
             * */
            /**
             * Solapa : Inicio
             * */
            $sections = $_SESSION['Affidavit']->getSection('ddjj-assets')->getSection('ddjj-properties')->getSections();
            $field = array('etiqueta' => 'Bienes inmuebles',
                'idFormComp' => $SADEFrm[ENVIROMENT]['separator_registro_bienes_inm'], //'33510',
                'inputName' => 'separator_registro_bienes_inm',
                'relevanciaBusqueda' => '0',
                'separadorRepetidor' => 'true',
                'valorLong' => (count($sections) >> 0) ? (count($sections) - 1) : count($sections)
            );
            array_push($arrFields, $field);
            if (count($sections) >= 1) {
                $i = 0;
                foreach ($sections as $aSession) {
                    foreach ($aSession->getFields() as $aField) {
                        $idFormComp = '';
                        $inputName = '';
                        switch ($aField->getId()) {
                            case 'ddjj-ownership':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titularidad_inmueble']; //'33568';
                                $inputName = 'titularidad_inmueble_R' . $i;
                                break;
                         /*   case 'ddjj-owner-name':
                                $idFormComp = $SADEFrm[ENVIROMENT]['apellido_nombre_inmueble'];
                                $inputName = 'apellido_nombre_inmueble_R' . $i;
                                break;*/
                            case 'ddjj-law':
                                $idFormComp = $SADEFrm[ENVIROMENT]['derecho_inmueble']; 
                                $inputName = 'derecho_inmueble_R' . $i;
                                break;
                            case 'ddjj-benefit-cause':
                                $idFormComp = $SADEFrm[ENVIROMENT]['causa_beneficio_inmueble']; 
                                $inputName = 'causa_beneficio_inmueble_R' . $i;
                                break;
                            case 'ddjj-time-limit':
                                $idFormComp = $SADEFrm[ENVIROMENT]['plazo_inmueble']; 
                                $inputName = 'plazo_inmueble_R' . $i;
                                break;
                            case 'ddjj-title-cost':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titulo_inmueble']; 
                                $inputName = 'titulo_inmueble_R' . $i;
                                break;
                            case 'ddjj-bond':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titular_bien_inmueble']; //'33530';
                                $inputName = 'titular_bien_inmueble_R' . $i;
                                break;
                            case 'ddjj-asset':
                                $idFormComp = $SADEFrm[ENVIROMENT]['tipo_bien_inmueble']; //'33560';
                                $inputName = 'tipo_bien_inmueble_R' . $i;
                                break;
                            case 'ddjj-country':
                                $idFormComp = $SADEFrm[ENVIROMENT]['pais']; //'33489';
                                $inputName = 'pais_R' . $i;
                                break;
                            case 'ddjj-province':
                                $idFormComp = $SADEFrm[ENVIROMENT]['provincia_inmueble']; //'33592';
                                $inputName = 'provincia_inmueble_R' . $i;
                                break;
                            case 'ddjj-town':
                                $idFormComp = $SADEFrm[ENVIROMENT]['localidad_inmueble']; //'33575';
                                $inputName = 'localidad_inmueble_R' . $i;
                                break;
                            case 'ddjj-neightborhood':
                                $idFormComp = $SADEFrm[ENVIROMENT]['zona_inmueble']; //'33505';
                                $inputName = 'zona_inmueble_R' . $i;
                                break;
                            case 'ddjj-titularity-percentage':
                                $idFormComp = $SADEFrm[ENVIROMENT]['porcentaje_titularidad_inm']; //'33608';
                                $inputName = 'porcentaje_titularidad_inm_R' . $i;
                                break;
                            case 'ddjj-entry-year':
                                $idFormComp = $SADEFrm[ENVIROMENT]['anio_ingreso_inmueble']; //'33565';
                                $inputName = 'anio_ingreso_inmueble_R' . $i;
                                break;
                            case 'ddjj-funds-origin':
                                $idFormComp = $SADEFrm[ENVIROMENT]['origen_fondos_inmueble']; //'33517';
                                $inputName = 'origen_fondos_inmueble_R' . $i;
                                break;
                            case 'ddjj-area':
                                $idFormComp = $SADEFrm[ENVIROMENT]['superficie']; //'33541';
                                $inputName = 'superficie_R' . $i;
                                break;
                            case 'ddjj-unit':
                                $idFormComp = $SADEFrm[ENVIROMENT]['unidad_inmueble']; //'33625';
                                $inputName = 'unidad_inmueble_R' . $i;
                                break;
                            case 'ddjj-fiscal-value':
                                $idFormComp = $SADEFrm[ENVIROMENT]['valor_fiscal_inmueble']; //'33607';
                                $inputName = 'valor_fiscal_inmueble_R' . $i;
                                break;
                            case 'ddjj-aquired-value':
                                $idFormComp = $SADEFrm[ENVIROMENT]['valor_adquirido_inmueble']; //'33563';
                                $inputName = 'valor_adquirido_inmueble_R' . $i;
                                break;
                            case 'ddjj-improvements':
                                $idFormComp = $SADEFrm[ENVIROMENT]['mejoras_1']; //'33539';
                                $inputName = 'mejoras_1_R' . $i;
                                break;
                            case 'ddjj-amount-improvements':
                                $idFormComp = $SADEFrm[ENVIROMENT]['monto_mejoras']; //'33564';
                                $inputName = 'monto_mejoras_R' . $i;
                                break;
                        }
                        //valido el tipo de dato que se va enviar
                        $type = DDJJ::getTypeValue($aField->getType()->name);
                        $field = array(
                            'etiqueta' => $aField->getName(),
                            'idFormComp' => $idFormComp,
                            'inputName' => ($i == 0) ? rtrim($inputName, '_R0') : $inputName,
                            $type => ($type === 'valorDate' ? DDJJ::encodeDateSade($aField->getValue()) : $aField->getValue())
                        );
                        $getDataValue = $aField->getValue();
                        $valueField = empty($getDataValue);
                        if ($getDataValue != "00-00-0000" && $valueField === false) {
                            array_push($arrFields, $field);
                        }
                    }
                    $i++;
                }
            }
            /**
             * Solapa : Fin
             * */
            /**
             * Solapa : Inicio
             * */
            $sections = $_SESSION['Affidavit']->getSection('ddjj-assets')->getSection('ddjj-stocks')->getSections();
            $field = array('etiqueta' => 'Títulos, acciones, fondos comunes de inversión',
                'idFormComp' => $SADEFrm[ENVIROMENT]['separator_titulos'], //'33492',
                'inputName' => 'separator_titulos',
                'relevanciaBusqueda' => '0',
                'separadorRepetidor' => 'true',
                'valorLong' => (count($sections) >> 0) ? (count($sections) - 1) : count($sections)
            );
            array_push($arrFields, $field);
            if (count($sections) >= 1) {
                $i = 0;
                foreach ($sections as $aSession) {
                    foreach ($aSession->getFields() as $aField) {
                        $idFormComp = '';
                        $inputName = '';
                        switch ($aField->getId()) {
                            case 'ddjj-ownership':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titularidad_titulos']; //'33630';
                                $inputName = 'titularidad_titulos_R' . $i;
                                break;
                            case 'ddjj-bond':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titular_titulos']; //'33621';
                                $inputName = 'titular_titulos_R' . $i;
                                break;
                            case 'ddjj-title-type-value':
                                $idFormComp = $SADEFrm[ENVIROMENT]['tipo_titulo']; //'33546';
                                $inputName = 'tipo_titulo_R' . $i;
                                break;
                            case 'ddjj-title-description':
                                $idFormComp = $SADEFrm[ENVIROMENT]['descripcion']; //'33598';
                                $inputName = 'descripcion_R' . $i;
                                break;
                            case 'ddjj-trade':
                                $idFormComp = $SADEFrm[ENVIROMENT]['objeto']; //'33626';
                                $inputName = 'objeto_R' . $i;
                                break;
                            case 'ddjj-stock-qty':
                                $idFormComp = $SADEFrm[ENVIROMENT]['cantidad_acciones']; //'33487';
                                $inputName = 'cantidad_acciones_R' . $i;
                                break;
                            case 'ddjj-aquired-date':
                                $idFormComp = $SADEFrm[ENVIROMENT]['fecha_adquiere_acciones']; //'33623';
                                $inputName = 'fecha_adquiere_acciones_R' . $i;
                                break;
                            case 'ddjj-funds-origin':
                                $idFormComp = $SADEFrm[ENVIROMENT]['origen_fondos_acciones']; //'33493';
                                $inputName = 'origen_fondos_acciones_R' . $i;
                                break;
                            case 'ddjj-quote-value':
                                $idFormComp = $SADEFrm[ENVIROMENT]['valor_cotizacion_acciones']; //'33502';
                                $inputName = 'valor_cotizacion_acciones_R' . $i;
                                break;
                            case 'ddjj-current-value':
                                $idFormComp = $SADEFrm[ENVIROMENT]['valor_actual_acciones']; //'33622';
                                $inputName = 'valor_actual_acciones_R' . $i;
                                break;
                            case 'ddjj-aquired-value':
                                $idFormComp = $SADEFrm[ENVIROMENT]['valor_adquirido_acciones']; //'33497';
                                $inputName = 'valor_adquirido_acciones_R' . $i;
                                break;
                        }
                        //valido el tipo de dato que se va enviar
                        $type = DDJJ::getTypeValue($aField->getType()->name);
                        $typeName = $aField->getType()->name;

                        if ($typeName === 'money_decimal') {
                            $get = $aField->getValue();
                            $value = DDJJ::encodeDecimalSade($get);
                        } else {
                            $value = $aField->getValue();
                        }

                        $field = array(
                            'etiqueta' => $aField->getName(),
                            'idFormComp' => $idFormComp,
                            'inputName' => ($i == 0) ? rtrim($inputName, '_R0') : $inputName,
                            $type => ($type === 'valorDate' ? DDJJ::encodeDateSade($aField->getValue()) : $value)
                        );
                        $getDataValue = $aField->getValue();
                        $valueField = empty($getDataValue);
                        if ($getDataValue != "00-00-0000" && $valueField === false) {
                            array_push($arrFields, $field);
                        }
                    }
                    $i++;
                }
            }
            /**
             * Solapa : Fin
             * */
            /**
             * Solapa : Inicio
             * */
            $sections = $_SESSION['Affidavit']->getSection('ddjj-assets')->getSection('ddjj-partnerships')->getSections();
            $field = array('etiqueta' => 'Sociedades',
                'idFormComp' => $SADEFrm[ENVIROMENT]['separator_sociedades'], //'33572',
                'inputName' => 'separator_sociedades',
                'relevanciaBusqueda' => '0',
                'separadorRepetidor' => 'true',
                'valorLong' => (count($sections) >> 0) ? (count($sections) - 1) : count($sections)
            );
            array_push($arrFields, $field);
            if (count($sections) >= 1) {
                $i = 0;
                foreach ($sections as $aSession) {
                    foreach ($aSession->getFields() as $aField) {
                        $idFormComp = '';
                        $inputName = '';
                        switch ($aField->getId()) {
                            case 'ddjj-ownership':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titularidad_sociedad']; //'33615';
                                $inputName = 'titularidad_sociedad_R' . $i;
                                break;
                            case 'ddjj-bond':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titular_sociedad']; //'33534';
                                $inputName = 'titular_sociedad_R' . $i;
                                break;
                            case 'ddjj-kind-society':
                                $idFormComp = $SADEFrm[ENVIROMENT]['tipo_sociedad']; //'33533';
                                $inputName = 'tipo_sociedad_R' . $i;
                                break;
                            case 'ddjj-partnership-trade':
                                $idFormComp = $SADEFrm[ENVIROMENT]['objeto_social_sociedad']; //'33555';
                                $inputName = 'objeto_social_sociedad_R' . $i;
                                break;
                            case 'ddjj-titularity-percentage':
                                $idFormComp = $SADEFrm[ENVIROMENT]['porcentaje_participacion_soc']; //'33531';
                                $inputName = 'porcentaje_participacion_soc_R' . $i;
                                break;
                            case 'ddjj-aquired-date':
                                $idFormComp = $SADEFrm[ENVIROMENT]['fecha_aduiere_sociedad']; //'33624';
                                $inputName = 'fecha_aduiere_sociedad_R' . $i;
                                break;
                            case 'ddjj-funds-origin':
                                $idFormComp = $SADEFrm[ENVIROMENT]['origen_fondos_sociedad']; //'33585';
                                $inputName = 'origen_fondos_sociedad_R' . $i;
                                break;
                            case 'ddjj-current-value':
                                $idFormComp = $SADEFrm[ENVIROMENT]['valor_actual_sociedad']; //'33500';
                                $inputName = 'valor_actual_sociedad_R' . $i;
                                break;
                            case 'ddjj-aquired-value':
                                $idFormComp = $SADEFrm[ENVIROMENT]['valor_adquisicion_sociedad']; //'33627';
                                $inputName = 'valor_adquisicion_sociedad_R' . $i;
                                break;
                            case 'ddjj-contributions':
                                $idFormComp = $SADEFrm[ENVIROMENT]['aportes']; //'33576';
                                $inputName = 'aportes_R' . $i;
                                break;
                        }
                        //valido el tipo de dato que se va enviar
                        $type = DDJJ::getTypeValue($aField->getType()->name);
                        $field = array(
                            'etiqueta' => $aField->getName(),
                            'idFormComp' => $idFormComp,
                            'inputName' => ($i == 0) ? rtrim($inputName, '_R0') : $inputName,
                            $type => ($type === 'valorDate' ? DDJJ::encodeDateSade($aField->getValue()) : $aField->getValue())
                        );
                        $getDataValue = $aField->getValue();
                        $valueField = empty($getDataValue);
                        if ($getDataValue != "00-00-0000" && $valueField === false) {
                            array_push($arrFields, $field);
                        }
                    }
                    $i++;
                }
            }
            /**
             * Solapa : Fin
             * */
            /**
             * Solapa : Inicio
             * */
            $sections = $_SESSION['Affidavit']->getSection('ddjj-assets')->getSection('ddjj-accounts')->getSections();
            $field = array('etiqueta' => 'Depósito bancario y dinero en efectivo',
                'idFormComp' => $SADEFrm[ENVIROMENT]['separator_efectivo'], //'33616',
                'inputName' => 'separator_efectivo',
                'relevanciaBusqueda' => '0',
                'separadorRepetidor' => 'true',
                'valorLong' => (count($sections) >> 0) ? (count($sections) - 1) : count($sections)
            );
            array_push($arrFields, $field);
            if (count($sections) >= 1) {
                $i = 0;
                foreach ($sections as $aSession) {
                    foreach ($aSession->getFields() as $aField) {
                        $idFormComp = '';
                        $inputName = '';
                        switch ($aField->getId()) {
                            case 'ddjj-ownership':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titularidad_efectivo']; //'33507';
                                $inputName = 'titularidad_efectivo_R' . $i;
                                break;
                            case 'ddjj-asset-type':
                                $idFormComp = $SADEFrm[ENVIROMENT]['tipo_bien_efectivo']; //'33561';
                                $inputName = 'tipo_bien_efectivo_R' . $i;
                                break;
                            case 'ddjj-bond':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titular_efectivo']; //'33520';
                                $inputName = 'titular_efectivo_R' . $i;
                                break;
                            case 'ddjj-account-type':
                                $idFormComp = $SADEFrm[ENVIROMENT]['tipo_cuenta_efectivo']; //'33503';
                                $inputName = 'tipo_cuenta_efectivo_R' . $i;
                                break;
                            case 'ddjj-titularity-percentage':
                                $idFormComp = $SADEFrm[ENVIROMENT]['porcentaje_titular_efectivo']; //'33604';
                                $inputName = 'porcentaje_titular_efectivo_R' . $i;
                                break;
                            case 'ddjj-currency':
                                $idFormComp = $SADEFrm[ENVIROMENT]['moneda_efectivo']; //'33525';
                                $inputName = 'moneda_efectivo_R' . $i;
                                break;
                            case 'ddjj-funds-origin':
                                $idFormComp = $SADEFrm[ENVIROMENT]['origen_fondos_efectivo']; //'33545';
                                $inputName = 'origen_fondos_efectivo_R' . $i;
                                break;
                            case 'ddjj-amount':
                                $idFormComp = $SADEFrm[ENVIROMENT]['monto_efectivo']; //'33609';
                                $inputName = 'monto_efectivo_R' . $i;
                                break;
                        }
                        //valido el tipo de dato que se va enviar
                        $type = DDJJ::getTypeValue($aField->getType()->name);
                        $field = array(
                            'etiqueta' => $aField->getName(),
                            'idFormComp' => $idFormComp,
                            'inputName' => ($i == 0) ? rtrim($inputName, '_R0') : $inputName,
                            $type => ($type === 'valorDate' ? DDJJ::encodeDateSade($aField->getValue()) : $aField->getValue())
                        );
                        $getDataValue = $aField->getValue();
                        $valueField = empty($getDataValue);
                        if ($getDataValue != "00-00-0000" && $valueField === false) {
                            array_push($arrFields, $field);
                        }
                    }
                    $i++;
                }
            }
            /**
             * Solapa : Fin
             * */
            /**
             * Solapa : Inicio
             * */
            $sections = $_SESSION['Affidavit']->getSection('ddjj-assets')->getSection('ddjj-other-jobs')->getSections();
            $field = array('etiqueta' => 'Ingreso por otros trabajos/actividades',
                'idFormComp' => $SADEFrm[ENVIROMENT]['separator_otros_trabajos'], //'33538',
                'inputName' => 'separator_otros_trabajos',
                'relevanciaBusqueda' => '0',
                'separadorRepetidor' => 'true',
                'valorLong' => (count($sections) >> 0) ? (count($sections) - 1) : count($sections)
            );
            array_push($arrFields, $field);
            if (count($sections) >= 1) {
                $i = 0;
                foreach ($sections as $aSession) {
                    foreach ($aSession->getFields() as $aField) {
                        $idFormComp = '';
                        $inputName = '';
                        switch ($aField->getId()) {
                            case 'ddjj-ownership':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titularidad_otros_trabajos']; //'33567';
                                $inputName = 'titularidad_otros_trabajos_R' . $i;
                                break;
                            case 'ddjj-bond':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titular_otros_trabajos']; //'33584';
                                $inputName = 'titular_otros_trabajos_R' . $i;
                                break;
                            case 'ddjj-position':
                                $idFormComp = $SADEFrm[ENVIROMENT]['cargo_otros_trabajos']; //'33552';
                                $inputName = 'cargo_otros_trabajos_R' . $i;
                                break;
                            case 'ddjj-employment-relationship':
                                $idFormComp = $SADEFrm[ENVIROMENT]['relacion_laboral_otros_trabajo']; //'33601';
                                $inputName = 'relacion_laboral_otros_trabajo_R' . $i;
                                break;
                            case 'ddjj-employer':
                                $idFormComp = $SADEFrm[ENVIROMENT]['empleador_otros_trabajos']; //'33588';
                                $inputName = 'empleador_otros_trabajos_R' . $i;
                                break;
                            case 'ddjj-employer-activity':
                                $idFormComp = $SADEFrm[ENVIROMENT]['actividad_otros_trabajos']; //'33518';
                                $inputName = 'actividad_otros_trabajos_R' . $i;
                                break;
                            case 'ddjj-net-anual-amount':
                                $idFormComp = $SADEFrm[ENVIROMENT]['monto_otros_trabajos']; //'33628';
                                $inputName = 'monto_otros_trabajos_R' . $i;
                                break;
                            case 'ddjj-other-job-from':
                                $idFormComp = $SADEFrm[ENVIROMENT]['fecha_desde_otros_trabajos']; //'33602';
                                $inputName = 'fecha_desde_otros_trabajos_R' . $i;
                                break;
                            case 'ddjj-activity-status':
                                $idFormComp = $SADEFrm[ENVIROMENT]['en_actividad_otros_trabajos']; //'33580';
                                $inputName = 'en_actividad_otros_trabajos_R' . $i;
                                break;
                        }
                        //valido el tipo de dato que se va enviar
                        $type = DDJJ::getTypeValue($aField->getType()->name);
                        $field = array(
                            'etiqueta' => $aField->getName(),
                            'idFormComp' => $idFormComp,
                            'inputName' => ($i == 0) ? rtrim($inputName, '_R0') : $inputName,
                            $type => ($type === 'valorDate' ? DDJJ::encodeDateSade($aField->getValue()) : $aField->getValue())
                        );
                        $getDataValue = $aField->getValue();
                        $valueField = empty($getDataValue);
                        if ($getDataValue != "00-00-0000" && $valueField === false) {
                            array_push($arrFields, $field);
                        }
                    }
                    $i++;
                }
            }
            /**
             * Solapa : Fin
             * */
            /**
             * Solapa : Inicio
             * */
            $sections = $_SESSION['Affidavit']->getSection('ddjj-assets')->getSection('ddjj-other-activities')->getSections();
            $field = array('etiqueta' => 'Otros ingresos / activos',
                'idFormComp' => $SADEFrm[ENVIROMENT]['separator_otro_ingreso'], //'33522',
                'inputName' => 'separator_otro_ingreso',
                'relevanciaBusqueda' => '0',
                'separadorRepetidor' => 'true',
                'valorLong' => (count($sections) >> 0) ? (count($sections) - 1) : count($sections)
            );
            array_push($arrFields, $field);
            if (count($sections) >= 1) {
                $i = 0;
                foreach ($sections as $aSession) {
                    foreach ($aSession->getFields() as $aField) {
                        $idFormComp = '';
                        $inputName = '';
                        switch ($aField->getId()) {
                            case 'ddjj-ownership':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titularidad_otro_ingreso']; //'33570';
                                $inputName = 'titularidad_otro_ingreso_R' . $i;
                                break;
                            case 'ddjj-bond':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titular_otro_ingreso']; //'33543';
                                $inputName = 'titular_otro_ingreso_R' . $i;
                                break;
                            case 'ddjj-income-type':
                                $idFormComp = $SADEFrm[ENVIROMENT]['tipo_ingreso']; //'33558';
                                $inputName = 'tipo_ingreso_R' . $i;
                                break;
                            case 'ddjj-origin-income':
                                $idFormComp = $SADEFrm[ENVIROMENT]['derecho_originante_ingreso']; //'33504';
                                $inputName = 'derecho_originante_ingreso_R' . $i;
                                break;
                            case 'ddjj-total-received-income':
                                $idFormComp = $SADEFrm[ENVIROMENT]['monto_total_ingreso']; //'33603';
                                $inputName = 'monto_total_ingreso_R' . $i;
                                break;
                        }
                        //valido el tipo de dato que se va enviar
                        $type = DDJJ::getTypeValue($aField->getType()->name);
                        $field = array(
                            'etiqueta' => $aField->getName(),
                            'idFormComp' => $idFormComp,
                            'inputName' => ($i == 0) ? rtrim($inputName, '_R0') : $inputName,
                            $type => ($type === 'valorDate' ? DDJJ::encodeDateSade($aField->getValue()) : $aField->getValue())
                        );
                        $getDataValue = $aField->getValue();
                        $valueField = empty($getDataValue);
                        if ($getDataValue != "00-00-0000" && $valueField === false) {
                            array_push($arrFields, $field);
                        }
                    }
                    $i++;
                }
            }
            /**
             * Solapa : Fin
             * */
            /**
             * Solapa : Inicio
             * */
            $sections = $_SESSION['Affidavit']->getSection('ddjj-assets')->getSection('ddjj-real-state-sellings')->getSections();
            $field = array('etiqueta' => 'Ingreso por venta de bienes inmuebles',
                'idFormComp' => $SADEFrm[ENVIROMENT]['separator_venta'], //'33549',
                'inputName' => 'separator_venta',
                'relevanciaBusqueda' => '0',
                'separadorRepetidor' => 'true',
                'valorLong' => (count($sections) >> 0) ? (count($sections) - 1) : count($sections)
            );
            array_push($arrFields, $field);
            if (count($sections) >= 1) {
                $i = 0;
                foreach ($sections as $aSession) {
                    foreach ($aSession->getFields() as $aField) {
                        $idFormComp = '';
                        $inputName = '';
                        switch ($aField->getId()) {
                            case 'ddjj-ownership':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titularidad_venta']; //'33485';
                                $inputName = 'titularidad_venta_R' . $i;
                                break;
                            case 'ddjj-bond':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titular_venta']; //'33571';
                                $inputName = 'titular_venta_R' . $i;
                                break;
                            case 'ddjj-asset':
                                $idFormComp = $SADEFrm[ENVIROMENT]['tipo_bien_venta']; //'33519';
                                $inputName = 'tipo_bien_venta_R' . $i;
                                break;
                            case 'ddjj-country':
                                $idFormComp = $SADEFrm[ENVIROMENT]['pais_venta']; //'33619';
                                $inputName = 'pais_venta_R' . $i;
                                break;
                            case 'ddjj-province':
                                $idFormComp = $SADEFrm[ENVIROMENT]['provincia_venta']; //'33514';
                                $inputName = 'provincia_venta_R' . $i;
                                break;
                            case 'ddjj-town':
                                $idFormComp = $SADEFrm[ENVIROMENT]['localidad_venta']; //'33513';
                                $inputName = 'localidad_venta_R' . $i;
                                break;
                            case 'ddjj-neightborhood':
                                $idFormComp = $SADEFrm[ENVIROMENT]['barrio_venta']; //'306203';
                                $inputName = 'barrio_venta_R' . $i;
                                break;
                            case 'ddjj-titularity-percentage':
                                $idFormComp = $SADEFrm[ENVIROMENT]['porcentaje_titularidad_venta']; //'33529';
                                $inputName = 'porcentaje_titularidad_venta_R' . $i;
                                break;
                            case 'ddjj-entry-year':
                                $idFormComp = $SADEFrm[ENVIROMENT]['anio_ingreso_venta']; //'33480';
                                $inputName = 'anio_ingreso_venta_R' . $i;
                                break;
                            case 'ddjj-currency':
                                $idFormComp = $SADEFrm[ENVIROMENT]['moneda_venta']; //'33579';
                                $inputName = 'moneda_venta_R' . $i;
                                break;
                            case 'ddjj-amount':
                                $idFormComp = $SADEFrm[ENVIROMENT]['monto_venta']; //'33580';
                                $inputName = 'monto_venta_R' . $i;
                                break;
                        }
                        //valido el tipo de dato que se va enviar
                        $type = DDJJ::getTypeValue($aField->getType()->name);
                        $field = array(
                            'etiqueta' => $aField->getName(),
                            'idFormComp' => $idFormComp,
                            'inputName' => ($i == 0) ? rtrim($inputName, '_R0') : $inputName,
                            $type => ($type === 'valorDate' ? DDJJ::encodeDateSade($aField->getValue()) : $aField->getValue())
                        );
                        $getDataValue = $aField->getValue();
                        $valueField = empty($getDataValue);
                        if ($getDataValue != "00-00-0000" && $valueField === false) {
                            array_push($arrFields, $field);
                        }
                    }
                    $i++;
                }
            }
            /**
             * Solapa : Fin
             * */
            /**
             * Solapa : Inicio
             * */
            $sections = $_SESSION['Affidavit']->getSection('ddjj-assets')->getSection('ddjj-debts')->getSections();
            $field = array('etiqueta' => 'Deudas',
                'idFormComp' => $SADEFrm[ENVIROMENT]['separator_deudas'], //'33483',
                'inputName' => 'separator_deudas',
                'relevanciaBusqueda' => '0',
                'separadorRepetidor' => 'true',
                'valorLong' => (count($sections) >> 0) ? (count($sections) - 1) : count($sections)
            );
            array_push($arrFields, $field);
            if (count($sections) >= 1) {
                $i = 0;
                foreach ($sections as $aSession) {
                    foreach ($aSession->getFields() as $aField) {
                        $idFormComp = '';
                        $inputName = '';
                        switch ($aField->getId()) {
                            case 'ddjj-ownership':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titularidad_deuda']; //'33486';
                                $inputName = 'titularidad_deuda_R' . $i;
                                break;
                            case 'ddjj-bond':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titular_deuda']; //'33578';
                                $inputName = 'titular_deuda_R' . $i;
                                break;
                            case 'ddjj-debt-type':
                                $idFormComp = $SADEFrm[ENVIROMENT]['tipo_deuda']; //'33591';
                                $inputName = 'tipo_deuda_R' . $i;
                                break;
                            case 'ddjj-currency':
                                $idFormComp = $SADEFrm[ENVIROMENT]['moneda_deuda']; //'33614';
                                $inputName = 'moneda_deuda_R' . $i;
                                break;
                        }
                        //valido el tipo de dato que se va enviar
                        $type = DDJJ::getTypeValue($aField->getType()->name);
                        $field = array(
                            'etiqueta' => $aField->getName(),
                            'idFormComp' => $idFormComp,
                            'inputName' => ($i == 0) ? rtrim($inputName, '_R0') : $inputName,
                            $type => ($type === 'valorDate' ? DDJJ::encodeDateSade($aField->getValue()) : $aField->getValue())
                        );
                        $getDataValue = $aField->getValue();
                        $valueField = empty($getDataValue);
                        if ($getDataValue != "00-00-0000" && $valueField === false) {
                            array_push($arrFields, $field);
                        }
                    }
                    $i++;
                }
            }
            /**
             * Solapa : Fin
             * */
            /**
             * Solapa : Inicio
             * */
            $sections = $_SESSION['Affidavit']->getSection('ddjj-assets')->getSection('ddjj-claims')->getSections();
            $field = array('etiqueta' => 'Acreencias',
                'idFormComp' => $SADEFrm[ENVIROMENT]['separator_acreencias'], //'33581',
                'inputName' => 'separator_acreencias',
                'relevanciaBusqueda' => '0',
                'separadorRepetidor' => 'true',
                'valorLong' => (count($sections) >> 0) ? (count($sections) - 1) : count($sections)
            );
            array_push($arrFields, $field);
            if (count($sections) >= 1) {
                $i = 0;
                foreach ($sections as $aSession) {
                    foreach ($aSession->getFields() as $aField) {
                        $idFormComp = '';
                        $inputName = '';
                        switch ($aField->getId()) {
                            case 'ddjj-ownership':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titularidad_acreencias']; //'33595';
                                $inputName = 'titularidad_acreencias_R' . $i;
                                break;
                            case 'ddjj-bond':
                                $idFormComp = $SADEFrm[ENVIROMENT]['titular_acreencias']; //'33542';
                                $inputName = 'titular_acreencias_R' . $i;
                                break;
                            case 'ddjj-debt-type':
                                $idFormComp = $SADEFrm[ENVIROMENT]['tipo_acreencia']; //'33583';
                                $inputName = 'tipo_acreencia_R' . $i;
                                break;
                            case 'ddjj-currency':
                                $idFormComp = $SADEFrm[ENVIROMENT]['moneda_acreencia']; //'33547';
                                $inputName = 'moneda_acreencia_R' . $i;
                                break;
                            case 'ddjj-amount':
                                $idFormComp = $SADEFrm[ENVIROMENT]['monto_acreencia']; //'33509';
                                $inputName = 'monto_acreencia_R' . $i;
                                break;
                        }
                        //valido el tipo de dato que se va enviar
                        $type = DDJJ::getTypeValue($aField->getType()->name);
                        $field = array(
                            'etiqueta' => $aField->getName(),
                            'idFormComp' => $idFormComp,
                            'inputName' => ($i == 0) ? rtrim($inputName, '_R0') : $inputName,
                            $type => ($type === 'valorDate' ? DDJJ::encodeDateSade($aField->getValue()) : $aField->getValue())
                        );
                        $getDataValue = $aField->getValue();
                        $valueField = empty($getDataValue);
                        if ($getDataValue != "00-00-0000" && $valueField === false) {
                            array_push($arrFields, $field);
                        }
                    }
                    $i++;
                }
            }
            /**
             * Solapa : Fin
             * */
            /*echo"<pre>";
             print_r($arrFields);
             echo"</pre>";
            */             

            $formSadeData['form_name'] = utf8_encode(SADE_FORM_NAME);
            $formSadeData['form_data'] = $arrFields;

            $transRst = Communicate::consumeFormSADE($formSadeData);
            if(isset($transRst['error']))
            {
                throw new Exception($transRst['error']['message']);//'No se ha podido enviar la información
                //throw new Exception('No se ha podido enviar la información');
            } 
            else 
            {

                $formnum = $transRst['success']['response']->return;
                $currentUser = new User();
                $currentUser->load();
                $currentUserId = $currentUser->getId();

                $GedoData['id_transaction'] = $formnum; //numero de transaccion
                $GedoData['acronym'] = 'DJPUB';//acronimo
                $GedoData['references']='Declaración Jurada';   //referencia  
                $GedoData['user'] = $currentUser->getSADEName();//Usuario SADE

                $GEDORst = Communicate::generateDocSADE($GedoData);
                if(isset($GEDORst['error']))
                {
                    throw new Exception($GEDORst['error']['message']);
                }
                else
                {
                    $Gedonum = $GEDORst['success']['response']->return->numero;
                    $encrypted = DDJJ::encrypt(DDJJ::createPDF());
                    //file_put_contents('../tmp/lpatext.bin', $encrypted);


                    $FILENETRst = Communicate::sendFileFilenet($encrypted);

                    if(isset($FILENETRst['error']))
                    {
                        throw new Exception($FILENETRst['error']['message']);
                    }
                    else
                    {
                        $filevalue = $FILENETRst['success']['response']->Response->document->properties->property->value;
                        $FILENET_id = substr($filevalue, 1, 36);
                        //$FILENET_id = '5438C917-81D1-4744-9713-0C6791AF0AE5';

                        if (empty($FILENET_id)) 
                        {
                            throw new Exception('No fue posible almacenar los datos de forma encriptada. Por su seguridad, la declaración no fue enviada');
                        }

                        $dbLink = Database::connect();

                        $stmt = $dbLink->prepare('select affidavit_type_supplied_value.value, revision_supplied_value.value from affidavit inner join supplied_value affidavit_type_supplied_value on affidavit_type_supplied_value.affidavit_id = affidavit.id inner join field affidavit_type_field on affidavit_type_field.id = affidavit_type_supplied_value.field_id and affidavit_type_field.element_id = ? inner join supplied_value revision_supplied_value on revision_supplied_value.affidavit_id = affidavit.id inner join field revision_field on revision_field.id = revision_supplied_value.field_id and revision_field.element_id = ? where affidavit.created_by = ? order by affidavit.created_at desc limit 1');
                        $lastAffidavitTypeElementId = $_SESSION['Affidavit']->getSection('ddjj-personal-data')->getSection('ddjj-personal-info')->getField('ddjj-type')->getId();
                        $lastAffidavitRevisionElementId = 'ddjj-revision';

                        $stmt->bind_param('ssi', $lastAffidavitTypeElementId, $lastAffidavitRevisionElementId, $currentUserId);
                        $stmt->execute();
                        $stmt->bind_result($lastAffidavitType, $lastRevision);
                        $stmt->fetch();
                        $stmt->close();

                        $stmt = $dbLink->prepare('insert into affidavit (`id`, `created_by`, `SADE_trans_UUID`, `SADE_GEDO_numero`, FILENET_id,`created_at`) values (null, ?, ?, ?, ?, now())');
                        $stmt->bind_param('isss', $currentUserId, $formnum, $Gedonum, $FILENET_id);
                        $stmt->execute();

                        $affidavitId = $dbLink->insert_id;

                        $stmt->close();

                        $fieldStmt = $dbLink->prepare('select id from field where element_id = ?');
                        $valueStmt = $dbLink->prepare('insert into supplied_value (`id`, `affidavit_id`, `field_id`, `secuence`, `value`) values (null, ?, ?, 1, ?)');

                        /* Affidavit Type */
                        $affidavitTypeElementId = $_SESSION['Affidavit']->getSection('ddjj-personal-data')->getSection('ddjj-personal-info')->getField('ddjj-type')->getId();
                        $fieldStmt->bind_param('s', $affidavitTypeElementId);
                        $fieldStmt->execute();
                        $fieldStmt->bind_result($affidavitType);
                        $fieldStmt->fetch();
                        $fieldStmt->free_result();

                        $affidavitTypeValue = $_SESSION['Affidavit']->getSection('ddjj-personal-data')->getSection('ddjj-personal-info')->getField('ddjj-type')->getValue();
                        $valueStmt->bind_param('iis', $affidavitId, $affidavitType, $affidavitTypeValue);
                        $valueStmt->execute();

                        /* Jurisdiction */
                        $jurisdictionElementId = $_SESSION['Affidavit']->getSection('ddjj-personal-data')->getSection('ddjj-gcba-work-info')->getField('ddjj-jurisdiction')->getId();
                        $fieldStmt->bind_param('s', $jurisdictionElementId);
                        $fieldStmt->execute();
                        $fieldStmt->bind_result($jurisdiction);
                        $fieldStmt->fetch();
                        $fieldStmt->free_result();

                        $jurisdictionValue = $_SESSION['Affidavit']->getSection('ddjj-personal-data')->getSection('ddjj-gcba-work-info')->getField('ddjj-jurisdiction')->getValue();
                        $valueStmt->bind_param('iis', $affidavitId, $jurisdiction, $jurisdictionValue);
                        $valueStmt->execute();

                        /* Ministry */
                        $ministryElementId = $_SESSION['Affidavit']->getSection('ddjj-personal-data')->getSection('ddjj-gcba-work-info')->getField('ddjj-ministry')->getId();
                        $fieldStmt->bind_param('s', $ministryElementId);
                        $fieldStmt->execute();
                        $fieldStmt->bind_result($ministry);
                        $fieldStmt->fetch();
                        $fieldStmt->free_result();

                        $ministryValue = $_SESSION['Affidavit']->getSection('ddjj-personal-data')->getSection('ddjj-gcba-work-info')->getField('ddjj-ministry')->getValue();
                        $valueStmt->bind_param('iis', $affidavitId, $ministry, $ministryValue);
                        $valueStmt->execute();

                        /* General Direction */
                        $generalDirectionElementId = $_SESSION['Affidavit']->getSection('ddjj-personal-data')->getSection('ddjj-gcba-work-info')->getField('ddjj-general-direction')->getId();
                        $fieldStmt->bind_param('s', $generalDirectionElementId);
                        $fieldStmt->execute();
                        $fieldStmt->bind_result($generalDirection);
                        $fieldStmt->fetch();
                        $fieldStmt->free_result();

                        $generalDirectionValue = $_SESSION['Affidavit']->getSection('ddjj-personal-data')->getSection('ddjj-gcba-work-info')->getField('ddjj-general-direction')->getValue();
                        $valueStmt->bind_param('iis', $affidavitId, $generalDirection, $generalDirectionValue);
                        $valueStmt->execute();

                        /* Job */
                        $jobElementId = $_SESSION['Affidavit']->getSection('ddjj-personal-data')->getSection('ddjj-gcba-work-info')->getField('ddjj-job')->getId();
                        $fieldStmt->bind_param('s', $jobElementId);
                        $fieldStmt->execute();
                        $fieldStmt->bind_result($job);
                        $fieldStmt->fetch();
                        $fieldStmt->free_result();

                        $jobValue = $_SESSION['Affidavit']->getSection('ddjj-personal-data')->getSection('ddjj-gcba-work-info')->getField('ddjj-job')->getValue();
                        $valueStmt->bind_param('iis', $affidavitId, $job, $jobValue);
                        $valueStmt->execute();

                        /* Revision */
                        $revisionElementId = 'ddjj-revision';
                        $fieldStmt->bind_param('s', $revisionElementId);
                        $fieldStmt->execute();
                        $fieldStmt->bind_result($revision);
                        $fieldStmt->fetch();
                        $fieldStmt->free_result();

                        $revisionValue = empty($lastAffidavitType) || empty($lastRevision) || $lastAffidavitType != $_SESSION['Affidavit']->getSection('ddjj-personal-data')->getSection('ddjj-personal-info')->getField('ddjj-type')->getValue() ? 1 : $lastRevision + 1;
                        $valueStmt->bind_param('iii', $affidavitId, $revision, $revisionValue);
                        $valueStmt->execute();

                        $fieldStmt->close();
                        $valueStmt->close();
                        $dbLink->close();

                        /* descuento del permiso del campo observacion */
                        if ($currentUser->hasCommentPrivilege()) 
                        {
                            $currentUser->claimCommentPrivilege($affidavitId);
                            $currentUser->save();
                        }
                    }
                }

            }

            unset($_SESSION['Affidavit']);
        } catch (Exception $e) {
           // self::saveError(FILENET."|".SADE_TRANSACTION_SERVICE."|".SADE_GEDO, $e->getMessage());
            $rtn['error']['code'] = 1;
            $rtn['error']['message'] = $e->getMessage();
        }

        return $rtn;
    }

    public static function getGEDOPDF($affidavitId) {
        $rtn = [
            'pdf' => [
                'date' => null,
                'data' => null
            ],
            'error' => [
                'code' => 0,
                'message' => ''
            ]
        ];
        $GEDONumero = null;
        $createdAt = null;

        try {
            $currentUser = new User();
            $currentUser->load();
            $currentUserId = $currentUser->getId();

            $dbLink = Database::connect();

            $stmt = $dbLink->prepare('select affidavit.SADE_GEDO_numero, affidavit.created_at from affidavit inner join user on user.id = ? and (user.supervisor= 1 or affidavit.created_by = ? ) where affidavit.id = ?');
            $stmt->bind_param('iii', $currentUserId, $currentUserId, $affidavitId);
            $stmt->execute();

            $stmt->bind_result($GEDONumero, $createdAt);
            $stmt->fetch();

            $stmt->close();
            $dbLink->close();

            if (empty($GEDONumero)) {
                throw new Exception('No se ha encontrado el documento especificado.');
            } 
            else 
            {
                $GedoDocData['doc'] = $GEDONumero;
                $GedoDocData['user'] = $currentUser->getSADEName();
                 
                $gedo_rtn = Communicate::getContentDocSADE($GedoDocData);
                if(isset($gedo_rtn['error']))
                {
                    throw new Exception($gedo_rtn['error']['message']);
                }
                else
                {
                    $rtn['PDF']['data'] = $gedo_rtn['success']['response']->return;
                    $rtn['PDF']['date'] = $createdAt;
                }
                    
            }

        } catch (Exception $e) {
            //self::saveError(SADE_GEDO_CONSULTA, $e);
            $rtn['error']['code'] = 1;
            $rtn['error']['message'] = $e->getMessage();
        }

        return $rtn;
    }
    // private static function saveError($webservice, $error)
    // {
        
    //     $currentUser = new User();
    //     $currentUser->load();
    //     $curUser = $currentUser->getSADEName(); 
    //     $dbLink = Database::connect();
    //     $stmt = $dbLink->prepare('INSERT INTO catch_error(`user`,`webservice`,`error`) values (?, ?, ?)');
    //     $stmt->bind_param('sss', $curUser, $webservice, $error);
    //     $stmt->execute();
       
    //     $body = '<table width="100%" border="1" cellspacing="0" cellpadding="0" align="center" valign="top">
    //               <tr><td>User</td><td>$curUser</td></tr>
    //               <tr><td>Webservice</td><td>$webservice</td></tr>
    //               <tr><td>Error</td><td>$error</td></tr>
    //               </table>';

    //    // $body = 'User:'.$curUser.'</brr\n Webservice: '.$webservice.'\r\n Error:'.$error;

    //     //        $to = 'driva@buenosaires.gob.ar';
    //     $to = ['alerts-ddjj@gcbait.mailclark.ai','gotyp.info@buenosaires.gob.ar'];
    //     $subject = 'Alert-DDJJ';
    //     $parameters = array($curUser,$webservice,$error);
    //     $fields = array('$curUser','$webservice','$error');

    //     self::sendMail($to,$subject, $body, $fields, $parameters);
    // }

    // private static function sendMail($to,$subject, $body, $fields = '', $parameters = '') {

    //     $result = "";
    //     $mailer = new PHPMailer();


    //     $body = str_replace($fields, $parameters, $body);

    //     #Configuracion SMTP
    //     $mailer->IsSMTP();
    //     $mailer->SMTPAuth = true;
    //     //$mailer->CharSet = 'utf-8';
    //     //$mailer->Encoding = 'quoted­printable';

    //     $mailer->Username = 'seclyt.mat@buenosaires.gob.ar';
    //     $mailer->Password = 'H4_c1rvv';
    //     $mailer->Host = 'smtp.buenosaires.gob.ar';

    //     #Remitente y destinatarios
    //     $mailer->From = 'ddjj@buenosaires.gob.ar';
    //     $mailer->FromName = "DDJJ";
    //     //  $mailer->AddReplyTo = 'seclyt@buenosaires.gob.ar';
    //     if (is_array($to) || is_object($to)) {
    //         foreach ($to as $email) {
    //             $mailer->AddAddress($email);
    //         }
    //     } else {
    //         $mailer->AddAddress($to);
    //     }
    //     //$mailer->AddAddress('aporcelli@buenosaires.gob.ar');
    //     #Contenido
    //     $mailer->Subject = $subject;
    //     $mailer->Body = $body;
    //     $mailer->AltBody = 'Cuerpo TXT';
    //     $mailer->CharSet = 'UTF-8';
    //     #Envio
    //     if (!$mailer->send()) {
    //         $result = 'error enviando: ' . $mailer->ErrorInfo;
    //     } else {
    //         $result = 'ok';
    //     }

    //   //  echo ($result);
    // }

    public static function getPrivate($affidavitId) {
        $rtn = [
            'pdf' => [
                'date' => null,
                'data' => null
            ],
            'error' => [
                'code' => 0,
                'message' => ''
            ]
        ];
        $F_id = null;
        $createdAt = null;

        try {
            $currentUser = new User();
            $currentUser->load();
            $currentUserId = $currentUser->getId();

            $dbLink = Database::connect();

            $stmt = $dbLink->prepare('select affidavit.FILENET_id, affidavit.created_at from affidavit inner join user on user.id = ? and user.unlocker= 1 where affidavit.id = ? ');
            $stmt->bind_param('ii', $currentUserId, $affidavitId);
            $stmt->execute();

            $stmt->bind_result($F_id, $createdAt);
            $stmt->fetch();

            $stmt->close();
            $dbLink->close();

            if (empty($F_id)) {
                throw new Exception('No se ha encontrado el documento especificado.');
            } 
            else
            {
                $filenet_rst = Communicate::downloadFileFilenet($F_id);
                if(isset($filenet_rst['error']))
                {
                    throw new Exception($filenet_rst['error']['message']); //'No se ha encontrado el documento especificado.'
                }
                else
                {
                    $filenet_rst = $filenet_rst['success']['response'];
                    $rtn['BIN']['data'] = base64_decode($filenet_rst->Response->file->content);
                    $rtn['BIN']['date'] = $createdAt;
                }
            }
    
        } catch (Exception $e) {
            //self::saveError(FILENET, $e);
            $rtn['error']['code'] = 1;
            $rtn['error']['message'] = $e->getMessage();
        }

        return $rtn;
    }

    public static function publish($affidavitId, $state) {
        $rtn = [
            'error' => [
                'code' => 0,
                'message' => ''
            ]
        ];

        try {
            $currentUser = new User();
            $currentUser->load();
            $currentUserId = $currentUser->getId();

            $dbLink = Database::connect();

            $stmt = $dbLink->prepare('update affidavit inner join user on user.id = ? and user.publisher = 1 set affidavit.published = ? where affidavit.id = ?');
            $stmt->bind_param('iii', $currentUserId, $state, $affidavitId);
            $stmt->execute();

            if ($stmt->affected_rows === 0) {
                throw new Exception("Ha ocurrido un error");
            }

            $stmt->close();
            $dbLink->close();
        } catch (Exception $e) {
            
            $rtn['error']['code'] = 1;
            $rtn['error']['message'] = $e->getMessage();
        }

        return $rtn;
    }

    public static function encrypt($data) {
        $rtn = '';

        $cert = file_get_contents('../../../Escribania_General.cer');
        $key = openssl_pkey_get_public($cert);
        $keyDetails = openssl_pkey_get_details($key);

        $blockSize = ($keyDetails['bits'] / 8) - 11;
        $dataSize = strlen($data);

        for ($blockCount = 0; $dataSize > $blockSize * $blockCount; $blockCount++) {
            if (openssl_public_encrypt(substr($data, $blockSize * $blockCount, $blockSize), $crypt, $key)) {
                $rtn .= $crypt;
                unset($crypt);
            } else {
                throw new Exception('No fue posible cifrar sus datos. Por su seguridad, se ha detenido el envío de información.');
            }
        }

        return $rtn;
    }

    public static function validate($section = null, $fields = null) {

        $rtn = ['error' => ['code' => 0, 'message' => '']];

        Session::retrieve();

        $allSections = $_SESSION['Affidavit'];
        $validatedArray = array();

        if ($fields != null && $section != null) {
            $firstSection = $allSections->getSection($section[0]['id']);
            for ($i = 1; $i < count($section); $i++) {
                // obtiene los datos de la sub pestaña o tab
                $firstSection = $firstSection->getSection($section[$i]['id']);
            }

            $firstSectionId = $firstSection->getId();
            DDJJ::recursiveValidate($firstSection, $validatedArray[$firstSectionId], $fields);

            $isEmpty = empty($validatedArray[$firstSectionId]);
            if ($isEmpty) {
                unset($validatedArray[$firstSectionId]);
            }
        } else {
            foreach ($allSections->getSections() as $aSection) {
                $aSectionId = $aSection->getId();
                DDJJ::recursiveValidate($aSection, $validatedArray[$aSectionId]);

                // Si la validación vuelve vacía
                $isEmpty = empty($validatedArray[$aSectionId]);
                if ($isEmpty) {
                    unset($validatedArray[$aSectionId]);
                }
            }
        }
        $isEmpty = empty($validatedArray);
        if ($isEmpty) {
            $validatedArray = $rtn;
        }

        return($validatedArray);
    }

    public static function recursiveValidate($aSession, &$validated, &$fields = null) {
        foreach ($aSession->getEverything() as $element) {

            if (is_a($element, "AffidavitSection")) {

                DDJJ::recursiveValidate($element, $validated[$element->getId()], $fields);

                if (in_array(null, $validated)) {
                    unset($validated[$element->getId()]);
                }
            } elseif (is_a($element, "AffidavitField") && $fields == null) {
                if ($element->validate()['code'] != 0) {

                    $validated[$element->getId()] ["Field"] = $element->getId();
                    $validated[$element->getId()] ["error"] = $element->validate();
                }
                //SE CONSULTA POR EL ID PARA QUE SOLO SE DEVUELVE AL ERROR DEL CAMPO SOLICITADO
            } elseif (is_a($element, "AffidavitField") && $element->getId() === $fields[0]['id']) {
                if ($element->validateType()['code'] != 0) {
                    $validated[$element->getId()] ["Field"] = $element->getId();
                    $validated[$element->getId()] ["error"] = $element->validateType();
                }
            }
        }
        return $validated;
    }

}

if (Security::isAjax() || DEBUG_AJAX) {
    if (isset($_REQUEST['action']) && !empty($_REQUEST['action'])) {
        switch ($_REQUEST['action']) {
            case 'destroySession':
                echo json_encode(DDJJ::destroySession());
                break;
            case 'getSessionTimeRemain':
                echo json_encode(DDJJ::getSessionTimeRemain());
                break;
            case 'getSessionTimeOut':
                echo json_encode(DDJJ::getSessionTimeOut());
                break;
            case 'setLifeTime':
                echo json_encode(DDJJ::setLifeTime());
                break;
            case 'extendSessionTimeOut':
                echo json_encode(DDJJ::extendSessionTimeOut());
                break;
            case 'getSessionMode':
                echo json_encode(DDJJ::getSessionMode());
                break;
            case 'loadJson':
                echo("<pre>");
                echo json_encode(DDJJ::loadJson());
                echo("</pre>");
                break;
            case 'removeTemplate':
                echo json_encode(DDJJ::removeTemplate($_REQUEST['sections'], $_REQUEST['id']));
                break;
            case 'cloneTemplate':
                echo json_encode(DDJJ::cloneTemplate($_REQUEST['section'], $_REQUEST['type'], $_REQUEST['id'], $_REQUEST['name']));
                break;
            case 'removeFields':
                echo json_encode(DDJJ::removeFields($_REQUEST['sections'], $_REQUEST['id']));
                break;
            case 'addFields':
                echo json_encode(DDJJ::addFields($_REQUEST['sections'], $_REQUEST['id']));
                break;
            case 'create':
                //setea todos los errores (NOTICE, WARNING etc.) como exepciones para su visualizacion con toaster (ya que los recuperamos con try/catch)
                //(solo para modo debug y es independiente de la configuracion display_error));
                //$exceptionErrorHandler = new ExceptionErrorHandler;
                echo json_encode(DDJJ::create($_REQUEST['type']));
                break;
            case 'save':
                DDJJ::save($_REQUEST['section'], $_REQUEST['fields']);
                $validate = DDJJ::validate($_REQUEST['section'], $_REQUEST['fields']);

                // si validate trae un error lo manda por ajax
                echo json_encode($validate);
                break;
            case 'commit':
                $validate = DDJJ::validate();

                // Si la validación no trae error se ejecuta el commit
                if (isset($validate['error'])) {
                    echo json_encode(DDJJ::commit());
                } else {
                    echo json_encode($validate);
                }
                break;
            case 'getGEDOPDF':
                ob_clean();
                $GEDOPDF = DDJJ::getGEDOPDF($_REQUEST['affidavitId']);

                if ($GEDOPDF['error']['code'] === 0) {
                    header('Content-Disposition: attachment; filename=Declaración Jurada - ' . $GEDOPDF['PDF']['date'] . '.pdf');
                    header('Content-Type: application/pdf');
                    echo $GEDOPDF['PDF']['data'];
                } else {
                    header('Content-Disposition: attachment; filename=error.txt');
                    header('Content-Type: text/plain');
                    echo $GEDOPDF['error']['message'];
                }

                break;
            case 'getPrivate':
                ob_clean();
                $PRIVATEBIN = DDJJ::getPrivate($_REQUEST['affidavitId']);

                if ($PRIVATEBIN['error']['code'] === 0) {
                    header('Content-Disposition: attachment; filename=Declaración Jurada - ' . $PRIVATEBIN['BIN']['date'] . '.bin');
                    header('Content-Type: application/octet-stream');
                    echo $PRIVATEBIN['BIN']['data'];
                } else {
                    header('Content-Disposition: attachment; filename=error.txt');
                    header('Content-Type: text/plain');
                    echo $PRIVATEBIN['error']['message'];
                }

                break;
            case 'PDFExportRequest':
                echo json_encode(DDJJ::validate());
                break;
            case 'exportPDF':
                ob_clean();
                header('Content-Disposition: attachment; filename=Declaración Jurada.pdf');
                header('Content-Type: application/pdf');
                echo DDJJ::createPDF();
                break;
            case 'publishDDJJ':
                $updateState = DDJJ::publish($_REQUEST['affidavitId'], $_REQUEST['state']);
                echo json_encode($updateState);
                break;
            case 'validate':
                echo json_encode(DDJJ::validate());
                break;
        }
    }
}
?>
