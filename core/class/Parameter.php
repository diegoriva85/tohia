<?php

class Parameter {

    private static $adBa;
    private static $enviroment;
    private static $filenet;
    private static $ldapService;
    private static $livecycleSignatureService;
    private static $sadeConsultaCuitCuil;
    private static $sadeConsultaUsuarioSade;
    private static $sadeForm;
    private static $sadeFormName;
    private static $sadeGedo;
    private static $sadeGedoConsulta;
    private static $sadeTransactionService;
    private static $version;
    private static $conexionAdUser;
    private static $conexionAdPass;

    public static function load() {

        $query = "select field,value from parameter where deleted = 0 order by field";

        $queryResult = Database::result($query);


        //@TODO: Por una cuestion de seguridad pregunto uno por uno
        //@TODO: Ver la forma de cargarlo no manualmente.
        foreach ($queryResult as $parameter) {
            if ($parameter['field'] === "AD_BA") {
                self::setAdBa($parameter['value']);
            }

            if ($parameter['field'] === "ENVIROMENT") {
                self::setEnviroment($parameter['value']);
            }

            if ($parameter['field'] === "FILENET") {
                self::setFilenet($parameter['value']);
            }

            if ($parameter['field'] === "LDAP_SERVICE") {
                self::setLapService($parameter['value']);
            }

            if ($parameter['field'] === "LIVECYCLE_SIGNATURE_SERVICE") {
                self::setLivecycleSignatureService($parameter['value']);
            }
            
            if ($parameter['field'] === "SADE_CONSULTA_CUIT_CUIL") {
                self::setSadeConsultaCuitCuil($parameter['value']);
            }
            
            if ($parameter['field'] === "SADE_CONSULTA_USUARIO_SADE") {
                self::setSadeConsultaUsuarioSade($parameter['value']);
            }
            
            if ($parameter['field'] === "SADE_FORM") {
                self::setSadeForm($parameter['value']);
            }
            
            if ($parameter['field'] === "SADE_FORM_NAME") {
                self::setSadeFormName($parameter['value']);
            }
            
            if ($parameter['field'] === "SADE_GEDO") {
                self::setSadeGedo($parameter['value']);
            }
            
            if ($parameter['field'] === "SADE_GEDO_CONSULTA") {
                self::setSadeGedoConsulta($parameter['value']);
            }
            
            if ($parameter['field'] === "SADE_TRANSACTION_SERVICE") {
                self::setTransactionService($parameter['value']);
            }
            
            if ($parameter['field'] === "VERSION") {
                self::setVersion($parameter['value']);
            }
            
            if ($parameter['field'] === "CONEXION_AD_USER") {
                self::setConexionAdUser($parameter['value']);
            }

            if ($parameter['field'] === "CONEXION_AD_PASS") {
                self::setConexionAdPass($parameter['value']);
            }            
        }
    }

    public function getAdBa() {

        return self::$adBa;
    }

    private static function setAdBa($adBa) {
        self::$adBa = $adBa;
    }

    private static function setEnviroment($enviroment) {
        self::$enviroment = $enviroment;
    }

    public function getEnviroment() {

        return self::$enviroment;
    }

    private static function setFilenet($filenet) {
        self::$filenet = $filenet;
    }

    public function getFilenet() {

        return self::$filenet;
    }

    private static function setLapService($ldapService) {
        self::$ldapService = $ldapService;
    }

    public function getLapService() {

        return self::$ldapService;
    }

    private static function setLivecycleSignatureService($livecycleSignatureService) {
        self::$livecycleSignatureService = $livecycleSignatureService;
    }

    public function getLivecycleSignatureService() {

        return self::$livecycleSignatureService;
    }
    
    private static function setSadeConsultaCuitCuil($sadeConsultaCuitCuil) {
        self::$sadeConsultaCuitCuil = $sadeConsultaCuitCuil;
    }

    public function getSadeConsultaCuitCuil() {

        return self::$sadeConsultaCuitCuil;
    }
    
    private static function setSadeConsultaUsuarioSade($sadeConsultaUsuarioSade) {
        self::$sadeConsultaUsuarioSade = $sadeConsultaUsuarioSade;
    }

    public function getSadeConsultaUsuarioSade() {

        return self::$sadeConsultaUsuarioSade;
    }
    
    private static function setSadeForm($sadeForm) {
        self::$sadeForm = $sadeForm;
    }

    public function getSadeForm() {

        return self::$sadeForm;
    }
    
    private static function setSadeFormName($sadeFormName) {
        self::$sadeFormName = $sadeFormName;
    }

    public function getSadeFormName() {

        return self::$sadeFormName;
    }
    
    private static function setSadeGedo($sadeGedo) {
        self::$sadeGedo = $sadeGedo;
    }

    public function getSadeGedo() {

        return self::$sadeGedo;
    }
    
    private static function setSadeGedoConsulta($sadeGedoConsulta) {
        self::$sadeGedoConsulta = $sadeGedoConsulta;
    }

    public function getSadeGedoConsulta() {

        return self::$sadeGedoConsulta;
    }
    
    private static function setTransactionService($sadeTransactionService) {
        self::$sadeTransactionService = $sadeTransactionService;
    }

    public function getTransactionService() {

        return self::$sadeTransactionService;
    }
    
    private static function setVersion($version) {
        self::$version = $version;
    }

    public function getVersion() {

        return self::$version;
    }

    private static function setConexionAdUser($conexionAdUser) {
        self::$conexionAdUser = $conexionAdUser;
    }

    public function getConexionAdUser() {

        return self::$conexionAdUser;
    }
    
    private static function setConexionAdPass($conexionAdPass) {
        self::$conexionAdPass = $conexionAdPass;
    }

    public function getConexionAdPass() {

        return self::$conexionAdPass;
    }    
}

?>