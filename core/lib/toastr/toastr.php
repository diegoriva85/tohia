<?php

$name = 'toastr';
$version = '2.1.2';
$url = PATH_LIB . "/toastr";
$description = "toastr notificaciones";
$cache = PROJECT_VERSION;
$min = (SERVER_URL == 'localhost') ? '' : '.min';

echo "<!-- libreria $description : inicio -->";
echo "<script type='text/javascript' src='$url/$version/$name$min.js?v=$cache'></script>";
echo "<link rel='stylesheet' href='$url/$version/build/$name.css?v=$cache'>";
echo "<!-- libreria $description : fin -->";

?>