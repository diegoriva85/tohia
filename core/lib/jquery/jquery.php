<?php

$name = 'jquery-2.1.1';
$version = '2.1.1';
$url = PATH_LIB . "/jquery";
$description = "jquery";
$cache = PROJECT_VERSION;
$min = MIN;

echo "<!-- libreria $description : inicio -->\n";
echo "<script type='text/javascript' src='$url/$version/$name$min.js?v=$cache'></script>\n";
echo "<!-- libreria $description : fin -->\n";

?>
