<?php

$name = 'jquery.jqpagination';
$version = '1.4';
$url = PATH_LIB . "/jqpagination";
$description = "jqpagination - paginador";
$cache = PROJECT_VERSION;
$min = MIN;

echo "<!-- libreria $description : inicio -->";
echo "<script type='text/javascript' src='$url/$version/js/$name$min.js?v=$cache'></script>";
echo "<link rel='stylesheet' href='$url/$version/css/$name$min.css?v=$cache'>";
echo "<!-- libreria $description : fin -->";

?>
