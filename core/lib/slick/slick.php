<?php

$name = 'slick';
$version = '1.6.0';
$url = PATH_LIB . "/slick";
$description = "carrusel";
$cache = PROJECT_VERSION;
$min = MIN;

echo "<!-- libreria $description : inicio -->";
echo "<script type='text/javascript' src='$url/$version/$name/$name$min.js?v=$cache'></script>";
echo "<link rel='stylesheet' href='$url/$version/$name/$name.css?v=$cache'>";
echo "<link rel='stylesheet' href='$url/$version/$name/$name-theme.css?v=$cache'>";
echo "<!-- libreria $description : fin -->";


?>