var name = 'jquery-ui';
var version = '1.11.4';
var url = Project.get("projectUrl") + "/sys/lib/jquery-ui";
var description = "jquery-ui";
var cache = Project.get("projectVersion");
var min = (Project.get("serverUrl") == 'localhost') ? '' : '.min';

//crea el elemento js y setea los valores
var script = document.createElement('script');
script.type = "text/javascript";
// script.async = true;
script.src = url + "/" + version + "/" + name + min + ".js" + "?v=" + cache;


//agrea los elementos creados a head

document.head.appendChild(document.createComment('libreria ' + description + ": inicio"));

document.head.appendChild(script);

document.head.appendChild(document.createComment('libreria ' + description + ": fin"));