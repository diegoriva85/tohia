<?php

$name = 'jquery-ui';
$version = '1.11.4';
$url = PATH_LIB . "/jquery-ui";
$description = "jquery-ui";
$cache = PROJECT_VERSION;
$min = MIN;

echo "<!-- libreria $description : inicio -->";
echo "<script type='text/javascript' src='$url/$version/$name$min.js?v=$cache'></script>";
echo "<link rel='stylesheet' href='$url/$version/$name$min.css?v=$cache'>";
echo "<!-- libreria $description : fin -->";

?>
