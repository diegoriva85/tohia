<?php

$name = 'bastrap';
$version = '3.0';
$url = PATH_LIB . "/bastrap";
$description = "bastrap";
$cache = PROJECT_VERSION;
$min = MIN;



/* CSS */
echo "<!-- libreria $description : inicio -->";
echo "<link rel='stylesheet' href='$url/$version/bastrap3/bootstrap.min.css?v=$cache'>";
echo "<!-- libreria $description : fin -->";

echo "<!-- libreria $description : inicio -->";
echo "<link rel='stylesheet' href='$url/$version/bastrap3/bastrap.css?v=$cache'>";
echo "<!-- libreria $description : fin -->";

echo "<!-- libreria $description : inicio -->";
echo "<link rel='stylesheet' href='$url/$version/docs/docs.css?v=$cache'>";
echo "<!-- libreria $description : fin -->";

echo "<!-- libreria $description : inicio -->";
echo "<link rel='stylesheet' href='$url/$version/bastrap3/vendor/chosen/chosen.min.css?v=$cache'>";
echo "<!-- libreria $description : fin -->";

echo "<!-- libreria $description : inicio -->";
echo "<link rel='stylesheet' href='$url/$version/docs/prettify/prettify.css?v=$cache'>";
echo "<!-- libreria $description : fin -->";
/*
Configure.cssInc(pathlib + '/bastrap/3.0/bastrap3/', 'bootstrap.min');
Configure.cssInc(pathlib + '/bastrap/3.0/bastrap3/', 'bastrap');
Configure.cssInc(pathlib + '/bastrap/3.0/docs/', 'docs');
Configure.cssInc(pathlib + '/bastrap/3.0/bastrap3/vendor/chosen/', 'chosen.min');
Configure.cssInc(pathlib + '/bastrap/3.0/docs/prettify/', 'prettify');
*/




/* JS */
echo "<!-- libreria $description : inicio -->";
echo "<script type='text/javascript' src='$url/$version/bastrap3/bootstrap.min.js?v=$cache'></script>";
echo "<!-- libreria $description : fin -->";

echo "<!-- libreria $description : inicio -->";
echo "<script type='text/javascript' src='$url/$version/docs/docs.js?v=$cache'></script>";
echo "<!-- libreria $description : fin -->";

echo "<!-- libreria $description : inicio -->";
echo "<script type='text/javascript' src='$url/$version/docs/prettify/prettify.js?v=$cache'></script>";
echo "<!-- libreria $description : fin -->";

echo "<!-- libreria $description : inicio -->";
echo "<script type='text/javascript' src='$url/$version/bastrap3/vendor/chosen/chosen.jquery.min.js?v=$cache'></script>";
echo "<!-- libreria $description : fin -->";
/*
Configure.jsInc(pathlib + '/bastrap/3.0/bastrap3/', 'bootstrap.min');
Configure.jsInc(pathlib + '/bastrap/3.0/docs/', 'docs');
Configure.jsInc(pathlib + '/bastrap/3.0/docs/prettify/', 'prettify');
Configure.jsInc(pathlib + '/bastrap/3.0/bastrap3/vendor/chosen/', 'chosen.jquery.min');
*/





?>
