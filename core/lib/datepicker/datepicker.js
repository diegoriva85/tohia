var name = 'bootstrap-datepicker';
var version = '2.0';
var url = Project.get("projectUrl") + "/sys/lib/datepicker";
var description = "datepicker";
var cache = Project.get("projectVersion");
var min = (Project.get("serverUrl") == 'localhost') ? '' : '.min';

//crea el elemento js y setea los valores
var script = document.createElement('script');
script.type = "text/javascript";
script.defer = true;
script.src = url + "/" + version + "/" + name + min + ".js" + "?v=" + cache;

//agrea los elementos creados a head

document.head.appendChild(document.createComment('libreria ' + description + ": inicio"));

document.head.appendChild(script);

document.head.appendChild(document.createComment('libreria ' + description + ": fin"));