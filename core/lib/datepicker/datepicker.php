<?php

$name = 'bootstrap-datepicker';
$version = '2.0';
$url = PATH_LIB . "/datepicker";
$description = "datepicker";
$cache = PROJECT_VERSION;
$min = MIN;

echo "<!-- libreria $description : inicio -->\n";
echo "<script type='text/javascript' src='$url/$version/js/$name$min.js?v=$cache'></script>\n";
echo "<script type='text/javascript' src='$url/$version/locales/$name.es.min.js?v=$cache'></script>\n";
echo "<link rel='stylesheet' href='$url/$version/css/bootstrap-datepicker3.css?v=$cache'>";
echo "<!-- libreria $description : fin -->\n";

?>