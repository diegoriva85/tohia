<?php

	//necesarios para las constantes del sistema
	require '../sys/conf/ini.conf';

	/********************************************************************************/
	/******************************** Data Table  ***********************************/
	/********************************************************************************/

	//iniciamos un ojbeto datatable
	DataTable::init($_REQUEST["table"], $_REQUEST["column_name"], $_REQUEST["index"]);
	
	//columnas de la tabla
	$aColumns = DataTable::getColumns();

	//formato de las columnas, por defecto las no espeficicadas son consideradas string
	(isset($_REQUEST["column_number"])) ? DataTable::format("number", $_REQUEST["column_number"]) : "";
	(isset($_REQUEST["column_money"]))  ? DataTable::format("money", $_REQUEST["column_money"])  : "";
	(isset($_REQUEST["column_date"]))   ? DataTable::format("date", $_REQUEST["column_date"])   : "";

	//prepara las columnas segun los formatos indicados (number,money,date,string)
	$aColumnsFunction = DataTable::prepare();

	//paginacion de la consulta
	(isset($_REQUEST["iDisplayStart"])) ? DataTable::paging( $_REQUEST['iDisplayStart'] , $_REQUEST['iDisplayLength'] ) : "";
	
	//orden de la tabla
	(isset($_REQUEST["iSortCol_0"])) ? DataTable::order($_REQUEST["iSortingCols"]) : "";
	
	//si cargo datos en el input de busqueda, anexa el mismo a la condicion general de la sentencia
	($_REQUEST["sSearch"] != "") ? DataTable::filter($_REQUEST["sSearch"]) : "";
	
	(isset($_REQUEST["left_join"])) ? DataTable::setJoin($_REQUEST["left_join"])  : "";
	
	//si cargo condicion adicional en el objeto
	(isset($_REQUEST["condition"])) ? DataTable::condition($_REQUEST["condition"]) : "";

	// echo DataTable::getSqlQuery();
	
	//ejecuta la consulta sql
	$rResult = DataTable::exec();


	//cantidad de registros filtrados del input busqueda 
	$iFilteredTotal = DataTable::countFilter();
	//cantidad tota de registro obtenidos de base
	$iTotal = DataTable::countSql();
	
	//salida a retornar
	$output = array
	(
		"sEcho" => intval($_REQUEST['sEcho']),
		"iTotalRecords" => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData" => array()
	);
	
	//recorre el resulta de la consulta, mientras haya filas a procesar
	while ( $aRow = mysqli_fetch_array( $rResult ) )
	{
		$aRow = array_map("utf8_encode", $aRow);
		$row = array();
		
		for ( $i=0 ; $i < count($aColumns) ; $i++ )
		{
			if ( $aColumns[$i] == "version" )
			{
				//formato especial para la columna version
				$row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
			}
			else if ( $aColumns[$i] != ' ' )
			{
				// Si hay un leftJoin le quita al punto del prefijo de la columna
				if(isset($_REQUEST["left_join"]))
				{
					$row[] = $aRow[ str_replace(".", "", $aColumns[$i]) ];
				}
				else
				{
					$row[] = $aRow[ ($aColumns[$i]) ];
				}
			}
		}
		$output['aaData'][] = $row;
	}

	echo json_encode( $output );
?>