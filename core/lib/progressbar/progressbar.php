<?php

$name = 'bootstrap-progressbar';
$version = '0.8.4';
$url = PATH_LIB . "/progressbar";
$description = "progressbar";
$cache = PROJECT_VERSION;
$min = MIN;

echo "<!-- libreria $description : inicio -->\n";
echo "<script type='text/javascript' src='$url/$version/$name$min.js?v=$cache'></script>\n";
echo "<!-- libreria $description : fin -->\n";

?>