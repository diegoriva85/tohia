<?php

$name = 'nprogress';
$version = '0.2.0';
$url = PATH_LIB . "/nprogress";
$description = "nprogress";
$cache = PROJECT_VERSION;
$min = (SERVER_URL == 'localhost') ? '' : '.min';

echo "<!-- libreria $description : inicio -->";
echo "<script type='text/javascript' src='$url/$version/$name$min.js?v=$cache'></script>";
echo "<link rel='stylesheet' href='$url/$version/css/$name.css?v=$cache'>";
echo "<!-- libreria $description : fin -->";

?>