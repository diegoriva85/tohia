<?php

$name = 'font-awesome';
$version = '4.6.3';
$url = PATH_LIB . "/font-awesome";
$description = "font-awesome - Tipografia simbolica";
$cache = PROJECT_VERSION;
$min = MIN;

echo "<!-- libreria $description : inicio -->";
echo "<link rel='stylesheet' href='$url/$version/css/$name$min.css?v=$cache'>";
echo "<!-- libreria $description : fin -->";

?>
