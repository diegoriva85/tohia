var name = 'font-awesome';
var version = '4.2.0';
var url = Project.get("projectUrl") + "/sys/media/font/font-awesome";
var description = "font-awesome";
var cache = Project.get("projectVersion");
var min = (Project.get("serverUrl") == 'localhost') ? '' : '.min';

//crea el elemento css y setea los valores
var link = document.createElement('link');
link.rel = 'stylesheet';
link.href = url + "/" + version + "/" + "css/font-awesome" + min + ".css" + "?v=" + cache;

//agrea los elementos creados a head

document.head.appendChild(document.createComment('libreria ' + description + ": inicio"));

document.head.appendChild(link);

document.head.appendChild(document.createComment('libreria ' + description + ": fin"));