<?php

$name = 'nicescroll';
$version = '3.2.0';
$url = PATH_LIB . "/nicescroll";
$description = "nicescroll";
$cache = PROJECT_VERSION;
$min = MIN;

echo "<!-- libreria $description : inicio -->";
echo "<script type='text/javascript' src='$url/$version/jquery.$name$min.js?v=$cache'></script>";
echo "<!-- libreria $description : fin -->";

?>
