<?php

//require_once __DIR__ . '/../../sys' . DIRECTORY_SEPARATOR . 'conf' . DIRECTORY_SEPARATOR . 'ini.conf';
define('ROOT_DIR', '/var/www/html/soft-tohia/');
define('ROOT_DIR_IMAGE', ROOT_DIR.'core/media/img/');
define('ROOT_DIR_PRD', ROOT_DIR.'core/media/products/');
define('ROOT_DIR_RES', ROOT_DIR.'core/media/resized/');

require ROOT_DIR.'core/lib/FPDF/fpdf.php';

class PDF {
    public static function createPDF() {
        
        $files  = scandir(ROOT_DIR_RES);
        unset($files[0],$files[1]);
        sort($files);

        $pdf = new FPDF('P', 'mm', 'A4');
        $pdf->AliasNbPages();
        //$pdf->SetFillColor(200,200,200);
        $fontTipe = 'Times'; //tipo de fuente
        setlocale(LC_TIME, 'es_ES.UTF-8');
        //$dateString = strftime("Buenos Aires, %A %d de %B de %Y");

        $pdf->AddPage();
        //$pdf->Image(ROOT_DIR_IMAGE.'portada.png', 5, 5, 200, 200, 'PNG');
       	$pdf->Image(ROOT_DIR_IMAGE.'logofinalsimple.jpg', 30, 5, 150, 200, 'JPG');
       	$pdf->SetXY(10,220);

        $pdf->SetFont($fontTipe, 'IB', 16);
        $pdf->cell(0, 10, utf8_decode("CATALOGO"), 0, 1, 'C');
    	//$pdf->SetFont('Helvetica', '', 10);


		//$name = 'Catalogo-rev';
		//$name = 'Novedades-rev';
		
		//$name = 'Novedades';
		$name = 'Catalogo';

        if ($name == 'Catalogo' || $name == 'Novedades' )
        {
			$pdf->Image(ROOT_DIR_IMAGE.'face.png', 50, 240, 15, 15, 'PNG','www.facebook.com/Tohiadeco/');
			$pdf->SetXY(66,241);
			$pdf->Cell(0,10 ,utf8_decode("Tohiadeco"),'','','',false,'www.facebook.com/Tohiadeco/'); 
			$pdf->Image(ROOT_DIR_IMAGE.'insta.png', 50, 258, 15, 15, 'PNG','www.instagram.com/tohia_deco/');
			$pdf->SetXY(66,259);

			$pdf->Cell(0,10 ,utf8_decode("tohia_deco"),'','','',false,'www.instagram.com/tohia_deco/'); 

			$pdf->Image(ROOT_DIR_IMAGE.'whatsapp.png', 110, 240, 15, 15, 'PNG','https://api.whatsapp.com/send?phone=+5491167426705&text=Hola, me contacto por un producto');
			$pdf->SetXY(126, 241);
			$pdf->Cell(0,10 ,utf8_decode("1167426705"),'','','',false,'https://api.whatsapp.com/send?phone=+5491167426705&text=Hola, me contacto por un producto'); 

			$pdf->Image(ROOT_DIR_IMAGE.'whatsapp.png', 110, 258, 15, 15, 'PNG','https://api.whatsapp.com/send?phone=+5491156138863&text=Hola, me contacto por un producto');
			$pdf->SetXY(126, 259);
			$pdf->Cell(0,10 ,utf8_decode("1156138863"),'','','',false,'https://api.whatsapp.com/send?phone=+5491156138863&text=Hola, me contacto por un producto'); 

        }else
        {
			$pdf->Image(ROOT_DIR_IMAGE.'face.png', 20, 250, 15, 15, 'PNG','www.facebook.com/Tohiadeco/');
			$pdf->SetXY(35,252);
			$pdf->Cell(0,10 ,utf8_decode("Tohiadeco"),'','','',false,'www.facebook.com/Tohiadeco/'); 
			

			$pdf->Image(ROOT_DIR_IMAGE.'insta.png', 75, 250, 15, 15, 'PNG','www.instagram.com/tohia_deco/');
			$pdf->SetXY(90,252);
			$pdf->Cell(0,10 ,utf8_decode("tohia_deco"),'','','',false,'www.instagram.com/tohia_deco/'); 


			$pdf->Image(ROOT_DIR_IMAGE.'whatsapp.png', 125, 250, 15, 15, 'PNG','https://api.whatsapp.com/send?phone=+5491167426705&text=Hola, me contacto por un producto');
        }

        $pdf->AddPage();
        $pdf->SetFont($fontTipe, 'I', 14);
        //VER DE SACAR LOS ESPACIOS DE ADELANTE DE LA RUTA.
        $count = count($files);
        $size = 96;
        $margin = 5;
		$margin_top = 22;
		$cont = 0;
		$title = '';
        $x_title = 0;
        $y_title = 0;

        for ($i=0; $i < $count; $i++)
        { 
   			$x = 7;
        	$y = 40;
        	$title = str_replace(strrchr($files[$i],'.'),'',$files[$i]);
   			$cont++;

			switch ($cont)
			{

				case 2:
			    	$x = $x+$margin+$size;
					break;
				case 3:
					$y = ($y+$margin_top+$size);
					break;
				case 4:
			    	$x =($x+$margin+$size);
					$y = ($y+$margin_top+$size);

				break;
			}
    		$x_title = (($x)+($size/2)-(strlen($title)));
    		$y_title = ($y+$size+2);

			$pdf->Image(ROOT_DIR_RES.$files[$i],$x,$y, $size, $size,substr(strrchr($files[$i],'.'),1));
			$pdf->SetXY($x_title,$y_title);
			$pdf->cell(10, 10, utf8_decode($title),'','','',false,'https://api.whatsapp.com/send?phone=+5491156138863&text=Hola, me gusto este articulo '.$title); 
			// $pdf->SetXY($x_title,$y_title+5);
			// $pdf->Cell(0,10 ,utf8_decode("consultar producto"),'','','',false,'https://api.whatsapp.com/send?phone=+5491156138863&text=Hola, me contacto por un producto'); 


			// $pdf->SetXY(($x+($size/2)-(strlen("Imán para celular"))),($y+$size+5));
			// $pdf->cell(10, 10, utf8_decode("$ 110"), 0, 1, '');
			
			if ($cont == 4) 
			{
				// $pdf->SetDrawColor(173, 173, 173);
    // 			$pdf->Line(10, 150, 200, 150);
				$pdf->AddPage();
				$cont = 0;
			}


        }
       // $pdf->Output('I','catalogo.pdf',true);

        return ($pdf->Output('F',ROOT_DIR.$name.'.pdf'));
    }


    public static function resize_image($file,$root,$w=null,$h=null) 
    {
		//BASADO EN JPEG, PARA USAR EN PNG, GIF ETC CAMBIAR EL NOMBRE DE LAS FUNCIONES
		    	
		//    //Imagen original
		$rtOriginal=$file;

		//Crear variable
		$original = imagecreatefromjpeg($rtOriginal);

		//Ancho y alto máximo
		$max_ancho = isset($w) ? $w : 800; 
		$max_alto = isset($h) ? $h : 600; 
		 
		//Medir la imagen
		list($ancho,$alto)=getimagesize($rtOriginal);

		//Ratio
		$x_ratio = $max_ancho / $ancho;
		$y_ratio = $max_alto / $alto;

		//Proporciones
		if(($ancho <= $max_ancho) && ($alto <= $max_alto) ){
		    $ancho_final = $ancho;
		    $alto_final = $alto;
		}
		else if(($x_ratio * $alto) < $max_alto){
		    $alto_final = ceil($x_ratio * $alto);
		    $ancho_final = $max_ancho;
		}
		else {
		    $ancho_final = ceil($y_ratio * $ancho);
		    $alto_final = $max_alto;
		}

		//Crear un lienzo
		$lienzo=imagecreatetruecolor($ancho_final,$alto_final); 

		//Copiar original en lienzo
		imagecopyresampled($lienzo,$original,0,0,0,0,$ancho_final, $alto_final,$ancho,$alto);
		 
		//Destruir la original
		imagedestroy($original);

		//Crear la imagen y guardar en directorio upload/
		imagejpeg($lienzo,$root);

		}

}
// $files  = scandir(ROOT_DIR_PRD);
// unset($files[0],$files[1]);
// sort($files);
// $count = count($files);

// for ($i=0; $i < $count; $i++)
// { 
// 	PDF::resize_image(ROOT_DIR_PRD.$files[$i],ROOT_DIR_RES.'/'.$files[$i],800, 600);
// }

PDF::createPDF();
      


 
//$img = PDF::resize_image(ROOT_DIR_PRD.'IMG_20170827_184500.jpg', 200, 200);
// $fp = fopen(ROOT_DIR_PRD.'IMG_20170827_184500-NEW.jpg', 'w+r');
// fwrite($fp, $img);
// //fwrite($fp, $img);
//fclose($fp);
//PDF::createPDF()
// ob_clean();
// header('Content-Disposition: attachment; filename=catalogo.pdf');
// header('Content-Type: application/pdf');
// echo PDF::createPDF();





?>
