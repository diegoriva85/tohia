-- MySQL dump 10.13  Distrib 5.6.19, for linux-glibc2.5 (x86_64)
--
-- Host: 10.79.0.73    Database: ddjj
-- ------------------------------------------------------
-- Server version	5.5.44-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `form_field`
--

DROP TABLE IF EXISTS `form_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_field` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `form_panel_id` tinyint(4) NOT NULL,
  `SADE_name` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text,
  `element_id` varchar(50) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `element_id_UNIQUE` (`element_id`),
  UNIQUE KEY `SADE_name_UNIQUE` (`SADE_name`),
  KEY `fk_form_field_form_panel1_idx` (`form_panel_id`),
  CONSTRAINT `fk_form_field_form_panel1` FOREIGN KEY (`form_panel_id`) REFERENCES `form_panel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_field`
--

LOCK TABLES `form_field` WRITE;
/*!40000 ALTER TABLE `form_field` DISABLE KEYS */;
INSERT INTO `form_field` VALUES (1,1,'Tipo de Documento','Tipo de Documento','Reemplazar por... descripciÃ³n del criterio','ddjj-campo1',0,'2016-05-16 19:44:01','2016-05-24 15:10:29'),(2,1,'CUIT','CUIT / CUIL','Reemplazar por... descripciÃ³n del criterio','ddjj-campo2',0,'2016-05-16 19:44:10','2016-05-24 15:10:29'),(3,1,'Apellido y Nombres','Nombre de Pila','Reemplazar por... descripciÃ³n del criterio','ddjj-campo3',0,'2016-05-16 19:44:18','2016-05-24 15:10:29'),(4,2,'Campo-A','Campo 1','','ddjj-campo4',0,'2016-05-16 19:44:42','2016-05-17 13:44:05'),(5,2,'Campo-B','Campo 2','sfrf','ddjj-campo5',0,'2016-05-16 19:44:51','2016-05-17 13:44:05'),(6,3,'Campo-X1','Campo 1','dfgdfgsdfsdfsdfsdf','ddjj-campo6',0,'2016-05-16 19:45:12','2016-05-17 11:46:23'),(7,3,'Campo-X2','Campo 2','dffdg','ddjj-campo7',0,'2016-05-16 19:45:20','2016-05-17 11:46:23');
/*!40000 ALTER TABLE `form_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_panel`
--

DROP TABLE IF EXISTS `form_panel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_panel` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text,
  `element_id` varchar(50) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `element_id_UNIQUE` (`element_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_panel`
--

LOCK TABLES `form_panel` WRITE;
/*!40000 ALTER TABLE `form_panel` DISABLE KEYS */;
INSERT INTO `form_panel` VALUES (1,'Datos Personales','descripcipón','ddjj-solapa1',0,'2016-05-12 19:30:47',NULL),(2,'Antecentes Laborales','descripcipón','ddjj-solapa2',0,'2016-05-13 19:12:43',NULL),(3,'Datos Familiares','Sin descripcipón','ddjj-solapa3',0,'2016-05-13 19:39:25',NULL);
/*!40000 ALTER TABLE `form_panel` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-26 15:08:09
